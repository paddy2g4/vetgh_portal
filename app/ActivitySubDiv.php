<?php

namespace App;
use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Database\Eloquent\Model;
#####use

class ActivitySubDiv extends Model
{
    use SearchableTrait;
    public $table = 'activity_sub_divs';

    public $fillable = [
        'activity_div_id',
        'activity_time',
        'classification',
        'user_id',
        'act_status',
        'del_status',
        'created_at',
        'updated_at',

    ];



    /**
     * Validation rules
     *
     * @var array
     */
    public   $rules = [
        // 'activity_div_id' => ['required','integer'],
        'classification'=>['required','min:1','max:200',"regex:/^[\p{L}\p{P}\p{Zs}\p{N}]+$/"],
        'start_date'=>'date_format:"m/d/Y g:i A"|required',
        'amount'=>['required','string']
    ];

    #####relation#####
}
