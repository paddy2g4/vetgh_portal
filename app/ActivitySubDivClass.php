<?php

namespace App;
use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Database\Eloquent\Model;
#####use

class ActivitySubDivClass extends Model
{
    use SearchableTrait;
    public $table = 'activity_sub_div_class';


//    public $searchable = [
//        'columns' => [
//            'awards.entity_id'=>100,
//            'awards.plan_name'=>101,
//            'awards.plan_desc'=>102,
//            'awards.start_date'=>103,
//            'awards.end_date'=>104,
//            'awards.logo'=>105,
//            'awards.user_id'=>106,
//            'awards.act_status'=>107,
//            'awards.del_status'=>108,
//            ########relation_id
//        ],
//        ########join
//    ];


    public $fillable = [
        'plan_id',
        'class_desc',
        'comment',
        'user_id',
        'act_status',
        'del_status',
        'created_at',
        'updated_at',

    ];



    /**
     * Validation rules
     *
     * @var array
     */
    public   $rules = [
        'entity_div_code' => ['required','min:1','max:50',"regex:/^[\p{L}\p{P}\p{Zs}\p{N}]+$/"],
        'class_desc'=>['required','min:1','max:200',"regex:/^[\p{L}\p{P}\p{Zs}\p{N}]+$/"],
        'comment'=>['max:200',"regex:/^[\p{L}\p{P}\p{Zs}\p{N}]+$/"]
    ];
    
    #####relation#####
}
