<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Slack extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'slack:testmsg';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Test Message to Slack Channel';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $message = "Hello, This is a test message from CastVOTEGH";
        \Slack::send($message);
    }
}
