<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EntityDivision extends Model
{
    // use SearchableTrait;
    public $table = 'entity_division'; 

    public $fillable = [
        'entity_id',
        'div_name',
        'div_alias',
        'div_initials',
        'activity_type_code',
        'service_label',
        'comment',
        'image_path',
        'image_data',
        'allow_qr',
        'sms_sender_id',
        'user_id',
        'show_client_result',
        'start_date',
        'end_date',
        'event_code',
        'active_status',
        'del_status'
    ];

 

    /**
     * Validation rules
     *
     * @var array
     */
    public   $rules = [
        'entity_id' => 'string',
        'div_name' => 'string|required',
        'activity_type_code' => 'string|required',
        'sms_sender_id' => 'string|required',
        'start_date' => 'date|required',
        'end_date' => 'date',
        'logo' => 'image',
        'user_id' => 'integer'
    ];
 #####relation#####
}
