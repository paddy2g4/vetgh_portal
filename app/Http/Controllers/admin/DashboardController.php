<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Mail;

class DashboardController extends Controller
{
    /**
     * @return mixed
     */
    public function index()
    {
        
        // $role = \DB::table('role_user')->select('role_id')->where('user_id',\Auth::user()->id)->first();
                
        // \Auth::user()->role_id = $role->role_id;
        if (\Session::has('role_id')) {
            logger(\Session::get('role_id'));
        }
//         dd(\Auth::user(), \Auth::user()->role_id, \Session::get('role_id'));
        // \Log::info("\Auth::user()->role_id = "+ \Auth::user()->role_id);

        $module_menus = app('App\Http\Controllers\admin\CrudBuilderController')->createMenumodule();
        return view('admin.dashboard')->with(['module_menus' => $module_menus]);
    }
}
