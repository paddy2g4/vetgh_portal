<?php

namespace App\Http\Controllers\admin;


// use entity_division\Model\entity_division;
// use EntityMaster\Model\EntityMaster;
use App\EntityDivision;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\admin\Module;
use App\admin\ModuleAlbum;
use App\Http\Controllers\Controller;
use Regulus\ActivityLog\Models\Activity;
use Validator;
use Illuminate\Http\Request;
use File;
use App\Http\Requests;
use Illuminate\Pagination\Paginator;
use Zizaco\Entrust\Traits\EntrustTrait;
use Nicolaslopezj\Searchable\SearchableTrait;
use JD\Cloudder\Facades\Cloudder;
use Response;
#####use
class EventSetupController extends Controller
{
        use SearchableTrait;
    //  use EntrustUserTrait ;
        protected $image_path = "uploads/award_logo";
	
    /**
     * @param Request $request
     * @return string
     * Export Excel & CSV & PDF
     */
    public function export_excel(Request $request){
        #### $request['export_type'] is export mode  "EXCEL or CSV"
        ### Check export PDF permission
        if($request['export_type']=='pdf'&& !Auth::user()->can('export_pdf') )
            return 'You not have this permission';
        ### Check export CSV permission
        if($request['export_type']=='csv'&& !Auth::user()->can('export_csv') )
            return 'You not have this permission';
        ### Check export EXCEL permission
        if($request['export_type']=='xls'&& !Auth::user()->can('export_xls') )
            return 'You not have this permission';
        if ($request['serach_txt']) {
            $Awards = entity_division::search($request['serach_txt'], null, false)->get();
        } else {  ###other
            $Awards = entity_division::all();
        }  if($request['export_type']=='pdf'){ //export PDF
            $html='<h1 style="text-align: center">YEP test pdf </h1>';
            $html .= '<style>
						table, th, td {text-align: center;}
						th, td {padding: 5px;}
						th {color: #43A047;border-color: black;background-color: #C5E1A5}
						</style>
						<table border="2" style="width:100%;">
						<tr>
							<th>Title</th>
							<th>Created date</th>
						</tr>';
            foreach ($Awards as $cat ){

                $html .="<tr>
							<td>$cat->title</td>
							<td>$cat->created_at</td>
						  </tr>";
            }
			$html .= '</table>';
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadHTML($html);
            return $pdf->download('entity_division.pdf');
        }else { // export excel & csv
            Excel::create('entity_division', function ($excel) use ($Awards) {
                $excel->sheet('Sheet 1', function ($sheet) use ($Awards) {
                    $sheet->fromArray($Awards);
                });
            })->download($request['export_type']);
        }
    }
	
 
    /**
     * @param Request $request
     * @return mixed
     * Search and paging
     */
    public function search(Request $request)
    {
        #####relation
        $currentPage = $request['page']; // You can set this to any page you want to paginate to
        Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });
        if ($request['paging'] > 0)
            $limit = $request['paging'];
        else
            $limit = 10;
        ### search
        if ($request['serach_txt']) {
            $Awards = entity_division::search($request['serach_txt'], null, false)->get();
            $page = $request->has('page') ? $request->page - 1 : 0;
            $total = $Awards->count();
            $Awards = $Awards->slice($page * $limit, $limit);
            $Awards = new \Illuminate\Pagination\LengthAwarePaginator($Awards, $total, $limit);
        } else {  ###other
        //    $Awards = entity_division::paginate($limit);

            $entity_masters = \DB::table('entity_master')->where('act_status',true)->where('del_status',false)->get();
            $Awards=\DB::table('entity_division')
                ->leftJoin('entity_master', 'entity_master.id', '=', 'entity_division.entity_id')
                ->select('entity_division.id','entity_division.plan_name','entity_division.plan_desc','entity_division.start_date', 'entity_division.end_date', 'entity_division.act_status', 'entity_division.logo', 'entity_master.id as entity_masters_id', 'entity_master.entity_name')
                ->where('entity_division.del_status',false)
                ->where('entity_master.active_status',true)
                ->where('entity_master.del_status',false)
                ->paginate($limit);

        }
        return view("AwardsView::Awardsajax")->with(['request' => $request, 'tab' =>1, 'entity_division' => $Awards,'entity_master' => $entity_masters]);
    }



    public function events_index(){
        $role_id = \Session::get('role_id');
        if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2 ) {
            $module_menus=app('App\Http\Controllers\admin\CrudBuilderController')->createMenumodule();

            $Entities=\DB::table('entity_master')
            ->select('*')
            ->where('active_status',true)
            ->where('del_status',false)
            ->orderby('created_at', 'desc')->get();
            
            // \Log::info($Entities);
            // \Session::forget('status');
            // \Session::forget('msg_status');
            return view('setups.events')->with(['module_menus'=>$module_menus, 'role_id' => $role_id, 'Entities' => $Entities]);
        }else{
            return redirect('404');
        }
    }



    public function events_getData(Request $request)
    {
        DB::enableQueryLog();
        
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length"); // Rows display per page

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');

        $columnIndex = $columnIndex_arr[0]['column']; // Column index
        $columnName = $columnName_arr[$columnIndex]['data']; // Column name
        $columnSortOrder = $order_arr[0]['dir']; // asc or desc
        $searchValue = $search_arr['value']; // Search value

        // Fetch records
        \Log::info('searching records: role_id: '.\Session::get('role_id').'. entity_id: '.\Auth::user()->entity_id);
  

        if (!empty($searchValue)){

            $records = DB::select( DB::raw("select * from event_view where (event_active_status = :event_active_status and event_del_status = :event_del_status) and (entity_div_code like :searchValue or entity_id like :searchValue or entity_name like :searchValue or div_name like :searchValue or div_alias like :searchValue or activity_type_code like :searchValue or service_label like :searchValue or start_date like :searchValue or end_date like :searchValue or show_client_result like :searchValue or div_initials like :searchValue or sms_sender_id like :searchValue or event_active_status like :searchValue or event_del_status like :searchValue or event_created_at like :searchValue) order by event_created_at desc limit :rowperpage offset :start"), array(
                'event_del_status' => false, 'searchValue' => '%'.$searchValue.'%', 'rowperpage' => $rowperpage, 'start' => $start
            ));

            $totalRecords = DB::select( DB::raw("select count(*) as allcount from event_view where (event_active_status = :event_active_status and event_del_status = :event_del_status) and (entity_div_code like :searchValue or entity_id like :searchValue or entity_name like :searchValue or div_name like :searchValue or div_alias like :searchValue or activity_type_code like :searchValue or service_label like :searchValue or start_date like :searchValue or end_date like :searchValue or show_client_result like :searchValue or div_initials like :searchValue or sms_sender_id like :searchValue or event_active_status like :searchValue or event_del_status like :searchValue or event_created_at like :searchValue)"), array(
                'event_del_status' => false, 'searchValue' => '%'.$searchValue.'%'
            ));

            $totalRecordswithFilter = DB::select( DB::raw("select count(*) as allcount from event_view where (event_active_status = :event_active_status and event_del_status = :event_del_status) and (entity_div_code like :searchValue or entity_id like :searchValue or entity_name like :searchValue or div_name like :searchValue or div_alias like :searchValue or activity_type_code like :searchValue or service_label like :searchValue or start_date like :searchValue or end_date like :searchValue or show_client_result like :searchValue or div_initials like :searchValue or sms_sender_id like :searchValue or event_active_status like :searchValue or event_del_status like :searchValue or event_created_at like :searchValue)"), array(
                'event_del_status' => false, 'searchValue' => '%'.$searchValue.'%'
            ));

            $totalRecords = $totalRecords[0]->allcount;
            $totalRecordswithFilter=$totalRecordswithFilter[0]->allcount;

        }
        else{

            $matchThese = ['event_del_status' => false, ];
            $records = \DB::table('event_view')
                ->where($matchThese)
                ->select('*')
                ->skip($start)
                ->take($rowperpage)
                ->orderby('event_created_at','desc');

            $totalRecords = \DB::table('event_view')
                ->where($matchThese)
                ->select('count(*) as allcount');

            $totalRecordswithFilter = \DB::table('event_view')->select('count(*) as allcount')
                ->where($matchThese);

            $records=$records->get();
            $totalRecords = $totalRecords->count();
            $totalRecordswithFilter=$totalRecordswithFilter->count();
        }


        $data_arr = array();
        $sno = $start+1;
        foreach($records as $key=>$record){
            $sn = $key+1;

            if($record->event_active_status == true && $record->event_del_status == false ){
                $status="Active";
            } else{
                $status="In Active";
            }


            switch ($record->activity_type_code) {
                case "VOT":
                    $activity_type = "Voting";
                    break;

                case "SHW":
                    $activity_type = "Show";
                    break;

                case "PAG":
                    $activity_type = "Pageant";
                    break;

                case "VOT-SHW":
                    $activity_type = "Voting & Show";
                    break;

                case "PAG-SHW":
                    $activity_type = "Pageant & Show";
                    break;
                
                default:
                    $activity_type = "Voting";
                    break;
            }

            if(date('Y-m-d H:i', strtotime($record->end_date) ) > date('Y-m-d H:i')){
                $date_status="ongoing";
            }else{
                $date_status="ended";
            }

            $data_arr[] = array(
                "sn" => $sn,
                "logo" => $record->image_path,
                "event_id" => $record->event_id,
                "entity_div_code" => $record->entity_div_code,
                "entity_name" => $record->entity_name,
                "div_name" => $record->div_name,
                "activity_type" => $activity_type,
                "start_date" => date('d-F-Y g:i A', strtotime($record->start_date) ),
                "end_date" => date('d-F-Y g:i A', strtotime($record->end_date) ),
                "date_status" => $date_status,
                "show_client_result" => $record->show_client_result,
                "status" => $status
            );
            
        }

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            "aaData" => $data_arr
        );

        // \Log::info(DB::getQueryLog());

        echo json_encode($response);
        exit;
    }


    public function getEventData(Request $request) {
        \Log::info($request);

        $Event=\DB::table('event_view')
            ->select('*')
            ->where('event_id',$request->event_id)
            ->get(1);

        \Log::info($Event);

        return Response::json(array('event_details'=>$Event));
    }


    public function edit_status(Request $request){

        $rules = array(
            'id'=>'required|Integer',
            "btnstatus" => 'required|alpha'
        );

        $Entities = \DB::table('entity_master')->where('active_status',true)->where('del_status',false)->get();
        $role_id = \Session::get('role_id');
        
        if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2 ) { // if(Auth::user()->can("add_EntityMaster")){  #####check permission
            if($rules){
                $validator = Validator::make($request->all(), $rules);
                if ($validator->fails()) {
                    $backAwards = Awards::paginate(10);
                    return view("AwardsView::Awardsajax")->withErrors($validator)->with(['tab' => 2,'Awards'=>$backAwards, 'editAwards' => $request->all()]);
                }
            }
            

            $this->user_id = \Auth::user()->id;

            if ($request->btnstatus == "disable"){
                $message = "Record Deactivated successfully";
                $action = false;
            }else{
                $message = "Record Activated successfully";
                $action = true;
            }

            $Awards = EntityDivision::find($request['id']);
            $Awards->active_status = $action;
            $Awards->user_id = $this->user_id;
            $Awards->save();


            Activity::log([
                'contentId'   => $Awards->id,
                'contentType' => 'Awards',
                'action'      => 'Update',
                'description' => 'Awards Status Changed',
                'details'     => 'Username: '.Auth::user()->name,
            ]);

            $Event=\DB::table('event_view')
            ->select('*')
            ->get();

            // return Response::json(array('event_details'=>$Event));
            return response()->json([
                'status' => $action,
                'message' => $message,
              ]);

        }else{

            $Event=\DB::table('event_view')
            ->select('*')
            ->get();

            return Response::json(array('event_details'=>$Event));
        }

    }



    public function set_event_date(Request $request){

        $rules = array(
            'id'=>'required|Integer',
            "btnstatus" => 'required|alpha'
        );

        $Entities = \DB::table('entity_master')->where('active_status',true)->where('del_status',false)->get();
        $role_id = \Session::get('role_id');
        
        if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2 ) { // if(Auth::user()->can("add_EntityMaster")){  #####check permission
            if($rules){
                $validator = Validator::make($request->all(), $rules);
                if ($validator->fails()) {
                    $backAwards = Awards::paginate(10);
                    return view("AwardsView::Awardsajax")->withErrors($validator)->with(['tab' => 2,'Awards'=>$backAwards, 'editAwards' => $request->all()]);
                }
            }
            

            $this->user_id = \Auth::user()->id;

            if ($request->btnstatus == "end"){
                $message = "Event has ended successfully";
                $action = true;
                $award_end_date = date("Y-m-d H:i");
            }else{
                $message = "Event has restarted";
                $action = true;
            }

            $Awards = EntityDivision::find($request['id']);
            $Awards->end_date = $award_end_date ? $award_end_date : null;
            $Awards->user_id = $this->user_id;
            $Awards->save();


            Activity::log([
                'contentId'   => $Awards->id,
                'contentType' => 'Awards',
                'action'      => 'Update',
                'description' => 'Awards Status Changed',
                'details'     => 'Username: '.Auth::user()->name,
            ]);

            $Event=\DB::table('event_view')->select('*')->get();

            return response()->json([
                'status' => $action,
                'message' => $message,
              ]);

        }else{

            $Event=\DB::table('event_view')
            ->select('*')
            ->get();

            return Response::json(array('event_details'=>$Event));
        }

    }



    public function set_result_status(Request $request){

        $rules = array(
            'id'=>'required|Integer',
            "btnstatus" => 'required|alpha'
        );

        $Entities = \DB::table('entity_master')->where('active_status',true)->where('del_status',false)->get();
        $role_id = \Session::get('role_id');
        
        if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2 ) { // if(Auth::user()->can("add_EntityMaster")){  #####check permission
            if($rules){
                $validator = Validator::make($request->all(), $rules);
                if ($validator->fails()) {
                    $backAwards = Awards::paginate(10);
                    return view("AwardsView::Awardsajax")->withErrors($validator)->with(['tab' => 2,'Awards'=>$backAwards, 'editAwards' => $request->all()]);
                }
            }
            

            $this->user_id = \Auth::user()->id;

            if ($request->btnstatus == "show"){
                $show_client_result = true;
                $message = "Results are showing for the said event.";
                $action = true;
            }else{
                $show_client_result = false;
                $message = "Results are hidden for the said event.";
                $action = true;
            }

            $Awards = EntityDivision::find($request['id']);
            $Awards->show_client_result = $show_client_result;
            $Awards->user_id = $this->user_id;
            $Awards->save();


            Activity::log([
                'contentId'   => $Awards->id,
                'contentType' => 'Awards',
                'action'      => 'Update',
                'description' => 'Awards Results status Changed',
                'details'     => 'Username: '.Auth::user()->name,
            ]);

            $Event=\DB::table('event_view')->select('*')->get();

            return response()->json([
                'status' => $action,
                'message' => $message,
              ]);

        }else{

            $Event=\DB::table('event_view')
            ->select('*')
            ->get();

            return Response::json(array('event_details'=>$Event));
        }

    }



    public function edit_event_enddate(Request $request){

        $rules = array(
            'id'=>'required|string',
            "end_event_end_date" => 'required|date'
        );

        $Entities = \DB::table('entity_master')->where('active_status',true)->where('del_status',false)->get();
        $role_id = \Session::get('role_id');
        $module_menus=app('App\Http\Controllers\admin\CrudBuilderController')->createMenumodule();
        
        if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2 ) { // if(Auth::user()->can("add_EntityMaster")){  #####check permission
            if($rules){
                $validator = Validator::make($request->all(), $rules);
                if ($validator->fails()) {
                    $backAwards = Awards::paginate(10);
                    return view("AwardsView::Awardsajax")->withErrors($validator)->with(['tab' => 2,'Awards'=>$backAwards, 'editAwards' => $request->all()]);
                }
            }
            

            $this->user_id = \Auth::user()->id;

            $message = "Event end date has been edited successfully";
            $action = true;

            $Awards = EntityDivision::find($request['id']);
            $Awards->end_date = date("Y-m-d H:i", strtotime($request->end_event_end_date));
            $Awards->user_id = $this->user_id;
            $Awards->save();


            Activity::log([
                'contentId'   => $Awards->id,
                'contentType' => 'Awards',
                'action'      => 'Update',
                'description' => 'Awards EndDate Changed',
                'details'     => 'Username: '.Auth::user()->name,
            ]);

            return view('setups.events')->with(['module_menus'=>$module_menus, 'flag'=>4, 'role_id' => $role_id, 'Entities' => $Entities]);

        }else{

            $Event=\DB::table('event_view')
            ->select('*')
            ->get();

            return Response::json(array('event_details'=>$Event));
        }

    }

    /**
     * Generate initials from a name
     *
     * @param string $name
     * @return string
     */
    public function generate(string $name)
    {
        $words = explode(' ', $name);
        if (count($words) >= 2) {
            return strtoupper(substr($words[0], 0, 1) . substr(end($words), 0, 1));
        }
        return $this->makeInitialsFromSingleWord($name);
    }

    /**
     * Make initials from a word with no spaces
     *
     * @param string $name
     * @return string
     */
    protected function makeInitialsFromSingleWord(string $name)
    {
        preg_match_all('#([A-Z]+)#', $name, $capitals);
        if (count($capitals[1]) >= 2) {
            return substr(implode('', $capitals[1]), 0, 2);
        }
        return strtoupper(substr($name, 0, 2));
    }

    public function generate_rand_string($strength = 16) {
        $input = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $input_length = strlen($input);
        $random_string = '';
        for($i = 0; $i < $strength; $i++) {
            $random_character = $input[mt_rand(0, $input_length - 1)];
            $random_string .= $random_character;
        }

        return $random_string;
    }


    public function generate_rand_number($strength = 16) {
        $input = '0123456789';
        $input_length = strlen($input);
        $random_string = '';
        for($i = 0; $i < $strength; $i++) {
            $random_character = $input[mt_rand(0, $input_length - 1)];
            $random_string .= $random_character;
        }

        return $random_string;
    }


    public function generateEventInitials(string $name){
        $div_initials="";
        $event_code="";

        do {
            $div_initials=$this->generate_rand_string(2);
            $event_code=$div_initials.$this->generate_rand_number(1);
        } while (DB::table('entity_division')->where('div_initials', $div_initials)->where('active_status',true)->count() > 0);

        \Log::info("LINE ".__LINE__.". In EventSetupController. div_initials = $div_initials, event_code = $event_code");

        return [$div_initials, $event_code];
    }


    public function create_event(Request $request){
        
        $rules = array(
            'entity_id' => 'required|string',
            'add_event_name' => 'required|string|min:1|max:200',
            'add_event_alias' => 'required|string|min:1|max:200',
            'new_event_type' => 'required|string|min:1|max:10',
            'add_event_initials' => 'required|string|min:1|max:10',
            'add_event_price' => 'required|numeric|between:0,1000.99',
            'add_event_service_code' => 'required|integer',
            'add_event_results' => 'required|string|min:1|max:5',
            'add_sms_sender_id' => 'required|string|min:1|max:11',
            'add_event_start_date' => 'date|required',
            'add_event_end_date' => 'date',
            'add_shw_start_date' => 'date',
            'add_shw_end_date' => 'date',
        //    'logo' => ['max:255',"regex:/^[\p{L}\p{P}\p{Zs}\p{N}]+$/"],
            // 'image_name'=>'mimes:jpeg,bmp,jpg,png|between:1, 6000',
        );

        
        $role_id = \Session::get('role_id');
        $Entities=\DB::table('entity_master') ->select('*')->where('active_status',true)->where('del_status',false)->orderby('created_at', 'desc')->get();
        $module_menus=app('App\Http\Controllers\admin\CrudBuilderController')->createMenumodule();
        
        if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2 ) {
            if($rules){
                $validator = Validator::make($request->all(), $rules);
                if ($validator->fails()) {
                    return view('setups.events')->withErrors($validator)->with(['module_menus'=>$module_menus, 'role_id' => $role_id, 'Entities' => $Entities, 'editAwards' => $request->all()]);
                }
            }

            DB::beginTransaction();

            try {
                \Log::info($request->all());

                $Awards=new EntityDivision;
                
                $show_client_result = ($request->add_event_results == "true") ? true : false ;
        
                $this->user_id = \Auth::user()->id;

                $div_initials="";
                $event_code="";

                if (\DB::table('entity_division')->where('div_initials', $request->add_event_initials)->count() > 0) {
                    $results=$this->generateEventInitials($request->add_event_name);
                    \Log::info("LINE ".__LINE__.". In EventSetupController.: store. results = $results[0], $results[1]");
                    $div_initials=$results[0];
                    $event_code=$results[1];
                }else{
                    $div_initials=$request->add_event_initials;
                    $event_code=$request->add_event_initials.$this->generate_rand_number(1);
                }

                switch ($request->new_event_type) {
                    case "VOT":
                        $award_start_date = date("Y-m-d H:i", strtotime($request->add_event_start_date));
                        $award_end_date = date("Y-m-d H:i", strtotime($request->add_event_end_date));
                        $Awards->start_date = $award_start_date;
                        $Awards->end_date = $award_end_date ? $award_end_date : null;
                        $Awards->activity_type_code = $request->new_event_type;
                        break;
                    // case "SHW":

                    //     $shw_start_date = date("Y-m-d H:i", strtotime($request->add_shw_start_date));
                    //     $shw_end_date = date("Y-m-d H:i", strtotime($request->add_shw_end_date));

                    //     $Awards->shw_start_date = $shw_start_date;
                    //     $Awards->shw_end_date = $shw_end_date ? $shw_end_date : null;
                    //     $Awards->activity_type_code = $request->new_event_type;
                    //     break;

                
                    default:
                        $award_start_date = date("Y-m-d H:i", strtotime($request->add_event_start_date));
                        $award_end_date = date("Y-m-d H:i", strtotime($request->add_event_end_date));
                        $Awards->start_date = $award_start_date;
                        $Awards->end_date = $award_end_date ? $award_end_date : null;

                        // $shw_start_date = date("Y-m-d H:i", strtotime($request->shw_start_date));
                        // $shw_end_date = date("Y-m-d H:i", strtotime($request->shw_end_date));

                        // $Awards->shw_start_date = $shw_start_date;
                        // $Awards->shw_end_date = $shw_end_date ? $shw_end_date : null;

                        if($request->new_event_type == "PAG"){
                            $Awards->activity_type_code = "VOT";
                            // $Awards->event_type = "P";
                        }elseif ($request->new_event_type == "PAG-SHW"){
                            $Awards->activity_type_code = "VOT-SHW";
                            // $Awards->event_type = "P";
                        }else{
                            $Awards->activity_type_code = $request->new_event_type;
                            // $Awards->event_type = "A";
                        }
                }

                
                $Awards->entity_id = $request->entity_id;
                $Awards->div_name = $request->add_event_name;
                $Awards->div_alias = $request->add_event_alias;
                $Awards->service_label = $request->add_event_alias;
                $Awards->sms_sender_id = $request->add_sms_sender_id;
                $Awards->show_client_result = $show_client_result;
                $Awards->div_initials = $div_initials;
                $Awards->event_code = $event_code;
                $Awards->user_id = $this->user_id;
                $Awards->save();

                if ($Awards->id) {

                    $event = EntityDivision::find($Awards->id);

                    DB::table('assigned_service_code')->insert(
                        ['entity_div_code' => $event->assigned_code, 'service_code' => $request->add_event_service_code, 'user_id'=>$this->user_id]
                    );
                    DB::table('assigned_fees')->insert(
                        ['entity_div_code' => $event->assigned_code, 'trans_type' => 'CTM', 'nw' =>'MOM', 'fee'=>2.000, 'orchard_fee'=>2.000, 'app_fee'=>0.000, 'flat_percent'=>'P', 'charged_to'=>'M', 'approved' =>true, 'user_id'=>$this->user_id]
                    );
                    DB::table('assigned_fees')->insert(
                        ['entity_div_code' => $event->assigned_code, 'trans_type' => 'CTM', 'nw' =>'CRD', 'fee'=>2.500, 'orchard_fee'=>2.500, 'app_fee'=>0.000, 'flat_percent'=>'P', 'charged_to'=>'M', 'approved' =>true, 'user_id'=>$this->user_id]
                    );

                    DB::table('voting_pricing')->insert(
                        ['entity_div_code' => $event->assigned_code, 'points' => 1, 'unit_cost' =>$request->add_event_price, 'user_id'=>$this->user_id]
                    );
        
                    DB::table('entity_service_account')->insert(
                        ['entity_div_code' => $event->assigned_code, 'gross_bal' => 0.000, 'net_bal' =>0.000]
                    );
                }


                Activity::log([
                    'contentId'   => $Awards->id,
                    'contentType' => 'Awards',
                    'action'      => 'Update',
                    'description' => 'Awards Status Changed',
                    'details'     => 'Username: '.Auth::user()->name,
                ]);

                DB::commit(); // all good
                // \Session::forget('status');
                // \Session::forget('msg_status');
                return view('setups.events')->with(['module_menus'=>$module_menus, 'flag'=>3, 'role_id' => $role_id, 'Entities' => $Entities]);
                // return redirect()->back()->with(['status'=>'Record added successful', 'module_menus'=>$module_menus, 'flag'=>3, 'role_id' => $role_id, 'Entities' => $Entities]);
                
            } catch (\Exception $e) {
                DB::rollback(); // something went wrong
            //     \Session::put('status', 'Sorry, an error occured. Please try again');
            //     \Session::put('msg_status', 'error');
                return view('setups.events')->with(['module_menus'=>$module_menus, 'role_id' => $role_id, 'Entities' => $Entities]);
                // return redirect()->back()->with(['status'=>'Record added successful', 'module_menus'=>$module_menus, 'role_id' => $role_id, 'Entities' => $Entities]);
                
            }

        }else{

            $Event=\DB::table('event_view')
            ->select('*')
            ->get();

            return Response::json(array('event_details'=>$Event));
        }
    }
		
}
