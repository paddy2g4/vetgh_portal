<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use JD\Cloudder\Facades\Cloudder;

class ImageUploadController extends Controller
{
    public function home()
    {
        return view('admin.cloudinary_homepage');
    }

    public function uploadImages(Request $request)
    {
        \Log::info("In ImageUploadController: uploadImages");
        \Log::info($request);

        $this->validate($request,[
            'image_name'=>'required|mimes:jpeg,bmp,jpg,png|between:1, 6000',
        ]);

//        $image = $request->file('image_name');
//        $name = $request->file('image_name')->getClientOriginalName();
//        $image_name = $request->file('image_name')->getRealPath();;
//
//        Cloudder::upload($image_name, null);
//        $tag ="CASTVOTEGH";
//        Cloudder::addTag($tag, Cloudder::getPublicId(), null);
//
//        list($width, $height) = getimagesize($image_name);
//        $image_url= Cloudder::show(Cloudder::getPublicId(), ["width" => $width, "height"=>$height]);
//
//
//        \Log::info("image_url = ".$image_url);

        return redirect()->back()->with('status', 'Image Uploaded Successfully');

    }
}
