<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Validator;
use Illuminate\Http\Request;
use File;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Mail;
use App\Charts\StatisticsChart;

class ReportsController extends Controller
{
    /**
     * @return mixed
     */

    public function SmsBalance() {
        $api_url = "http://64.227.58.231:5515/req_general_sms_balance";

        $data = array("service_id" => "","trans_type"=> "BLC");
        $params=json_encode($data);

        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$api_url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch,CURLOPT_POST,1);
        curl_setopt($ch,CURLOPT_POSTFIELDS,$params);
        $return=curl_exec($ch);
        curl_close($ch);
        $character = json_decode($return);
//        $result = explode(" :: ",$return);
//        $blast_result = $result[0];

        return $character;

    }

    public function nominee_code_reports()
    {

        $awards = "";
        if (\Session::get('role_id') == 3){

           $nominee_code_list = \DB::table('nominee_code_list_view')->where('assigned_code',\Auth::user()->entity_id)->get();

//            $awards = \DB::table('entity_division')
//                ->leftJoin('entity_master', 'entity_master.entity_id', '=', 'entity_division.entity_id')
//                ->select('entity_master.entity_name','entity_division.id','entity_division.div_name as plan_name','entity_division.assigned_code as plan_id')
//                ->where('entity_division.active_status',true)->where('entity_division.del_status',false)
//                ->where('entity_division.entity_id',\Auth::user()->entity_id)
//                ->orderby('entity_division.id', 'entity_division.asc')->get();

        }else{

            $nominee_code_list = \DB::table('nominee_code_list_view')->get();

//            $awards = \DB::table('entity_division')
//                ->leftJoin('entity_master', 'entity_master.entity_id', '=', 'entity_division.entity_id')
//                ->select('entity_master.entity_name','entity_division.id','entity_division.div_name as plan_name','entity_division.assigned_code as plan_id')
//                ->where('entity_division.active_status',true)->where('entity_division.del_status',false)
//                ->orderby('entity_division.id', 'entity_division.asc')->get();
        }


        $module_menus = app('App\Http\Controllers\admin\CrudBuilderController')->createMenumodule();

        return view('reports.nominee_reports')->with(['module_menus' => $module_menus, 'nominee_code_list' => $nominee_code_list]);
    }



    public function filter_events_statistics(Request $request){

        $rules = array(
            'plan_id' => ['required',"regex:/^[\p{L}\p{P}\p{Zs}\p{N}]+$/"],
        );

        if($rules){
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->with(['tab'=>2,'editPlanID'=>$request->all()]);
            }
        }

        $plan_id = $request->plan_id;

        $total_transactions = \DB::table('transaction_statistics_mat_view')->where('plan_id',$plan_id)->whereNotNull('processed')->count();
        $total_passed_transactions = \DB::table('transaction_statistics_mat_view')->where('plan_id',$plan_id)->where('processed', true)->count();
        $total_pending_transactions = \DB::table('transaction_statistics_mat_view')->where('plan_id',$plan_id)->whereNull('processed')->count();
        $total_failed_transactions = \DB::table('transaction_statistics_mat_view')->where('plan_id',$plan_id)->where('processed', false)->count();
        $total_sum_amount = \DB::table('transaction_statistics_mat_view')->where('plan_id',$plan_id)->where('processed', true)->sum('amount');
        $passed_transactions_today = \DB::table('transaction_statistics_mat_view')->where('plan_id',$plan_id)->where('processed', true)->whereDate('created_at', '=' ,date('Y-m-d'))->count(); //\DB::select('select count(trans_requests_id) as passed_transactions_today from transaction_statistics_mat_view where status = true and to_char(created_at, \'YYYY-MM-DD\') = CURRENT_DATE::text;');
        $pending_transactions_today = \DB::table('transaction_statistics_mat_view')->where('plan_id',$plan_id)->whereNull('processed')->whereDate('created_at', '=' ,date('Y-m-d'))->count(); //\DB::select('select count(trans_requests_id) as pending_transactions_today from transaction_statistics_mat_view where status is null and to_char(created_at, \'YYYY-MM-DD\') = CURRENT_DATE::text;');
        $failed_transactions_today = \DB::table('transaction_statistics_mat_view')->where('plan_id',$plan_id)->where('processed', false)->whereDate('created_at', '=' ,date('Y-m-d'))->count();//\DB::select('select count(trans_requests_id) as failed_transactions_today from transaction_statistics_mat_view where status = false and to_char(created_at, \'YYYY-MM-DD\') = CURRENT_DATE::text;');
        $transactions_today = \DB::table('transaction_statistics_mat_view')->where('plan_id',$plan_id)->whereDate('created_at', '=' ,date('Y-m-d'))->whereNotNull('processed')->count();
        $sum_amount_today = \DB::table('transaction_statistics_mat_view')->where('plan_id',$plan_id)->where('processed', true)->whereDate('created_at', '=' ,date('Y-m-d'))->sum('amount');
        $web_trans =  \DB::table('transaction_statistics_mat_view')->where('src', 'WEB')->where('plan_id',$plan_id)->count();
        $ussd_trans = \DB::table('transaction_statistics_mat_view')->where('src', 'USSD')->where('plan_id',$plan_id)->count();


//        \Log::info("plan_id = $plan_id ");
//        \Log::info("total_transactions = $total_transactions ");

        $balances = \DB::table('entity_service_account')->select('gross_bal','net_bal')->where('entity_div_code',$plan_id)->where('active_status',true)->where('del_status',false)->first();
//        dd($balances->gross_bal,$balances->net_bal);


        if (\Session::get('role_id') == 3){

            $awards = \DB::table('entity_division')
                ->leftJoin('entity_master', 'entity_master.entity_id', '=', 'entity_division.entity_id')
                ->select('entity_master.entity_name','entity_division.id','entity_division.div_name as plan_name','entity_division.assigned_code as plan_id')
                ->where('entity_division.active_status',true)->where('entity_division.del_status',false)
                ->where('entity_division.entity_id',\Auth::user()->entity_id)
                ->orderby('entity_division.id', 'entity_division.asc')->get();

        }else{

            $awards = \DB::table('entity_division')
                ->leftJoin('entity_master', 'entity_master.entity_id', '=', 'entity_division.entity_id')
                ->select('entity_master.entity_name','entity_division.id','entity_division.div_name as plan_name','entity_division.assigned_code as plan_id')
                ->where('entity_division.active_status',true)->where('entity_division.del_status',false)
                ->orderby('entity_division.id', 'entity_division.asc')->get();
        }


        $module_menus = app('App\Http\Controllers\admin\CrudBuilderController')->createMenumodule();

        $statisticsChart = new StatisticsChart();
//        $statisticsChart->labels(['Jan', 'Feb', 'Mar']);
//        $statisticsChart->dataset('Users by trimester', 'line', [10, 25, 13]);

        $borderColors = [
            "rgba(255, 99, 132, 1.0)",
            "rgba(22,160,133, 1.0)",
            "rgba(255, 205, 86, 1.0)",
            "rgba(51,105,232, 1.0)",
            "rgba(244,67,54, 1.0)",
            "rgba(34,198,246, 1.0)",
            "rgba(153, 102, 255, 1.0)",
            "rgba(255, 159, 64, 1.0)",
            "rgba(233,30,99, 1.0)",
            "rgba(205,220,57, 1.0)"
        ];
        $fillColors = [
            "rgba(255, 99, 132, 0.2)",
            "rgba(22,160,133, 0.2)",
            "rgba(255, 205, 86, 0.2)",
            "rgba(51,105,232, 0.2)",
            "rgba(244,67,54, 0.2)",
            "rgba(34,198,246, 0.2)",
            "rgba(153, 102, 255, 0.2)",
            "rgba(255, 159, 64, 0.2)",
            "rgba(233,30,99, 0.2)",
            "rgba(205,220,57, 0.2)"

        ];

        $statisticsChart->minimalist(true);
        $statisticsChart->labels(['WEB', 'USSD']);
        $statisticsChart->dataset('Transactions by source', 'doughnut', [$web_trans, $ussd_trans])
            ->color($borderColors)
            ->backgroundcolor($fillColors);

        $sms_balance = "";
        $balance_result = $this->SmsBalance(); //app('App\Http\Controllers\external\OrchardController')->checkSmsBalance();

        if ($balance_result->resp_code == "000"){
            $sms_balance = $balance_result->sms_bal;
        }

//        dd($total_transactions,$web_trans, $ussd_trans);
//        dd($statisticsChart);
        return view('reports.statistics')->with(['module_menus' => $module_menus, 'total_transactions' => $total_transactions, 'total_passed_transactions'=> $total_passed_transactions, 'total_pending_transactions'=>$total_pending_transactions, 'total_failed_transactions'=>$total_failed_transactions, 'passed_transactions_today'=>$passed_transactions_today, 'pending_transactions_today'=>$pending_transactions_today, 'failed_transactions_today'=>$failed_transactions_today, 'transactions_today'=>$transactions_today, 'total_sum_amount'=>$total_sum_amount, 'sum_amount_today' => $sum_amount_today,
            'statisticsChart' => $statisticsChart, 'awards'=>$awards,'event_gross_bal'=>$balances->gross_bal,'event_net_bal'=>$balances->net_bal, 'sms_balance'=>$sms_balance]);
    }
}
