<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Mail;
use DB;
use Excel;
use Regulus\ActivityLog\Models\Activity;
use Validator;
use File;
use Illuminate\Pagination\Paginator;
use Zizaco\Entrust\Traits\EntrustTrait;
use Nicolaslopezj\Searchable\SearchableTrait;

class SMSController extends Controller
{

    protected $username;
    protected $password;
    protected $from ;
    protected $churchid;
    protected $adminrole = "administrator";
    protected $institution_id;
//    protected $sms_url = "http://isms.wigalsolutions.com/ismsweb/sendmsg/?"; //"https://api.amazingsystems.org/send_sms";
    protected $sms_balance_url = "http://64.227.58.231:5501/check_wallet_balance";//"https://isms.wigalsolutions.com/ismsweb/Query/?";//"https://api.amazingsystems.org/check_SMS_balance";
    protected $sms_url = "http://64.227.58.231:5501/sendSms";//"https://isms.wigalsolutions.com/ismsweb/Query/?";//"https://api.amazingsystems.org/check_SMS_balance";
    protected $service_id;


    public function __construct()
    {
//        $sms_accounts = DB::table('sms_accounts')->select('username','password','sender_id','service_id')->where([['institution_id',$this->institution_id], ['act_status',1]])->first();
//        // dd($sms_accounts);
//        if (!empty($sms_accounts)){
//            $this->username = $sms_accounts->username;
//            $this->password = $sms_accounts->password;
//            $this->from = $sms_accounts->sender_id;
//            $this->service_id = $sms_accounts->service_id;
//        }else{
//            $this->username = "paddy2g4";
//            $this->password = "paddy0541840988";
//            $this->from = "EPSU MADINA";
//        }

        $this->username = "pceawards";
        $this->password = "pceawards";
        $this->from = "CASTVOTE";


    }

    /**
     * @return mixed
     */

    public function sms_blast_index()
    {

        if (\Session::has('role_id')) {
            logger(\Session::get('role_id'));
        }

        $module_menus = app('App\Http\Controllers\admin\CrudBuilderController')->createMenumodule();

        $message_logs = \DB::table('bulk_sms')->where('status',true)->orderby('id','desc')->get();

        $sms_balance = $this->checkSMSBalance();

        return view('admin.smsblast')->with(['module_menus' => $module_menus, 'message_logs'=>$message_logs, 'sms_balance'=>$sms_balance]);
    }


//    public function sms_blast(Request $request){
//        dd($request);
//    }


    public function generateRandomString($length = 8) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        $timezone  = 0;
        $time_g = gmdate("His", time() + 3600*($timezone+date("I")));

        return $randomString.$time_g;
    }


    public function sms_blast(Request $request)
    {

        $rules = array(
            'select_file'  => 'required',#'mimes:xls,xlsx',
            'msg_body' => ['required',"string"],
        );
//        dd($request);

        if($rules){
            $validator = Validator::make($request->all(), $rules);
//            dd($validator,$validator->fails());
            if ($validator->fails()) {
                $module_menus = app('App\Http\Controllers\admin\CrudBuilderController')->createMenumodule();
                return redirect()->back()->withErrors($validator)->with(['tab'=>2,'editSMS'=>$request->all()]);
            }
        }

        $path = $request->file('select_file')->getRealPath();

        $data = Excel::load($path)->get();

        $contacts = "";
        $sms_count = intval($data->count());

        $count = 0;

        $sender_id = "VETGH";
        $message=$request->msg_body;

        if($data->count() > 0)
        {
            foreach($data->toArray() as $key => $value)
            {
                $count ++;
//                dd($value["mobile_number"]);
                if (empty($contacts)){
                    $contacts = $contacts. $this->formatPhoneNumber(strval($value['customer_number']));
                }else{
                    $contacts = $contacts.",".$this->formatPhoneNumber(strval($value['customer_number']));
                }

                $this->wigalSMSBlast($contacts,$message);
                $ch = curl_init($this->sms_url);//Initiate cURL.
                $sms_unique = $this->generateRandomString(10);

                $contact_number = $this->formatPhoneNumber(strval($value['customer_number']));

                //The JSON data.
                $jsonData = array(
                    'sender_id' => $sender_id, 'msg_body' => $message,'recipient_number' => $contact_number, 'service_id'=> 4, 'trans_type' => 'SMS', 'unique_id' => $sms_unique, 'msg_type' => 'T'
                );

                $jsonDataEncoded = json_encode($jsonData); //Encode the array into JSON.
                curl_setopt($ch, CURLOPT_POST, 1); //Tell cURL that we want to send a POST request.
                curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);  //Attach our encoded JSON string to the POST fields.
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); //Set the content type to application/json
                //Execute the request
                $result = curl_exec($ch);

//                $db_insert_values[] = array(
//                    'sender_id' => $sender_id, 'message' => $message, 'phone_number' => $this->formatPhoneNumber(strval($value['customer_number'])), 'status' => true
//                );

                $timezone  = 0; //(GMT -5:00) EST (U.S. & Canada)
                $now = $ts = gmdate("Y-m-d H:i:s", time() + 3600*($timezone+date("I")));

                \DB::table('message_logs')->insert(
                    ['sender_id' => $sender_id, 'message' => $message, 'phone_number' => $this->formatPhoneNumber(strval($value['customer_number'])), 'status' => true, 'created_at'=>$now]
                );

            }

            if(!empty($contacts))
            {


                \Log::info('Bulk SMS: '.$sms_count. ' SMS sent.') ;
            }
        }

        if ($sms_count > 0){
            $message_logs = \DB::table('message_logs')->where('status',true)->orderby('id','desc')->get();
            return redirect()->back()->with('success', $sms_count. ' SMS sent. successfully.')->with('$message_logs',$message_logs);
        }else{
            $message_logs = \DB::table('message_logs')->where('status',true)->orderby('id','desc')->get();
            return redirect()->back()->with('danger', 'SMS Sending failed')->with('$message_logs',$message_logs);
        }

    }




    public function sms_blast_original(Request $request)
    {

        $rules = array(
            'select_file'  => 'required',#'mimes:xls,xlsx',
            'msg_body' => ['required',"string"],
        );
//        dd($request);

        if($rules){
            $validator = Validator::make($request->all(), $rules);
//            dd($validator,$validator->fails());
            if ($validator->fails()) {
                $module_menus = app('App\Http\Controllers\admin\CrudBuilderController')->createMenumodule();
                return redirect()->back()->withErrors($validator)->with(['tab'=>2,'editSMS'=>$request->all()]);
            }
        }

        $path = $request->file('select_file')->getRealPath();

        $data = Excel::load($path)->get();

        $contacts = "";
        $sms_count = intval($data->count());

        $count = 0;

        $sender_id = "VETGH";
        $message=$request->msg_body;

        if($data->count() > 0)
        {
            foreach($data->toArray() as $key => $value)
            {
                $count ++;
//                dd($value["mobile_number"]);
                if (empty($contacts)){
                    $contacts = $contacts. $this->formatPhoneNumber(strval($value['customer_number']));
                }else{
                    $contacts = $contacts.",".$this->formatPhoneNumber(strval($value['customer_number']));
                }

//                $db_insert_values[] = array(
//                    'sender_id' => $sender_id, 'message' => $message, 'phone_number' => $this->formatPhoneNumber(strval($value['customer_number'])), 'status' => true
//                );

                $timezone  = 0; //(GMT -5:00) EST (U.S. & Canada)
                $now = $ts = gmdate("Y-m-d H:i:s", time() + 3600*($timezone+date("I")));

                \DB::table('message_logs')->insert(
                    ['sender_id' => $sender_id, 'message' => $message, 'phone_number' => $this->formatPhoneNumber(strval($value['customer_number'])), 'status' => true, 'created_at'=>$now]
                );

            }

            if(!empty($contacts))
            {

                $this->wigalSMSBlast($contacts,$message);
                $ch = curl_init($this->sms_url);//Initiate cURL.
                $sms_unique = $this->generateRandomString(10);

                //The JSON data.
                $jsonData = array(
                    'sender_id' => $sender_id, 'msg_body' => $message,'recipient_number' => $contacts, 'service_id'=> 4, 'trans_type' => 'SMS', 'unique_id' => $sms_unique, 'msg_type' => 'T'
                );

                $jsonDataEncoded = json_encode($jsonData); //Encode the array into JSON.
                curl_setopt($ch, CURLOPT_POST, 1); //Tell cURL that we want to send a POST request.
                curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);  //Attach our encoded JSON string to the POST fields.
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); //Set the content type to application/json

                //Execute the request
                $result = curl_exec($ch);
                \Log::info('Bulk SMS: '.$sms_count. ' SMS sent.') ;
            }
        }

        if ($sms_count > 0){
            $message_logs = \DB::table('message_logs')->where('status',true)->orderby('id','desc')->get();
            return redirect()->back()->with('success', $sms_count. ' SMS sent. successfully.')->with('$message_logs',$message_logs);
        }else{
            $message_logs = \DB::table('message_logs')->where('status',true)->orderby('id','desc')->get();
            return redirect()->back()->with('danger', 'SMS Sending failed')->with('$message_logs',$message_logs);
        }

    }

    /**
     * @return string
     */
    public function wigalSMSBlast($phonenumbers,$message)
    {

        $params = "username=".$this->username."&password=".$this->password."&from=".$this->from."&to=".$phonenumbers."&message=".$message;

        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$this->sms_url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch,CURLOPT_POST,1);
        curl_setopt($ch,CURLOPT_POSTFIELDS,$params);
        $return=curl_exec($ch);
        curl_close($ch);
        $result = explode(" :: ",$return);
        $blast_result = $result[0];

        return $blast_result;
    }


    public function checkSMSBalance() {

//        $smsurl="https://api.amazingsystems.org/check_SMS_balance";  //Amazing SMS API

        //Initiate cURL.
        $ch = curl_init($this->sms_balance_url);

        //The JSON data.
        $jsonData = array(
            'service_id' => 4
        );

        $jsonDataEncoded = json_encode($jsonData);  //Encode the array into JSON.
        curl_setopt($ch, CURLOPT_POST, 1);  //Tell cURL that we want to send a POST request.
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded); //Attach our encoded JSON string to the POST fields.
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));  //Set the content type to application/json
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);

        $return = curl_exec($ch);

        $sms_balance_result = json_decode($return,true);
//        \Log::info('SMS BAlance result : '.$sms_balance_result) ;
        $sms_balance = $sms_balance_result["sms_bal"];
//        dd($sms_balance);
        \Session::put('checksmsbalance', $sms_balance);
        return $sms_balance;
//
//
//

        /*
        $params = "username=".$this->username."&password=".$this->password."&query=balance";
        $ch = curl_init($this->sms_balance_url);   //Initiate cURL.

        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$this->sms_balance_url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch,CURLOPT_POST,1);
        curl_setopt($ch,CURLOPT_POSTFIELDS,$params);
        $return=curl_exec($ch);
        curl_close($ch);
        $result = explode(" :: ",$return);
        $sms_balance = $result[0];

//        dd($result);
        \Session::put('checksmsbalance', $sms_balance);
        return $sms_balance;
        */

    }

    public function formatPhoneNumber($customer_number){
        $new_customer_number = "";
        if (strlen($customer_number) == 9){
            $customer_number = strtolower($customer_number);
            $new_customer_number =  "233".substr($customer_number,0, 9);
        }else{
            if (substr($customer_number,0,1) == "0"){
                $customer_number = strtolower($customer_number);
                $new_customer_number =  "233".substr($customer_number,1, 9);
            }elseif (substr($customer_number,0,3) != "233") {
                $customer_number = strtolower($customer_number);
                $new_customer_number =  "233".substr($customer_number,1, 9);
            }else {

                $new_customer_number = $customer_number;
            }
        }

        return $new_customer_number;
    }


}
