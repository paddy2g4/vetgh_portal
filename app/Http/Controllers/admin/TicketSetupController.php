<?php


namespace App\Http\Controllers\admin;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\candidatesss;
use Regulus\ActivityLog\Models\Activity;
use App\ActivitySubDivClass;
use App\ActivitySubDiv;
use Validator;
use Response;


class TicketSetupController extends Controller
{
    public function ticket_types_index(){
        if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2) {


            $ticket_types=\DB::table('activity_sub_div_class')
                ->leftJoin('entity_division', 'entity_division.assigned_code', '=', 'activity_sub_div_class.entity_div_code')
                ->select('activity_sub_div_class.id','entity_division.div_name','activity_sub_div_class.class_desc', 'activity_sub_div_class.comment', 'activity_sub_div_class.active_status', 'activity_sub_div_class.created_at')
                ->where('activity_sub_div_class.del_status',false)
                ->where('entity_division.active_status',true)
                ->where('entity_division.del_status',false)
                ->orderBy('activity_sub_div_class.id', 'desc')
                ->get();

            $Awards=\DB::table('entity_division')
                ->leftJoin('entity_master', 'entity_master.entity_id', '=', 'entity_division.entity_id')
                ->select('entity_division.id', 'entity_division.assigned_code as entity_div_code', 'entity_division.div_name as event_name','entity_division.service_label as event_desc', 'entity_division.active_status', 'entity_master.id as entity_master_id', 'entity_master.entity_name')
                ->whereIn('entity_division.activity_type_code', ['SHW', 'VOT-SHW'])
                ->where('entity_division.del_status',false)
                ->where('entity_master.active_status',true)
                ->where('entity_master.del_status',false)
                ->orderBy('entity_division.created_at', 'desc')
                ->get();

            $module_menus=app('App\Http\Controllers\admin\CrudBuilderController')->createMenumodule();
            return view('admin.tickets.ticket_types')->with(['module_menus'=>$module_menus, 'ticket_types' => $ticket_types, 'Awards'=>$Awards]);
        }else{
            return redirect('404');
        }
    }


    public function ticket_types_create(Request $request){

        //    \Log::info($request);

        $ActivitySubDivClass=new ActivitySubDivClass;
        if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2 || \Session::get('role_id') == 4 ) { // if(Auth::user()->can("add_Awards")){  #####check permission
            if($ActivitySubDivClass->rules){
                $validator = Validator::make($request->all(), $ActivitySubDivClass->rules);
                if ($validator->fails()) {
                    $backAwards = ActivitySubDivClass::paginate(10);
                    return view("AwardsView::Awardsajax")->withErrors($validator)->with(['Awards'=>$backAwards,'tab'=>2,'editAwards'=>$request->all()]);
                }
            }

            $end_date=null;
            $user_id = \Auth::user()->id;

            // dd($request->all());
            $ActivitySubDivClass->entity_div_code = $request->entity_div_code;
            $ActivitySubDivClass->class_desc = $request->class_desc;
            $ActivitySubDivClass->comment = $request->comment;
            $ActivitySubDivClass->user_id = $user_id;
            $ActivitySubDivClass->save();

            $entity_master = \DB::table('entity_master')->where('active_status',true)->where('del_status',false)->get();

            $ticket_types=\DB::table('activity_sub_div_class')
                ->leftJoin('entity_division', 'entity_division.assigned_code', '=', 'activity_sub_div_class.entity_div_code')
                ->select('activity_sub_div_class.id','entity_division.div_name','activity_sub_div_class.class_desc', 'activity_sub_div_class.comment', 'activity_sub_div_class.active_status', 'activity_sub_div_class.created_at')
                ->where('activity_sub_div_class.del_status',false)
                ->where('entity_division.active_status',true)
                ->where('entity_division.del_status',false)
                ->orderBy('activity_sub_div_class.id', 'desc')
                ->get();

            $Awards=\DB::table('entity_division')
                ->leftJoin('entity_master', 'entity_master.entity_id', '=', 'entity_division.entity_id')
                ->select('entity_division.id', 'entity_division.assigned_code','entity_division.div_name','entity_division.service_label as event_desc', 'entity_division.active_status', 'entity_master.id as entity_master_id', 'entity_master.entity_name')
                ->whereIn('entity_division.activity_type_code', ['SHW', 'VOT-SHW'])
                ->where('entity_division.del_status',false)
                ->where('entity_master.active_status',true)
                ->where('entity_master.del_status',false)
                ->orderBy('entity_division.created_at', 'desc')
                ->get();

            $flash_message="Record added successfully";
            flash('Verification!')->success($flash_message);

            return redirect()->back()->with(['status'=>$flash_message, 'tab'=>1,'flag'=>3,'ticket_types'=>$ticket_types, 'Awards'=>$Awards]);

        }else{

            $ticket_types=\DB::table('activity_sub_div_class')
                ->leftJoin('entity_division', 'entity_division.assigned_code', '=', 'activity_sub_div_class.entity_div_code')
                ->select('activity_sub_div_class.id','entity_division.div_name','activity_sub_div_class.class_desc', 'activity_sub_div_class.comment', 'activity_sub_div_class.active_status', 'activity_sub_div_class.created_at')
            //    ->where('activity_sub_div_class.active_status',true)
                ->where('activity_sub_div_class.del_status',false)
                ->where('entity_division.active_status',true)
                ->where('entity_division.del_status',false)
                ->orderBy('activity_sub_div_class.id', 'desc')
                ->get();

            $Awards=\DB::table('entity_division')
                ->leftJoin('entity_master', 'entity_master.entity_id', '=', 'entity_division.entity_id')
                ->select('entity_division.id', 'entity_division.assigned_code','entity_division.div_name','entity_division.service_label as event_desc', 'entity_division.active_status', 'entity_master.id as entity_master_id', 'entity_master.entity_name')
                ->whereIn('entity_division.activity_type_code', ['SHW', 'VOT-SHW'])
                ->where('entity_division.del_status',false)
                ->where('entity_master.active_status',true)
                ->where('entity_master.del_status',false)
                ->orderBy('entity_division.created_at', 'desc')
                ->get();

        //    return view("AwardsView::Awardsajax")->with(['tab'=>1,'flag'=>6,'Awards'=>$Awards]);
            return view('admin.tickets.ticket_types')->with([ 'ticket_types' => $ticket_types, 'Awards'=>$Awards]);
        }


    }


    public function ticket_types_get_details(Request $request){

        \Log::info($request);
        // \Log::info($request->route('id'));
        // dd($request);

        // $ActivitySubDivClass=new ActivitySubDivClass;
        if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2 || \Session::get('role_id') == 4 ) {

            $ticket_types_details=\DB::table('activity_sub_div_class')
                ->leftJoin('entity_division', 'entity_division.assigned_code', '=', 'activity_sub_div_class.entity_div_code')
                ->select('activity_sub_div_class.id','entity_division.div_name','entity_division.assigned_code', 'activity_sub_div_class.class_desc', 'activity_sub_div_class.comment', 'activity_sub_div_class.active_status', 'activity_sub_div_class.created_at')
                ->where('activity_sub_div_class.id',$request['ticket_type_id'])
                ->where('activity_sub_div_class.del_status',false)
                ->where('entity_division.active_status',true)
                ->where('entity_division.del_status',false)
                ->orderBy('activity_sub_div_class.id', 'desc')
                ->limit(1)
                ->get();

            \Log::info($ticket_types_details);


            $end_date=null;
            $action=null;

            $Awards=\DB::table('entity_division')
                ->leftJoin('entity_master', 'entity_master.entity_id', '=', 'entity_division.entity_id')
                ->select('entity_division.id', 'entity_division.assigned_code','entity_division.div_name','entity_division.service_label as event_desc', 'entity_division.active_status', 'entity_master.id as entity_master_id', 'entity_master.entity_name')
                ->whereIn('entity_division.activity_type_code', ['SHW', 'VOT-SHW'])
                ->where('entity_division.del_status',false)
                ->where('entity_master.active_status',true)
                ->where('entity_master.del_status',false)
                ->orderBy('entity_division.created_at', 'desc')
                ->get();

            return Response::json(array('ticket_types_details'=>$ticket_types_details, 'awards'=>$Awards)); //redirect()->back()->with(['ticket_types_details'=>$ticket_types_details, 'tab'=>1,'flag'=>3, 'Awards'=>$Awards]);

        }else{

            $ticket_types=\DB::table('activity_sub_div_class')
                ->leftJoin('entity_division', 'entity_division.assigned_code', '=', 'activity_sub_div_class.entity_div_code')
                ->select('activity_sub_div_class.id','entity_division.div_name','activity_sub_div_class.class_desc', 'activity_sub_div_class.comment', 'activity_sub_div_class.active_status', 'activity_sub_div_class.created_at')
                ->where('activity_sub_div_class.del_status',false)
                ->where('entity_division.active_status',true)
                ->where('entity_division.del_status',false)
                ->orderBy('activity_sub_div_class.id', 'desc')
                ->get();

            $Awards=\DB::table('entity_division')
                ->leftJoin('entity_master', 'entity_master.entity_id', '=', 'entity_division.entity_id')
                ->select('entity_division.id', 'entity_division.assigned_code','entity_division.div_name','entity_division.service_label as event_desc', 'entity_division.active_status', 'entity_master.id as entity_master_id', 'entity_master.entity_name')
                ->whereIn('entity_division.activity_type_code', ['SHW', 'VOT-SHW'])
                ->where('entity_division.del_status',false)
                ->where('entity_master.active_status',true)
                ->where('entity_master.del_status',false)
                ->orderBy('entity_division.created_at', 'desc')
                ->get();

            return view('admin.tickets.ticket_types')->with([ 'ticket_types' => $ticket_types, 'Awards'=>$Awards]);
        }


    }


    public function ticket_types_update(Request $request){

        \Log::info($request);
        // $ActivitySubDivClass=new ActivitySubDivClass;
        if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2 || \Session::get('role_id') == 4 ) {
 
            $end_date=null;
            $action=null;

            $user_id = \Auth::user()->id;
            if (!empty($request['ticket_type_id'])){
                $ActivitySubDivClass = ActivitySubDivClass::find($request['ticket_type_id']);
                if($ActivitySubDivClass){

                    $ActivitySubDivClass->entity_div_code = $request->edit_plan_id;
                    $ActivitySubDivClass->class_desc = $request->edit_class_desc;
                    $ActivitySubDivClass->comment = $request->edit_comment;
                    $ActivitySubDivClass->user_id = $user_id;
                    $ActivitySubDivClass->save();

                    $flash_message="Record updated successfully";
                    flash('Ticket Setup!')->success($flash_message);
                }else{
                    $flash_message="Record couldn't be updated";
                    flash('Ticket Setup!')->error($flash_message);
                }

            }else{
                $flash_message="Record couldn't be updated";
                flash('Ticket Setup!')->error($flash_message);
            }



            $ticket_types=\DB::table('activity_sub_div_class')
                ->leftJoin('entity_division', 'entity_division.assigned_code', '=', 'activity_sub_div_class.entity_div_code')
                ->select('activity_sub_div_class.id','entity_division.div_name','activity_sub_div_class.class_desc', 'activity_sub_div_class.comment', 'activity_sub_div_class.active_status', 'activity_sub_div_class.created_at')
                ->where('activity_sub_div_class.del_status',false)
                ->where('entity_division.active_status',true)
                ->where('entity_division.del_status',false)
                ->orderBy('activity_sub_div_class.id', 'desc')
                ->get();

            $Awards=\DB::table('entity_division')
                ->leftJoin('entity_master', 'entity_master.entity_id', '=', 'entity_division.entity_id')
                ->select('entity_division.id', 'entity_division.assigned_code','entity_division.div_name','entity_division.service_label as event_desc', 'entity_division.active_status', 'entity_master.id as entity_master_id', 'entity_master.entity_name')
                ->whereIn('entity_division.activity_type_code', ['SHW', 'VOT-SHW'])
                ->where('entity_division.del_status',false)
                ->where('entity_master.active_status',true)
                ->where('entity_master.del_status',false)
                ->orderBy('entity_division.created_at', 'desc')
                ->get();




            return redirect()->back()->with(['status'=>$flash_message, 'tab'=>1,'flag'=>3,'ticket_types'=>$ticket_types, 'Awards'=>$Awards]);

        }else{

            $ticket_types=\DB::table('activity_sub_div_class')
                ->leftJoin('entity_division', 'entity_division.assigned_code', '=', 'activity_sub_div_class.entity_div_code')
                ->select('activity_sub_div_class.id','entity_division.div_name','activity_sub_div_class.class_desc', 'activity_sub_div_class.comment', 'activity_sub_div_class.active_status', 'activity_sub_div_class.created_at')
                ->where('activity_sub_div_class.del_status',false)
                ->where('entity_division.active_status',true)
                ->where('entity_division.del_status',false)
                ->orderBy('activity_sub_div_class.id', 'desc')
                ->get();

            $Awards=\DB::table('entity_division')
                ->leftJoin('entity_master', 'entity_master.entity_id', '=', 'entity_division.entity_id')
                ->select('entity_division.id', 'entity_division.assigned_code','entity_division.div_name','entity_division.service_label as event_desc', 'entity_division.active_status', 'entity_master.id as entity_master_id', 'entity_master.entity_name')
                ->whereIn('entity_division.activity_type_code', ['SHW', 'VOT-SHW'])
                ->where('entity_division.del_status',false)
                ->where('entity_master.active_status',true)
                ->where('entity_master.del_status',false)
                ->orderBy('entity_division.created_at', 'desc')
                ->get();

            return view('admin.tickets.ticket_types')->with([ 'ticket_types' => $ticket_types, 'Awards'=>$Awards]);
        }


    }


    public function ticket_types_edit(Request $request){

        \Log::info($request);

        if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2 || \Session::get('role_id') == 4 ) {

            $end_date=null;
            $action=null;

            $user_id = \Auth::user()->id;
            if (!empty($request["btn_status"]) && !empty($request->route('id'))){
                $ActivitySubDivClass = ActivitySubDivClass::find($request->route('id'));
                if($ActivitySubDivClass){
                    if ($request["btn_status"] == "deactivate") {
                        $ActivitySubDivClass->active_status=false;
                    }else{
                        $ActivitySubDivClass->active_status=true;
                    }

                    $ActivitySubDivClass->user_id = $user_id;
                    $ActivitySubDivClass->save();

                    Activity::log([
                        'contentId'   => $ActivitySubDivClass->id,
                        'contentType' => 'ActivitySubDivClass',
                        'action'      => 'Status Update',
                        'description' => 'Updated Status to'.$action,
                        'details'     => 'UserID: '.Auth::user()->id,
                    ]);
                    $flash_message="Record updated successfully";
                    flash('Ticket Setup!')->success($flash_message);
                }else{
                    $flash_message="Record couldn't be updated";
                    flash('Ticket Setup!')->error($flash_message);
                }

            }else{
                $flash_message="Record couldn't be updated";
                flash('Ticket Setup!')->error($flash_message);
            }



            $ticket_types=\DB::table('activity_sub_div_class')
                ->leftJoin('entity_division', 'entity_division.assigned_code', '=', 'activity_sub_div_class.entity_div_code')
                ->select('activity_sub_div_class.id','entity_division.div_name','activity_sub_div_class.class_desc', 'activity_sub_div_class.comment', 'activity_sub_div_class.active_status', 'activity_sub_div_class.created_at')
                ->where('activity_sub_div_class.del_status',false)
                ->where('entity_division.active_status',true)
                ->where('entity_division.del_status',false)
                ->orderBy('activity_sub_div_class.id', 'desc')
                ->get();

            $Awards=\DB::table('entity_division')
                ->leftJoin('entity_master', 'entity_master.entity_id', '=', 'entity_division.entity_id')
                ->select('entity_division.id', 'entity_division.assigned_code','entity_division.div_name','entity_division.service_label as event_desc', 'entity_division.active_status', 'entity_master.id as entity_master_id', 'entity_master.entity_name')
                ->whereIn('entity_division.activity_type_code', ['SHW', 'VOT-SHW'])
                ->where('entity_division.del_status',false)
                ->where('entity_master.active_status',true)
                ->where('entity_master.del_status',false)
                ->orderBy('entity_division.created_at', 'desc')
                ->get();


            return redirect()->back()->with(['status'=>$flash_message, 'tab'=>1,'flag'=>3,'ticket_types'=>$ticket_types, 'Awards'=>$Awards]);

        }else{

            $ticket_types=\DB::table('activity_sub_div_class')
                ->leftJoin('entity_division', 'entity_division.assigned_code', '=', 'activity_sub_div_class.entity_div_code')
                ->select('activity_sub_div_class.id','entity_division.div_name','activity_sub_div_class.class_desc', 'activity_sub_div_class.comment', 'activity_sub_div_class.active_status', 'activity_sub_div_class.created_at')
                ->where('activity_sub_div_class.del_status',false)
                ->where('entity_division.active_status',true)
                ->where('entity_division.del_status',false)
                ->orderBy('activity_sub_div_class.id', 'desc')
                ->get();

            $Awards=\DB::table('entity_division')
                ->leftJoin('entity_master', 'entity_master.entity_id', '=', 'entity_division.entity_id')
                ->select('entity_division.id', 'entity_division.assigned_code','entity_division.div_name','entity_division.service_label as event_desc', 'entity_division.active_status', 'entity_master.id as entity_master_id', 'entity_master.entity_name')
                ->whereIn('entity_division.activity_type_code', ['SHW', 'VOT-SHW'])
                ->where('entity_division.del_status',false)
                ->where('entity_master.active_status',true)
                ->where('entity_master.del_status',false)
                ->orderBy('entity_division.created_at', 'desc')
                ->get();

            return view('admin.tickets.ticket_types')->with([ 'ticket_types' => $ticket_types, 'Awards'=>$Awards]);
        }


    }




    public function ticket_classifications_index(){
        if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2  || \Session::get('role_id') == 4 ) {  //if(Auth::user()->can("print_monthlyspeaking_reports")) {

            Activity::log([
                'contentId'   => Auth::user()->id,
                'contentType' => 'Ticket Classifications Index',
                'action'      => 'Viewed',
                'description' => 'Ticket Classifications Index page has been viewed.',
                'details'     => Auth::user()->name,
            ]);

            $ticket_classifications=\DB::table('ticket_classifications_view')
            //    ->orderBy('activity_sub_div_class.id', 'desc')
                ->get();

            $ticket_types=\DB::table('activity_sub_div_class')
                ->leftJoin('entity_division', 'entity_division.assigned_code', '=', 'activity_sub_div_class.entity_div_code')
                ->select('activity_sub_div_class.id','entity_division.div_name','activity_sub_div_class.class_desc', 'activity_sub_div_class.comment', 'activity_sub_div_class.active_status', 'activity_sub_div_class.created_at')
            //    ->where('activity_sub_div_class.active_status',true)
                ->where('activity_sub_div_class.del_status',false)
                ->where('entity_division.active_status',true)
                ->where('entity_division.del_status',false)
                ->orderBy('activity_sub_div_class.id', 'desc')
                ->get();

            $Awards=\DB::table('entity_division')
                ->leftJoin('entity_master', 'entity_master.entity_id', '=', 'entity_division.entity_id')
                ->select('entity_division.id', 'entity_division.assigned_code','entity_division.div_name','entity_division.service_label as event_desc', 'entity_division.active_status', 'entity_master.id as entity_master_id', 'entity_master.entity_name')
                ->whereIn('entity_division.activity_type_code', ['SHW', 'VOT-SHW'])
                ->where('entity_division.del_status',false)
                ->where('entity_master.active_status',true)
                ->where('entity_master.del_status',false)
                ->orderBy('entity_division.created_at', 'desc')
                ->get();


            $module_menus=app('App\Http\Controllers\admin\CrudBuilderController')->createMenumodule();
            return view('admin.tickets.ticket_classifications')->with(['module_menus'=>$module_menus, 'ticket_classifications' => $ticket_classifications, 'ticket_types' => $ticket_types, 'Awards'=>$Awards]);
        }else{
            return redirect('404');
        }
    }


    public function get_ticket_types(){
        $plan_id = Input::get('plan_id');
        // dd($plan_id);
        $ticket_types=\DB::table('activity_sub_div_class')
            ->leftJoin('entity_division', 'entity_division.assigned_code', '=', 'activity_sub_div_class.entity_div_code')
            ->select('activity_sub_div_class.id','entity_division.div_name','activity_sub_div_class.class_desc', 'activity_sub_div_class.comment', 'activity_sub_div_class.active_status', 'activity_sub_div_class.created_at')
            ->where('entity_division.assigned_code',$plan_id)
            ->where('activity_sub_div_class.del_status',false)
            ->where('entity_division.active_status',true)
            ->where('entity_division.del_status',false)
            ->orderBy('activity_sub_div_class.id', 'desc')
            ->get();
        return Response::json($ticket_types);
    }


    public function ticket_classifications_create(Request $request){

        // \Log::info($request);

        $ActivitySubDiv=new ActivitySubDiv;
        if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2 || \Session::get('role_id') == 4 ) { // if(Auth::user()->can("add_Awards")){  #####check permission
            if($ActivitySubDiv->rules){
                $validator = Validator::make($request->all(), $ActivitySubDiv->rules);
                if ($validator->fails()) {
                    $backAwards = ActivitySubDiv::paginate(10);
                    return view("AwardsView::Awardsajax")->withErrors($validator)->with(['Awards'=>$backAwards,'tab'=>2,'editAwards'=>$request->all()]);
                }
            }

            $end_date=null;
            $user_id = \Auth::user()->id;


            // dd($request->all());
            $date=null;
            $date_desc=null;
            $activity_div_id=null;

            $event_datetime = strtotime($request->start_date);
            $event_date = date('Y-m-d',$event_datetime);
            $event_time = date('H:i:s',$event_datetime);
            $activity_time_desc = date('g A',$event_datetime);
            // dd($event_date,$event_time,$request);

            #create date and time first
            $activity_div=DB::table('activity_divs')->where('entity_div_code', $request->plan_id)->where('activity_date', $event_date)->where('activity_divs.active_status',true)->first();
            if ($activity_div != null) {
                $activity_div_id=$activity_div->id;
            } else {
                $activity_div_id = DB::table('activity_divs')->insertGetId(
                    ['entity_div_code' => $request->plan_id, 'activity_date' => $event_date, 'activity_div_desc' => $event_date, 'user_id'=>$user_id]
                );
            }

            $activity_sub_divs_id = DB::table('activity_sub_divs')->insertGetId(
                ['activity_div_id' => $activity_div_id, 'activity_time' => $event_time, 'classification' => $request->classification, 'amount'=> $request->amount, 'activity_time_desc'=>$activity_time_desc, 'user_id'=>$user_id]
            );

            $ticket_types=\DB::table('activity_sub_div_class')
                ->leftJoin('entity_division', 'entity_division.assigned_code', '=', 'activity_sub_div_class.entity_div_code')
                ->select('activity_sub_div_class.id','entity_division.div_name','activity_sub_div_class.class_desc', 'activity_sub_div_class.comment', 'activity_sub_div_class.active_status', 'activity_sub_div_class.created_at')
            //    ->where('activity_sub_div_class.active_status',true)
                ->where('activity_sub_div_class.del_status',false)
                ->where('entity_division.active_status',true)
                ->where('entity_division.del_status',false)
                ->orderBy('activity_sub_div_class.id', 'desc')
                ->get();

            $Awards=\DB::table('entity_division')
                ->leftJoin('entity_master', 'entity_master.entity_id', '=', 'entity_division.entity_id')
                ->select('entity_division.id', 'entity_division.assigned_code','entity_division.div_name','entity_division.service_label as event_desc', 'entity_division.active_status', 'entity_master.id as entity_master_id', 'entity_master.entity_name')
                ->whereIn('entity_division.activity_type_code', ['SHW', 'VOT-SHW'])
                ->where('entity_division.del_status',false)
                ->where('entity_master.active_status',true)
                ->where('entity_master.del_status',false)
                ->orderBy('entity_division.created_at', 'desc')
                ->get();

            $ticket_classifications=DB::table('ticket_classifications_view')->get();

            $flash_message="Record added successfully";
            flash('Ticket Details!')->success($flash_message);

            return redirect()->back()->with(['status'=>$flash_message, 'tab'=>1,'flag'=>3,'ticket_types'=>$ticket_types, 'Awards'=>$Awards, 'ticket_classifications'=>$ticket_classifications]);

        }else{

            $ticket_types=DB::table('activity_sub_div_class')
                ->leftJoin('entity_division', 'entity_division.assigned_code', '=', 'activity_sub_div_class.entity_div_code')
                ->select('activity_sub_div_class.id','entity_division.div_name','activity_sub_div_class.class_desc', 'activity_sub_div_class.comment', 'activity_sub_div_class.active_status', 'activity_sub_div_class.created_at')
            //    ->where('activity_sub_div_class.active_status',true)
                ->where('activity_sub_div_class.del_status',false)
                ->where('entity_division.active_status',true)
                ->where('entity_division.del_status',false)
                ->orderBy('activity_sub_div_class.id', 'desc')
                ->get();

            $Awards=DB::table('entity_division')
                ->leftJoin('entity_master', 'entity_master.entity_id', '=', 'entity_division.entity_id')
                ->select('entity_division.id', 'entity_division.assigned_code','entity_division.div_name','entity_division.service_label as event_desc', 'entity_division.active_status', 'entity_master.id as entity_master_id', 'entity_master.entity_name')
                ->whereIn('entity_division.activity_type_code', ['SHW', 'VOT-SHW'])
                ->where('entity_division.del_status',false)
                ->where('entity_master.active_status',true)
                ->where('entity_master.del_status',false)
                ->orderBy('entity_division.created_at', 'desc')
                ->get();
            $ticket_classifications=DB::table('ticket_classifications_view')->get();

            return view('admin.tickets.ticket_classifications')->with([ 'ticket_types' => $ticket_types, 'Awards'=>$Awards, 'ticket_classifications'=>$ticket_classifications]);
        }


    }


    public function ticket_classifications_get_details(Request $request){
        if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2  || \Session::get('role_id') == 4 ) {  //if(Auth::user()->can("print_monthlyspeaking_reports")) {


            $ticket_classifications=\DB::table('ticket_classifications_view')
                ->where('activity_sub_div_id', $request->activity_sub_div_id)
                ->limit(1)
                ->get();

            $ticket_types=\DB::table('activity_sub_div_class')
                ->leftJoin('entity_division', 'entity_division.assigned_code', '=', 'activity_sub_div_class.entity_div_code')
                ->select('activity_sub_div_class.id','entity_division.div_name','activity_sub_div_class.class_desc', 'activity_sub_div_class.comment', 'activity_sub_div_class.active_status', 'activity_sub_div_class.created_at')
            //    ->where('activity_sub_div_class.active_status',true)
                ->where('activity_sub_div_class.del_status',false)
                ->where('entity_division.active_status',true)
                ->where('entity_division.del_status',false)
                ->orderBy('activity_sub_div_class.id', 'desc')
                ->get();

                $Awards=\DB::table('entity_division')
                ->leftJoin('entity_master', 'entity_master.entity_id', '=', 'entity_division.entity_id')
                ->select('entity_division.id', 'entity_division.assigned_code as entity_div_code', 'entity_division.div_name as event_name','entity_division.service_label as event_desc', 'entity_division.active_status', 'entity_master.id as entity_master_id', 'entity_master.entity_name')
                ->whereIn('entity_division.activity_type_code', ['SHW', 'VOT-SHW'])
                ->where('entity_division.del_status',false)
                ->where('entity_master.active_status',true)
                ->where('entity_master.del_status',false)
                ->orderBy('entity_division.created_at', 'desc')
                ->get();

        //    \Log::info($ticket_classifications[0]->plan_id);
            $selected_plan_ticket_types=\DB::table('activity_sub_div_class')
                ->leftJoin('entity_division', 'entity_division.assigned_code', '=', 'activity_sub_div_class.entity_div_code')
                ->select('activity_sub_div_class.id','entity_division.div_name','activity_sub_div_class.class_desc', 'activity_sub_div_class.comment', 'activity_sub_div_class.active_status', 'activity_sub_div_class.created_at')
                ->where('entity_division.assigned_code',$ticket_classifications[0]->entity_div_code)
                ->where('activity_sub_div_class.del_status',false)
                ->where('entity_division.active_status',true)
                ->where('entity_division.del_status',false)
                ->orderBy('activity_sub_div_class.id', 'desc')
                ->get();

        //    $module_menus=app('App\Http\Controllers\admin\CrudBuilderController')->createMenumodule();
        //    return view('admin.tickets.ticket_classifications')->with(['module_menus'=>$module_menus, 'ticket_classifications' => $ticket_classifications, 'ticket_types' => $ticket_types, 'Awards'=>$Awards]);

            return Response::json(array('awards'=>$Awards,'ticket_types' => $ticket_types,'ticket_classifications' => $ticket_classifications, 'selected_plan_ticket_types'=>$selected_plan_ticket_types));

        }else{
            return redirect('404');
        }
    }



    public function ticket_classifications_update(Request $request){
        // \Log::info($request);
        //dd($request);

        if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2 || \Session::get('role_id') == 4 ) {

            $end_date=null;
            $action=null;

            $user_id = \Auth::user()->id;

            if ( !empty($request['selected_activity_sub_div_id'])){

                $event_datetime = strtotime($request->edit_start_date);
                $event_date = date('Y-m-d',$event_datetime);
                $event_date_desc = date('d F Y',$event_datetime);
                $event_time = date('H:i:s',$event_datetime);
                $activity_time_desc = date('g A',$event_datetime);

                // \DB::table('activity_divs')->where('id', $request->selected_activity_div_id)->update(['activity_date'=>$event_date,'activity_div_desc' => $event_date_desc, 'user_id'=>$user_id]);
                \DB::table('activity_sub_divs')->where('id', $request->selected_activity_sub_div_id)->update(['activity_time' => $event_time, 'classification' => $request->edit_classification, 'amount'=> $request->edit_amount, 'activity_time_desc'=>$activity_time_desc, 'user_id'=>$user_id]);

                $flash_message="Record updated successfully";
                flash('Ticket Setup!')->success($flash_message);

            }else{
                $flash_message="Record couldn't be updated";
                flash('Ticket Setup!')->error($flash_message);
            }



            $ticket_types=\DB::table('activity_sub_div_class')
                ->leftJoin('entity_division', 'entity_division.assigned_code', '=', 'activity_sub_div_class.entity_div_code')
                ->select('activity_sub_div_class.id','entity_division.div_name','activity_sub_div_class.class_desc', 'activity_sub_div_class.comment', 'activity_sub_div_class.active_status', 'activity_sub_div_class.created_at')
                ->where('activity_sub_div_class.del_status',false)
                ->where('entity_division.active_status',true)
                ->where('entity_division.del_status',false)
                ->orderBy('activity_sub_div_class.id', 'desc')
                ->get();

            $Awards=\DB::table('entity_division')
                ->leftJoin('entity_master', 'entity_master.entity_id', '=', 'entity_division.entity_id')
                ->select('entity_division.id', 'entity_division.assigned_code','entity_division.div_name','entity_division.service_label as event_desc', 'entity_division.active_status', 'entity_master.id as entity_master_id', 'entity_master.entity_name')
                ->whereIn('entity_division.activity_type_code', ['SHW', 'VOT-SHW'])
                ->where('entity_division.del_status',false)
                ->where('entity_master.active_status',true)
                ->where('entity_master.del_status',false)
                ->orderBy('entity_division.created_at', 'desc')
                ->get();

            $ticket_classifications=\DB::table('ticket_classifications_view')->get();

            return redirect()->back()->with(['status'=>$flash_message, 'tab'=>1,'flag'=>3,'ticket_types'=>$ticket_types, 'Awards'=>$Awards,'ticket_classifications'=>$ticket_classifications]);

        }else{

            $ticket_types=\DB::table('activity_sub_div_class')
                ->leftJoin('entity_division', 'entity_division.assigned_code', '=', 'activity_sub_div_class.entity_div_code')
                ->select('activity_sub_div_class.id','entity_division.div_name','activity_sub_div_class.class_desc', 'activity_sub_div_class.comment', 'activity_sub_div_class.active_status', 'activity_sub_div_class.created_at')
                ->where('activity_sub_div_class.del_status',false)
                ->where('entity_division.active_status',true)
                ->where('entity_division.del_status',false)
                ->orderBy('activity_sub_div_class.id', 'desc')
                ->get();

            $Awards=\DB::table('entity_division')
                ->leftJoin('entity_master', 'entity_master.entity_id', '=', 'entity_division.entity_id')
                ->select('entity_division.id', 'entity_division.assigned_code','entity_division.div_name','entity_division.service_label as event_desc', 'entity_division.active_status', 'entity_master.id as entity_master_id', 'entity_master.entity_name')
                ->whereIn('entity_division.activity_type_code', ['SHW', 'VOT-SHW'])
                ->where('entity_division.del_status',false)
                ->where('entity_master.active_status',true)
                ->where('entity_master.del_status',false)
                ->orderBy('entity_division.created_at', 'desc')
                ->get();

            $ticket_classifications=\DB::table('ticket_classifications_view')->get();

            return view('admin.tickets.ticket_classifications')->with([ 'ticket_types' => $ticket_types, 'Awards'=>$Awards,'ticket_classifications'=>$ticket_classifications]);
        }
    }




    public function ticket_classifications_edit(Request $request){
        // \Log::info($request);

        if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2 || \Session::get('role_id') == 4 ) {

            $end_date=null;
            $action=null;

            $user_id = \Auth::user()->id;

            if (!empty($request["btn_status"]) && !empty($request['activity_div_id']) && !empty($request['activity_sub_div_id'])){

                if ($request["btn_status"] == "deactivate") {
                    $action=false;
                }else{
                    $action=true;
                }

                // \DB::table('activity_divs')->where('id', $request->activity_div_id)->update(['active_status' => $action, 'user_id'=>$user_id]);
                \DB::table('activity_sub_divs')->where('id', $request->activity_sub_div_id)->update(['active_status' => $action, 'user_id'=>$user_id]);

                $flash_message="Record updated successfully";
                flash('Ticket Setup!')->success($flash_message);

            }else{
                $flash_message="Record couldn't be updated";
                flash('Ticket Setup!')->error($flash_message);
            }


            $ticket_types=\DB::table('activity_sub_div_class')
                ->leftJoin('entity_division', 'entity_division.assigned_code', '=', 'activity_sub_div_class.entity_div_code')
                ->select('activity_sub_div_class.id','entity_division.div_name','activity_sub_div_class.class_desc', 'activity_sub_div_class.comment', 'activity_sub_div_class.active_status', 'activity_sub_div_class.created_at')
                ->where('activity_sub_div_class.del_status',false)
                ->where('entity_division.active_status',true)
                ->where('entity_division.del_status',false)
                ->orderBy('activity_sub_div_class.id', 'desc')
                ->get();

            $Awards=\DB::table('entity_division')
                ->leftJoin('entity_master', 'entity_master.entity_id', '=', 'entity_division.entity_id')
                ->select('entity_division.id', 'entity_division.assigned_code','entity_division.div_name','entity_division.service_label as event_desc', 'entity_division.active_status', 'entity_master.id as entity_master_id', 'entity_master.entity_name')
                ->whereIn('entity_division.activity_type_code', ['SHW', 'VOT-SHW'])
                ->where('entity_division.del_status',false)
                ->where('entity_master.active_status',true)
                ->where('entity_master.del_status',false)
                ->orderBy('entity_division.created_at', 'desc')
                ->get();

            $ticket_classifications=\DB::table('ticket_classifications_view')->get();

            return redirect()->back()->with(['status'=>$flash_message, 'tab'=>1,'flag'=>3,'ticket_types'=>$ticket_types, 'Awards'=>$Awards,'ticket_classifications'=>$ticket_classifications]);

        }else{

            $ticket_types=\DB::table('activity_sub_div_class')
                ->leftJoin('entity_division', 'entity_division.assigned_code', '=', 'activity_sub_div_class.entity_div_code')
                ->select('activity_sub_div_class.id','entity_division.div_name','activity_sub_div_class.class_desc', 'activity_sub_div_class.comment', 'activity_sub_div_class.active_status', 'activity_sub_div_class.created_at')
                ->where('activity_sub_div_class.del_status',false)
                ->where('entity_division.active_status',true)
                ->where('entity_division.del_status',false)
                ->orderBy('activity_sub_div_class.id', 'desc')
                ->get();

            $Awards=\DB::table('entity_division')
                ->leftJoin('entity_master', 'entity_master.entity_id', '=', 'entity_division.entity_id')
                ->select('entity_division.id', 'entity_division.assigned_code','entity_division.div_name','entity_division.service_label as event_desc', 'entity_division.active_status', 'entity_master.id as entity_master_id', 'entity_master.entity_name')
                ->whereIn('entity_division.activity_type_code', ['SHW', 'VOT-SHW'])
                ->where('entity_division.del_status',false)
                ->where('entity_master.active_status',true)
                ->where('entity_master.del_status',false)
                ->orderBy('entity_division.created_at', 'desc')
                ->get();

            $ticket_classifications=\DB::table('ticket_classifications_view')->get();

            return view('admin.tickets.ticket_classifications')->with([ 'ticket_types' => $ticket_types, 'Awards'=>$Awards,'ticket_classifications'=>$ticket_classifications]);
        }
    }

}