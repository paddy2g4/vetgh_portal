<?php


namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\candidatesss;
//use \GeneralSettings\Model\GeneralSettings;
use Regulus\ActivityLog\Models\Activity;


class TransactionsController extends Controller
{
    public function index(){
    //    dd(\Session::get('role_id') );
        if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2  || \Session::get('role_id') == 3 ) {  //if(Auth::user()->can("print_monthlyspeaking_reports")) {
            // Activity::log([
            //     'contentId'   => Auth::user()->id,
            //     'contentType' => 'Transactions',
            //     'action'      => 'Viewed',
            //     'description' => 'Transactions page has been viewed.',
            //     'details'     => Auth::user()->name,
            // ]);

            if (\Session::get('role_id') == 3){
                $transactions = \DB::table('transaction_mat_view')
                    ->where('entity_id',\Auth::user()->entity_id)
                   ->where('award_act_status',true)->where('award_del_status',false)
                    ->orderby('created_at','desc')
                    ->take(6000)
                    ->get();
            }else{
                $transactions = \DB::table('transaction_mat_view')
                    ->where('award_act_status',true)->where('award_del_status',false)
                    ->orderby('created_at','desc')
                    ->take(6000)
                    ->get();
            }

        //    dd(\Session::get('role_id'),\Auth::user()->entity_id,$transactions);

            $module_menus=app('App\Http\Controllers\admin\CrudBuilderController')->createMenumodule();
            return view('reports.transactions')->with(['module_menus'=>$module_menus, 'transactions'=>$transactions]);
        }else{
            return redirect('404');
        }
    }


    public function getData(Request $request)
    {
        DB::enableQueryLog();
        
        $draw = $request->get('draw');
        $start = $request->get("start");
        $rowperpage = $request->get("length"); // Rows display per page

        $columnIndex_arr = $request->get('order');
        $columnName_arr = $request->get('columns');
        $order_arr = $request->get('order');
        $search_arr = $request->get('search');

        $columnIndex = $columnIndex_arr[0]['column']; // Column index
        $columnName = $columnName_arr[$columnIndex]['data']; // Column name
        $columnSortOrder = $order_arr[0]['dir']; // asc or desc
        $searchValue = $search_arr['value']; // Search value


        // Fetch records
        \Log::info('searching records: role_id: '.\Session::get('role_id').'. entity_id: '.\Auth::user()->entity_id);
  

        if (!empty($searchValue)){

            if (\Session::get('role_id') == 3 || \Session::get('role_id') == '3'){

                $records = DB::select( DB::raw("select * from transaction_mat_view where (entity_id = :entity_id and award_act_status = :award_act_status and award_del_status = :award_del_status) and (trans_ref like :searchValue or plan_name like :searchValue or cat_name like :searchValue or nom_name like :searchValue or customer_number like :searchValue or nw like :searchValue or payment_med like :searchValue) order by created_at desc limit :rowperpage offset :start"), array(
                    'entity_id' => \Auth::user()->entity_id, 'award_act_status' => true, 'award_del_status' => false, 'searchValue' => '%'.$searchValue.'%', 'rowperpage' => $rowperpage, 'start' => $start
                ));
                
                $totalRecords = DB::select( DB::raw("select count(*) as allcount from transaction_mat_view where (entity_id = :entity_id and award_act_status = :award_act_status and award_del_status = :award_del_status) and (trans_ref like :searchValue or plan_name like :searchValue or cat_name like :searchValue or nom_name like :searchValue or customer_number like :searchValue or nw like :searchValue or payment_med like :searchValue)"), array(
                    'entity_id' => \Auth::user()->entity_id, 'award_act_status' => true, 'award_del_status' => false, 'searchValue' => '%'.$searchValue.'%'
                ));

                $totalRecordswithFilter = DB::select( DB::raw("select count(*) as allcount from transaction_mat_view where (entity_id = :entity_id and award_act_status = :award_act_status and award_del_status = :award_del_status) and (trans_ref like :searchValue or plan_name like :searchValue or cat_name like :searchValue or nom_name like :searchValue or customer_number like :searchValue or nw like :searchValue or payment_med like :searchValue)"), array(
                    'entity_id' => \Auth::user()->entity_id, 'award_act_status' => true, 'award_del_status' => false, 'searchValue' => '%'.$searchValue.'%'
                ));
    
            }else{
    
                $records = DB::select( DB::raw("select * from transaction_mat_view where (award_act_status = :award_act_status and award_del_status = :award_del_status) and (trans_ref like :searchValue or plan_name like :searchValue or cat_name like :searchValue or nom_name like :searchValue or customer_number like :searchValue or nw like :searchValue or payment_med like :searchValue) order by created_at desc limit :rowperpage offset :start"), array(
                        'award_act_status' => true, 'award_del_status' => false, 'searchValue' => '%'.$searchValue.'%', 'rowperpage' => $rowperpage, 'start' => $start
                ));
                
                $totalRecords = DB::select( DB::raw("select count(*) as allcount from transaction_mat_view where (award_act_status = :award_act_status and award_del_status = :award_del_status) and (trans_ref like :searchValue or plan_name like :searchValue or cat_name like :searchValue or nom_name like :searchValue or customer_number like :searchValue or nw like :searchValue or payment_med like :searchValue)"), array(
                    'award_act_status' => true, 'award_del_status' => false, 'searchValue' => '%'.$searchValue.'%'
                ));

                $totalRecordswithFilter = DB::select( DB::raw("select count(*) as allcount from transaction_mat_view where (award_act_status = :award_act_status and award_del_status = :award_del_status) and (trans_ref like :searchValue or plan_name like :searchValue or cat_name like :searchValue or nom_name like :searchValue or customer_number like :searchValue or nw like :searchValue or payment_med like :searchValue)"), array(
                    'award_act_status' => true, 'award_del_status' => false, 'searchValue' => '%'.$searchValue.'%'
                ));

            }


            $totalRecords = $totalRecords[0]->allcount;
            $totalRecordswithFilter=$totalRecordswithFilter[0]->allcount;

        }
        else{

            if (\Session::get('role_id') == 3 || \Session::get('role_id') == '3'){

                $matchThese = ['entity_id' => \Auth::user()->entity_id, 'award_act_status' => true, 'award_del_status' => false, ];
                $records = \DB::table('transaction_mat_view')
                    ->where($matchThese)
                    ->select('*')
                    ->skip($start)
                    ->take($rowperpage)
                    ->orderby('created_at','desc');

                $totalRecords = \DB::table('transaction_mat_view')
                    ->where($matchThese)
                    ->select('count(*) as allcount');

                $totalRecordswithFilter = \DB::table('transaction_mat_view')->select('count(*) as allcount')
                    ->where($matchThese);

            }else{

                $matchThese = ['award_act_status' => true, 'award_del_status' => false, ];
                $records = \DB::table('transaction_mat_view')
                    ->where($matchThese)
                    ->select('*')
                    ->skip($start)
                    ->take($rowperpage)
                    ->orderby('created_at','desc');

                $totalRecords = \DB::table('transaction_mat_view')
                    ->where($matchThese)
                    ->select('count(*) as allcount');

                $totalRecordswithFilter = \DB::table('transaction_mat_view')->select('count(*) as allcount')
                    ->where($matchThese);
            }


            $records=$records->get();
            $totalRecords = $totalRecords->count();
            $totalRecordswithFilter=$totalRecordswithFilter->count();
        }


        $data_arr = array();
        $sno = $start+1;
        foreach($records as $key=>$record){
            $sn = $key+1;

            if($record->status == '0'){
                $status="Failed";
            } elseif ($record->status == '1'){
                $status="Passed";
            }else{
                $status="Pending";
            }

            $data_arr[] = array(
                "sn" => $sn,
                "trans_ref" => $record->trans_ref,
                "plan_name" => $record->plan_name,
                // "cat_name" => $record->cat_name,
                "nom_name" => $record->nom_name,
                "customer_number" => $record->customer_number,
                "amount" => $record->amount,
                "nw" => $record->nw,
                "payment_med" => $record->payment_med,
                // "vote_count" => $record->vote_count,
                "status" => $status,
                "created_at" => date('d-F-Y g:i A', strtotime($record->created_at) )
            );
        }

        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordswithFilter,
            "aaData" => $data_arr
        );

        // \Log::info(DB::getQueryLog());

        echo json_encode($response);
        exit;
    }


    public function ticket_transactions_index(){
//        dd(\Session::get('role_id') );
        ini_set('memory_limit', '1048M');
        if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2  || \Session::get('role_id') == 3 ) {  //if(Auth::user()->can("print_monthlyspeaking_reports")) {
            Activity::log([
                'contentId'   => Auth::user()->id,
                'contentType' => 'Tickets Transactions',
                'action'      => 'Viewed',
                'description' => 'Tickets Transactions page has been viewed.',
                'details'     => Auth::user()->name,
            ]);

            if (\Session::get('role_id') == 3){
                $transactions = \DB::table('tickets_transaction_view')
                    ->where('entity_id',\Auth::user()->entity_id)
                    ->where('award_act_status',true)->where('award_del_status',false)
                    ->orderby('created_at','desc')
                    ->get();
            }else{
                $transactions = \DB::table('tickets_transaction_view')
                    ->where('award_act_status',true)->where('award_del_status',false)
                    ->orderby('created_at','desc')
                    ->get();
            }

//            dd($transactions);

            $module_menus=app('App\Http\Controllers\admin\CrudBuilderController')->createMenumodule();
            return view('reports.tickets_transactions')->with(['module_menus'=>$module_menus, 'transactions' => $transactions]);
        }else{
            return redirect('404');
        }
    }


    public function tickets_sold_report_index(){
//        dd(\Session::get('role_id') );
        ini_set('memory_limit', '1048M');
        if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2  || \Session::get('role_id') == 3 ) {  //if(Auth::user()->can("print_monthlyspeaking_reports")) {
            Activity::log([
                'contentId'   => Auth::user()->id,
                'contentType' => 'Tickets Sold Report',
                'action'      => 'Viewed',
                'description' => 'Tickets Sold Report page has been viewed.',
                'details'     => Auth::user()->name,
            ]);

            if (\Session::get('role_id') == 3){
                $tickets_sold = \DB::table('ticket_sold_view')
                    ->where('entity_id',\Auth::user()->entity_id)
                    ->orderby('qr_date_created','desc')
                    ->get();
            }else{
                $tickets_sold = \DB::table('ticket_sold_view')
                    ->orderby('qr_date_created','desc')
                    ->get();
            }

            \Log::info($tickets_sold);

            $module_menus=app('App\Http\Controllers\admin\CrudBuilderController')->createMenumodule();
            return view('reports.tickets_sold_report')->with(['module_menus'=>$module_menus, 'tickets_sold' => $tickets_sold]);
        }else{
            return redirect('404');
        }
    }

}