<?php


namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\admin\Module;
use App\admin\ModuleAlbum;
use Regulus\ActivityLog\Models\Activity;
use Validator;
use File;
use Illuminate\Pagination\Paginator;
use Zizaco\Entrust\Traits\EntrustTrait;
use Nicolaslopezj\Searchable\SearchableTrait;
use App\Charts\StatisticsChart;


class VoteResultsController extends Controller
{
    public function index(){
        ini_set('max_execution_time', 180); // 180 (seconds) = 3 Minutes
//        dd(\Session::get('role_id') );
        if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2  || \Session::get('role_id') == 3 ) {  //if(Auth::user()->can("print_monthlyspeaking_reports")) {
            Activity::log([
                'contentId'   => Auth::user()->id,
                'contentType' => 'Voting Results',
                'action'      => 'Viewed',
                'description' => 'Voting Results page has been viewed.',
                'details'     => Auth::user()->name,
            ]);

            if (\Session::get('role_id') == 3){
                $entity_masters = \DB::table('entity_master')->where('active_status',true)->where('del_status',false)->get();

                $awards = \DB::table('entity_division')
                    ->leftJoin('entity_master', 'entity_master.entity_id', '=', 'entity_division.entity_id')
                    ->select('entity_master.entity_name','entity_division.id','entity_division.div_name as plan_name','entity_division.assigned_code as plan_id')
                    ->where('entity_division.active_status',true)->where('entity_division.del_status',false)
                    ->where('entity_division.entity_id',\Auth::user()->entity_id)
                    ->orderby('entity_division.id', 'entity_division.asc')->get();

                $transactions = \DB::table('transaction_mat_view')->where('entity_id',\Auth::user()->entity_id)->orderby('payment_info_id','desc')->get();
            }else {
                $entity_masters = \DB::table('entity_master')->where('active_status', true)->where('del_status', false)->get();
                $awards = \DB::table('entity_division')
                    ->leftJoin('entity_master', 'entity_master.entity_id', '=', 'entity_division.entity_id')
                    ->select('entity_master.entity_name', 'entity_division.id', 'entity_division.div_name as plan_name', 'entity_division.assigned_code as plan_id')
                    ->where('entity_division.active_status', true)->where('entity_division.del_status', false)
                    ->orderby('entity_division.id', 'entity_division.asc')->get();

                $transactions = \DB::table('transaction_mat_view')->orderby('payment_info_id', 'desc')->get();
            }

//            dd(ini_get('max_execution_time') );

            $module_menus=app('App\Http\Controllers\admin\CrudBuilderController')->createMenumodule();
            return view('reports.voteresults')->with(['module_menus'=>$module_menus, 'transactions' => $transactions, 'entity_masters'=>$entity_masters, 'awards'=>$awards]);
        }else{
            return redirect('404');
        }
    }




    public function store(Request $request){

//        ini_set('max_execution_time', 180); // 180 (seconds) = 3 Minutes

        $rules = array(
            'entity_div_code' => ['required',"regex:/^[\p{L}\p{P}\p{Zs}\p{N}]+$/"],
        );

        if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2  || \Session::get('role_id') == 3 ) {  //if(Auth::user()->can("print_monthlyspeaking_reports")) {

            if($rules){
                $validator = Validator::make($request->all(), $rules);
                if ($validator->fails()) {
                    $backPricing = Pricing::paginate(10);
                    return view("reports.voteresults")->withErrors($validator)->with(['Pricing'=>$backPricing,'tab'=>2,'editPricing'=>$request->all()]);
                }
            }

            \Log::info($request);

            Activity::log([
                'contentId'   => Auth::user()->id,
                'contentType' => 'Voting Results',
                'action'      => 'Viewed',
                'description' => 'Voting Results has been searched.',
                'details'     => Auth::user()->name,
            ]);

            \Session::put('result_entity_div_code', $request->entity_div_code);

            $results = \DB::table('voting_results')->select('voting_results.plan_name','voting_results.cat_name','voting_results.nom_name', 'voting_results.nom_code', 'voting_results.nom_id', 'voting_results.vote_count')->where('plan_id',$request->entity_div_code)->get();
            $positions = \DB::table('voting_results')->select(DB::raw('DISTINCT(cat_name)'))->where('plan_id',$request->entity_div_code)->get();


            if (\Session::get('role_id') == 3){
                $entity_masters = \DB::table('entity_master')->where('active_status',true)->where('del_status',false)->where('entity_id',\Auth::user()->entity_id)->get();
                $awards_rec = \DB::table('entity_division')
                    ->select('entity_division.div_name','entity_division.assigned_code')
                    ->where('entity_division.assigned_code',$request->entity_div_code)
                    ->orderby('entity_division.assigned_code', 'entity_division.asc')->first();

                $award_name = $awards_rec->div_name;
                $plan_id = $awards_rec->assigned_code;

                $awards = \DB::table('entity_division')
                    ->leftJoin('entity_master', 'entity_master.entity_id', '=', 'entity_division.entity_id')
                    ->select('entity_master.entity_name','entity_division.id','entity_division.div_name as plan_name','entity_division.assigned_code as plan_id')
                    ->where('entity_division.active_status',true)->where('entity_division.del_status',false)
                    ->where('entity_division.entity_id',\Auth::user()->entity_id)
                    ->orderby('entity_division.id', 'entity_division.asc')->get();

                $transactions = \DB::table('transaction_mat_view')->where('entity_id',\Auth::user()->entity_id)->orderby('payment_info_id','desc')->get();

            }else{
                $entity_masters = \DB::table('entity_master')->where('active_status',true)->where('del_status',false)->get();
                $awards_rec = \DB::table('entity_division')
                    ->select('entity_division.div_name','entity_division.assigned_code')
                    ->where('entity_division.assigned_code',$request->entity_div_code)
                    ->orderby('entity_division.assigned_code', 'entity_division.asc')->first();


                $award_name = $awards_rec->div_name;
                $plan_id = $awards_rec->assigned_code;

                $awards = \DB::table('entity_division')
                    ->leftJoin('entity_master', 'entity_master.entity_id', '=', 'entity_division.entity_id')
                    ->select('entity_master.entity_name','entity_division.id','entity_division.div_name as plan_name','entity_division.assigned_code as plan_id')
                    ->where('entity_division.active_status',true)->where('entity_division.del_status',false)
                    ->orderby('entity_division.id', 'entity_division.asc')->get();

                $transactions = \DB::table('transaction_mat_view')->orderby('payment_info_id','desc')->get();
            }





            $result_view = '';
            $result_view .= '<table id="example3" class="table table-striped table-bordered width-100 cellspace-0">';
            $result_view .=  '<tbody>';

            if ($positions){

                foreach ($positions as  $key=>$positions) {
                    $result_view .=  '<tr><td colspan="5" style="text-align:center">CATEGORY : <strong>'.strtoupper($positions->cat_name).'</strong></td></tr>';
                    $result_view .=  '<tr>';
                    $result_view .=  '<td>CANDIDATE NAME</td>';
                    $result_view .=  '<td>VOTES</td>';
                    $result_view .=  '</tr>';

                    $cat_name =  $positions->cat_name;

                    $filter_result = array_filter($results, function ($result) use ($cat_name) {
                        return $result->cat_name === $cat_name;
                    });

                    $total_vote_per_category = 0;

                    foreach ($filter_result as $filter_result) {
                        if ($filter_result->vote_count) {
                            $vote_count = intval($filter_result->vote_count);
                        }else{
                            $vote_count = 0;
                        }
                        $result_view .=  '<tr><td><strong>'.strtoupper($filter_result->nom_name).'</strong> - '.strtoupper($filter_result->nom_code).'</td><td><strong>'.htmlentities($vote_count).'</strong></td></tr>';

                        $total_vote_per_category = $total_vote_per_category + $vote_count;

                    }

                    $result_view .= '<tr><td colspan="1"></td><td><strong>TOTAL =  '.intval($total_vote_per_category).'</strong></td></tr>';
                    $result_view .= '<tr><td colspan="3"></td></tr>';

                }
            }

            $result_view .=  '</tbody>';
            $result_view .=  '</table>';

            \Log::info($awards_rec->assigned_code);

//            dd(ini_get('max_execution_time') );

            
            $module_menus=app('App\Http\Controllers\admin\CrudBuilderController')->createMenumodule();
            return view('reports.voteresults')->with(['module_menus'=>$module_menus, 'transactions' => $transactions, 'entity_master'=>$entity_masters, 'results'=>$results, 'result_view'=>$result_view, 'award_name'=>$award_name, 'awards'=>$awards, 'plan_id'=>$plan_id]);
        }else{
            return redirect('404');
        }
    }

}