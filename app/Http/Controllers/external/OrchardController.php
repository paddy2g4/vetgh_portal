<?php namespace App\Http\Controllers\External;

use App\Http\Controllers\Controller;
use App\User;
use App\Libary\SiteHelpers;
use Socialize;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect, DB ;

class OrchardController extends Controller {


    protected $orchard_url = "https://orchard-api.anmgw.com/check_wallet_balance";
    protected $sms_url;
    protected $service_id;
    protected $client_token;
    protected $secret_token;

    protected $layout = "layouts.main";

    public function __construct() {
//        parent::__construct();
//        $this->data = array();

        $this->service_id = "";
        $this->client_token = "";
        $this->secret_token = "";

    }


    public function checkSmsBalance() {

        $timezone  = 0; //(GMT -5:00) EST (U.S. & Canada)
        $ts = gmdate("Y-m-d H:i:s", time() + 3600*($timezone+date("I")));
        $data = array("service_id" => $this->service_id,"trans_type"=> "BLC", "ts" => $ts);

        $result = $this->orchardRequest($data);

        return $result;
    }


    public function orchardRequest($data) {

        $data_string = json_encode($data);
        $signature =  hash_hmac ( 'sha256' , $data_string , $this->secret_token );
        $auth = $this->client_token.':'.$signature;

        $ch = curl_init($this->orchard_url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Authorization: '.$auth,
                'Content-Type: application/json',
                'timeout: 180',
                'open_timeout: 180'
            )
        );

        $result = curl_exec($ch);
        $character = json_decode($result);

        \Log::info('Orchard payment result = '.$character->resp_code. ' and desc = '.$character->resp_desc);
        \Log::info($character);

        return $character;

    }






    public function redirectPage($page) {
        header("Location: ".$page);
        exit();
    }


    public function generateRandomString($length = 8) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        $timezone  = 0;
        $time_g = gmdate("His", time() + 3600*($timezone+date("I")));

        return "Castvotegh_".$randomString.$time_g;
    }



    public function payment_callback(Request $request) {

        //get Orchards's callback
        // dd($request);
        $callbackcount = \DB::table('callback_statuses')->where('trans_ref',$request->trans_ref)->count();
        if ($callbackcount == 0){
            $resp_code = substr($request->trans_status,0, 3);

            \Log::info("payment_callback Request = ". $request);
            \Log::info("payment_callback Request = ". $request->message);
            \Slack::send("Voting: Orchard payment_callback = ".$request->message.".\nDate & Time: ".date('d M, Y H:i:s'));


            $callback_statuses = \DB::table('callback_statuses')->insertGetId(array('resp_code'=>$resp_code,'resp_desc'=>$request->message,'trans_ref' => $request->trans_ref,
                'nw_trans_id' => $request->trans_id,'status' => $request->trans_status
            ));

            if ($resp_code == "000"){
                $trans_request_update = \DB::table('trans_requests')->where('trans_ref', '=', $request->trans_ref)->update(array('status' => 1));
                if ($trans_request_update){
                    $trans_requests = \DB::table('trans_requests')->where('trans_ref', '=', $request->trans_ref)->first();

                    $vote_master = \DB::table('vote_masters')->insertGetId(array('election_id'=>$trans_requests->election_id,'nominee_id'=>$trans_requests->nominee_id,'voter_id'=>$trans_requests->voter_id,
                        'vote_count'=>$trans_requests->vote_count,'trans_ref'=>$trans_requests->trans_ref,'act_status' => 1
                    ));

                    //Initiate SMS
                    $nominee = \DB::table('nominees_masters')->select('name')->where('nominee_id',$trans_requests->nominee_id)->first();

                    \Slack::send("Voting successful. *Voter ".$trans_requests->voter_id.".* \nDate & Time: ".date('d M, Y H:i:s'));

                    app('App\Http\Controllers\External\SMSController')->sendSMS($nominee->name, $trans_requests->amount,$request->trans_ref,$trans_requests->customer_number,$trans_requests->voter_id,$trans_requests->election_id);

                }
            }else{
                \DB::table('trans_requests')->where('trans_ref', '=', $request->trans_ref)->update(array('status' => 0));
            }

        }
        //check if successfull or failure.
        //if successful, insert into callback_statuses table
        //update trans_request
        //insert into voting table
        //send SMS to voter
    }


    public function send_SMS_Trial(Request $request){

        $trans_requests = \DB::table('trans_requests')->where('trans_ref', '=', $request->trans_ref)->first();

        $nominee = \DB::table('nominees_masters')->select('name')->where('nominee_id',$trans_requests->nominee_id)->first();
        app('App\Http\Controllers\External\SMSController')->sendSMS($nominee->name, $trans_requests->amount,$request->trans_ref,$trans_requests->customer_number,$trans_requests->voter_id,$trans_requests->election_id);

    }

    public function resend_SMS(Request $request){
        // dd($request);
        if ($request->has('sms_id')){

            $sms =\DB::table('sms_outboxes')->select('message','recipient_number')->where('id',$request->sms_id)->first();

            // dd($sms);

            \Slack::send("resend_SMS. \nDate & Time: ".date('d M, Y H:i:s'));

            app('App\Http\Controllers\External\SMSController')->resendSMS($sms->message,$sms->recipient_number);

        }else{
            \Slack::send("resend_SMS failed.SMS ID empty. \nDate & Time: ".date('d M, Y H:i:s'));
            return json_decode("SMS ID Cannot be empty");
        }
    }

    public function NotifyAdminError(){

    }

    public function formatPhoneNumber($customer_number){
        $new_customer_number = "";
        if (substr($customer_number,0,1) == "0"){

            $customer_number = strtolower($customer_number);
            $new_customer_number =  "233".substr($customer_number,1, 9);
        }else{
            $new_customer_number = $customer_number;
        }
        return $new_customer_number;
    }

}
