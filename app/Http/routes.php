<?php

Route::controller('auth','auth\AuthController');
/**
 * register & reset password & auth
 */
Route::auth();


Route::get('/','auth\AuthController@getLogin');
Route::get('/news','front_end\FrontEndController@newsIndex');
Route::get('/shop','front_end\FrontEndController@shopIndex');
Route::get('/contact','front_end\FrontEndController@contactIndex');

//Route::group(['middleware' => ['web']], function () {
    Route::group(['prefix' => 'admin'], function () {

        /**
         * Activity Log
         */
        Route::resource('activitylogs','admin\ActivityLogController');
        Route::post('activitylogs/export/excel','admin\ActivityLogController@export_excel');
        Route::post('activitylogs/search','admin\ActivityLogController@search');
        
        /**
         * image route
         */

        Route::post('/uploadimage','admin\UploadController@uploadimage');
        Route::post('{id}/uploadimage','admin\UploadController@deleteimage');
        Route::post('{id}/deletemultiimage','admin\UploadController@deleteMultiUpload');


        /**
         *
         * language route
         */
        Route::get('language','admin\LangController@change');


        /**
        *  CRUD builder
        */
        Route::post('modules/new','admin\CrudBuilderController@generateModule');
        Route::post('modules/updatesecondlevel','admin\CrudBuilderController@updateSecondLevel');
        Route::post('modules/disable','admin\CrudBuilderController@disableModule');
        Route::post('modules/modulerollback','admin\CrudBuilderController@migrateRollback');

        Route::get('modulesbuilder','admin\CrudBuilderController@view');
        Route::resource('modules','admin\CrudBuilderController');
        /**
         * relation builder
         */
        Route::post('relation/modulerollback','admin\CrudBuilderController@migrateRollback');
        Route::get('relation/showcolumn/{tablename}','admin\RelationBuilderController@showColumn');
        Route::get('relation/getrelation','admin\RelationBuilderController@getRelation');
        Route::resource('relation','admin\RelationBuilderController');

        /**
       * import excel csv Route
       */
        Route::post('news/import_excel_csv','admin\ImportController@importCSVEXCEl');
        Route::post('news/import_excel_csv_database','admin\ImportController@importCSVEXCElDatabase');
        Route::post('news/{id}/delete_excel_csv','admin\ImportController@deleteCSVEXCEl');

    /**
     * upload_excel_csv
     * Upload Route
     */
    Route::post('users/uploadimage','admin\UploadController@uploadimage');
    Route::post('users/{id}/uploadimage','admin\UploadController@deleteUserUpload');
    /*
    |--------------------------------------------------------------------------
    | Routes File
    |--------------------------------------------------------------------------
    |
    | Here is where you will register all of the routes in an application.
    | It's a breeze. Simply tell Laravel the URIs it should respond to
    | and give it the controller to call when that URI is requested.
    |
    */
    /**
     * Upload Route
     */
    Route::post('news/uploadimage','admin\UploadController@uploadimage');
    Route::post('news/{id}/uploadimage','admin\UploadController@deleteNewsUpload');
    Route::post('news/{id}/newsalbum','admin\UploadController@deleteNewsMultipleImageGallery');

    /**
    |--------------------------------------------------------------------------
    | Login  Routes
    |--------------------------------------------------------------------------
    |*/

    Route::get('dashboard','admin\DashboardController@index');

    Route::get('blastSMS','admin\SMSController@sms_blast_index');

    Route::post('blastSMS/sms_blast', 'admin\SMSController@sms_blast');

    Route::post('statistics/filter_events', 'admin\StatisticsController@filter_events_statistics');

    Route::get('cloudinary_upload', 'admin\ImageUploadController@home');

    Route::post('/upload/images', [
        'uses'   =>  'admin\ImageUploadController@uploadImages',
        'as'     =>  'uploadImage'
    ]);
    


    /**
    |--------------------------------------------------------------------------
    | User  Routes
    |--------------------------------------------------------------------------
    |*/
    Route::resource('users','admin\UserController');
    Route::post('users/search','admin\UserController@search');
    Route::post('users/export/excel','admin\UserController@export_excel');

    /**
    |--------------------------------------------------------------------------
    | Role  Routes
    |--------------------------------------------------------------------------
    |*/
    Route::resource('roles','admin\RoleController');
    Route::post('roles/search','admin\RoleController@search');
    Route::post('roles/export/excel','admin\RoleController@export_excel');
    /**
    |--------------------------------------------------------------------------
    | Permission  Routes
    |--------------------------------------------------------------------------
    |*/
    Route::resource('permissions','admin\PermissionController');
    Route::post('permissions/search','admin\PermissionController@search');
    Route::post('permissions/export/excel','admin\PermissionController@export_excel');

    /**
    |--------------------------------------------------------------------------
    | Reports  Routes
    |--------------------------------------------------------------------------
    |*/

    //    Route::get('reports','admin\ReportsmenuController@index');
    Route::resource('transactions','admin\TransactionsController');
    Route::resource('votingresults','admin\VoteResultsController');
    Route::resource('statistics','admin\StatisticsController');
    Route::get('transactions/getdata','admin\TransactionsController@getData')->name('transactions.getdata');

    Route::get('ticket_types','admin\TicketSetupController@ticket_types_index');
    Route::post('ticket_types/create','admin\TicketSetupController@ticket_types_create');
    Route::post('ticket_types/get_ticket_types_details','admin\TicketSetupController@ticket_types_get_details');
    Route::post('ticket_types/edit/{id}','admin\TicketSetupController@ticket_types_edit');
    Route::post('ticket_types/ticket_types_update','admin\TicketSetupController@ticket_types_update');
    Route::get('ticket_classifications','admin\TicketSetupController@ticket_classifications_index');
    Route::get('ticket_classifications/get_ticket_types','admin\TicketSetupController@get_ticket_types');
    Route::post('ticket_classifications/edit','admin\TicketSetupController@ticket_classifications_edit');
    Route::post('ticket_classifications/create','admin\TicketSetupController@ticket_classifications_create');
    Route::post('ticket_classifications/get_details','admin\TicketSetupController@ticket_classifications_get_details');
    Route::post('ticket_classifications/update','admin\TicketSetupController@ticket_classifications_update');
    Route::get('tickets_transactions','admin\TransactionsController@ticket_transactions_index');
    Route::get('statistics','admin\StatisticsController@index');
    Route::post('statistics/filter_events', 'admin\StatisticsController@filter_events_statistics');


    Route::get('tickets_statistics','admin\StatisticsController@ticketing_index');
    Route::post('tickets_statistics/filter_events', 'admin\StatisticsController@filter_ticket_events_statistics');

    Route::get('tickets_sold_report','admin\TransactionsController@tickets_sold_report_index');

    Route::get('nominees_reports','admin\ReportsController@nominee_code_reports');

    Route::get('events','admin\EventSetupController@events_index');
    Route::get('events/getdata','admin\EventSetupController@events_getData')->name('events.events_getData');
    Route::post('events/getEventDetails','admin\EventSetupController@getEventData')->name('events.getEventDetails');
    Route::post('events/create','admin\EventSetupController@create_event');
    Route::post('events/req_edit_status','admin\EventSetupController@edit_status');
    Route::post('events/req_set_event_date','admin\EventSetupController@set_event_date');
    Route::post('events/req_results_status','admin\EventSetupController@set_result_status');
    Route::post('events/enddate_edit','admin\EventSetupController@edit_event_enddate');

    
    });
 