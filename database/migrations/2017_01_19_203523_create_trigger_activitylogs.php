<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTriggerActivitylogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          DB::unprepared('CREATE TRIGGER activitylog_to_activitylogs AFTER INSERT ON `activity_log` FOR EACH ROW
                BEGIN
                   INSERT INTO `activity_logs` (`user_id`, `content_type`, `content_id`, `action`, `description`, `details`, `developer`, `ip_address`, `user_agent`, `created_at`, `updated_at`) VALUES (NEW.user_id, NEW.content_type, NEW.content_id, NEW.action, NEW.description, NEW.details, NEW.developer, NEW.ip_address, NEW.user_agent, NEW.created_at, NEW.updated_at);
                END');
    }
    public function down()
    {
       DB::unprepared('DROP TRIGGER `activitylog_to_activitylogs`');
    }
}
