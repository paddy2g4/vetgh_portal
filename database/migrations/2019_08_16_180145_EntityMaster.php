<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class EntityMaster extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    #####start_up_function#####
        Schema::create('entity_masters', function (Blueprint $table) {
            $table->increments('id');
            $table->string('entity_id', 255);
            $table->string('entity_name', 255);
            $table->boolean('act_status');
            $table->boolean('del_status');
            $table->timestamps();
        });
        DB::table("modules")->insert(
            array("name" =>"EntityMaster","link_name" => "entity_masters","status"=>1,"created_at"=>"2019-08-16 18:01:45")
        );
		        /**
         * role permission
         */
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'view_EntityMaster','display_name' => 'view_EntityMaster')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'add_EntityMaster','display_name' => 'add_EntityMaster')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'edit_EntityMaster','display_name' => 'edit_EntityMaster')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'delete_EntityMaster','display_name' => 'delete_EntityMaster')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
     #####end_up_function#####
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
     #####start_down_function#####
        DB::table('permissions')->where('name',  'view_EntityMaster')->delete();
        DB::table('permissions')->where('name',  'add_EntityMaster')->delete();
        DB::table('permissions')->where('name',  'edit_EntityMaster')->delete();
        DB::table('permissions')->where('name',  'delete_EntityMaster')->delete();
        ######remove primary key
        Schema::drop('entity_masters');
     #####end_down_function#####
    }
}
