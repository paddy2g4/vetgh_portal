<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Entitycontacts extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    #####start_up_function#####
        Schema::create('entitycontacts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('entity_id');
            $table->string('phone_number', 200);
            $table->string('email', 200);
            $table->string('other_details', 255);
            $table->string('user_id', 30);
            $table->timestamps();
        });
        DB::table("modules")->insert(
            array("name" =>"Entitycontacts","description" =>"Manage entity contacts","link_name" => "entitycontacts","status"=>1,"created_at"=>"2019-08-16 18:07:59")
        );
		        /**
         * role permission
         */
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'view_Entitycontacts','display_name' => 'view_Entitycontacts')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'add_Entitycontacts','display_name' => 'add_Entitycontacts')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'edit_Entitycontacts','display_name' => 'edit_Entitycontacts')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'delete_Entitycontacts','display_name' => 'delete_Entitycontacts')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
     #####end_up_function#####
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
     #####start_down_function#####
        DB::table('permissions')->where('name',  'view_Entitycontacts')->delete();
        DB::table('permissions')->where('name',  'add_Entitycontacts')->delete();
        DB::table('permissions')->where('name',  'edit_Entitycontacts')->delete();
        DB::table('permissions')->where('name',  'delete_Entitycontacts')->delete();
        ######remove primary key
        Schema::drop('entitycontacts');
     #####end_down_function#####
    }
}
