<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Award extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    #####start_up_function#####
        Schema::create('awards', function (Blueprint $table) {
            $table->increments('id');
            $table->string('entity_id', 255);
            $table->string('plan_name', 255);
            $table->string('plan_desc', 255);
            $table->dateTime('start_date');
            $table->dateTime('end_date');
            $table->boolean('end_status');
            $table->string('pic_path', 30);
            $table->string('pic_name', 30);
            $table->string('user_id', 30);
            $table->timestamps();
        });
        DB::table("modules")->insert(
            array("name" =>"Award","link_name" => "awards","status"=>1,"created_at"=>"2019-08-24 12:47:10")
        );
		        /**
         * role permission
         */
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'view_Award','display_name' => 'view_Award')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'add_Award','display_name' => 'add_Award')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'edit_Award','display_name' => 'edit_Award')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'delete_Award','display_name' => 'delete_Award')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
     #####end_up_function#####
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
     #####start_down_function#####
        DB::table('permissions')->where('name',  'view_Award')->delete();
        DB::table('permissions')->where('name',  'add_Award')->delete();
        DB::table('permissions')->where('name',  'edit_Award')->delete();
        DB::table('permissions')->where('name',  'delete_Award')->delete();
        ######remove primary key
        Schema::drop('awards');
     #####end_down_function#####
    }
}
