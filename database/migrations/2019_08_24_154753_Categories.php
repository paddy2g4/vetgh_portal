<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Categories extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    #####start_up_function#####
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cat_id', 255);
            $table->string('cat_name', 255);
            $table->string('cat_desc', 255);
            $table->integer('plan_id');
            $table->string('avatar', 255);
            $table->string('user_id', 30);
            $table->string('act_status', 30);
            $table->string('del_status', 30);
            $table->timestamps();
        });
        DB::table("modules")->insert(
            array("name" =>"Categories","description" =>"manage all categories for an award","link_name" => "categories","status"=>1,"created_at"=>"2019-08-24 15:47:53")
        );
		        /**
         * role permission
         */
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'view_Categories','display_name' => 'view_Categories')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'add_Categories','display_name' => 'add_Categories')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'edit_Categories','display_name' => 'edit_Categories')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'delete_Categories','display_name' => 'delete_Categories')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
     #####end_up_function#####
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
     #####start_down_function#####
        DB::table('permissions')->where('name',  'view_Categories')->delete();
        DB::table('permissions')->where('name',  'add_Categories')->delete();
        DB::table('permissions')->where('name',  'edit_Categories')->delete();
        DB::table('permissions')->where('name',  'delete_Categories')->delete();
        ######remove primary key
        Schema::drop('categories');
     #####end_down_function#####
    }
}
