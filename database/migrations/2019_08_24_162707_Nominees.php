<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Nominees extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    #####start_up_function#####
        Schema::create('nominees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nom_id', 30);
            $table->string('nom_name', 30);
            $table->string('nom_desc', 30);
            $table->string('avatar', 30);
            $table->string('cat_id', 30);
            $table->string('user_id', 30);
            $table->string('act_status', 30);
            $table->string('del_status', 30);
            $table->timestamps();
        });
        DB::table("modules")->insert(
            array("name" =>"Nominees","description" =>"Manage all Nominees","link_name" => "nominees","status"=>1,"created_at"=>"2019-08-24 16:27:07")
        );
		        /**
         * role permission
         */
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'view_Nominees','display_name' => 'view_Nominees')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'add_Nominees','display_name' => 'add_Nominees')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'edit_Nominees','display_name' => 'edit_Nominees')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'delete_Nominees','display_name' => 'delete_Nominees')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
     #####end_up_function#####
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
     #####start_down_function#####
        DB::table('permissions')->where('name',  'view_Nominees')->delete();
        DB::table('permissions')->where('name',  'add_Nominees')->delete();
        DB::table('permissions')->where('name',  'edit_Nominees')->delete();
        DB::table('permissions')->where('name',  'delete_Nominees')->delete();
        ######remove primary key
        Schema::drop('nominees');
     #####end_down_function#####
    }
}
