<?php

namespace Awards;

use Illuminate\Support\ServiceProvider;

class AwardsServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $this->app->bind('Awards',function($app){
            return new Awards;
        });

        // Define Router file
        require __DIR__ . '/Route/routes.php';

        // Define path for view files
        $this->loadViewsFrom(__DIR__.'/View','AwardsView');

        // Define path for translation
        $this->loadTranslationsFrom(__DIR__.'/Lang','AwardsLang');

        // Define Published migrations files
        $this->publishes([
            __DIR__.'/Migration' => database_path('migrations'),
        ], 'migrations');

        // Define Published Asset files
        $this->publishes([
            __DIR__.'/Asset' => public_path('assets/'),
        ], 'public');

    }


    public function register()
    {
        //
    }
}