<?php

namespace Awards\Controller;


use entity_division\Model\entity_division;
use EntityMaster\Model\EntityMaster;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\admin\Module;
use App\admin\ModuleAlbum;
use App\Http\Controllers\Controller;
use Regulus\ActivityLog\Models\Activity;
use Validator;
use Illuminate\Http\Request;
use File;
use App\Http\Requests;
use Illuminate\Pagination\Paginator;
use Zizaco\Entrust\Traits\EntrustTrait;
use Nicolaslopezj\Searchable\SearchableTrait;
use JD\Cloudder\Facades\Cloudder;
#####use
class AwardsController extends Controller
{
        use SearchableTrait;
    //  use EntrustUserTrait ;
        protected $image_path = "uploads/award_logo";
	
    /**
     * @param Request $request
     * @return string
     * Export Excel & CSV & PDF
     */
    public function export_excel(Request $request){
        #### $request['export_type'] is export mode  "EXCEL or CSV"
        ### Check export PDF permission
        if($request['export_type']=='pdf'&& !Auth::user()->can('export_pdf') )
            return 'You not have this permission';
        ### Check export CSV permission
        if($request['export_type']=='csv'&& !Auth::user()->can('export_csv') )
            return 'You not have this permission';
        ### Check export EXCEL permission
        if($request['export_type']=='xls'&& !Auth::user()->can('export_xls') )
            return 'You not have this permission';
        if ($request['serach_txt']) {
            $Awards = entity_division::search($request['serach_txt'], null, false)->get();
        } else {  ###other
            $Awards = entity_division::all();
        }  if($request['export_type']=='pdf'){ //export PDF
            $html='<h1 style="text-align: center">YEP test pdf </h1>';
            $html .= '<style>
						table, th, td {text-align: center;}
						th, td {padding: 5px;}
						th {color: #43A047;border-color: black;background-color: #C5E1A5}
						</style>
						<table border="2" style="width:100%;">
						<tr>
							<th>Title</th>
							<th>Created date</th>
						</tr>';
            foreach ($Awards as $cat ){

                $html .="<tr>
							<td>$cat->title</td>
							<td>$cat->created_at</td>
						  </tr>";
            }
			$html .= '</table>';
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadHTML($html);
            return $pdf->download('entity_division.pdf');
        }else { // export excel & csv
            Excel::create('entity_division', function ($excel) use ($Awards) {
                $excel->sheet('Sheet 1', function ($sheet) use ($Awards) {
                    $sheet->fromArray($Awards);
                });
            })->download($request['export_type']);
        }
    }
	
 
    /**
     * @param Request $request
     * @return mixed
     * Search and paging
     */
    public function search(Request $request)
    {
        #####relation
        $currentPage = $request['page']; // You can set this to any page you want to paginate to
        Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });
        if ($request['paging'] > 0)
            $limit = $request['paging'];
        else
            $limit = 10;
        ### search
        if ($request['serach_txt']) {
            $Awards = entity_division::search($request['serach_txt'], null, false)->get();
            $page = $request->has('page') ? $request->page - 1 : 0;
            $total = $Awards->count();
            $Awards = $Awards->slice($page * $limit, $limit);
            $Awards = new \Illuminate\Pagination\LengthAwarePaginator($Awards, $total, $limit);
        } else {  ###other
//            $Awards = entity_division::paginate($limit);

            $entity_masters = \DB::table('entity_master')->where('act_status',true)->where('del_status',false)->get();
            $Awards=\DB::table('entity_division')
                ->leftJoin('entity_master', 'entity_master.id', '=', 'entity_division.entity_id')
                ->select('entity_division.id','entity_division.plan_name','entity_division.plan_desc','entity_division.start_date', 'entity_division.end_date', 'entity_division.act_status', 'entity_division.logo', 'entity_master.id as entity_masters_id', 'entity_master.entity_name')
                ->where('entity_division.del_status',false)
                ->where('entity_master.active_status',true)
                ->where('entity_master.del_status',false)
                ->paginate($limit);

        }
        return view("AwardsView::Awardsajax")->with(['request' => $request, 'tab' =>1, 'entity_division' => $Awards,'entity_master' => $entity_masters]);
    }
	
    /**
     * @return mixed
     */
    public function index()
    {
        #####relation
        if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2 ) { // if(Auth::user()->can("view_Awards")) {
            $module_menus=app('App\Http\Controllers\admin\CrudBuilderController')->createMenumodule();
//            $Awards = entity_division::paginate(10);

//            dd(\Auth::user()->entity_id);

            if (\Session::get('role_id') == 3){

                $entity_masters = \DB::table('entity_master')->where('active_status',true)->where('del_status',false)->get();
                $Awards=\DB::table('entity_division')
                    ->leftJoin('entity_master', 'entity_master.entity_id', '=', 'entity_division.entity_id')
                    ->select('entity_division.id','entity_division.entity_id','entity_division.div_name','entity_division.div_alias','entity_division.start_date', 'entity_division.end_date', 'entity_division.active_status','entity_division.image_path as logo', 'entity_division.activity_type_code', 'entity_division.show_client_result', 'entity_master.id as entity_masters_id', 'entity_master.entity_name')
                    ->where('entity_division.del_status',false)
                    ->where('entity_master.active_status',true)
                    ->where('entity_master.del_status',false)
                    ->where('entity_master.entity_id',\Auth::user()->entity_id)
                    ->paginate(10);

            }else{
                $entity_masters = \DB::table('entity_master')->where('active_status',true)->where('del_status',false)->get();
                $Awards=\DB::table('entity_division')
                    ->leftJoin('entity_master', 'entity_master.entity_id', '=', 'entity_division.entity_id')
                    ->select('entity_division.id','entity_division.entity_id','entity_division.div_name','entity_division.div_alias','entity_division.start_date', 'entity_division.end_date', 'entity_division.active_status','entity_division.image_path as logo', 'entity_division.activity_type_code',  'entity_division.show_client_result', 'entity_master.id as entity_masters_id', 'entity_master.entity_name')
                    //->where('entity_division.active_status',true)
                    ->where('entity_division.del_status',false)
                    ->where('entity_master.active_status',true)
                    ->where('entity_master.del_status',false)
                    ->paginate(10);
            }


            return view("AwardsView::Awardsajax")->with(['module_menus'=>$module_menus,'tab' => 1, 'Awards' => $Awards, 'entity_master' => $entity_masters]);

        }else{
            return redirect('404');
        }
    }


    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
        \Log::info("In AwardsController: store");


        if ($request->btnupdate == "update"){
            $result = $this->update($request, $request->id);
            \Log::info("LINE ".__LINE__.". In AwardsController: store");
            return $result;
        }
        \Log::info("LINE ".__LINE__.". In AwardsController: store");

         $module_id=\Request::segment(2);
         $module_id=Module::where('link_name',$module_id)->first();
         $Module_type_id=$module_id->id;
         
         #####relation
        $rules = array(
            'entity_id' => 'required|integer',
            'plan_name'=>['required','min:1','max:200',"regex:/^[\p{L}\p{P}\p{Zs}\p{N}]+$/"],
            'plan_desc'=>['max:200',"regex:/^[\p{L}\p{P}\p{Zs}\p{N}]+$/"],
            'start_date' => 'date|required',
            'end_date' => 'date',
//            'logo' => ['max:255',"regex:/^[\p{L}\p{P}\p{Zs}\p{N}]+$/"],
            'image_name'=>'required|mimes:jpeg,bmp,jpg,png|between:1, 6000',
        );

//        \Log::info($request);

		$Awards=new entity_division;
        if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2) { // if(Auth::user()->can("add_Awards")){  #####check permission
			if($Awards->rules){
				$validator = Validator::make($request->all(), $rules);
				if ($validator->fails()) {
					$backAwards = entity_division::paginate(10);
					return view("AwardsView::Awardsajax")->withErrors($validator)->with(['entity_division'=>$backAwards,'tab'=>2,'editAwards'=>$request->all()]);
				}
            }
            
            $end_date=null;
            $user_id = \Auth::user()->id;

            $award_start_date = date("Y-m-d H:i", strtotime($request->start_date));
            $award_end_date = date("Y-m-d H:i", strtotime($request->end_date));
//            dd($request->start_date,$award_start_date,$request->end_date);

            // dd($request->all());
            $Awards->entity_id = $request->entity_id;
            $Awards->plan_name = $request->plan_name;
            $Awards->plan_desc = $request->plan_desc;
            $Awards->start_date = $award_start_date;

            $Awards->end_date = $award_end_date ? $award_end_date : null;
            $Awards->user_id = $user_id;
            $Awards->save();

            #SAVE IMAGE
            $image_name = $request->file('image_name') ? $request->file('image_name')->getRealPath() : null;

            \Log::info("Line ".__LINE__ .". In AwardsController: update: Award has been updated");


            if(!empty($image_name)){

                Cloudder::upload($image_name, null);
                $tag ="CASTVOTEGH";
                Cloudder::addTag($tag, Cloudder::getPublicId(), null);

                list($width, $height) = getimagesize($image_name);
                $image_url= Cloudder::show(Cloudder::getPublicId(), ["width" => $width, "height"=>$height]);
            }

            $Awards = entity_division::find($Awards->id);
            \Log::info("Line ".__LINE__ .". entity_division Controller: image_url = ".$image_url. " entity_division ID: ".$Awards->id);

            if(!empty($image_url)){
                $Awards->logo = $image_url;
                $Awards->save();
            }


	 	    // $Awards=entity_division::create($request->all());
	 	     Activity::log([
                'contentId'   => $Awards->id,
                'contentType' => 'entity_division',
                'action'      => 'Create',
                'description' => 'Created an Award',
                'details'     => 'UserID: '.Auth::user()->id,
             ]);
			####Upload image

//            if (file_exists('temp/'.$request["logo"]) && $request["logo"] != "")
//            File::move("temp/" . $request["logo"], $this->image_path . $request["logo"]);

	 	    ####Multi upload image
            ######store
//            $Awards = entity_division::paginate(10);
            $entity_masters = \DB::table('entity_master')->where('act_status',true)->where('del_status',false)->get();

            $Awards=\DB::table('entity_division')
                ->leftJoin('entity_master', 'entity_master.id', '=', 'entity_division.entity_id')
                ->select('entity_division.id','entity_division.plan_name','entity_division.plan_desc','entity_division.start_date', 'entity_division.end_date', 'entity_division.act_status','entity_division.logo','entity_division.show_client_result', 'entity_master.id as entity_masters_id', 'entity_master.entity_name')
                ->where('entity_division.del_status',false)
                ->where('entity_master.active_status',true)
                ->where('entity_master.del_status',false)
                ->paginate(10);
//            return view("AwardsView::Awardsajax")->with(['tab'=>1,'flag'=>3,'entity_division'=>$Awards, 'entity_master'=>$entity_masters]);
            return redirect()->back()->with(['status'=>'Record added successful', 'tab'=>1,'flag'=>3,'entity_division'=>$Awards, 'entity_master'=>$entity_masters]);
        }else{
            $Awards = entity_division::paginate(10);
            return view("AwardsView::Awardsajax")->with(['tab'=>1,'flag'=>6,'entity_division'=>$Awards]);
        }

    }
	
    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function edit(Request $request, $id)
    {
         $module_id=\Request::segment(2);
         $module_id=Module::where('link_name',$module_id)->first();
         $Module_type_id=$module_id->id;
        #####relation
        #####edit many to many
        $Awards = entity_division::paginate(10);
        $editAwards = entity_division::find($id);

        $entity_masters  = \DB::table('entity_master')->where('act_status',true)->where('del_status',false)->get();
        $EditEntity_masters = \DB::table('entity_master')->select('id as entity_masters_id')->where('id',$editAwards->entity_id)->where('act_status',true)->where('del_status',false)->first();

        ####Edit multiple upload image
        return view("AwardsView::Awardsajax")->with(['editAwards'=>$editAwards,'tab'=>2,'entity_division'=>$Awards,'entity_master'=>$entity_masters, 'EditEntity_masters'=>$EditEntity_masters]);
    }
    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function update(Request $request, $id)
    {
        \Log::info("Line ".__LINE__ ."In AwardsController: update");
        $module_id=\Request::segment(2);
        $module_id=Module::where('link_name',$module_id)->first();
        $Module_type_id=$module_id->id;

        $award_start_date = date("Y-m-d H:i", strtotime($request->start_date));
        $award_end_date = date("Y-m-d H:i", strtotime($request->end_date));

        \Log::info(date("m/d/Y H:i A", strtotime($request->start_date))." request logo: ". $request->logo);

		#####relation
        $rules = array(
            'entity_id' => 'required|integer',
            'plan_name'=>['required','min:1','max:200',"regex:/^[\p{L}\p{P}\p{Zs}\p{N}]+$/"],
            'plan_desc'=>['max:200',"regex:/^[\p{L}\p{P}\p{Zs}\p{N}]+$/"],
//            'start_date' => 'required|date_format:m/d/Y H:i A', //'date|required',
//            'end_date' => 'date_format:m/d/Y H:i A',
//            'logo' => ['max:255',"regex:/^[\p{L}\p{P}\p{Zs}\p{N}]+$/"],
//            'image_name'=>'mimes:jpeg,bmp,jpg,png|between:1, 6000',
        );

        $entity_masters = \DB::table('entity_master')->where('act_status',true)->where('del_status',false)->get();
        \Log::info("Line ".__LINE__ ." In AwardsController: update: entity masters. $request->start_date, $request->end_date, $award_start_date, $award_end_date ".\Session::get('role_id'));

//        $Awards=new entity_division;
        if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2) { // if(Auth::user()->can('edit_Awards')) {  #####check permission
			if($rules){
                \Log::info("Line ".__LINE__ ." In AwardsController: update: entity masters. \Session::get('role_id') = ".\Session::get('role_id'));
				//$validator = Validator::make($request->all(),$Awards->rules);
                $validator = Validator::make($request->all(), $rules);
                \Log::info("Line ".__LINE__ ." In AwardsController: update: entity masters. \Session::get('role_id') = ".\Session::get('role_id'));
				if ($validator->fails()) {
                    \Log::info("Line ".__LINE__ ." In AwardsController: update: entity masters. \Session::get('role_id') = ".\Session::get('role_id'));
					$backAwards = entity_division::paginate(10);
					return view("AwardsView::Awardsajax")->withErrors($validator)->with(['tab' => 2,'entity_division'=>$backAwards, 'editAwards' => $request->all()]);
				}
			}

            \Log::info("Line ".__LINE__ ."In AwardsController: update");

            $end_date = $image_url = null;
            $user_id = \Auth::user()->id;

            $Awards = entity_division::find($request['id']);
//            $Awards->update($request->all());

            \Log::info("Line ".__LINE__ ."In AwardsController: update");

            $Awards->entity_id = $request->entity_id;
            $Awards->plan_name = $request->plan_name;
            $Awards->plan_desc = $request->plan_desc;
            $Awards->start_date = $award_start_date;

            \Log::info("Line ".__LINE__ ."In AwardsController: update");


            if(!empty($award_end_date)){
                $end_date = $award_end_date;
            }
            $Awards->end_date = $end_date;

            #SAVE IMAGE
//            $image_name = $request->file('image_name')->getRealPath();

            $image_name = $request->file('image_name') ? $request->file('image_name')->getRealPath() : null;
            $Awards->user_id = $user_id;
            $Awards->save();

            \Log::info("Line ".__LINE__ .". In AwardsController: update: Award has been updated");


            if($image_name){

                \Log::info("Line ".__LINE__ .". In AwardsController: update");

                Cloudder::upload($image_name, null);
                $tag ="CASTVOTEGH";
                Cloudder::addTag($tag, Cloudder::getPublicId(), null);

                list($width, $height) = getimagesize($image_name);
                $image_url= Cloudder::show(Cloudder::getPublicId(), ["width" => $width, "height"=>$height]);

                \Log::info("Line ".__LINE__ .". In AwardsController: update");


                if($image_url){
                    $Awards = entity_division::find($request['id']);
                    \Log::info("Line ".__LINE__ .". entity_division Controller: image_url = ".$image_url. " entity_division ID: ".$Awards->id);
                    $Awards->logo = $image_url;
                    $Awards->save();
                }

            }

            \Log::info("Line ".__LINE__ .". In AwardsController: update, $Awards->id");

            Activity::log([
                'contentId'   => $Awards->id,
                'contentType' => 'entity_division',
                'description' => 'Update a entity_division',
                'details'     => 'Username: '.Auth::user()->name,
                'updated'     => $Awards->id,
            ]);
			####update Multi upload image
			####Upload image
			######update Many to many
            ######store
//            $backAwards = entity_division::paginate(10);

            \Log::info("Line ".__LINE__ .". In AwardsController: update, $Awards->id");
            $entity_masters = \DB::table('entity_master')->where('act_status',true)->where('del_status',false)->get();

            $backAwards=\DB::table('entity_division')
                ->leftJoin('entity_master', 'entity_master.id', '=', 'entity_division.entity_id')
                ->select('entity_division.id','entity_division.plan_name','entity_division.plan_desc','entity_division.start_date', 'entity_division.end_date', 'entity_division.act_status','entity_division.logo', 'entity_division.show_client_result','entity_master.id as entity_masters_id', 'entity_master.entity_name')
                ->where('entity_division.del_status',false)
                ->where('entity_master.active_status',true)
                ->where('entity_master.del_status',false)
                ->paginate(10);

            \Log::info("Line ".__LINE__ .". In AwardsController: update, $Awards->id");

            $backAwards->setPath('/entity_division');
            \Log::info("Line ".__LINE__ .". In AwardsController: update backAwards");
//            return view("AwardsView::Awardsajax")->with(['tab' => 1, 'entity_division' => $backAwards, 'entity_master' => $entity_masters,'flag' => 4]);
            return redirect()->back()->with(['status'=>'Record updated successful', 'tab'=>1,'flag'=>4,'entity_division'=>$backAwards, 'entity_master'=>$entity_masters]);
        }else{
            $backAwards = entity_division::paginate(10);
            return view("AwardsView::Awardsajax")->with(['tab' => 1, 'entity_division' => $backAwards, 'entity_master' => $entity_masters, 'flag' => 6]);
        }
    }

    public function edit_status(Request $request){

        $rules = array(
            'div_id1'=>'required|Integer',
            "btnstatus" => 'required|alpha'
        );

        $entity_masters = \DB::table('entity_master')->where('act_status',true)->where('del_status',false)->get();

        if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2 ) { // if(Auth::user()->can("add_EntityMaster")){  #####check permission
            if($rules){
                $validator = Validator::make($request->all(), $rules);
                if ($validator->fails()) {
                    $backAwards = entity_division::paginate(10);
                    return view("AwardsView::Awardsajax")->withErrors($validator)->with(['tab' => 2,'entity_division'=>$backAwards, 'editAwards' => $request->all()]);
                }
            }

            $this->user_id = \Auth::user()->id;

            if ($request->btnstatus == "disable"){
                $message = "Record Disabled successfully";
                $action = false;
            }else{
                $message = "Record Enabled successfully";
                $action = true;
            }

            $Awards = entity_division::find($request['div_id1']);

            $Awards->act_status = $action;
//            $EntityMaster->updated_at = $request->entity_name;
            $Awards->user_id = $this->user_id;
            $Awards->save();


            Activity::log([
                'contentId'   => $Awards->id,
                'contentType' => 'entity_division',
                'action'      => 'Update',
                'description' => 'entity_division Status Changed',
                'details'     => 'Username: '.Auth::user()->name,
            ]);

            $entity_masters = \DB::table('entity_master')->where('act_status',true)->where('del_status',false)->get();

            $backAwards=\DB::table('entity_division')
                ->leftJoin('entity_master', 'entity_master.id', '=', 'entity_division.entity_id')
                ->select('entity_division.id','entity_division.plan_name','entity_division.plan_desc','entity_division.start_date', 'entity_division.end_date', 'entity_division.act_status', 'entity_division.logo','entity_division.show_client_result', 'entity_master.id as entity_masters_id', 'entity_master.entity_name')
                ->where('entity_division.del_status',false)
                ->where('entity_master.active_status',true)
                ->where('entity_master.del_status',false)
                ->paginate(10);

            $backAwards->setPath('/entity_division');
            return redirect()->back()->with(['status'=>'Record updated successful', 'tab'=>1,'flag'=>4,'entity_division'=>$backAwards, 'entity_master'=>$entity_masters]);
        }else{
            $backAwards = entity_division::paginate(10);
            return view("AwardsView::Awardsajax")->with(['tab' => 1, 'entity_division' => $backAwards, 'entity_master' => $entity_masters, 'flag' => 6]);
        }

    }


    public function client_result_status(Request $request){

        $rules = array(
            'div_id'=>'required|Integer',
            "btnstatus" => 'required|alpha'
        );

        $entity_masters = \DB::table('entity_master')->where('act_status',true)->where('del_status',false)->get();

        if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2 ) { // if(Auth::user()->can("add_EntityMaster")){  #####check permission
            if($rules){
                $validator = Validator::make($request->all(), $rules);
                if ($validator->fails()) {
                    $backAwards = entity_division::paginate(10);
                    return view("AwardsView::Awardsajax")->withErrors($validator)->with(['tab' => 2,'entity_division'=>$backAwards, 'editAwards' => $request->all()]);
                }
            }

            $this->user_id = \Auth::user()->id;

            $Awards = entity_division::find($request['div_id']);

            if ($request->btnstatus == "hide"){
                $message = "$Awards->plan_name results hidden.";
                $action = false;
            }else{
                $message = "$Awards->plan_name results Showing";
                $action = true;
            }

            $Awards->show_client_result = $action;
            $Awards->user_id = $this->user_id;
            $Awards->save();


            Activity::log([
                'contentId'   => $Awards->id,
                'contentType' => 'entity_division',
                'action'      => 'Update',
                'description' => 'entity_division Status Changed',
                'details'     => 'Username: '.Auth::user()->name,
            ]);

            $entity_masters = \DB::table('entity_master')->where('act_status',true)->where('del_status',false)->get();

            $backAwards=\DB::table('entity_division')
                ->leftJoin('entity_master', 'entity_master.id', '=', 'entity_division.entity_id')
                ->select('entity_division.id','entity_division.plan_name','entity_division.plan_desc','entity_division.start_date', 'entity_division.end_date', 'entity_division.act_status', 'entity_division.logo','entity_division.show_client_result', 'entity_master.id as entity_masters_id', 'entity_master.entity_name')
                ->where('entity_division.del_status',false)
                ->where('entity_master.active_status',true)
                ->where('entity_master.del_status',false)
                ->paginate(10);

            $backAwards->setPath('/entity_division');
            return redirect()->back()->with(['status'=>$message, 'tab'=>1,'flag'=>4,'entity_division'=>$backAwards, 'entity_master'=>$entity_masters]);
        }else{
            $backAwards = entity_division::paginate(10);
            return view("AwardsView::Awardsajax")->with(['tab' => 1, 'entity_division' => $backAwards, 'entity_master' => $entity_masters, 'flag' => 6]);
        }

    }


    public function delete_record(Request $request){

        $rules = array(
            'award_id'=>'required|Integer',
            "btnstatus" => 'required|alpha'
        );

        $entity_masters = \DB::table('entity_master')->where('act_status',true)->where('del_status',false)->get();

        if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2 ) { // if(Auth::user()->can("add_EntityMaster")){  #####check permission
            if($rules){
                $validator = Validator::make($request->all(), $rules);
                if ($validator->fails()) {
                    $backAwards = entity_division::paginate(10);
                    return view("AwardsView::Awardsajax")->withErrors($validator)->with(['tab' => 2,'entity_division'=>$backAwards, 'editAwards' => $request->all()]);
                }
            }

            $this->user_id = \Auth::user()->id;

            if ($request->btnstatus == "disable"){
                $message = "Record Deleted successfully";
                $action = false;
            }

            $Awards = entity_division::find($request['award_id']);
//            dd($Awards);

            $Awards->del_status = true;
            $Awards->user_id = $this->user_id;
            $Awards->save();




            Activity::log([
                'contentId'   => $Awards->id,
                'contentType' => 'entity_division',
                'action'      => 'Update',
                'description' => 'Award Deleted',
                'details'     => 'Username: '.Auth::user()->name,
            ]);

            $entity_masters = \DB::table('entity_master')->where('act_status',true)->where('del_status',false)->get();

            $backAwards=\DB::table('entity_division')
                ->leftJoin('entity_master', 'entity_master.id', '=', 'entity_division.entity_id')
                ->select('entity_division.id','entity_division.plan_name','entity_division.plan_desc','entity_division.start_date', 'entity_division.end_date', 'entity_division.act_status', 'entity_division.logo', 'entity_division.show_client_result','entity_master.id as entity_masters_id', 'entity_master.entity_name')
                ->where('entity_division.del_status',false)
                ->where('entity_master.active_status',true)
                ->where('entity_master.del_status',false)
                ->paginate(10);

            $backAwards->setPath('/entity_division');
            return redirect()->back()->with(['status'=>'Record deleted successful', 'tab'=>1,'flag'=>4,'entity_division'=>$backAwards, 'entity_master'=>$entity_masters]);
        }else{
            $backAwards = entity_division::paginate(10);
            return view("AwardsView::Awardsajax")->with(['tab' => 1, 'entity_division' => $backAwards, 'entity_master' => $entity_masters, 'flag' => 6]);
        }

    }



    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $module_id=\Request::segment(2);
        $module_id=Module::where('link_name',$module_id)->first();
        $Module_type_id=$module_id->id;
         #####relation
         if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2 || \Session::get('role_id') == 3 ) { // if(Auth::user()->can('delete_Awards')) {  #####check permission
            $temp = explode(",", $id);
            foreach ($temp as $val) {
                if ($temp) {
                   ### delete multi uploader
                    $user = entity_division::find($val);
                    $user->delete();
                     Activity::log([
                            'contentId'   => $val,
                            'contentType' => 'entity_division',
                            'action'      => 'Delete',
                            'description' => 'Delete  a entity_division',
                            'details'     => 'Username: '.Auth::user()->name,
                     ]);
                }
            }
            $Awards = entity_division::paginate(10);
            $Awards->setPath('/entity_division');
            return view("AwardsView::Awardsajax")->with(['tab' => 1, 'entity_division' => $Awards, 'flag' => 5]);
        }else{
            $Awards = entity_division::paginate(10);
            return view("AwardsView::Awardsajax")->with(['tab' => 1, 'entity_division' => $Awards, 'flag' => 6]);
        }
    }	
}
