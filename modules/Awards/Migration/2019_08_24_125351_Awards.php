<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Awards extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    #####start_up_function#####
        Schema::create('awards', function (Blueprint $table) {
            $table->increments('id');
            $table->string('entity_id', 30);
            $table->string('plan_name', 255);
            $table->string('plan_desc', 255);
            $table->dateTime('start_date');
            $table->dateTime('end_date');
            $table->string('logo', 255);
            $table->integer('user_id');
            $table->boolean('act_status');
            $table->boolean('del_status');
            $table->timestamps();
        });
        DB::table("modules")->insert(
            array("name" =>"Awards","description" =>"Manage All Awards","link_name" => "awards","status"=>1,"created_at"=>"2019-08-24 12:53:51")
        );
		        /**
         * role permission
         */
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'view_Awards','display_name' => 'view_Awards')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'add_Awards','display_name' => 'add_Awards')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'edit_Awards','display_name' => 'edit_Awards')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'delete_Awards','display_name' => 'delete_Awards')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
     #####end_up_function#####
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
     #####start_down_function#####
        DB::table('permissions')->where('name',  'view_Awards')->delete();
        DB::table('permissions')->where('name',  'add_Awards')->delete();
        DB::table('permissions')->where('name',  'edit_Awards')->delete();
        DB::table('permissions')->where('name',  'delete_Awards')->delete();
        ######remove primary key
        Schema::drop('awards');
     #####end_down_function#####
    }
}
