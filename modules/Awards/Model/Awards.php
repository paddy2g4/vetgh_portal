<?php

namespace Awards\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Database\Eloquent\Model;
#####use
 
class Awards extends Model
{
  use SearchableTrait;
    public $table = 'awards';
    
	
	 public $searchable = [
	    'columns' => [
            'awards.entity_id'=>100,
    'awards.plan_name'=>101,
    'awards.plan_desc'=>102,
    'awards.start_date'=>103,
    'awards.end_date'=>104,
    'awards.logo'=>105,
    'awards.user_id'=>106,
    'awards.act_status'=>107,
    'awards.del_status'=>108,
        ########relation_id
        ],
        ########join
	  ];
 

    public $fillable = [
        'entity_id',
        'plan_name',
        'plan_desc',
        'start_date',
        'end_date',
        'logo',
        'user_id',
        'act_status',
        'del_status'
    ];

 

    /**
     * Validation rules
     *
     * @var array
     */
    public   $rules = [
        'entity_id' => 'integer',
        'plan_name' => 'string|required',
        'start_date' => 'date|required',
        'end_date' => 'date',
        'logo' => 'image',
        'user_id' => 'integer'
    ];
 #####relation#####
}
