<?php
Route::group(['prefix' => 'admin'], function () {
####start
   /*
    |--------------------------------------------------------------------------
    | Awards  Routes
    |--------------------------------------------------------------------------
    |*/

    Route::resource('awards','Awards\Controller\AwardsController');
    Route::post('awards/search','Awards\Controller\AwardsController@search');
	Route::post('awards/export/excel','Awards\Controller\AwardsController@export_excel');

    Route::post('awards/action_status','Awards\Controller\AwardsController@edit_status');
    Route::post('awards/client_result_status','Awards\Controller\AwardsController@client_result_status');
    Route::post('awards/delete','Awards\Controller\AwardsController@delete_record');
####end
#####relation#####
});
 