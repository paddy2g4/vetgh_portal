@if(empty($content->logo))
    <td data-title="logo"><img class="img-circle" src="{{ asset('/uploads/no_avatar.jpg') }}" style="width:50px; height:50px"></td>
@else
    <td data-title="logo"><img class="img-circle" src="{{ asset($content->logo) }}" style="width:60px; height:60px"</td>
@endif

<td data-title="plan_name">{{$content->entity_name}}</td>

<td data-title="plan_name">{{$content->div_name}}</td>

<td data-title="plan_name">
    @if($content->activity_type_code == 'VOT')
        Awards
    @elseif ($content->activity_type_code == 'SHW')
        Tickets
    @elseif ($content->activity_type_code == 'VOT-SHW')
        Awards and Tickets
    @endif

</td>

<td data-title="start_date">{{$content->start_date}}</td>

<td data-title="end_date">{{$content->end_date}}</td>



