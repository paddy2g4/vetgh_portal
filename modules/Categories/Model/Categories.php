<?php

namespace Categories\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Database\Eloquent\Model;
#####use
 
class Categories extends Model
{
  use SearchableTrait;
    public $table = 'voting_categories';
    
	
	 public $searchable = [
	    'columns' => [
            'categories.cat_id'=>100,
    'categories.cat_name'=>101,
    'categories.cat_desc'=>102,
    'categories.plan_id'=>103,
    'categories.avatar'=>104,
    'categories.user_id'=>105,
    'categories.act_status'=>106,
    'categories.del_status'=>107,
        ########relation_id
        ],
        ########join
	  ];
 

    public $fillable = [
        'cat_id',
        'cat_name',
        'cat_desc',
        'plan_id',
        'avatar',
        'user_id',
        'act_status',
        'del_status'
    ];

 

    /**
     * Validation rules
     *
     * @var array
     */
    public   $rules = [
        'cat_name' => 'required|string',
        'cat_desc' => 'string'
    ];
 #####relation#####
}
