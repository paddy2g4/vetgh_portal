<?php

namespace Classes;

use Illuminate\Support\ServiceProvider;

class ClassesServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $this->app->bind('Classes',function($app){
            return new Classes;
        });

        // Define Router file
        require __DIR__ . '/Route/routes.php';

        // Define path for view files
        $this->loadViewsFrom(__DIR__.'/View','ClassesView');

        // Define path for translation
        $this->loadTranslationsFrom(__DIR__.'/Lang','ClassesLang');

        // Define Published migrations files
        $this->publishes([
            __DIR__.'/Migration' => database_path('migrations'),
        ], 'migrations');

        // Define Published Asset files
        $this->publishes([
            __DIR__.'/Asset' => public_path('assets/'),
        ], 'public');

    }


    public function register()
    {
        //
    }
}