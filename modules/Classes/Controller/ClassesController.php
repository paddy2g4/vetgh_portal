<?php

namespace Classes\Controller;


use Classes\Model\Classes;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\admin\Module;
use App\admin\ModuleAlbum;
use App\Http\Controllers\Controller;
use Regulus\ActivityLog\Models\Activity;
use Validator;
use Illuminate\Http\Request;
use File;
use App\Http\Requests;
use Illuminate\Pagination\Paginator;
use Zizaco\Entrust\Traits\EntrustTrait;
use Nicolaslopezj\Searchable\SearchableTrait;
#####use
class ClassesController extends Controller
{
        use SearchableTrait;
    //  use EntrustUserTrait ;
	
    /**
     * @param Request $request
     * @return string
     * Export Excel & CSV & PDF
     */
    public function export_excel(Request $request){
        #### $request['export_type'] is export mode  "EXCEL or CSV"
        ### Check export PDF permission
        if($request['export_type']=='pdf'&& !Auth::user()->can('export_pdf') )
            return 'You not have this permission';
        ### Check export CSV permission
        if($request['export_type']=='csv'&& !Auth::user()->can('export_csv') )
            return 'You not have this permission';
        ### Check export EXCEL permission
        if($request['export_type']=='xls'&& !Auth::user()->can('export_xls') )
            return 'You not have this permission';
        if ($request['serach_txt']) {
            $Classes = Classes::search($request['serach_txt'], null, false)->get();
        } else {  ###other
            $Classes = Classes::all();
        }  if($request['export_type']=='pdf'){ //export PDF
            $html='<h1 style="text-align: center">YEP test pdf </h1>';
            $html .= '<style>
						table, th, td {text-align: center;}
						th, td {padding: 5px;}
						th {color: #43A047;border-color: black;background-color: #C5E1A5}
						</style>
						<table border="2" style="width:100%;">
						<tr>
							<th>Title</th>
							<th>Created date</th>
						</tr>';
            foreach ($Classes as $cat ){

                $html .="<tr>
							<td>$cat->title</td>
							<td>$cat->created_at</td>
						  </tr>";
            }
			$html .= '</table>';
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadHTML($html);
            return $pdf->download('Classes.pdf');
        }else { // export excel & csv
            Excel::create('Classes', function ($excel) use ($Classes) {
                $excel->sheet('Sheet 1', function ($sheet) use ($Classes) {
                    $sheet->fromArray($Classes);
                });
            })->download($request['export_type']);
        }
    }
	
 
    /**
     * @param Request $request
     * @return mixed
     * Search and paging
     */
    public function search(Request $request)
    {
        #####relation
        $currentPage = $request['page']; // You can set this to any page you want to paginate to
        Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });
        if ($request['paging'] > 0)
            $limit = $request['paging'];
        else
            $limit = 10;
        ### search
        if ($request['serach_txt']) {
            $Classes = Classes::search($request['serach_txt'], null, false)->get();
            $page = $request->has('page') ? $request->page - 1 : 0;
            $total = $Classes->count();
            $Classes = $Classes->slice($page * $limit, $limit);
            $Classes = new \Illuminate\Pagination\LengthAwarePaginator($Classes, $total, $limit);
        } else {  ###other
            $Classes = Classes::paginate($limit);
        }
        return view("ClassesView::Classesajax")->with(['request' => $request, 'tab' =>1, 'Classes' => $Classes]);
    }
	
    /**
     * @return mixed
     */
    public function index()
    {
        #####relation
        if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2 ) { // if(Auth::user()->can("view_Classes")) {
            $module_menus=app('App\Http\Controllers\admin\CrudBuilderController')->createMenumodule();
            $Classes = Classes::paginate(10);
            return view("ClassesView::Classes")->with(['module_menus'=>$module_menus,'tab' => 1, 'Classes' => $Classes]);
        }else{
            return redirect('404');
        }
    }
	
    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
         $module_id=\Request::segment(2);
         $module_id=Module::where('link_name',$module_id)->first();
         $Module_type_id=$module_id->id;
         #####relation
		$Classes=new Classes;
        if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2 ) { // if(Auth::user()->can("add_Classes")){  #####check permission
			if($Classes->rules){
				$validator = Validator::make($request->all(), $Classes->rules);
				if ($validator->fails()) {
					$backClasses = Classes::paginate(10);
					return view("ClassesView::Classesajax")->withErrors($validator)->with(['Classes'=>$backClasses,'tab'=>2,'editClasses'=>$request->all()]);
				}
			}

	 	    $Classes=Classes::create($request->all());
	 	     Activity::log([
                'contentId'   => $Classes->id,
                'contentType' => 'Classes',
                'action'      => 'Create',
                'description' => 'Created a Classes',
                'details'     => 'Username: '.Auth::user()->name,
             ]);
			####Upload image
	 	    ####Multi upload image
            ######store
            $Classes = Classes::paginate(10);
            return view("ClassesView::Classesajax")->with(['tab'=>1,'flag'=>3,'Classes'=>$Classes]);
        }else{
            $Classes = Classes::paginate(10);
            return view("ClassesView::Classesajax")->with(['tab'=>1,'flag'=>6,'Classes'=>$Classes]);
        }

    }
	
    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function edit(Request $request, $id)
    {
         $module_id=\Request::segment(2);
         $module_id=Module::where('link_name',$module_id)->first();
         $Module_type_id=$module_id->id;
        #####relation
        #####edit many to many
        $Classes = Classes::paginate(10);
        $editClasses = Classes::find($id);
        ####Edit multiple upload image
        return view("ClassesView::Classesajax")->with(['editClasses'=>$editClasses,'tab'=>2,'Classes'=>$Classes]);
    }
    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function update(Request $request, $id)
    {
        $module_id=\Request::segment(2);
        $module_id=Module::where('link_name',$module_id)->first();
        $Module_type_id=$module_id->id;
		#####relation
		$Classes=new Classes;
        if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2 ) { // if(Auth::user()->can('edit_Classes')) {  #####check permission
			if($Classes->rules){
				$validator = Validator::make($request->all(),$Classes->rules);
				if ($validator->fails()) {
					$backClasses = Classes::paginate(10);
					return view("ClassesView::Classesajax")->withErrors($validator)->with(['tab' => 2,'Classes'=>$backClasses, 'editClasses' => $request->all()]);
				}
			}
           $Classes = Classes::find($request['id']);
            $Classes->update($request->all());
            Activity::log([
                'contentId'   => $Classes->id,
                'contentType' => 'Classes',
                'description' => 'Update a Classes',
                'details'     => 'Username: '.Auth::user()->name,
                'updated'     => (bool) $Classes->id,
            ]);
			####update Multi upload image
			####Upload image
			######update Many to many
            ######store
            $backClasses = Classes::paginate(10);
            $backClasses->setPath('/Classes');
            return view("ClassesView::Classesajax")->with(['tab' => 1, 'Classes' => $backClasses, 'flag' => 4]);
        }else{
            $backClasses = Classes::paginate(10);
            return view("ClassesView::Classesajax")->with(['tab' => 1, 'Classes' => $backClasses, 'flag' => 6]);
        }
    }	
    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $module_id=\Request::segment(2);
        $module_id=Module::where('link_name',$module_id)->first();
        $Module_type_id=$module_id->id;
         #####relation
         if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2 ) { // if(Auth::user()->can('delete_Classes')) {  #####check permission
            $temp = explode(",", $id);
            foreach ($temp as $val) {
                if ($temp) {
                   ### delete multi uploader
                    $user = Classes::find($val);
                    $user->delete();
                     Activity::log([
                            'contentId'   => $val,
                            'contentType' => 'Classes',
                            'action'      => 'Delete',
                            'description' => 'Delete  a Classes',
                            'details'     => 'Username: '.Auth::user()->name,
                     ]);
                }
            }
            $Classes = Classes::paginate(10);
            $Classes->setPath('/Classes');
            return view("ClassesView::Classesajax")->with(['tab' => 1, 'Classes' => $Classes, 'flag' => 5]);
        }else{
            $Classes = Classes::paginate(10);
            return view("ClassesView::Classesajax")->with(['tab' => 1, 'Classes' => $Classes, 'flag' => 6]);
        }
    }	
}
