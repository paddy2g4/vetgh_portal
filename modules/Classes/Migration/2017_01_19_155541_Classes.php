<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Classes extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    #####start_up_function#####
        Schema::create('classes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('classname', 255);
            $table->enum('status', ['YES', 'NO']);
            $table->enum('moutable', ['YES', 'NO']);
            $table->timestamps();
        });
        DB::table("modules")->insert(
            array("name" =>"Classes","description" =>"Records of classes in the school","link_name" => "classes","status"=>1,"created_at"=>"2017-01-19 15:55:41")
        );
		        /**
         * role permission
         */
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'view_Classes','display_name' => 'view_Classes')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'add_Classes','display_name' => 'add_Classes')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'edit_Classes','display_name' => 'edit_Classes')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'delete_Classes','display_name' => 'delete_Classes')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
     #####end_up_function#####
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
     #####start_down_function#####
        DB::table('permissions')->where('name',  'view_Classes')->delete();
        DB::table('permissions')->where('name',  'add_Classes')->delete();
        DB::table('permissions')->where('name',  'edit_Classes')->delete();
        DB::table('permissions')->where('name',  'delete_Classes')->delete();
        ######remove primary key
        Schema::drop('classes');
     #####end_down_function#####
    }
}
