<?php

namespace Classes\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Database\Eloquent\Model;
#####use
 
class Classes extends Model
{
  use SearchableTrait;
    public $table = 'classes';
    
	
	 public $searchable = [
	    'columns' => [
            'classes.classname'=>100,
    'classes.status'=>101,
    'classes.moutable'=>102,
        ########relation_id
        ],
        ########join
	  ];
 

    public $fillable = [
        'classname',
        'status',
        'moutable'
    ];

 

    /**
     * Validation rules
     *
     * @var array
     */
    public   $rules = [
        'classname' => 'required|string|distinct',
        'status' => 'required',
        'moutable' => 'required'
    ];
 #####relation#####
}
