<?php
Route::group(['prefix' => 'admin'], function () {
####start
   /*
    |--------------------------------------------------------------------------
    | Classes  Routes
    |--------------------------------------------------------------------------
    |*/
    Route::resource('classes','Classes\Controller\ClassesController');
    Route::post('classes/search','Classes\Controller\ClassesController@search');
	Route::post('classes/export/excel','Classes\Controller\ClassesController@export_excel');
####end
#####relation#####
});
 