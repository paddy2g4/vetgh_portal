<!-- Classname Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Classname
	</label>
	<div class="col-sm-9">
		<input value="{{isset($editClasses['classname']) ? $editClasses['classname'] : ''}}" name="classname" type="text" placeholder="" class="form-control">
	</div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label" for="form-field-1">
       Status
    </label>
    <div class="col-sm-9">
        <select name="status" class="form-control">
            <option  {{isset($editClasses["status"])&&$editClasses["status"]=="YES" ? "selected" : ""}}   value="YES"">YES</option><option  {{isset($editClasses["status"])&&$editClasses["status"]=="NO" ? "selected" : ""}}   value="NO"">NO</option>
        </select>
    </div>
</div>




<div class="form-group">
    <label class="col-sm-3 control-label" for="form-field-1">
       Moutable
    </label>
    <div class="col-sm-9">
        <select name="moutable" class="form-control">
            <option  {{isset($editClasses["moutable"])&&$editClasses["moutable"]=="YES" ? "selected" : ""}}   value="YES"">YES</option><option  {{isset($editClasses["moutable"])&&$editClasses["moutable"]=="NO" ? "selected" : ""}}   value="NO"">NO</option>
        </select>
    </div>
</div>



