<?php

namespace EntityMaster\Controller;


use EntityMaster\Model\EntityMaster;
use Entitycontacts\Model\Entitycontacts;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\admin\Module;
use App\admin\ModuleAlbum;
use App\Http\Controllers\Controller;
use Regulus\ActivityLog\Models\Activity;
use Validator;
use Illuminate\Http\Request;
use File;
use App\Http\Requests;
use Illuminate\Pagination\Paginator;
use Zizaco\Entrust\Traits\EntrustTrait;
use Nicolaslopezj\Searchable\SearchableTrait;
#####use


class EntityMasterController extends Controller
{
        use SearchableTrait;
    //  use EntrustUserTrait ;
    protected $user_id;
	
    /**
     * @param Request $request
     * @return string
     * Export Excel & CSV & PDF
     */
    public function export_excel(Request $request){
        #### $request['export_type'] is export mode  "EXCEL or CSV"
        ### Check export PDF permission
        if($request['export_type']=='pdf'&& !Auth::user()->can('export_pdf') )
            return 'You not have this permission';
        ### Check export CSV permission
        if($request['export_type']=='csv'&& !Auth::user()->can('export_csv') )
            return 'You not have this permission';
        ### Check export EXCEL permission
        if($request['export_type']=='xls'&& !Auth::user()->can('export_xls') )
            return 'You not have this permission';
        if ($request['serach_txt']) {
            $EntityMaster = EntityMaster::search($request['serach_txt'], null, false)->get();
        } else {  ###other
            $EntityMaster = EntityMaster::all();
        }  if($request['export_type']=='pdf'){ //export PDF
            $html='<h1 style="text-align: center">YEP test pdf </h1>';
            $html .= '<style>
						table, th, td {text-align: center;}
						th, td {padding: 5px;}
						th {color: #43A047;border-color: black;background-color: #C5E1A5}
						</style>
						<table border="2" style="width:100%;">
						<tr>
							<th>Title</th>
							<th>Created date</th>
						</tr>';
            foreach ($EntityMaster as $cat ){

                $html .="<tr>
							<td>$cat->title</td>
							<td>$cat->created_at</td>
						  </tr>";
            }
			$html .= '</table>';
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadHTML($html);
            return $pdf->download('EntityMaster.pdf');
        }else { // export excel & csv
            Excel::create('EntityMaster', function ($excel) use ($EntityMaster) {
                $excel->sheet('Sheet 1', function ($sheet) use ($EntityMaster) {
                    $sheet->fromArray($EntityMaster);
                });
            })->download($request['export_type']);
        }
    }
	
 
    /**
     * @param Request $request
     * @return mixed
     * Search and paging
     */
    public function search(Request $request)
    {
        #####relation
        \Log::info("In search");
        \Log::info($request['serach_txt']);

        $currentPage = $request['page']; // You can set this to any page you want to paginate to
        Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });
        if ($request['paging'] > 0)
            $limit = $request['paging'];
        else
            $limit = 10;
        ### search
        if ($request['serach_txt']) {
            $EntityMaster = EntityMaster::search($request['serach_txt'], null, false)->get();
            $page = $request->has('page') ? $request->page - 1 : 0;
            $total = $EntityMaster->count();
            $EntityMaster = $EntityMaster->slice($page * $limit, $limit);
            $EntityMaster = new \Illuminate\Pagination\LengthAwarePaginator($EntityMaster, $total, $limit);
        } else {  ###other
            $EntityMaster = EntityMaster::paginate($limit);
        }
        return view("EntityMasterView::EntityMasterajax")->with(['request' => $request, 'tab' =>1, 'EntityMaster' => $EntityMaster]);
    }
	
    /**
     * @return mixed
     */
    public function index()
    {
        #####relation

        if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2 ) { // if(Auth::user()->can("view_EntityMaster")) {
            $module_menus=app('App\Http\Controllers\admin\CrudBuilderController')->createMenumodule();
//            $EntityMaster = EntityMaster::paginate(10);

            $EntityMaster=\DB::table('entity_master')
                ->leftJoin('entity_contact', 'entity_contact.entity_id', '=', 'entity_master.entity_id')
//                ->leftJoin('active_masters', 'entity_master.active_status', '=', 'entity_master.status_val')
                ->select('entity_master.id','entity_master.entity_id','entity_master.entity_name','entity_master.active_status', 'entity_contact.phone_number', 'entity_contact.email', 'entity_contact.other_details')
//                ->where('entity_masters.act_status',true)
                ->where('entity_master.del_status',false)
                ->where('entity_contact.active_status',true)
                ->where('entity_contact.del_status',false)
                ->orderBy('entity_master.created_at', 'desc')
                ->paginate(10);

//            dd($EntityMaster);


            return view("EntityMasterView::EntityMaster")->with(['module_menus'=>$module_menus,'tab' => 1, 'EntityMaster' => $EntityMaster]);
        }else{
            return redirect('404');
        }
    }
	
    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
         $module_id=\Request::segment(2);
         $module_id=Module::where('link_name',$module_id)->first();
//         $Module_type_id=$module_id->id;
         #####relation

        $EntityMaster=new EntityMaster;

        $rules = array(
			'entity_name'=>['required','min:1','max:200',"regex:/^[\p{L}\p{P}\p{Zs}\p{N}]+$/"],
			"phone_number" => ['required','min:9','max:15',"regex:/^(\+\d{12,15}|\d{10}|\+\d{2}-\d{3}-\d{7})$/"],
			"email" => 'required|email|max:150',
			"other_details" => ['min:2','max:200',"regex:/^[\p{L}\p{P}\p{Zs}\p{N}]+$/"]
        );
            
        if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2 ) { // if(Auth::user()->can("add_EntityMaster")){  #####check permission
			if($EntityMaster->rules){
				$validator = Validator::make($request->all(), $rules);
				if ($validator->fails()) {
					$backEntityMaster = EntityMaster::paginate(10);
					return view("EntityMasterView::EntityMasterajax")->withErrors($validator)->with(['EntityMaster'=>$backEntityMaster,'tab'=>2,'editEntityMaster'=>$request->all()]);
				}
            }
            
            $this->user_id = \Auth::user()->id;


            // $EntityMaster=EntityMaster::create($request->all());
            
            $EntityMaster->entity_name = $request->entity_name;
            $EntityMaster->user_id = $this->user_id;
            $EntityMaster->save();

            $EntityMaster_record = EntityMaster::find($EntityMaster->id);

            $Entitycontacts=new Entitycontacts;
            $Entitycontacts->entity_id = $EntityMaster_record->entity_id;
            $Entitycontacts->phone_number = $request->phone_number;
            $Entitycontacts->email = $request->email;
            $Entitycontacts->other_details = $request->other_details;
            $Entitycontacts->user_id = $this->user_id;
            $Entitycontacts->save();


            // \Log::info("EntityMaster ID = ".$EntityMaster);
                
	 	     Activity::log([
                'contentId'   => $EntityMaster->id,
                'contentType' => 'EntityMaster',
                'action'      => 'Create',
                'description' => 'Created a EntityMaster',
                'details'     => 'Username: '.Auth::user()->name,
             ]);
			####Upload image
	 	    ####Multi upload image
            ######store

            $EntityMaster=\DB::table('entity_master')
                ->leftJoin('entity_contact', 'entity_contact.entity_id', '=', 'entity_master.entity_id')
//                ->leftJoin('active_masters', 'entity_master.active_status', '=', 'entity_master.status_val')
                ->select('entity_master.id','entity_master.entity_id','entity_master.entity_name','entity_master.active_status', 'entity_contact.phone_number', 'entity_contact.email', 'entity_contact.other_details')
//                ->where('entity_masters.act_status',true)
                ->where('entity_master.del_status',false)
                ->where('entity_contact.active_status',true)
                ->where('entity_contact.del_status',false)
                ->orderBy('entity_master.created_at', 'desc')
                ->paginate(10);

            return view("EntityMasterView::EntityMasterajax")->with(['tab'=>1,'flag'=>3,'EntityMaster'=>$EntityMaster]);
        }else{
            $EntityMaster=\DB::table('entity_master')
                ->leftJoin('entity_contact', 'entity_contact.entity_id', '=', 'entity_master.entity_id')
//                ->leftJoin('active_masters', 'entity_master.active_status', '=', 'entity_master.status_val')
                ->select('entity_master.id','entity_master.entity_id','entity_master.entity_name','entity_master.active_status', 'entity_contact.phone_number', 'entity_contact.email', 'entity_contact.other_details')
//                ->where('entity_masters.act_status',true)
                ->where('entity_master.del_status',false)
                ->where('entity_contact.active_status',true)
                ->where('entity_contact.del_status',false)
                ->orderBy('entity_master.created_at', 'desc')
                ->paginate(10);
            return view("EntityMasterView::EntityMasterajax")->with(['tab'=>1,'flag'=>6,'EntityMaster'=>$EntityMaster]);
        }

    }

    public function edit_status(Request $request){
//        dd($request->all(),"In edit_status");

        $rules = array(
            'id'=>'required|Integer',
            "btnstatus" => 'required|alpha'
        );

        if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2 ) { // if(Auth::user()->can("add_EntityMaster")){  #####check permission
            if($rules){
                $validator = Validator::make($request->all(), $rules);
                if ($validator->fails()) {
                    $backEntityMaster = EntityMaster::paginate(10);
                    return view("EntityMasterView::EntityMasterajax")->withErrors($validator)->with(['EntityMaster'=>$backEntityMaster,'tab'=>2,'editEntityMaster'=>$request->all()]);
                }
            }

            $this->user_id = \Auth::user()->id;

            if ($request->btnstatus == "disable"){
                $message = "Record Disabled successfully";
                $action = false;
            }else{
                $message = "Record Enabled successfully";
                $action = true;
            }

            $EntityMaster = EntityMaster::find($request['id']);

            $EntityMaster->active_status = $action;
//            $EntityMaster->updated_at = $request->entity_name;
            $EntityMaster->user_id = $this->user_id;
            $EntityMaster->save();


            Activity::log([
                'contentId'   => $EntityMaster->id,
                'contentType' => 'EntityMaster',
                'action'      => 'Update',
                'description' => 'EntityMaster Status Changed',
                'details'     => 'Username: '.Auth::user()->name,
            ]);
            ####Upload image
            ####Multi upload image
            ######store

            $EntityMaster = EntityMaster::paginate(10);
//            return view("EntityMasterView::EntityMasterajax")->with(['tab'=>1,'flag'=>3,'EntityMaster'=>$EntityMaster]);
            return redirect()->back()->with(['status'=>$message, 'tab'=>1,'flag'=>4, 'EntityMaster'=>$EntityMaster]);
        }else{
            $EntityMaster = EntityMaster::paginate(10);
            return view("EntityMasterView::EntityMasterajax")->with(['tab'=>1,'flag'=>6,'EntityMaster'=>$EntityMaster]);
        }


    }
	
    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function edit(Request $request, $id)
    {
        \Log::info($request);
        \Log::info($id);
         $module_id=\Request::segment(2);
         $module_id=Module::where('link_name',$module_id)->first();
//         $Module_type_id=$module_id->id;
        #####relation

        #####edit many to many

        $EntityMaster = EntityMaster::paginate(10);
        $editEntityMaster = EntityMaster::find($id);


        $Entitycontacts1 = Entitycontacts::paginate(10);
//        $editEntitycontacts = Entitycontacts::find($id);
        $editEntityContact1=\DB::table('entity_contact')
            ->select('entity_contact.id','entity_contact.phone_number', 'entity_contact.email', 'entity_contact.other_details')
            ->where('entity_contact.active_status',true)
            ->where('entity_contact.del_status',false)
            ->where('entity_contact.entity_id', '=', $id)->first();

//        \Log::info($editEntityContact1);

        ####Edit multiple upload image
        return view("EntityMasterView::EntityMasterajax")->with(['editEntityMaster'=>$editEntityMaster,'tab'=>2,'EntityMaster'=>$EntityMaster, 'editEntityContact1' => $editEntityContact1, 'Entitycontacts1'=>$Entitycontacts1]);
    }
    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function update(Request $request, $id)
    {
        $module_id=\Request::segment(2);
        $module_id=Module::where('link_name',$module_id)->first();
//        $Module_type_id=$module_id->id;
		#####relation

		$EntityMaster=new EntityMaster;
        if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2 ) { // if(Auth::user()->can('edit_EntityMaster')) {  #####check permission
			if($EntityMaster->rules){
				$validator = Validator::make($request->all(),$EntityMaster->rules);
				if ($validator->fails()) {
					$backEntityMaster = EntityMaster::paginate(10);
					return view("EntityMasterView::EntityMasterajax")->withErrors($validator)->with(['tab' => 2,'EntityMaster'=>$backEntityMaster, 'editEntityMaster' => $request->all()]);
				}
			}

			$this->user_id = \Auth::user()->id;

            \Log::info($request);
            \Log::info($id);

            $EntityMaster = EntityMaster::find($request['id']);
            $EntityMaster->update($request->all());

            $update_result = \DB::table('entity_contact')
                ->where('entity_id', $request['entity_id'])
                ->update(
                    ['phone_number' => $request['phone_number'], 'email'=>$request['email'], 'other_details'=>$request['other_details'], 'user_id'=>$this->user_id]
                );
            \Log::info($update_result);


            Activity::log([
                'contentId'   => $EntityMaster->id,
                'contentType' => 'EntityMaster',
                'description' => 'Update a EntityMaster',
                'details'     => 'Username: '.Auth::user()->name,
                'updated'     => (bool) $EntityMaster->id,
            ]);
			####update Multi upload image
			####Upload image
			######update Many to many
            ######store

//            $backEntityMaster = EntityMaster::paginate(10);
            $backEntityMaster=\DB::table('entity_master')
                ->leftJoin('entity_contact', 'entity_contact.entity_id', '=', 'entity_master.entity_id')
                ->leftJoin('active_masters', 'entity_master.active_status', '=', 'active_masters.status_val')
                ->select('entity_master.id','entity_master.entity_id','entity_master.entity_name','entity_master.active_status', 'entity_contact.phone_number', 'entity_contact.email', 'entity_contact.other_details','active_masters.status_name')
//                ->where('entity_master.act_status',true)
                ->where('entity_master.del_status',false)
                ->where('entity_contact.active_status',true)
                ->where('entity_contact.del_status',false)
                ->orderBy('entity_master.created_at', 'desc')
                ->paginate(10);

            $backEntityMaster->setPath('/EntityMaster');

            return view("EntityMasterView::EntityMasterajax")->with(['tab' => 1, 'EntityMaster' => $backEntityMaster, 'flag' => 4]);
        }else{
            $backEntityMaster=\DB::table('entity_master')
                ->leftJoin('entity_contact', 'entity_contact.entity_id', '=', 'entity_master.entity_id')
                ->leftJoin('active_masters', 'entity_master.active_status', '=', 'active_masters.status_val')
                ->select('entity_master.id','entity_master.entity_id','entity_master.entity_name','entity_master.active_status', 'entity_contact.phone_number', 'entity_contact.email', 'entity_contact.other_details','active_masters.status_name')
//                ->where('entity_master.act_status',true)
                ->where('entity_master.del_status',false)
                ->where('entity_contact.active_status',true)
                ->where('entity_contact.del_status',false)
                ->orderBy('entity_master.created_at', 'desc')
                ->paginate(10);
            return view("EntityMasterView::EntityMasterajax")->with(['tab' => 1, 'EntityMaster' => $backEntityMaster, 'flag' => 6]);
        }
    }	
    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $module_id=\Request::segment(2);
        $module_id=Module::where('link_name',$module_id)->first();
        $Module_type_id=$module_id->id;
         #####relation

         if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2 ) { // if(Auth::user()->can('delete_EntityMaster')) {  #####check permission
            $temp = explode(",", $id);
            foreach ($temp as $val) {
                if ($temp) {
                   ### delete multi uploader
                    $user = EntityMaster::find($val);
                    $user->delete();
                     Activity::log([
                            'contentId'   => $val,
                            'contentType' => 'EntityMaster',
                            'action'      => 'Delete',
                            'description' => 'Delete  a EntityMaster',
                            'details'     => 'Username: '.Auth::user()->name,
                     ]);
                }
            }
             $EntityMaster=\DB::table('entity_master')
                 ->leftJoin('entity_contact', 'entity_contact.entity_id', '=', 'entity_master.entity_id')
                 ->leftJoin('active_masters', 'entity_master.active_status', '=', 'active_masters.status_val')
                 ->select('entity_master.id','entity_master.entity_id','entity_master.entity_name','entity_master.active_status', 'entity_contact.phone_number', 'entity_contact.email', 'entity_contact.other_details','active_masters.status_name')
//                ->where('entity_master.act_status',true)
                 ->where('entity_master.del_status',false)
                 ->where('entity_contact.active_status',true)
                 ->where('entity_contact.del_status',false)
                 ->orderBy('entity_master.created_at', 'desc')
                 ->paginate(10);

            $EntityMaster->setPath('/EntityMaster');
            return view("EntityMasterView::EntityMasterajax")->with(['tab' => 1, 'EntityMaster' => $EntityMaster, 'flag' => 5]);
        }else{
             $EntityMaster=\DB::table('entity_master')
                 ->leftJoin('entity_contact', 'entity_contact.entity_id', '=', 'entity_master.entity_id')
                 ->leftJoin('active_masters', 'entity_master.active_status', '=', 'active_masters.status_val')
                 ->select('entity_master.id','entity_master.entity_id','entity_master.entity_name','entity_master.active_status', 'entity_contact.phone_number', 'entity_contact.email', 'entity_contact.other_details','active_masters.status_name')
//                ->where('entity_master.act_status',true)
                 ->where('entity_master.del_status',false)
                 ->where('entity_contact.active_status',true)
                 ->where('entity_contact.del_status',false)
                 ->orderBy('entity_master.created_at', 'desc')
                 ->paginate(10);
            return view("EntityMasterView::EntityMasterajax")->with(['tab' => 1, 'EntityMaster' => $EntityMaster, 'flag' => 6]);
        }
    }	
}
