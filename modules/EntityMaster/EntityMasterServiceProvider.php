<?php

namespace EntityMaster;

use Illuminate\Support\ServiceProvider;

class EntityMasterServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $this->app->bind('EntityMaster',function($app){
            return new EntityMaster;
        });

        // Define Router file
        require __DIR__ . '/Route/routes.php';

        // Define path for view files
        $this->loadViewsFrom(__DIR__.'/View','EntityMasterView');

        // Define path for translation
        $this->loadTranslationsFrom(__DIR__.'/Lang','EntityMasterLang');

        // Define Published migrations files
        $this->publishes([
            __DIR__.'/Migration' => database_path('migrations'),
        ], 'migrations');

        // Define Published Asset files
        $this->publishes([
            __DIR__.'/Asset' => public_path('assets/'),
        ], 'public');

    }


    public function register()
    {
        //
    }
}