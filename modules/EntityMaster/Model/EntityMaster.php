<?php

namespace EntityMaster\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Database\Eloquent\Model;
#####use
 
class EntityMaster extends Model
{
  use SearchableTrait;
    public $table = 'entity_master';
    
	
	 public $searchable = [
	    'columns' => [
            'entity_master.entity_id'=>100,
    'entity_master.entity_name'=>101,
    'entity_master.active_status'=>102,
    'entity_master.del_status'=>103,
        ########relation_id

        ],
        ########join
'joins' => [
            ],###
	  ];
 

    public $fillable = [
        'entity_id',
        'entity_name',
//        'entity_cat_id',
        'active_status',
        'del_status'
    ];

 

    /**
     * Validation rules
     *
     * @var array
     */
    public   $rules = [
        'entity_name' => 'required',
        'active_status' => 'boolean'
    ];
 #####relation#####
}