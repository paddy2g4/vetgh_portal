<?php
Route::group(['prefix' => 'admin'], function () {
####start
   /*
    |--------------------------------------------------------------------------
    | EntityMaster  Routes
    |--------------------------------------------------------------------------
    |*/
    Route::resource('entitymasters','EntityMaster\Controller\EntityMasterController');
    Route::post('entitymasters/search','EntityMaster\Controller\EntityMasterController@search');
	Route::post('entitymasters/export/excel','EntityMaster\Controller\EntityMasterController@export_excel');

    Route::post('entitymasters/action_status','EntityMaster\Controller\EntityMasterController@edit_status');
####end
#####relation#####

   /*
    |--------------------------------------------------------------------------
    | EntityMaster  Routes
    |--------------------------------------------------------------------------
    |*/
    
    
	

});
 