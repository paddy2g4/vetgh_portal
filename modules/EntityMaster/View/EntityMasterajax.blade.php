<div class="col-md-12" id="ajax_div">
    <!--panel details_company -->
    <div class="panel with-nav-tabs panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">
                <div class="bars pull-right">
                    <a href="#"><i class="maximum fa fa-expand" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Maximize"></i></a>
                    <a href="#"><i class="minimize fa fa-chevron-down" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Collapse"></i></a>
                </div>
            </h3>
            <ul class="nav nav-tabs">
                <li class="{{isset($tab)&&$tab==1?'active':''}}">
                    <a href="#tab1EntityMaster" data-toggle="tab">
                        <i class="fa fa-EntityMaster fa-lg danger"></i>
                        Institution
                    </a>
                </li>
                <li class="{{isset($tab)&&$tab==2?'active':''}}" @if($tab!=2)style="display:none;"@endif>
                    <a href="#newEntityMaster" id="newEntityMaster-1" data-toggle="tab">
                        <i class="fa fa-EntityMaster-plus success"></i>
                        New Institution
                    </a>
                </li>
            </ul>
        </div>
        <div class="panel-body no-padding">
            @include('message')
            <div class="tab-content">
                <div class="tab-pane fade  {{isset($tab)&&$tab==1?'active in':''}} tab1" id="tab1EntityMaster">
                    <div class="col-md-12"></br>
                        <div class="form-inline">
                            <div class="form-group">
                                <select class="form-control paging" id="paging" name="paging">
                                    <option  {{isset($request['paging'])&&$request['paging']==10 ?  'selected' :''}}  value="10">10</option>
                                    <option  {{isset($request['paging'])&&$request['paging']==25 ?  'selected' :''}}  value="25">25</option>
                                    <option  {{isset($request['paging'])&&$request['paging']==50?  'selected' :''}}  value="50">50</option>
                                    <option  {{isset($request['paging'])&&$request['paging']==100?  'selected' :''}}  value="100">100</option>
                                </select>
                            </div>

                            <div class="btn-group">
                                <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" >
                                    Actions <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        @if(\Session::get('role_id') == 1)<!-- @permission('delete_EntityMaster') -->
                                        <a href="javascript:void(0);" data-toggle="modal" data-target="#mydelete"><i class="fa fa-trash danger"></i> Delete</a>
                                        <!-- @endpermission -->
                                        <!-- @permission('export_csv') -->
                                        <a href="#" id='csv' class="export" ><i class="fa fa-download blue"></i> Export to CSV</a>
                                        <!-- @endpermission -->
                                        <!-- @permission('export_xls') -->
                                        <a href="#"  id='xls' class="export" ><i class="fa fa-download green"></i> Export to EXCEL</a>
                                        <!-- @endpermission -->
                                        <!-- @permission('export_pdf') -->
                                        <a href="#"  id='pdf' class="export" ><i class="fa fa-download red"></i> Export to PDF</a>
                                        @endif<!-- @endpermission -->
                                    </li>
                                </ul>
                            </div>

                            @if(\Session::get('role_id') == 1 || \Session::get('role_id') == 2)
                            <div class="pull-right">
                                <a href="#" class="btn btn-success pull-right" data-tab-destination="newEntityMaster-1">
                                    <i class="fa fa-plus"></i> Add
                                </a>
                            </div>
                            @endif
                        </div>
                        <hr>
                        <div class="res-table">
                            <table class="table table-striped table-hover table-fixed ellipsis-table" >
                                <thead>
                                    <tr>
                                        <th width="5%" class="center">
                                            <div class="checkbox checkbox-primary">
                                                <input id="checkbox2" type="checkbox" class="conchkSelectAll">
                                                <label for="checkbox2">
                                                </label>
                                            </div>
                                        </th>
                                        @include("EntityMasterView::EntityMastershow_table_header")
                                        <th width="5%">
                                            <i class="fa fa-bolt"></i>
                                            <a href="#"><i class="fa fa-search search-toggle-btn"></i></a>
                                        </th>
                                        <th width="10%">
                                            <i class="fa fa-bolt">Enable/Disable</i>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="3">
                                            <input  value="{{isset($request['serach_txt'])?  $request['serach_txt']:''}}" id='search_text' name="search_text" type="text" class="form-control" placeholder="search">
                                        </td>
                                        <td>
                                            <a class="btn btn-default search-btn"><i class="fa fa-search"></i></a>
                                        </td>
                                    </tr>
                                    @if(isset($EntityMaster))
                                        @foreach($EntityMaster as $content)
                                            <tr>
                                                <td class="center">
                                                    <div class="checkbox checkbox-primary">
                                                        <input id="{{$content->id}}" type="checkbox" class="conchkNumber">
                                                        <label for="{{$content->id}}">
                                                        </label>
                                                    </div>
                                                </td>
                                                @include("EntityMasterView::EntityMastershow_table_fields")
                                                <td>
                                                    @if(\Session::get('role_id') == 1 || \Session::get('role_id') == 2) {{-- @permission('edit_EntityMaster')--}}
                                                    <a id="{{$content->entity_id}}" href="#" class="fa fa-pencil fa-lg edit-href" ></a>
                                                    @endif  {{--  @endpermission--}}
                                                </td>

                                                <td data-title="active">
                                                    <div class="m-switch">
                                                        <form id="form"  class="form-horizontal form" method="post" action="{{url("/admin/entitymasters/action_status")}}" autocomplete="off">
                                                            <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
                                                            <input type="hidden" name="id" id="id" value="{{isset($content->id) ? $content->id : 0 }}">
                                                            @if($content->active_status == 1)
                                                                <button type="submit" class="btn btn-danger" name="btnstatus" value="disable"> Disable</span> </button>
                                                            @else
                                                                <button type="submit" class="btn btn-success" name="btnstatus" value="enable"> Enable</span> </button>
                                                            @endif
                                                        </form>

                                                    </div>
                                                </td>

                                            </tr>
                                        @endforeach
                                    @endif

                                </tbody>
                            </table>
                            @if(isset($EntityMaster))
                                {!!  $EntityMaster->render() !!}
                            @endif
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade  {{isset($tab)&&$tab==2?'active in':''}} tab2" id="newEntityMaster">
                    @include('errors')
                    <form id="form"   class="form-horizontal form" method="post" action="{{url("/admin/entitymasters")}}" autocomplete="off">
                        <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="url" id="url" value="{{url("/admin/entitymasters")}}">
                        <input type="hidden" name="id" id="id" value="{{isset($editEntityMaster['id']) ? $editEntityMaster['id'] : 0 }}">
                        <input type="hidden" name="entity_id" id="entity_id" value="{{isset($editEntityMaster['entity_id']) ? $editEntityMaster['entity_id'] : 0 }}">
                        <input type="hidden" name="page" id="page" value="{{isset($request['page']) ?$request['page'] : 1 }}">
                        <div class="col-md-6"><br>
							@include("EntityMasterView::EntityMasterfields")
							    {{--relation fields--}}
                                

                        </div>
                        <div class="col-md-12 layout no-padding">
                            <div class="panel-footer">
                                <div class="progress-btn">
                                    @if(\Session::get('role_id') == 1 || \Session::get('role_id') == 2)
                                    @if(!isset($editEntityMaster['id'])||$editEntityMaster['id']=="")
                                        <a   class=" submit-btn btn btn-success ladda-button" data-style="expand-right"><i class="fa fa-save"></i><span class="ladda-label"> Save</span></a>
                                    @endif
                                    @endif
                                    @if(\Session::get('role_id') == 1 || \Session::get('role_id') == 2)
                                    @if(isset($editEntityMaster['id'])&&$editEntityMaster['id']>"")
                                        <a    class="btn btn-info edit-btn"><i class="fa fa-save"></i>Update</a>
                                    @endif
                                    @endif
                                     <button class="btn btn-link cancel-btn"> Cancel</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- end panel -->

</div>