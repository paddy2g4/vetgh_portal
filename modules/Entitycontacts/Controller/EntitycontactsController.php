<?php

namespace Entitycontacts\Controller;


use Entitycontacts\Model\Entitycontacts;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\admin\Module;
use App\admin\ModuleAlbum;
use App\Http\Controllers\Controller;
use Regulus\ActivityLog\Models\Activity;
use Validator;
use Illuminate\Http\Request;
use File;
use App\Http\Requests;
use Illuminate\Pagination\Paginator;
use Zizaco\Entrust\Traits\EntrustTrait;
use Nicolaslopezj\Searchable\SearchableTrait;
#####use
class EntitycontactsController extends Controller
{
        use SearchableTrait;
    //  use EntrustUserTrait ;
	
    /**
     * @param Request $request
     * @return string
     * Export Excel & CSV & PDF
     */
    public function export_excel(Request $request){
        #### $request['export_type'] is export mode  "EXCEL or CSV"
        ### Check export PDF permission
        if($request['export_type']=='pdf'&& !Auth::user()->can('export_pdf') )
            return 'You not have this permission';
        ### Check export CSV permission
        if($request['export_type']=='csv'&& !Auth::user()->can('export_csv') )
            return 'You not have this permission';
        ### Check export EXCEL permission
        if($request['export_type']=='xls'&& !Auth::user()->can('export_xls') )
            return 'You not have this permission';
        if ($request['serach_txt']) {
            $Entitycontacts = Entitycontacts::search($request['serach_txt'], null, false)->get();
        } else {  ###other
            $Entitycontacts = Entitycontacts::all();
        }  if($request['export_type']=='pdf'){ //export PDF
            $html='<h1 style="text-align: center">YEP test pdf </h1>';
            $html .= '<style>
						table, th, td {text-align: center;}
						th, td {padding: 5px;}
						th {color: #43A047;border-color: black;background-color: #C5E1A5}
						</style>
						<table border="2" style="width:100%;">
						<tr>
							<th>Title</th>
							<th>Created date</th>
						</tr>';
            foreach ($Entitycontacts as $cat ){

                $html .="<tr>
							<td>$cat->title</td>
							<td>$cat->created_at</td>
						  </tr>";
            }
			$html .= '</table>';
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadHTML($html);
            return $pdf->download('Entitycontacts.pdf');
        }else { // export excel & csv
            Excel::create('Entitycontacts', function ($excel) use ($Entitycontacts) {
                $excel->sheet('Sheet 1', function ($sheet) use ($Entitycontacts) {
                    $sheet->fromArray($Entitycontacts);
                });
            })->download($request['export_type']);
        }
    }
	
 
    /**
     * @param Request $request
     * @return mixed
     * Search and paging
     */
    public function search(Request $request)
    {
        #####relation
        $currentPage = $request['page']; // You can set this to any page you want to paginate to
        Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });
        if ($request['paging'] > 0)
            $limit = $request['paging'];
        else
            $limit = 10;
        ### search
        if ($request['serach_txt']) {
            $Entitycontacts = Entitycontacts::search($request['serach_txt'], null, false)->get();
            $page = $request->has('page') ? $request->page - 1 : 0;
            $total = $Entitycontacts->count();
            $Entitycontacts = $Entitycontacts->slice($page * $limit, $limit);
            $Entitycontacts = new \Illuminate\Pagination\LengthAwarePaginator($Entitycontacts, $total, $limit);
        } else {  ###other
            $Entitycontacts = Entitycontacts::paginate($limit);
        }
        return view("EntitycontactsView::Entitycontactsajax")->with(['request' => $request, 'tab' =>1, 'Entitycontacts' => $Entitycontacts]);
    }
	
    /**
     * @return mixed
     */
    public function index()
    {
        #####relation
        if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2 ) { // if(Auth::user()->can("view_Entitycontacts")) {
            $module_menus=app('App\Http\Controllers\admin\CrudBuilderController')->createMenumodule();
            $Entitycontacts = Entitycontacts::paginate(10);
            return view("EntitycontactsView::Entitycontacts")->with(['module_menus'=>$module_menus,'tab' => 1, 'Entitycontacts' => $Entitycontacts]);
        }else{
            return redirect('404');
        }
    }
	
    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
         $module_id=\Request::segment(2);
         $module_id=Module::where('link_name',$module_id)->first();
         $Module_type_id=$module_id->id;
         #####relation
		$Entitycontacts=new Entitycontacts;
        if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2 ) { // if(Auth::user()->can("add_Entitycontacts")){  #####check permission
			if($Entitycontacts->rules){
				$validator = Validator::make($request->all(), $Entitycontacts->rules);
				if ($validator->fails()) {
					$backEntitycontacts = Entitycontacts::paginate(10);
					return view("EntitycontactsView::Entitycontactsajax")->withErrors($validator)->with(['Entitycontacts'=>$backEntitycontacts,'tab'=>2,'editEntitycontacts'=>$request->all()]);
				}
			}

	 	    $Entitycontacts=Entitycontacts::create($request->all());
	 	     Activity::log([
                'contentId'   => $Entitycontacts->id,
                'contentType' => 'Entitycontacts',
                'action'      => 'Create',
                'description' => 'Created a Entitycontacts',
                'details'     => 'Username: '.Auth::user()->name,
             ]);
			####Upload image
	 	    ####Multi upload image
            ######store
            $Entitycontacts = Entitycontacts::paginate(10);
            return view("EntitycontactsView::Entitycontactsajax")->with(['tab'=>1,'flag'=>3,'Entitycontacts'=>$Entitycontacts]);
        }else{
            $Entitycontacts = Entitycontacts::paginate(10);
            return view("EntitycontactsView::Entitycontactsajax")->with(['tab'=>1,'flag'=>6,'Entitycontacts'=>$Entitycontacts]);
        }

    }
	
    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function edit(Request $request, $id)
    {
         $module_id=\Request::segment(2);
         $module_id=Module::where('link_name',$module_id)->first();
         $Module_type_id=$module_id->id;
        #####relation
        #####edit many to many
        $Entitycontacts = Entitycontacts::paginate(10);
        $editEntitycontacts = Entitycontacts::find($id);
        ####Edit multiple upload image
        return view("EntitycontactsView::Entitycontactsajax")->with(['editEntitycontacts'=>$editEntitycontacts,'tab'=>2,'Entitycontacts'=>$Entitycontacts]);
    }
    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function update(Request $request, $id)
    {
        $module_id=\Request::segment(2);
        $module_id=Module::where('link_name',$module_id)->first();
        $Module_type_id=$module_id->id;
		#####relation
		$Entitycontacts=new Entitycontacts;
        if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2 ) { // if(Auth::user()->can('edit_Entitycontacts')) {  #####check permission
			if($Entitycontacts->rules){
				$validator = Validator::make($request->all(),$Entitycontacts->rules);
				if ($validator->fails()) {
					$backEntitycontacts = Entitycontacts::paginate(10);
					return view("EntitycontactsView::Entitycontactsajax")->withErrors($validator)->with(['tab' => 2,'Entitycontacts'=>$backEntitycontacts, 'editEntitycontacts' => $request->all()]);
				}
			}
           $Entitycontacts = Entitycontacts::find($request['id']);
            $Entitycontacts->update($request->all());
            Activity::log([
                'contentId'   => $Entitycontacts->id,
                'contentType' => 'Entitycontacts',
                'description' => 'Update a Entitycontacts',
                'details'     => 'Username: '.Auth::user()->name,
                'updated'     => (bool) $Entitycontacts->id,
            ]);
			####update Multi upload image
			####Upload image
			######update Many to many
            ######store
            $backEntitycontacts = Entitycontacts::paginate(10);
            $backEntitycontacts->setPath('/Entitycontacts');
            return view("EntitycontactsView::Entitycontactsajax")->with(['tab' => 1, 'Entitycontacts' => $backEntitycontacts, 'flag' => 4]);
        }else{
            $backEntitycontacts = Entitycontacts::paginate(10);
            return view("EntitycontactsView::Entitycontactsajax")->with(['tab' => 1, 'Entitycontacts' => $backEntitycontacts, 'flag' => 6]);
        }
    }	
    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $module_id=\Request::segment(2);
        $module_id=Module::where('link_name',$module_id)->first();
        $Module_type_id=$module_id->id;
         #####relation
         if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2 ) { // if(Auth::user()->can('delete_Entitycontacts')) {  #####check permission
            $temp = explode(",", $id);
            foreach ($temp as $val) {
                if ($temp) {
                   ### delete multi uploader
                    $user = Entitycontacts::find($val);
                    $user->delete();
                     Activity::log([
                            'contentId'   => $val,
                            'contentType' => 'Entitycontacts',
                            'action'      => 'Delete',
                            'description' => 'Delete  a Entitycontacts',
                            'details'     => 'Username: '.Auth::user()->name,
                     ]);
                }
            }
            $Entitycontacts = Entitycontacts::paginate(10);
            $Entitycontacts->setPath('/Entitycontacts');
            return view("EntitycontactsView::Entitycontactsajax")->with(['tab' => 1, 'Entitycontacts' => $Entitycontacts, 'flag' => 5]);
        }else{
            $Entitycontacts = Entitycontacts::paginate(10);
            return view("EntitycontactsView::Entitycontactsajax")->with(['tab' => 1, 'Entitycontacts' => $Entitycontacts, 'flag' => 6]);
        }
    }	
}
