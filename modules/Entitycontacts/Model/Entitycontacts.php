<?php

namespace Entitycontacts\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Database\Eloquent\Model;
#####use
 
class Entitycontacts extends Model
{
  use SearchableTrait;
    public $table = 'entity_contact';
    
	
	 public $searchable = [
	    'columns' => [
            'entity_contact.entity_id'=>100,
    'entity_contact.phone_number'=>101,
    'entity_contact.email'=>102,
    'entity_contact.other_details'=>103,
    'entity_contact.user_id'=>104,
        ########relation_id
        ],
        ########join
	  ];
 

    public $fillable = [
        'entity_id',
        'phone_number',
        'email',
        'other_details',
        'user_id'
    ];

 

    /**
     * Validation rules
     *
     * @var array
     */
    public   $rules = [
        'phone_number' => 'required',
        'email' => 'email'
    ];
 #####relation#####
}
