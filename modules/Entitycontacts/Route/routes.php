<?php
Route::group(['prefix' => 'admin'], function () {
####start
   /*
    |--------------------------------------------------------------------------
    | Entitycontacts  Routes
    |--------------------------------------------------------------------------
    |*/
    Route::resource('entitycontacts','Entitycontacts\Controller\EntitycontactsController');
    Route::post('entitycontacts/search','Entitycontacts\Controller\EntitycontactsController@search');
	Route::post('entitycontacts/export/excel','Entitycontacts\Controller\EntitycontactsController@export_excel');
####end
#####relation#####
});
 