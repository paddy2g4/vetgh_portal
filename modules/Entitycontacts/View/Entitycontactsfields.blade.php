<!-- Entity Id Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Entity Id
	</label>
	<div class="col-sm-9">
		<input value="{{isset($editEntitycontacts['entity_id']) ? $editEntitycontacts['entity_id'] : ''}}" name="entity_id" type="text" placeholder="" class="form-control">
	</div>
</div>

<!-- Phone Number Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Phone Number
	</label>
	<div class="col-sm-9">
		<input value="{{isset($editEntitycontacts['phone_number']) ? $editEntitycontacts['phone_number'] : ''}}" name="phone_number" type="text" placeholder="" class="form-control">
	</div>
</div>

<!-- Email Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Email
	</label>
	<div class="col-sm-9">
		<input value="{{isset($editEntitycontacts['email']) ? $editEntitycontacts['email'] : ''}}" name="email" type="text" placeholder="" class="form-control">
	</div>
</div>

<!-- Other Details Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Other Details
	</label>
	<div class="col-sm-9">
		<input value="{{isset($editEntitycontacts['other_details']) ? $editEntitycontacts['other_details'] : ''}}" name="other_details" type="text" placeholder="" class="form-control">
	</div>
</div>

<!-- User Id Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		User Id
	</label>
	<div class="col-sm-9">
		<input value="{{isset($editEntitycontacts['user_id']) ? $editEntitycontacts['user_id'] : ''}}" name="user_id" type="text" placeholder="" class="form-control">
	</div>
</div>
