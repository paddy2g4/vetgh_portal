<?php

namespace Keys\Controller;


use Keys\Model\Keys;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\admin\Module;
use App\admin\ModuleAlbum;
use App\Http\Controllers\Controller;
use Regulus\ActivityLog\Models\Activity;
use Validator;
use Illuminate\Http\Request;
use File;
use App\Http\Requests;
use Illuminate\Pagination\Paginator;
use Zizaco\Entrust\Traits\EntrustTrait;
use Nicolaslopezj\Searchable\SearchableTrait;
#####use
class KeysController extends Controller
{
        use SearchableTrait;
    //  use EntrustUserTrait ;
	
    /**
     * @param Request $request
     * @return string
     * Export Excel & CSV & PDF
     */
    public function export_excel(Request $request){
        #### $request['export_type'] is export mode  "EXCEL or CSV"
        ### Check export PDF permission
        if($request['export_type']=='pdf'&& !Auth::user()->can('export_pdf') )
            return 'You not have this permission';
        ### Check export CSV permission
        if($request['export_type']=='csv'&& !Auth::user()->can('export_csv') )
            return 'You not have this permission';
        ### Check export EXCEL permission
        if($request['export_type']=='xls'&& !Auth::user()->can('export_xls') )
            return 'You not have this permission';
        if ($request['serach_txt']) {
            $Keys = Keys::search($request['serach_txt'], null, false)->get();
        } else {  ###other
            $Keys = Keys::all();
        }  if($request['export_type']=='pdf'){ //export PDF
            $html='<h1 style="text-align: center">YEP test pdf </h1>';
            $html .= '<style>
						table, th, td {text-align: center;}
						th, td {padding: 5px;}
						th {color: #43A047;border-color: black;background-color: #C5E1A5}
						</style>
						<table border="2" style="width:100%;">
						<tr>
							<th>Title</th>
							<th>Created date</th>
						</tr>';
            foreach ($Keys as $cat ){

                $html .="<tr>
							<td>$cat->title</td>
							<td>$cat->created_at</td>
						  </tr>";
            }
			$html .= '</table>';
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadHTML($html);
            return $pdf->download('Keys.pdf');
        }else { // export excel & csv
            Excel::create('Keys', function ($excel) use ($Keys) {
                $excel->sheet('Sheet 1', function ($sheet) use ($Keys) {
                    $sheet->fromArray($Keys);
                });
            })->download($request['export_type']);
        }
    }
	
 
    /**
     * @param Request $request
     * @return mixed
     * Search and paging
     */
    public function search(Request $request)
    {
        #####relation
        $currentPage = $request['page']; // You can set this to any page you want to paginate to
        Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });
        if ($request['paging'] > 0)
            $limit = $request['paging'];
        else
            $limit = 10;
        ### search
        if ($request['serach_txt']) {
            $Keys = Keys::search($request['serach_txt'], null, false)->get();
            $page = $request->has('page') ? $request->page - 1 : 0;
            $total = $Keys->count();
            $Keys = $Keys->slice($page * $limit, $limit);
            $Keys = new \Illuminate\Pagination\LengthAwarePaginator($Keys, $total, $limit);
        } else {  ###other
            $Keys = Keys::paginate($limit);
        }
        return view("KeysView::Keysajax")->with(['request' => $request, 'tab' =>1, 'Keys' => $Keys]);
    }
	
    /**
     * @return mixed
     */
    public function index()
    {
        #####relation
        if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2 ) { // if(Auth::user()->can("view_Keys")) {
            $module_menus=app('App\Http\Controllers\admin\CrudBuilderController')->createMenumodule();
            $Keys = Keys::paginate(10);
            return view("KeysView::Keys")->with(['module_menus'=>$module_menus,'tab' => 1, 'Keys' => $Keys]);
        }else{
            return redirect('404');
        }
    }
	
    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
         $module_id=\Request::segment(2);
         $module_id=Module::where('link_name',$module_id)->first();
         $Module_type_id=$module_id->id;
         #####relation
		$Keys=new Keys;
        if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2 ) { // if(Auth::user()->can("add_Keys")){  #####check permission
			if($Keys->rules){
				$validator = Validator::make($request->all(), $Keys->rules);
				if ($validator->fails()) {
					$backKeys = Keys::paginate(10);
					return view("KeysView::Keysajax")->withErrors($validator)->with(['Keys'=>$backKeys,'tab'=>2,'editKeys'=>$request->all()]);
				}
			}

	 	    $Keys=Keys::create($request->all());
	 	     Activity::log([
                'contentId'   => $Keys->id,
                'contentType' => 'Keys',
                'action'      => 'Create',
                'description' => 'Created a Keys',
                'details'     => 'Username: '.Auth::user()->name,
             ]);
			####Upload image
	 	    ####Multi upload image
            ######store
            $Keys = Keys::paginate(10);
            return view("KeysView::Keysajax")->with(['tab'=>1,'flag'=>3,'Keys'=>$Keys]);
        }else{
            $Keys = Keys::paginate(10);
            return view("KeysView::Keysajax")->with(['tab'=>1,'flag'=>6,'Keys'=>$Keys]);
        }

    }
	
    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function edit(Request $request, $id)
    {
         $module_id=\Request::segment(2);
         $module_id=Module::where('link_name',$module_id)->first();
         $Module_type_id=$module_id->id;
        #####relation
        #####edit many to many
        $Keys = Keys::paginate(10);
        $editKeys = Keys::find($id);
        ####Edit multiple upload image
        return view("KeysView::Keysajax")->with(['editKeys'=>$editKeys,'tab'=>2,'Keys'=>$Keys]);
    }
    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function update(Request $request, $id)
    {
        $module_id=\Request::segment(2);
        $module_id=Module::where('link_name',$module_id)->first();
        $Module_type_id=$module_id->id;
		#####relation
		$Keys=new Keys;
        if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2 ) { // if(Auth::user()->can('edit_Keys')) {  #####check permission
			if($Keys->rules){
				$validator = Validator::make($request->all(),$Keys->rules);
				if ($validator->fails()) {
					$backKeys = Keys::paginate(10);
					return view("KeysView::Keysajax")->withErrors($validator)->with(['tab' => 2,'Keys'=>$backKeys, 'editKeys' => $request->all()]);
				}
			}
           $Keys = Keys::find($request['id']);
            $Keys->update($request->all());
            Activity::log([
                'contentId'   => $Keys->id,
                'contentType' => 'Keys',
                'description' => 'Update a Keys',
                'details'     => 'Username: '.Auth::user()->name,
                'updated'     => (bool) $Keys->id,
            ]);
			####update Multi upload image
			####Upload image
			######update Many to many
            ######store
            $backKeys = Keys::paginate(10);
            $backKeys->setPath('/Keys');
            return view("KeysView::Keysajax")->with(['tab' => 1, 'Keys' => $backKeys, 'flag' => 4]);
        }else{
            $backKeys = Keys::paginate(10);
            return view("KeysView::Keysajax")->with(['tab' => 1, 'Keys' => $backKeys, 'flag' => 6]);
        }
    }	
    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $module_id=\Request::segment(2);
        $module_id=Module::where('link_name',$module_id)->first();
        $Module_type_id=$module_id->id;
         #####relation
         if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2 ) { // if(Auth::user()->can('delete_Keys')) {  #####check permission
            $temp = explode(",", $id);
            foreach ($temp as $val) {
                if ($temp) {
                   ### delete multi uploader
                    $user = Keys::find($val);
                    $user->delete();
                     Activity::log([
                            'contentId'   => $val,
                            'contentType' => 'Keys',
                            'action'      => 'Delete',
                            'description' => 'Delete  a Keys',
                            'details'     => 'Username: '.Auth::user()->name,
                     ]);
                }
            }
            $Keys = Keys::paginate(10);
            $Keys->setPath('/Keys');
            return view("KeysView::Keysajax")->with(['tab' => 1, 'Keys' => $Keys, 'flag' => 5]);
        }else{
            $Keys = Keys::paginate(10);
            return view("KeysView::Keysajax")->with(['tab' => 1, 'Keys' => $Keys, 'flag' => 6]);
        }
    }	
}
