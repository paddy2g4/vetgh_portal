<?php

namespace Keys;

use Illuminate\Support\ServiceProvider;

class KeysServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $this->app->bind('Keys',function($app){
            return new Keys;
        });

        // Define Router file
        require __DIR__ . '/Route/routes.php';

        // Define path for view files
        $this->loadViewsFrom(__DIR__.'/View','KeysView');

        // Define path for translation
        $this->loadTranslationsFrom(__DIR__.'/Lang','KeysLang');

        // Define Published migrations files
        $this->publishes([
            __DIR__.'/Migration' => database_path('migrations'),
        ], 'migrations');

        // Define Published Asset files
        $this->publishes([
            __DIR__.'/Asset' => public_path('assets/'),
        ], 'public');

    }


    public function register()
    {
        //
    }
}