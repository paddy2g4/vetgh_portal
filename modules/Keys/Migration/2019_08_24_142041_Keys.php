<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Keys extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    #####start_up_function#####
        Schema::create('keys', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('entity_id');
            $table->string('client_token', 255);
            $table->string('secret_token', 255);
            $table->string('trans_type', 30);
            $table->string('client_id', 255);
            $table->string('payment_ref', 255);
            $table->integer('user_id');
            $table->boolean('act_status');
            $table->boolean('del_status');
            $table->timestamps();
        });
        DB::table("modules")->insert(
            array("name" =>"Keys","description" =>"Manage all service and payment keys","link_name" => "keys","status"=>1,"created_at"=>"2019-08-24 14:20:41")
        );
		        /**
         * role permission
         */
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'view_Keys','display_name' => 'view_Keys')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'add_Keys','display_name' => 'add_Keys')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'edit_Keys','display_name' => 'edit_Keys')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'delete_Keys','display_name' => 'delete_Keys')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
     #####end_up_function#####
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
     #####start_down_function#####
        DB::table('permissions')->where('name',  'view_Keys')->delete();
        DB::table('permissions')->where('name',  'add_Keys')->delete();
        DB::table('permissions')->where('name',  'edit_Keys')->delete();
        DB::table('permissions')->where('name',  'delete_Keys')->delete();
        ######remove primary key
        Schema::drop('keys');
     #####end_down_function#####
    }
}
