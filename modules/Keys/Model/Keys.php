<?php

namespace Keys\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Database\Eloquent\Model;
#####use
 
class Keys extends Model
{
  use SearchableTrait;
    public $table = 'keys';
    
	
	 public $searchable = [
	    'columns' => [
            'keys.entity_id'=>100,
    'keys.client_token'=>101,
    'keys.secret_token'=>102,
    'keys.trans_type'=>103,
    'keys.client_id'=>104,
    'keys.payment_ref'=>105,
    'keys.user_id'=>106,
    'keys.act_status'=>107,
    'keys.del_status'=>108,
        ########relation_id
        ],
        ########join
	  ];
 

    public $fillable = [
        'entity_id',
        'client_token',
        'secret_token',
        'trans_type',
        'client_id',
        'payment_ref',
        'user_id',
        'act_status',
        'del_status'
    ];

 

    /**
     * Validation rules
     *
     * @var array
     */
    public   $rules = [
        'entity_id' => 'integer|required',
        'client_token' => 'string|required',
        'secret_token' => 'string|required',
        'trans_type' => 'alpha',
        'client_id' => 'string',
        'payment_ref' => 'string'
    ];
 #####relation#####
}
