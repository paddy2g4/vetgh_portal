<?php
Route::group(['prefix' => 'admin'], function () {
####start
   /*
    |--------------------------------------------------------------------------
    | Keys  Routes
    |--------------------------------------------------------------------------
    |*/
    Route::resource('keys','Keys\Controller\KeysController');
    Route::post('keys/search','Keys\Controller\KeysController@search');
	Route::post('keys/export/excel','Keys\Controller\KeysController@export_excel');
####end
#####relation#####
});
 