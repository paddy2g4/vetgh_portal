<?php

namespace Nominees\Controller;


use Nominees\Model\Nominees;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\admin\Module;
use App\admin\ModuleAlbum;
use App\Http\Controllers\Controller;
use Regulus\ActivityLog\Models\Activity;
use Validator;
use Illuminate\Http\Request;
use File;
use App\Http\Requests;
use Illuminate\Pagination\Paginator;
use Zizaco\Entrust\Traits\EntrustTrait;
use Nicolaslopezj\Searchable\SearchableTrait;
use JD\Cloudder\Facades\Cloudder;
#####use
class NomineesController extends Controller
{
    use SearchableTrait;
    //  use EntrustUserTrait ;
    protected $nominee_path = "uploads/voting_nominees";

    /**
     * @param Request $request
     * @return string
     * Export Excel & CSV & PDF
     */
    public function export_excel(Request $request){
        #### $request['export_type'] is export mode  "EXCEL or CSV"
        ### Check export PDF permission
        if($request['export_type']=='pdf'&& !Auth::user()->can('export_pdf') )
            return 'You not have this permission';
        ### Check export CSV permission
        if($request['export_type']=='csv'&& !Auth::user()->can('export_csv') )
            return 'You not have this permission';
        ### Check export EXCEL permission
        if($request['export_type']=='xls'&& !Auth::user()->can('export_xls') )
            return 'You not have this permission';
        if ($request['serach_txt']) {
            $Nominees = Nominees::search($request['serach_txt'], null, false)->get();
        } else {  ###other
            $Nominees = Nominees::all();
        }  if($request['export_type']=='pdf'){ //export PDF
            $html='<h1 style="text-align: center">YEP test pdf </h1>';
            $html .= '<style>
						table, th, td {text-align: center;}
						th, td {padding: 5px;}
						th {color: #43A047;border-color: black;background-color: #C5E1A5}
						</style>
						<table border="2" style="width:100%;">
						<tr>
							<th>Title</th>
							<th>Created date</th>
						</tr>';
            foreach ($Nominees as $cat ){

                $html .="<tr>
							<td>$cat->title</td>
							<td>$cat->created_at</td>
						  </tr>";
            }
            $html .= '</table>';
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadHTML($html);
            return $pdf->download('Nominees.pdf');
        }else { // export excel & csv
            Excel::create('Nominees', function ($excel) use ($Nominees) {
                $excel->sheet('Sheet 1', function ($sheet) use ($Nominees) {
                    $sheet->fromArray($Nominees);
                });
            })->download($request['export_type']);
        }
    }


    /**
     * @param Request $request
     * @return mixed
     * Search and paging
     */
    public function search(Request $request)
    {
        #####relation
        $currentPage = $request['page']; // You can set this to any page you want to paginate to
        Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });
        if ($request['paging'] > 0)
            $limit = $request['paging'];
        else
            $limit = 10;
        ### search


        $Nominees=\DB::table('voting_nominees')
            ->leftJoin('voting_categories', 'voting_categories.cat_id', '=', 'voting_nominees.cat_id')
            ->leftJoin('entity_division', 'voting_categories.entity_div_code', '=', 'entity_division.assigned_code')
            ->select('voting_nominees.*', 'entity_division.div_name as plan_name', 'voting_categories.cat_name', 'voting_categories.cat_id')
            ->where('voting_nominees.del_status', '=', false)
            ->where('entity_division.active_status',true)->where('entity_division.del_status',false)
            ->orderby('voting_nominees.id', 'desc');

        $awards = \DB::table('entity_division')->select('id','plan_name')
            ->leftJoin('entity_master', 'entity_division.entity_id', '=', 'entity_master.entity_id')
            ->select('entity_division.*', 'entity_master.entity_name')
            ->where('entity_division.active_status', '=', true)->where('entity_division.del_status', '=', false);

        $categories = \DB::table('voting_categories')
            ->leftJoin('entity_division', 'entity_division.assigned_code', '=', 'voting_categories.entity_div_code')
            ->select('voting_categories.*','entity_division.div_name as plan_name')
            ->where('voting_categories.active_status', '=', true)->where('voting_categories.del_status', '=', false)
            ->where('entity_division.active_status',true)->where('entity_division.del_status',false)
            ->orderby('voting_categories.cat_id', 'asc')
            ->get();


        if ($request['serach_txt']) {

            $Nominees = $Nominees
                ->where('voting_nominees.nom_name', 'like', '%' .$request['serach_txt'] . '%')
                ->orWhere('voting_nominees.nom_code', 'like', '%' .$request['serach_txt'] . '%')
                ->orWhere('entity_division.div_name', 'like', '%' .$request['serach_txt'] . '%')
                ->orWhere('voting_categories.cat_name', 'like', '%' .$request['serach_txt'] . '%');
            $awards = $awards->orderby('entity_division.id', 'asc');

        } else {  ###other
            $awards = $awards->orderby('entity_division.id', 'asc');
        }

        if (\Session::get('role_id') == 3){
            $Nominees=$Nominees->where('entity_masters.id',\Auth::user()->entity_id)->paginate($limit);
            $awards = $awards->where('entity_masters.id',\Auth::user()->entity_id)->orderby('entity_division.id', 'asc')->get();
        }
        else{
            $Nominees=$Nominees->paginate($limit);
            $awards = $awards->get();
        }

        return view("NomineesView::Nomineesajax")->with(['request' => $request, 'tab' =>1, 'Nominees' => $Nominees, 'voting_categories' => $categories, 'entity_division'=>$awards]);
    }

    /**
     * @return mixed
     */
    public function index()
    {
        #####relation
        if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2 || \Session::get('role_id') == 3) { // if(Auth::user()->can("view_Nominees")) {
            $module_menus=app('App\Http\Controllers\admin\CrudBuilderController')->createMenumodule();
        //    $Nominees = Nominees::paginate(10);

            if (\Session::get('role_id') == 3){

                $Nominees=\DB::table('voting_nominees')
                    ->leftJoin('voting_categories', 'voting_categories.cat_id', '=', 'voting_nominees.cat_id')
                    ->leftJoin('entity_division', 'voting_categories.entity_div_code', '=', 'entity_division.assigned_code')
                    ->leftJoin('entity_master', 'entity_division.entity_id', '=', 'entity_master.entity_id')
                    ->select('voting_nominees.*', 'entity_division.div_name as plan_name', 'voting_categories.cat_name', 'voting_categories.cat_id')
                    ->where('entity_master.entity_id',\Auth::user()->entity_id)
                    ->where('voting_nominees.del_status', '=', false)
                    ->where('entity_division.active_status',true)->where('entity_division.del_status',false)
                    ->orderby('voting_nominees.id', 'desc')
                    ->paginate(10);

                $awards = \DB::table('entity_division')->select('id','plan_name')
                    ->leftJoin('entity_master', 'entity_division.entity_id', '=', 'entity_master.entity_id')
                    ->select('entity_division.*', 'entity_master.entity_name')
                    ->where('entity_division.active_status', '=', true)->where('entity_division.del_status', '=', false)->where('entity_master.entity_id',\Auth::user()->entity_id)->orderby('entity_division.id', 'asc')->get();

                $categories = \DB::table('voting_categories')
                    ->leftJoin('entity_division', 'entity_division.assigned_code', '=', 'voting_categories.entity_div_code')
                    ->select('voting_categories.*','entity_division.div_name as plan_name')
                    ->where('voting_categories.active_status', '=', true)->where('voting_categories.del_status', '=', false)
                    ->where('entity_division.active_status',true)->where('entity_division.del_status',false)
                    ->orderby('voting_categories.cat_id', 'desc')
                    ->get();
            }
            else{
                $Nominees=\DB::table('voting_nominees')
                    ->leftJoin('voting_categories', 'voting_categories.cat_id', '=', 'voting_nominees.cat_id')
                    ->leftJoin('entity_division', 'voting_categories.entity_div_code', '=', 'entity_division.assigned_code')
                    ->select('voting_nominees.*', 'entity_division.div_name as plan_name', 'voting_categories.cat_name', 'voting_categories.cat_id')
                    ->where('voting_nominees.del_status', '=', false)
                    ->where('entity_division.active_status',true)->where('entity_division.del_status',false)
                    ->orderby('voting_nominees.id', 'desc')
                    ->paginate(10);

                $awards = \DB::table('entity_division')->select('id','plan_name')
                    ->leftJoin('entity_master', 'entity_division.entity_id', '=', 'entity_master.entity_id')
                    ->select('entity_division.*', 'entity_master.entity_name')
                    ->where('entity_division.active_status', '=', true)->where('entity_division.del_status', '=', false)->orderby('entity_division.id', 'asc')->get();

                $categories = \DB::table('voting_categories')
                    ->leftJoin('entity_division', 'entity_division.assigned_code', '=', 'voting_categories.entity_div_code')
                    ->select('voting_categories.*','entity_division.div_name as plan_name')
                    ->where('voting_categories.active_status', '=', true)->where('voting_categories.del_status', '=', false)
                    ->where('entity_division.active_status',true)->where('entity_division.del_status',false)
                    ->orderby('voting_categories.cat_id', 'desc')
                    ->get();
            }


        //    dd($categories);

            return view("NomineesView::Nominees")->with(['module_menus'=>$module_menus,'tab' => 1, 'Nominees' => $Nominees, 'categories' => $categories, 'awards'=>$awards]);
        }else{
            return redirect('404');
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {

        if ($request->btnupdate == "update"){
            $result = $this->update($request, $request->id);
            \Log::info("LINE ".__LINE__.". In NomineesController: store");
            return $result;
        }

        $module_id=\Request::segment(2);
        $module_id=Module::where('link_name',$module_id)->first();
        // $Module_type_id=$module_id->id;
        #####relation
        $rules = array(
            'nom_name'=>['required','min:1','max:200',"regex:/^[\p{L}\p{P}\p{Zs}\p{N}]+$/"],
            'avatar' => ['max:255',"regex:/^[\p{L}\p{P}\p{Zs}\p{N}]+$/"],
            'cat_id' => ['required',"string"],
            'image_name'=>'required|mimes:jpeg,bmp,jpg,png|between:1, 6000',
        );

        $Nominees=new Nominees;
        if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2 ) { // if(Auth::user()->can("add_Nominees")){  #####check permission
            if($Nominees->rules){
                $validator = Validator::make($request->all(), $rules);
                if ($validator->fails()) {
                    $backNominees = Nominees::paginate(10);
                    return view("NomineesView::Nomineesajax")->withErrors($validator)->with(['Nominees'=>$backNominees,'tab'=>2,'editNominees'=>$request->all()]);
                }
            }

            //  $Nominees=Nominees::create($request->all());
            $user_id = \Auth::user()->id;

            $entity_div_code = \DB::table('voting_categories')->where('cat_id',$request->cat_id)->value('entity_div_code'); //select entity_div_code from voting_categories where cat_id = '000000001';


            #SAVE IMAGE
            $image_name = $request->file('image_name') ? $request->file('image_name')->getRealPath() : null;
            if(!empty($image_name)){

            //    Cloudder::upload($image_name, null);
                Cloudder::upload($image_name,null,[
                    "folder"=>"vetgh/nominees"
                ]);
                $tag ="VETGH";
                Cloudder::addTag($tag, Cloudder::getPublicId(), null);

                list($width, $height) = getimagesize($image_name);
                $image_url= Cloudder::show(Cloudder::getPublicId(), ["width" => $width, "height"=>$height]);
                \Log::info("Nominees Controller: image_url = ".$image_url);

                if($image_url){
                    $Nominees->image_path = $image_url;
                }
            }

            $searchForValue = ',';
            if( strpos($request->nom_name, $searchForValue) !== false ) {
                $nom_arr = explode (",", $request->nom_name);

                foreach ($nom_arr as $nom_name){
                   //dd($nom_name);
                    $Nominees=new Nominees;
                    $current_nom_code_str = \DB::table('voting_nominees')->where('entity_div_code',$entity_div_code)->orderBy('id', 'desc')->value('nom_code'); //select entity_div_code from voting_categories where cat_id = '000000001';
                    $count_str = strlen($current_nom_code_str);
                    $code_length = $count_str - 2;
                    $current_nom_code=substr($current_nom_code_str, 2, $code_length);
                    $div_initials=\DB::table('entity_division')->where('assigned_code',$entity_div_code)->orderBy('id', 'desc')->value('div_initials');
                    \Log::info("entity_div_code = $entity_div_code");
                    \Log::info("current_nom_code = $current_nom_code, div_initials = $div_initials");
                    $new_nom_code_part = intval($current_nom_code) + 1;
                    $new_nom_code = $div_initials.''.$new_nom_code_part;
                    \Log::info("new_nom_code = $new_nom_code");

                    $Nominees->nom_name = $nom_name;
                    $Nominees->nom_code = $new_nom_code;
                    $Nominees->cat_id = $request->cat_id;
                    $Nominees->entity_div_code = $entity_div_code;
                    $Nominees->user_id = $user_id;

                    $Nominees->save();

                    \Log::info($Nominees);

                    Activity::log([
                        'contentId'   => $Nominees->id,
                        'contentType' => 'Nominees',
                        'action'      => 'Create',
                        'description' => 'Created a Nominees',
                        'details'     => 'User ID: '.Auth::user()->id,
                    ]);
                }

            }else{

                $current_nom_code_str = \DB::table('voting_nominees')->where('entity_div_code',$entity_div_code)->orderBy('id', 'desc')->value('nom_code'); //select entity_div_code from voting_categories where cat_id = '000000001';
                \Log::info("current_nom_code_str = $current_nom_code_str");
                $count_str = strlen($current_nom_code_str);
                $code_length = $count_str - 2;
                $current_nom_code=substr($current_nom_code_str, 2, $code_length);
                $div_initials=\DB::table('entity_division')->where('assigned_code',$entity_div_code)->orderBy('id', 'desc')->value('div_initials');
                $new_nom_code_part = intval($current_nom_code) + 1;
                $new_nom_code = $div_initials.''.$new_nom_code_part;

                \Log::info("entity_div_code = $entity_div_code");
                \Log::info("count_str = $count_str, code_length = $code_length, current_nom_code_str = $current_nom_code_str, current_nom_code = $current_nom_code, div_initials = $div_initials, new_nom_code_part = $new_nom_code_part, new_nom_code = $new_nom_code");

                $Nominees->nom_name = $request->nom_name;
                $Nominees->nom_code = $new_nom_code;
                $Nominees->cat_id = $request->cat_id;
                $Nominees->entity_div_code = $entity_div_code;
                $Nominees->user_id = $user_id;

                $Nominees->save();

                Activity::log([
                    'contentId'   => $Nominees->id,
                    'contentType' => 'Nominees',
                    'action'      => 'Create',
                    'description' => 'Created a Nominees',
                    'details'     => 'User ID: '.Auth::user()->id,
                ]);
            }

            // dd($request->all());
//            $Nominees->nom_name = $request->nom_name;
//            $Nominees->cat_id = $request->cat_id;
//            $Nominees->entity_div_code = $entity_div_code;
////            $Nominees->image_path = $this->nominee_path.'/'.$request->avatar;
//            $Nominees->user_id = $user_id;
//
//            #SAVE IMAGE
//            $image_name = $request->file('image_name') ? $request->file('image_name')->getRealPath() : null;
//            if(!empty($image_name)){
//
//                Cloudder::upload($image_name, null);
//                $tag ="CASTVOTEGH";
//                Cloudder::addTag($tag, Cloudder::getPublicId(), null);
//
//                list($width, $height) = getimagesize($image_name);
//                $image_url= Cloudder::show(Cloudder::getPublicId(), ["width" => $width, "height"=>$height]);
//                \Log::info("Nominees Controller: image_url = ".$image_url);
//
//                if($image_url){
//                    $Nominees->image_path = $image_url;
//                }
//            }
//
//            $Nominees->save();
//
//            Activity::log([
//                'contentId'   => $Nominees->id,
//                'contentType' => 'Nominees',
//                'action'      => 'Create',
//                'description' => 'Created a Nominees',
//                'details'     => 'User ID: '.Auth::user()->id,
//            ]);

            //  


            ####Upload image
            ####Multi upload image
            ######store
//            $Nominees = Nominees::paginate(10);

//            if (file_exists('temp/'.$request["avatar"]) && $request["avatar"] != "")
//                File::move("temp/" . $request["avatar"], $this->nominee_path . $request["avatar"]);
//
//            $Nominees=\DB::table('voting_nominees')
//                ->leftJoin('voting_categories', 'voting_categories.cat_id', '=', 'voting_nominees.cat_id')
//                ->leftJoin('entity_division', 'voting_categories.entity_div_code', '=', 'entity_division.assigned_code')
//                ->select('voting_nominees.*', 'entity_division.div_name as plan_name', 'voting_categories.cat_name')
//                ->where('voting_nominees.del_status', '=', false)
//                ->where('entity_division.active_status',true)->where('entity_division.del_status',false)
//                ->orderby('voting_nominees.id', 'desc')
//                ->paginate(10);
//
//            $categories = \DB::table('voting_categories')
//                ->leftJoin('entity_division', 'entity_division.assigned_code', '=', 'voting_categories.entity_div_code')
//                ->select('voting_categories.*','entity_division.div_name as plan_name')
//                ->where('voting_categories.active_status', '=', true)->where('voting_categories.del_status', '=', false)
//                ->where('entity_division.active_status',true)->where('entity_division.del_status',false)
//                ->orderby('voting_categories.cat_id', 'asc')
//                ->get();
//
////            return view("NomineesView::Nomineesajax")->with(['tab'=>1,'flag'=>3,'Nominees'=>$Nominees, 'voting_categories'=>$categories]);
//            return redirect()->back()->with(['tab'=>1,'flag'=>3,'Nominees'=>$Nominees, 'voting_categories'=>$categories]);


            $module_menus=app('App\Http\Controllers\admin\CrudBuilderController')->createMenumodule();
//            $Nominees = Nominees::paginate(10);

            if (\Session::get('role_id') == 3){

                $Nominees=\DB::table('voting_nominees')
                    ->leftJoin('voting_categories', 'voting_categories.cat_id', '=', 'voting_nominees.cat_id')
                    ->leftJoin('entity_division', 'voting_categories.entity_div_code', '=', 'entity_division.assigned_code')
                    ->leftJoin('entity_master', 'entity_division.entity_id', '=', 'entity_master.entity_id')
                    ->select('voting_nominees.*', 'entity_division.div_name as plan_name', 'voting_categories.cat_name', 'voting_categories.cat_id')
                    ->where('entity_master.entity_id',\Auth::user()->entity_id)
                    ->where('voting_nominees.del_status', '=', false)
                    ->where('entity_division.active_status',true)->where('entity_division.del_status',false)
                    ->orderby('voting_nominees.id', 'desc')
                    ->paginate(10);

                $awards = \DB::table('entity_division')->select('id','plan_name')
                    ->leftJoin('entity_master', 'entity_division.entity_id', '=', 'entity_master.entity_id')
                    ->select('entity_division.*', 'entity_master.entity_name')
                    ->where('entity_division.active_status', '=', true)->where('entity_division.del_status', '=', false)->where('entity_master.entity_id',\Auth::user()->entity_id)->orderby('entity_division.id', 'asc')->get();

                $categories = \DB::table('voting_categories')
                    ->leftJoin('entity_division', 'entity_division.assigned_code', '=', 'voting_categories.entity_div_code')
                    ->select('voting_categories.*','entity_division.div_name as plan_name')
                    ->where('voting_categories.active_status', '=', true)->where('voting_categories.del_status', '=', false)
                    ->where('entity_division.active_status',true)->where('entity_division.del_status',false)
                    ->orderby('voting_categories.cat_id', 'desc')
                    ->get();
            }
            else{
                $Nominees=\DB::table('voting_nominees')
                    ->leftJoin('voting_categories', 'voting_categories.cat_id', '=', 'voting_nominees.cat_id')
                    ->leftJoin('entity_division', 'voting_categories.entity_div_code', '=', 'entity_division.assigned_code')
                    ->select('voting_nominees.*', 'entity_division.div_name as plan_name', 'voting_categories.cat_name', 'voting_categories.cat_id')
                    ->where('voting_nominees.del_status', '=', false)
                    ->where('entity_division.active_status',true)->where('entity_division.del_status',false)
                    ->orderby('voting_nominees.id', 'desc')
                    ->paginate(10);

                $awards = \DB::table('entity_division')->select('id','plan_name')
                    ->leftJoin('entity_master', 'entity_division.entity_id', '=', 'entity_master.entity_id')
                    ->select('entity_division.*', 'entity_master.entity_name')
                    ->where('entity_division.active_status', '=', true)->where('entity_division.del_status', '=', false)->orderby('entity_division.id', 'asc')->get();

                $categories = \DB::table('voting_categories')
                    ->leftJoin('entity_division', 'entity_division.assigned_code', '=', 'voting_categories.entity_div_code')
                    ->select('voting_categories.*','entity_division.div_name as plan_name')
                    ->where('voting_categories.active_status', '=', true)->where('voting_categories.del_status', '=', false)
                    ->where('entity_division.active_status',true)->where('entity_division.del_status',false)
                    ->orderby('voting_categories.cat_id', 'desc')
                    ->get();
            }


            return view("NomineesView::Nominees")->with(['module_menus'=>$module_menus,'tab' => 1, 'flag'=>3, 'Nominees' => $Nominees, 'categories' => $categories, 'awards'=>$awards]);

        }else{
            $Nominees = Nominees::paginate(10);
            return view("NomineesView::Nomineesajax")->with(['tab'=>1,'flag'=>6,'Nominees'=>$Nominees]);
        }

    }

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function edit(Request $request, $id)
    {
        #####relation
        #####edit many to many
        // $Nominees = Nominees::paginate(10);

        $Nominees=\DB::table('voting_nominees')
            ->leftJoin('voting_categories', 'voting_categories.cat_id', '=', 'voting_nominees.cat_id')
            ->leftJoin('entity_division', 'voting_categories.entity_div_code', '=', 'entity_division.assigned_code')
            ->select('voting_nominees.*', 'entity_division.div_name as plan_name', 'voting_categories.cat_name')
            ->where('voting_nominees.del_status', '=', false)
            ->where('entity_division.active_status',true)->where('entity_division.del_status',false)
            ->orderby('voting_nominees.id', 'desc')
            ->paginate(10);
        $editNominees = Nominees::find($id);

        $categories = \DB::table('voting_categories')
            ->leftJoin('entity_division', 'entity_division.assigned_code', '=', 'voting_categories.entity_div_code')
            ->select('voting_categories.*','entity_division.div_name as plan_name')
            ->where('voting_categories.active_status', '=', true)->where('voting_categories.del_status', '=', false)
            ->where('entity_division.active_status',true)->where('entity_division.del_status',false)
            ->orderby('voting_categories.cat_id', 'asc')
            ->get();

        // \Log::info($editNominees);

        ####Edit multiple upload image
        return view("NomineesView::Nomineesajax")->with(['editNominees'=>$editNominees,'tab'=>2,'Nominees'=>$Nominees, 'categories'=>$categories]);
    }
    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function update(Request $request, $id)
    {
        $module_menus=app('App\Http\Controllers\admin\CrudBuilderController')->createMenumodule();
        #####relation
        $Nominees=new Nominees;
        if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2 ) { // if(Auth::user()->can('edit_Nominees')) {  #####check permission
            if($Nominees->rules){
                $validator = Validator::make($request->all(),$Nominees->rules);
                if ($validator->fails()) {
                    $backNominees = Nominees::paginate(10);
                    return view("NomineesView::Nomineesajax")->withErrors($validator)->with(['tab' => 2,'Nominees'=>$backNominees, 'editNominees' => $request->all()]);
                }
            }


            $user_id = \Auth::user()->id;

            $Nominees = Nominees::find($request['id']);
            //$Nominees->update($request->all());

            $Nominees->nom_name = $request->nom_name;
            $Nominees->cat_id = $request->cat_id;
//            $Nominees->image_path = $this->nominee_path.'/'.$request->avatar;
            $Nominees->user_id = $user_id;
            $Nominees->save();


            $image_name = $request->file('image_name') ? $request->file('image_name')->getRealPath() : null;
//            dd($request);
            \Log::info("Line ".__LINE__ .". In NomineesController: update: Nominee has been updated, image_name: $image_name");

            if($image_name){

                \Log::info("Line ".__LINE__ .". In NomineesController: update");

                Cloudder::upload($image_name, null);
                $tag ="CASTVOTEGH";
                Cloudder::addTag($tag, Cloudder::getPublicId(), null);

                list($width, $height) = getimagesize($image_name);
                $image_url= Cloudder::show(Cloudder::getPublicId(), ["width" => $width, "height"=>$height]);

                \Log::info("Line ".__LINE__ .". In NomineesController: update");

                if($image_url){
                    $Nominees = Nominees::find($request['id']);
                    \Log::info("Line ".__LINE__ .". Nominees Controller: image_url = ".$image_url. " Nominees ID: ".$Nominees->id);
                    $Nominees->image_path = $image_url;
                    $Nominees->save();
                }

            }

            Activity::log([
                'contentId'   => $Nominees->id,
                'contentType' => 'Nominees',
                'description' => 'Update a Nominees',
                'details'     => 'Username: '.Auth::user()->name,
                'updated'     => (bool) $Nominees->id,
            ]);


            ####update Multi upload image
            ####Upload image
            ######update Many to many
            ######store
//            $backNominees = Nominees::paginate(10);

            if (file_exists('temp/'.$request["avatar"]) && $request["avatar"] != "")
                File::move("temp/" . $request["avatar"], $this->nominee_path . $request["avatar"]);

            $backNominees=\DB::table('voting_nominees')
                ->leftJoin('voting_categories', 'voting_categories.cat_id', '=', 'voting_nominees.cat_id')
                ->leftJoin('entity_division', 'voting_categories.entity_div_code', '=', 'entity_division.assigned_code')
                ->select('voting_nominees.*', 'entity_division.div_name as plan_name', 'voting_categories.cat_name')
                ->where('voting_nominees.del_status', '=', false)
                ->where('entity_division.active_status',true)->where('entity_division.del_status',false)
                ->orderby('voting_nominees.id', 'desc')
                ->paginate(10);

            $categories = \DB::table('voting_categories')
                ->leftJoin('entity_division', 'entity_division.assigned_code', '=', 'voting_categories.entity_div_code')
                ->select('voting_categories.*','entity_division.div_name as plan_name')
                ->where('voting_categories.active_status', '=', true)->where('voting_categories.del_status', '=', false)
                ->where('entity_division.active_status',true)->where('entity_division.del_status',false)
                ->orderby('voting_categories.cat_id', 'asc')
                ->get();

            $backNominees->setPath('/Nominees');
            // return redirect()->back()->with(['status'=>'Record updated successful', 'tab'=>1,'flag'=>4, 'Nominees' => $backNominees,'voting_categories'=>$categories]);
            return view("NomineesView::Nominees")->with(['module_menus'=>$module_menus,'tab' => 1, 'flag'=>4, 'Nominees' => $backNominees, 'categories' => $categories]);
        }else{
            $backNominees = Nominees::paginate(10);
            return redirect()->back()->with(['status'=>'Record updated successful', 'tab'=>1,'flag'=>6, 'Nominees' => $backNominees,'voting_categories'=>$categories]);
        }
    }


    public function get_category_by_plan($plan_id)
    {

        $categories=\DB::table('voting_categories')
//            ->leftJoin('voting_categories', 'voting_categories.cat_id', '=', 'voting_nominees.cat_id')
//            ->leftJoin('entity_division', 'voting_categories.entity_div_code', '=', 'entity_division.assigned_code')
            ->select('voting_categories.cat_id', 'voting_categories.cat_name')
            ->where('plan_id', $plan_id)
            ->where('active_status',true)
            ->where('del_status', false)
            ->get();

//        dd($plan_id, $categories);

//        return response()->json($categories);
        return response()->json(json_encode($categories));

    }


    public function edit_status(Request $request)
    {
        // $module_id=\Request::segment(2);
        // $module_id=Module::where('link_name',$module_id)->first();
        // $Module_type_id=$module_id->id;
        #####relation
        $rules = array(
            'id'=>'required|Integer',
            "btnstatus" => 'required|alpha'
        );

        if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2 ) { // if(Auth::user()->can('edit_Nominees')) {  #####check permission
            if($rules){
                $validator = Validator::make($request->all(),$rules);
                if ($validator->fails()) {
                    $backNominees = Nominees::paginate(10);
                    return view("NomineesView::Nomineesajax")->withErrors($validator)->with(['tab' => 2,'Nominees'=>$backNominees, 'editNominees' => $request->all()]);
                }
            }

            $user_id = \Auth::user()->id;

            if ($request->btnstatus == "disable"){
                $message = "Record Disabled successfully";
                $action = false;
            }else{
                $message = "Record Enabled successfully";
                $action = true;
            }

            $Nominees = Nominees::find($request['id']);
            $Nominees->active_status = $action;
        //    $Nominees->updated_at = $request->entity_name;
            $Nominees->user_id = $user_id;
            $Nominees->save();


            Activity::log([
                'contentId'   => $Nominees->id,
                'contentType' => 'Nominees',
                'description' => 'Nominee status edited.',
                'details'     => 'Username: '.Auth::user()->name,
                'updated'     => (bool) $Nominees->id,
            ]);


            $backNominees=\DB::table('voting_nominees')
                ->leftJoin('voting_categories', 'voting_categories.cat_id', '=', 'voting_nominees.cat_id')
                ->leftJoin('entity_division', 'voting_categories.entity_div_code', '=', 'entity_division.assigned_code')
                ->select('voting_nominees.*', 'entity_division.div_name as plan_name', 'voting_categories.cat_name')
                ->where('voting_nominees.del_status', '=', false)
                ->where('entity_division.active_status',true)->where('entity_division.del_status',false)
                ->orderby('voting_nominees.id', 'desc')
                ->paginate(10);

            $categories = \DB::table('voting_categories')
                ->leftJoin('entity_division', 'entity_division.assigned_code', '=', 'voting_categories.entity_div_code')
                ->select('voting_categories.*','entity_division.div_name as plan_name')
                ->where('voting_categories.active_status', '=', true)->where('voting_categories.del_status', '=', false)
                ->where('entity_division.active_status',true)->where('entity_division.del_status',false)
                ->orderby('voting_categories.cat_id', 'asc')
                ->get();

            $backNominees->setPath('/Nominees');
            return redirect()->back()->with(['status'=>$message, 'tab'=>1,'Nominees' => $backNominees,'voting_categories'=>$categories, 'flag' => 4]);
        }else{
            $backNominees = Nominees::paginate(10);
            return view("NomineesView::Nomineesajax")->with(['tab' => 1, 'Nominees' => $backNominees, 'flag' => 6]);
        }
    }



    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $module_id=\Request::segment(2);
        $module_id=Module::where('link_name',$module_id)->first();
        $Module_type_id=$module_id->id;
        #####relation
        if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2 ) { // if(Auth::user()->can('delete_Nominees')) {  #####check permission
            $temp = explode(",", $id);
            foreach ($temp as $val) {
                if ($temp) {
                    ### delete multi uploader
                    $user = Nominees::find($val);
                    $user->delete();
                    Activity::log([
                        'contentId'   => $val,
                        'contentType' => 'Nominees',
                        'action'      => 'Delete',
                        'description' => 'Delete  a Nominees',
                        'details'     => 'Username: '.Auth::user()->name,
                    ]);
                }
            }
            $Nominees = Nominees::paginate(10);
            $Nominees->setPath('/Nominees');
            return view("NomineesView::Nomineesajax")->with(['tab' => 1, 'Nominees' => $Nominees, 'flag' => 5]);
        }else{
            $Nominees = Nominees::paginate(10);
            return view("NomineesView::Nomineesajax")->with(['tab' => 1, 'Nominees' => $Nominees, 'flag' => 6]);
        }
    }
}
