<?php

namespace Nominees\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Database\Eloquent\Model;
#####use
 
class Nominees extends Model
{
  use SearchableTrait;
    public $table = 'voting_nominees';
    
	
	 public $searchable = [
	    'columns' => [
            'voting_nominees.nom_id'=>100,
    'voting_nominees.nom_name'=>101,
    'voting_nominees.nom_desc'=>102,
    'voting_nominees.nom_code'=>102,
//    'voting_nominees.image_path'=>103,
//    'voting_nominees.cat_id'=>104,
//    'voting_nominees.user_id'=>105,
//    'voting_nominees.active_status'=>106,
//    'voting_nominees.del_status'=>107,
        ########relation_id
        ],
        ########join
	  ];
 

    public $fillable = [
        'nom_id',
        'nom_name',
        'nom_code',
        'nom_desc',
        'image_path',
        'cat_id',
        'entity_div_code',
        'user_id',
        'active_status',
        'del_status'
    ];

 

    /**
     * Validation rules
     *
     * @var array
     */
    public   $rules = [
        
    ];
 #####relation#####
}
