<?php

namespace Nominees;

use Illuminate\Support\ServiceProvider;

class NomineesServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $this->app->bind('Nominees',function($app){
            return new Nominees;
        });

        // Define Router file
        require __DIR__ . '/Route/routes.php';

        // Define path for view files
        $this->loadViewsFrom(__DIR__.'/View','NomineesView');

        // Define path for translation
        $this->loadTranslationsFrom(__DIR__.'/Lang','NomineesLang');

        // Define Published migrations files
        $this->publishes([
            __DIR__.'/Migration' => database_path('migrations'),
        ], 'migrations');

        // Define Published Asset files
        $this->publishes([
            __DIR__.'/Asset' => public_path('assets/'),
        ], 'public');

    }


    public function register()
    {
        //
    }
}