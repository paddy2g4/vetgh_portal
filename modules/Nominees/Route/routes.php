<?php
Route::group(['prefix' => 'admin'], function () {
####start
   /*
    |--------------------------------------------------------------------------
    | Nominees  Routes
    |--------------------------------------------------------------------------
    |*/
    Route::resource('nominees','Nominees\Controller\NomineesController');
    Route::post('nominees/search','Nominees\Controller\NomineesController@search');
	Route::post('nominees/export/excel','Nominees\Controller\NomineesController@export_excel');

    Route::get('nominees/get_category_by_plan/{id}','Nominees\Controller\NomineesController@get_category_by_plan');

    Route::post('nominees/action_status','Nominees\Controller\NomineesController@edit_status');

####end
#####relation#####
});
 