<!-- Cat Id Field -->
{{--<div class="form-group">--}}
{{--	<label class="col-sm-4 control-label" for="form-field-1">--}}
{{--		Award--}}
{{--	</label>--}}
{{--	<div class="col-sm-8">--}}
{{--		<select name="award_id" id="award_id" onchange="filtercategory()" class="js-example-basic-single col-sm-12">--}}

{{--			@if(isset($awards))--}}
{{--				@foreach($awards as $awards)--}}
{{--					<option  {{isset($editNominees["plan_id"])&&$editNominees["plan_id"]==$awards->plan_id ? "selected" : ""}} value={{$awards->plan_id}}>{{$awards->plan_name}} - {{$awards->entity_name}}</option>--}}
{{--				@endforeach--}}
{{--			@endif--}}

{{--		</select>--}}
{{--	</div>--}}
{{--</div>--}}


<div class="form-group">
	<label class="col-sm-4 control-label" for="form-field-1">
		Category
	</label>
	<div class="col-sm-8">
		<select name="cat_id" class="js-example-basic-single col-sm-12">

			@if(isset($categories))
				@foreach($categories as $categories)
					<option  {{isset($editNominees["cat_id"])&&$editNominees["cat_id"]==$categories->cat_id ? "selected" : ""}} value={{$categories->cat_id}}>{{$categories->cat_name}} - {{$categories->plan_name}}</option>
				@endforeach
			@endif

		</select>
	</div>
</div>

<!-- Nom Name Field -->
<div class="form-group">
	<label class="col-sm-4 control-label" for="form-field-1">
		Nominee
	</label>
	<div class="col-sm-8">
		<input value="{{isset($editNominees['nom_name']) ? $editNominees['nom_name'] : ''}}" name="nom_name" type="text" placeholder="" class="form-control">
	</div>
</div>

<!-- Avatar Field -->
<div class="form-group">
	<label class="col-sm-4 control-label" for="form-field-1">
		Nominee's Avatar
	</label>
	<div class="col-sm-8">
{{--		<div class="dropzone dz-clickable dz-single dz-uploadimage1" data-name="avatar" ></div>--}}
{{--		<input name="avatar" type="hidden" value="{{ isset($editNominees['avatar']) ? $editNominees['avatar'] : ''}}">--}}
		<input type="file" name="image_name" class="form-control" id="name" value="" onchange="loadFile(event)"><br>
		<img id="output" src="#" alt="your image" width="120px" height="120px"/>
	</div>
</div>



<script type="text/javascript">
	$(document).ready(function() {
  		$(".js-example-basic-single").select2();
	});

	var url1 = window.location.href//window.location.pathname;
	var url = url1.substring(0,url1.lastIndexOf('/')+1);

	function filtercategory(){
		selected_award = document.getElementById("award_id").value;
		this.get_selectedcategory(selected_award);
	}


	function get_selectedcategory(plan_id){
		console.log(`Plan Award: ${plan_id} has been selected`);


		var myurl = url + 'nominees/get_category_by_plan/'+plan_id;
		var data1 ="";
		console.log(myurl);
		// var reciever = $('#monthtotalamount');



		$.ajax({
			url: myurl,
			type: 'get',
			dataType: 'json',
			// data: {'plan_id':plan_id},
			success: function (data){
				// console.log(data);

				for (var i = 0; i < data.length; i++) {
					// console.log(data[i].cat_name);
					// options.push('<option value="',
					// 		result[i].ImageFolderID, '">',
					// 		result[i].Name, '</option>');
				}

				// var $award_dropdown = $("#award_id");
				// $.each(data, function() {
				// 	console.log(this.text);
				//
				// 	// $award_dropdown.append($("<option />").val(this.ImageFolderID).text(this.Name));
				// });

				// var amount = data[0];
				// reciever.text('GHS '+amount.getspeakingamountmonth);
				//alert(amount.getspeakingamountmonth);

			}
		});
	}

</script>

<script>
	var loadFile = function(event) {
		var output = document.getElementById('output');
		output.src = URL.createObjectURL(event.target.files[0]);
	};
</script>