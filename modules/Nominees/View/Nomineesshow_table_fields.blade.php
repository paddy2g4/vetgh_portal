
@if(empty($content->avatar))
    <td data-title="logo"><img class="img-circle" src="{{ asset('/uploads/no_avatar.jpg') }}" style="width:50px; height:50px"></td>
@else
    <td data-title="logo"><img class="img-circle" src="{{ asset($content->avatar) }}" style="width:60px; height:60px"</td>
@endif

<td data-title="nom_name">{{$content->nom_name}}</td>

<td data-title="nom_name">{{$content->nom_code}}</td>

<td data-title="nom_name">{{$content->cat_name}}</td>

<td data-title="nom_name">{{$content->plan_name}}</td>

<td data-title="act_status">
    @if($content->active_status == 1)
        Active
    @else
        In Active
    @endif
</td>
