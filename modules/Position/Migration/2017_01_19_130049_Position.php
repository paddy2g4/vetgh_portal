<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Position extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    #####start_up_function#####
        Schema::create('positions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('positionname', 50);
            $table->enum('status', ['YES', 'NO']);
            $table->enum('mountable', ['YES', 'NO']);
            $table->timestamps();
        });
        DB::table("modules")->insert(
            array("name" =>"Position","description" =>"Table to manage records of positions to be vied for in the election.","link_name" => "positions","status"=>1,"created_at"=>"2017-01-19 13:00:49")
        );
		        /**
         * role permission
         */
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'view_Position','display_name' => 'view_Position')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'add_Position','display_name' => 'add_Position')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'edit_Position','display_name' => 'edit_Position')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'delete_Position','display_name' => 'delete_Position')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
     #####end_up_function#####
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
     #####start_down_function#####
        DB::table('permissions')->where('name',  'view_Position')->delete();
        DB::table('permissions')->where('name',  'add_Position')->delete();
        DB::table('permissions')->where('name',  'edit_Position')->delete();
        DB::table('permissions')->where('name',  'delete_Position')->delete();
        ######remove primary key
        Schema::drop('positions');
     #####end_down_function#####
    }
}
