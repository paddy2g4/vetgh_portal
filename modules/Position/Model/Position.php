<?php

namespace Position\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Database\Eloquent\Model;
#####use
 
class Position extends Model
{
  use SearchableTrait;
    public $table = 'positions';
    
	
	 public $searchable = [
	    'columns' => [
            'positions.positionname'=>100,
    'positions.status'=>101,
    'positions.mountable'=>102,
        ########relation_id
        ],
        ########join
	  ];
 

    public $fillable = [
        'positionname',
        'status',
        'mountable'
    ];

 

    /**
     * Validation rules
     *
     * @var array
     */
    public   $rules = [
        'positionname' => 'distinct|required|string',
        'status' => 'required',
        'mountable' => 'required'
    ];
 #####relation#####
}
