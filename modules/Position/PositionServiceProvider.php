<?php

namespace Position;

use Illuminate\Support\ServiceProvider;

class PositionServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $this->app->bind('Position',function($app){
            return new Position;
        });

        // Define Router file
        require __DIR__ . '/Route/routes.php';

        // Define path for view files
        $this->loadViewsFrom(__DIR__.'/View','PositionView');

        // Define path for translation
        $this->loadTranslationsFrom(__DIR__.'/Lang','PositionLang');

        // Define Published migrations files
        $this->publishes([
            __DIR__.'/Migration' => database_path('migrations'),
        ], 'migrations');

        // Define Published Asset files
        $this->publishes([
            __DIR__.'/Asset' => public_path('assets/'),
        ], 'public');

    }


    public function register()
    {
        //
    }
}