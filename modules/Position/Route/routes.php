<?php
Route::group(['prefix' => 'admin'], function () {
####start
   /*
    |--------------------------------------------------------------------------
    | Position  Routes
    |--------------------------------------------------------------------------
    |*/
    Route::resource('positions','Position\Controller\PositionController');
    Route::post('positions/search','Position\Controller\PositionController@search');
	Route::post('positions/export/excel','Position\Controller\PositionController@export_excel');
####end
#####relation#####
});
 