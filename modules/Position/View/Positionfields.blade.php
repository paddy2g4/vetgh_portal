<!-- Positionname Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Positionname
	</label>
	<div class="col-sm-9">
		<input value="{{isset($editPosition['positionname']) ? $editPosition['positionname'] : ''}}" name="positionname" type="text" placeholder="" class="form-control">
	</div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label" for="form-field-1">
       Status
    </label>
    <div class="col-sm-9">
        <select name="status" class="form-control">
            <option  {{isset($editPosition["status"])&&$editPosition["status"]=="YES" ? "selected" : ""}}   value="YES"">YES</option><option  {{isset($editPosition["status"])&&$editPosition["status"]=="NO" ? "selected" : ""}}   value="NO"">NO</option>
        </select>
    </div>
</div>




<div class="form-group">
    <label class="col-sm-3 control-label" for="form-field-1">
       Mountable
    </label>
    <div class="col-sm-9">
        <select name="mountable" class="form-control">
            <option  {{isset($editPosition["mountable"])&&$editPosition["mountable"]=="YES" ? "selected" : ""}}   value="YES"">YES</option><option  {{isset($editPosition["mountable"])&&$editPosition["mountable"]=="NO" ? "selected" : ""}}   value="NO"">NO</option>
        </select>
    </div>
</div>



