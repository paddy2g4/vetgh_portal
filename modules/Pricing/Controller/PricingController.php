<?php

namespace Pricing\Controller;


use Pricing\Model\Pricing;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\admin\Module;
use App\admin\ModuleAlbum;
use App\Http\Controllers\Controller;
use Regulus\ActivityLog\Models\Activity;
use Validator;
use Illuminate\Http\Request;
use File;
use App\Http\Requests;
use Illuminate\Pagination\Paginator;
use Zizaco\Entrust\Traits\EntrustTrait;
use Nicolaslopezj\Searchable\SearchableTrait;
#####use
class PricingController extends Controller
{
        use SearchableTrait;
    //  use EntrustUserTrait ;
	
    /**
     * @param Request $request
     * @return string
     * Export Excel & CSV & PDF
     */
    public function export_excel(Request $request){
        #### $request['export_type'] is export mode  "EXCEL or CSV"
        ### Check export PDF permission
        if($request['export_type']=='pdf'&& !Auth::user()->can('export_pdf') )
            return 'You not have this permission';
        ### Check export CSV permission
        if($request['export_type']=='csv'&& !Auth::user()->can('export_csv') )
            return 'You not have this permission';
        ### Check export EXCEL permission
        if($request['export_type']=='xls'&& !Auth::user()->can('export_xls') )
            return 'You not have this permission';
        if ($request['serach_txt']) {
            $Pricing = Pricing::search($request['serach_txt'], null, false)->get();
        } else {  ###other
            $Pricing = Pricing::all();
        }  if($request['export_type']=='pdf'){ //export PDF
            $html='<h1 style="text-align: center">YEP test pdf </h1>';
            $html .= '<style>
						table, th, td {text-align: center;}
						th, td {padding: 5px;}
						th {color: #43A047;border-color: black;background-color: #C5E1A5}
						</style>
						<table border="2" style="width:100%;">
						<tr>
							<th>Title</th>
							<th>Created date</th>
						</tr>';
            foreach ($Pricing as $cat ){

                $html .="<tr>
							<td>$cat->title</td>
							<td>$cat->created_at</td>
						  </tr>";
            }
			$html .= '</table>';
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadHTML($html);
            return $pdf->download('Pricing.pdf');
        }else { // export excel & csv
            Excel::create('Pricing', function ($excel) use ($Pricing) {
                $excel->sheet('Sheet 1', function ($sheet) use ($Pricing) {
                    $sheet->fromArray($Pricing);
                });
            })->download($request['export_type']);
        }
    }
	
 
    /**
     * @param Request $request
     * @return mixed
     * Search and paging
     */
    public function search(Request $request)
    {
        #####relation
        $currentPage = $request['page']; // You can set this to any page you want to paginate to
        Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });
        if ($request['paging'] > 0)
            $limit = $request['paging'];
        else
            $limit = 10;
        ### search
        if ($request['serach_txt']) {
            $Pricing = Pricing::search($request['serach_txt'], null, false)->get();
            $page = $request->has('page') ? $request->page - 1 : 0;
            $total = $Pricing->count();
            $Pricing = $Pricing->slice($page * $limit, $limit);
            $Pricing = new \Illuminate\Pagination\LengthAwarePaginator($Pricing, $total, $limit);
        } else {  ###other
            $Pricing = Pricing::paginate($limit);
        }
        return view("PricingView::Pricingajax")->with(['request' => $request, 'tab' =>1, 'Pricing' => $Pricing]);
    }
	
    /**
     * @return mixed
     */
    public function index()
    {
        #####relation
        if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2 ) { // if(Auth::user()->can("view_Pricing")) {
            $module_menus=app('App\Http\Controllers\admin\CrudBuilderController')->createMenumodule();
//            $Pricing = Pricing::paginate(10);
            $awards = \DB::table('entity_division')
                ->leftJoin('entity_master', 'entity_master.entity_id', '=', 'entity_division.entity_id')
                ->select('entity_master.entity_name','entity_division.id','entity_division.div_name as plan_name','entity_division.assigned_code as plan_id')
                ->where('entity_division.active_status',true)->where('entity_division.del_status',false)
                ->orderby('entity_division.id', 'entity_division.asc')->get();

            $Pricing=\DB::table('voting_pricing')
                ->leftJoin('entity_division', 'voting_pricing.entity_div_code', '=', 'entity_division.assigned_code')
                ->leftJoin('entity_master', 'entity_master.entity_id', '=', 'entity_division.entity_id')
                ->select('voting_pricing.id','entity_division.div_name','voting_pricing.entity_div_code','voting_pricing.points', 'voting_pricing.unit_cost', 'entity_master.entity_name', 'voting_pricing.active_status')
                ->where('entity_division.active_status',true)
                ->where('entity_division.del_status',false)
//                ->where('pricings.act_status',true)
                ->where('voting_pricing.del_status',false)
                ->paginate(10);

//            dd($Pricing);

            return view("PricingView::Pricing")->with(['module_menus'=>$module_menus,'tab' => 1, 'Pricing' => $Pricing, 'awards'=>$awards]);
        }else{
            return redirect('404');
        }
    }
	
    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
         $module_id=\Request::segment(2);
         $module_id=Module::where('link_name',$module_id)->first();
         $Module_type_id=$module_id->id;
         #####relation
         $rules = array(
            'plan_id' => ['required',"regex:/^[\p{L}\p{P}\p{Zs}\p{N}]+$/"], //'required|unique:pricings',
            'unit_cost' => 'numeric|required'
         );

        $awards = \DB::table('entity_division')
            ->leftJoin('entity_master', 'entity_master.entity_id', '=', 'entity_division.entity_id')
            ->select('entity_master.entity_name','entity_division.id', 'entity_division.active_status', 'entity_division.div_name as plan_name','entity_division.assigned_code as plan_id')
            ->where('entity_division.active_status',true)->where('entity_division.del_status',false)
            ->orderby('entity_division.id', 'entity_division.asc')->get();

		$Pricing=new Pricing;
        if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2 ) { // if(Auth::user()->can("add_Pricing")){  #####check permission
			if($Pricing->rules){
				$validator = Validator::make($request->all(), $rules);
				if ($validator->fails()) {
					$backPricing = Pricing::paginate(10);
					return view("PricingView::Pricingajax")->withErrors($validator)->with(['Pricing'=>$backPricing,'tab'=>2,'editPricing'=>$request->all()]);
				}
			}

             // $Pricing=Pricing::create($request->all());
            $user_id = \Auth::user()->id;

            $Pricing->plan_id = $request->plan_id;
            $Pricing->unit_cost = $request->unit_cost;
            $Pricing->user_id = $user_id;
            $Pricing->save();


	 	     Activity::log([
                'contentId'   => $Pricing->id,
                'contentType' => 'Pricing',
                'action'      => 'Create',
                'description' => 'Created a Pricing',
                'details'     => 'User ID: '.Auth::user()->id,
             ]);
			####Upload image
	 	    ####Multi upload image
            ######store
//            $Pricing = Pricing::paginate(10);


            $Pricing=\DB::table('voting_pricing')
                ->leftJoin('entity_division', 'voting_pricing.entity_div_code', '=', 'entity_division.assigned_code')
                ->leftJoin('entity_master', 'entity_master.entity_id', '=', 'entity_division.entity_id')
                ->select('voting_pricing.id','entity_division.div_name','voting_pricing.entity_div_code','voting_pricing.points', 'voting_pricing.unit_cost', 'entity_master.entity_name', 'voting_pricing.active_status')
                ->where('entity_division.active_status',true)
                ->where('entity_division.del_status',false)
                //->where('pricings.act_status',true)
                ->where('voting_pricing.del_status',false)
                ->paginate(10);


            return view("PricingView::Pricingajax")->with(['tab'=>1,'flag'=>3,'Pricing'=>$Pricing, 'awards'=>$awards]);
        }else{
            $Pricing = Pricing::paginate(10);
            return view("PricingView::Pricingajax")->with(['tab'=>1,'flag'=>6,'Pricing'=>$Pricing, 'awards'=>$awards]);
        }

    }
	
    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function edit(Request $request, $id)
    {
         $module_id=\Request::segment(2);
         $module_id=Module::where('link_name',$module_id)->first();
         //$Module_type_id=$module_id->id;
        #####relation
        #####edit many to many
        $Pricing = Pricing::paginate(10);
        $editPricing = Pricing::find($id);
//        dd($editPricing);

        /*
        $awards = \DB::table('awards')
            ->leftJoin('entity_masters', 'entity_masters.id', '=', 'awards.entity_id')
            ->select('entity_masters.entity_name','awards.id','awards.plan_name','awards.plan_id')
            ->where('awards.act_status',true)->where('awards.del_status',false)
            ->orderby('awards.id', 'awards.asc')->get();
        */
        $awards = \DB::table('entity_division')
            ->leftJoin('entity_master', 'entity_master.entity_id', '=', 'entity_division.entity_id')
            ->select('entity_master.entity_name','entity_division.id', 'entity_division.active_status', 'entity_division.div_name as plan_name','entity_division.assigned_code as plan_id')
            ->where('entity_division.active_status',true)->where('entity_division.del_status',false)
            ->orderby('entity_division.id', 'entity_division.asc')->get();
        //$EditEntity_masters = \DB::table('awards')->select('plan_id')->where('plan_id',$editAwards->entity_id)->where('act_status',true)->where('del_status',false)->first();


        ####Edit multiple upload image
        return view("PricingView::Pricingajax")->with(['editPricing'=>$editPricing,'tab'=>2,'Pricing'=>$Pricing, 'awards'=>$awards]);
    }
    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function update(Request $request, $id)
    {
        $module_id=\Request::segment(2);
        $module_id=Module::where('link_name',$module_id)->first();
        //$Module_type_id=$module_id->id;
		#####relation

        $awards = \DB::table('entity_division')
            ->leftJoin('entity_master', 'entity_master.entity_id', '=', 'entity_division.entity_id')
            ->select('entity_master.entity_name','entity_division.id', 'entity_division.active_status', 'entity_division.div_name as plan_name','entity_division.assigned_code as plan_id')
            ->where('entity_division.active_status',true)->where('entity_division.del_status',false)
            ->orderby('entity_division.id', 'entity_division.asc')->get();

		$Pricing=new Pricing;
        if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2 ) { // if(Auth::user()->can('edit_Pricing')) {  #####check permission

            $rules = array(
                'plan_id' => ['required',"regex:/^[\p{L}\p{P}\p{Zs}\p{N}]+$/"], //'required|unique:pricings',
                'unit_cost' => 'numeric|required'
            );

			if($Pricing->rules){
				$validator = Validator::make($request->all(),$rules);
				if ($validator->fails()) {
					$backPricing = Pricing::paginate(10);
					return view("PricingView::Pricingajax")->withErrors($validator)->with(['tab' => 2,'Pricing'=>$backPricing, 'editPricing' => $request->all()]);
				}
			}
           $Pricing = Pricing::find($request['id']);
            $Pricing->update($request->all());
            Activity::log([
                'contentId'   => $Pricing->id,
                'contentType' => 'Pricing',
                'description' => 'Update a Pricing',
                'details'     => 'Username: '.Auth::user()->name,
                'updated'     => (bool) $Pricing->id,
            ]);
			####update Multi upload image
			####Upload image
			######update Many to many
            ######store
            //$backPricing = Pricing::paginate(10);

            /*
            $awards = \DB::table('entity_division')
                ->leftJoin('entity_master', 'entity_master.entity_id', '=', 'entity_division.entity_id')
                ->select('entity_master.entity_name','entity_division.id', 'entity_division.active_status', 'entity_division.div_name as plan_name','entity_division.assigned_code as plan_id')
                ->where('entity_division.active_status',true)->where('entity_division.del_status',false)
                ->orderby('entity_division.id', 'entity_division.asc')->get();

            $backPricing=\DB::table('pricings')
                ->leftJoin('awards', 'pricings.plan_id', '=', 'awards.plan_id')
                ->leftJoin('entity_masters', 'entity_masters.id', '=', 'awards.entity_id')
                ->select('pricings.id','awards.plan_name','pricings.plan_id','pricings.points', 'pricings.unit_cost', 'entity_masters.entity_name', 'pricings.act_status')
                ->where('awards.act_status',true)
                ->where('awards.del_status',false)
              //->where('pricings.act_status',true)
                ->where('pricings.del_status',false)
                ->paginate(10);
            */

            $backPricing=\DB::table('voting_pricing')
                ->leftJoin('entity_division', 'voting_pricing.entity_div_code', '=', 'entity_division.assigned_code')
                ->leftJoin('entity_master', 'entity_master.entity_id', '=', 'entity_division.entity_id')
                ->select('voting_pricing.id','entity_division.div_name','voting_pricing.entity_div_code','voting_pricing.points', 'voting_pricing.unit_cost', 'entity_master.entity_name', 'voting_pricing.active_status')
                ->where('entity_division.active_status',true)
                ->where('entity_division.del_status',false)
                //->where('pricings.act_status',true)
                ->where('voting_pricing.del_status',false)
                ->paginate(10);


            $backPricing->setPath('/Pricing');
            return view("PricingView::Pricingajax")->with(['tab' => 1, 'Pricing' => $backPricing, 'awards'=>$awards, 'flag' => 4]);
        }else{
            $backPricing = Pricing::paginate(10);
            return view("PricingView::Pricingajax")->with(['tab' => 1, 'Pricing' => $backPricing, 'awards'=>$awards,'flag' => 6]);
        }
    }

    public function edit_status(Request $request){

        $rules = array(
            'id'=>'required|Integer',
            "btnstatus" => 'required|alpha'
        );
        
        $awards = \DB::table('entity_division')
            ->leftJoin('entity_master', 'entity_master.entity_id', '=', 'entity_division.entity_id')
            ->select('entity_master.entity_name','entity_division.id', 'entity_division.active_status', 'entity_division.div_name as plan_name','entity_division.assigned_code as plan_id')
            ->where('entity_division.active_status',true)->where('entity_division.del_status',false)
            ->orderby('entity_division.id', 'entity_division.asc')->get();

        //$Pricing=new Pricing;

        if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2 ) { // if(Auth::user()->can("add_EntityMaster")){  #####check permission
            if($rules){
                $validator = Validator::make($request->all(), $rules);
                if ($validator->fails()) {
                    $backAwards = Awards::paginate(10);
                    return view("AwardsView::Awardsajax")->withErrors($validator)->with(['tab' => 2,'Awards'=>$backAwards, 'editAwards' => $request->all()]);
                }
            }

            $this->user_id = \Auth::user()->id;

            if ($request->btnstatus == "disable"){
                $message = "Record Disabled successfully";
                $active_status = false;
                $del_status = true;
            }else{
                $message = "Record Enabled successfully";
                $active_status = true;
                $del_status = false;
            }

            $Pricing = Pricing::find($request['id']);

            $Pricing->active_status = $active_status;
            $Pricing->del_status = $del_status;
            $Pricing->user_id = $this->user_id;
            $Pricing->save();


            Activity::log([
                'contentId'   => $Pricing->id,
                'contentType' => 'Pricing',
                'action'      => 'Update',
                'description' => 'Pricing Status Changed',
                'details'     => 'Username: '.Auth::user()->name,
            ]);
            /*
            $awards = \DB::table('awards')
                ->leftJoin('entity_masters', 'entity_masters.id', '=', 'awards.entity_id')
                ->select('entity_masters.entity_name','awards.id','awards.plan_name','awards.plan_id')
                ->where('awards.act_status',true)->where('awards.del_status',false)
                ->orderby('awards.id', 'awards.asc')->get();
            */

            $backPricing=\DB::table('voting_pricing')
                ->leftJoin('entity_division', 'voting_pricing.entity_div_code', '=', 'entity_division.assigned_code')
                ->leftJoin('entity_master', 'entity_master.entity_id', '=', 'entity_division.entity_id')
                ->select('voting_pricing.id','entity_division.div_name','voting_pricing.entity_div_code','voting_pricing.points', 'voting_pricing.unit_cost', 'entity_master.entity_name', 'voting_pricing.active_status')
                ->where('entity_division.active_status',true)
                ->where('entity_division.del_status',false)
                //->where('pricings.act_status',true)
                ->where('voting_pricing.del_status',false)
                ->paginate(10);

            $backPricing->setPath('/Pricing');
            return redirect()->back()->with(['status'=>'Record updated successful', 'tab'=>1,'flag'=>4,'Awards'=>$awards,'Pricing' => $backPricing]);
        }else{
            $backPricing=\DB::table('voting_pricing')
                ->leftJoin('entity_division', 'voting_pricing.entity_div_code', '=', 'entity_division.assigned_code')
                ->leftJoin('entity_master', 'entity_master.entity_id', '=', 'entity_division.entity_id')
                ->select('voting_pricing.id','entity_division.div_name','voting_pricing.entity_div_code','voting_pricing.points', 'voting_pricing.unit_cost', 'entity_master.entity_name', 'voting_pricing.active_status')
                ->where('entity_division.active_status',true)
                ->where('entity_division.del_status',false)
//                ->where('pricings.act_status',true)
                ->where('voting_pricing.del_status',false)
                ->paginate(10);
            return view("PricingView::Pricingajax")->with(['tab' => 1, 'Pricing' => $backPricing, 'awards'=>$awards,'flag' => 6]);
        }


    }


    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $module_id=\Request::segment(2);
        $module_id=Module::where('link_name',$module_id)->first();
        $Module_type_id=$module_id->id;
         #####relation
         if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2 ) { // if(Auth::user()->can('delete_Pricing')) {  #####check permission
            $temp = explode(",", $id);
            foreach ($temp as $val) {
                if ($temp) {
                   ### delete multi uploader
                    $user = Pricing::find($val);
                    $user->delete();
                     Activity::log([
                            'contentId'   => $val,
                            'contentType' => 'Pricing',
                            'action'      => 'Delete',
                            'description' => 'Delete  a Pricing',
                            'details'     => 'Username: '.Auth::user()->name,
                     ]);
                }
            }
            $Pricing = Pricing::paginate(10);
            $Pricing->setPath('/Pricing');
            return view("PricingView::Pricingajax")->with(['tab' => 1, 'Pricing' => $Pricing, 'flag' => 5]);
        }else{
            $Pricing = Pricing::paginate(10);
            return view("PricingView::Pricingajax")->with(['tab' => 1, 'Pricing' => $Pricing, 'flag' => 6]);
        }
    }	
}
