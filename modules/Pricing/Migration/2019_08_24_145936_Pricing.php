<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Pricing extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    #####start_up_function#####
        Schema::create('pricings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('plan_id');
            $table->double('points');
            $table->double('unit_cost');
            $table->integer('user_id');
            $table->boolean('act_status');
            $table->boolean('del_status');
            $table->timestamps();
        });
        DB::table("modules")->insert(
            array("name" =>"Pricing","description" =>"Manage Prices/Cost of each award","link_name" => "pricings","status"=>1,"created_at"=>"2019-08-24 14:59:36")
        );
		        /**
         * role permission
         */
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'view_Pricing','display_name' => 'view_Pricing')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'add_Pricing','display_name' => 'add_Pricing')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'edit_Pricing','display_name' => 'edit_Pricing')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'delete_Pricing','display_name' => 'delete_Pricing')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
     #####end_up_function#####
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
     #####start_down_function#####
        DB::table('permissions')->where('name',  'view_Pricing')->delete();
        DB::table('permissions')->where('name',  'add_Pricing')->delete();
        DB::table('permissions')->where('name',  'edit_Pricing')->delete();
        DB::table('permissions')->where('name',  'delete_Pricing')->delete();
        ######remove primary key
        Schema::drop('pricings');
     #####end_down_function#####
    }
}
