<?php

namespace Pricing\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Database\Eloquent\Model;
#####use
 
class Pricing extends Model
{
  use SearchableTrait;
    public $table = 'voting_pricing';
    
	
	 public $searchable = [
	    'columns' => [
            'voting_pricing.plan_id'=>100,
    'voting_pricing.points'=>101,
    'voting_pricing.unit_cost'=>102,
    'voting_pricing.user_id'=>103,
    'voting_pricing.active_status'=>104,
    'voting_pricing.del_status'=>105,
        ########relation_id
        ],
        ########join
	  ];
 

    public $fillable = [
        'plan_id',
        'points',
        'unit_cost',
        'user_id',
        'act_status',
        'del_status'
    ];

 

    /**
     * Validation rules
     *
     * @var array
     */
    public   $rules = [
        'plan_id' => 'required|integer',
        'points' => 'numeric',
        'unit_cost' => 'required|numeric'
    ];
 #####relation#####
}
