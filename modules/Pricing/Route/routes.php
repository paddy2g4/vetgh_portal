<?php
Route::group(['prefix' => 'admin'], function () {
####start
   /*
    |--------------------------------------------------------------------------
    | Pricing  Routes
    |--------------------------------------------------------------------------
    |*/
    Route::resource('pricings','Pricing\Controller\PricingController');
    Route::post('pricings/search','Pricing\Controller\PricingController@search');
	Route::post('pricings/export/excel','Pricing\Controller\PricingController@export_excel');

    Route::post('pricings/action_status','Pricing\Controller\PricingController@edit_status');
####end
#####relation#####
});
 