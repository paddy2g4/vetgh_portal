<!-- Plan Id Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Award
	</label>
	<div class="col-sm-9">
		<select name="plan_id" class="col-sm-12 js-example-basic-single">


			@if(isset($awards))
				@foreach($awards as $awards)
					<option  {{isset($editPricing)&&$editPricing->entity_div_code==$awards->plan_id ?  'selected' :''}}  value="{{$awards->plan_id}}"> {{$awards->plan_name}} - {{$awards->entity_name}}</option>
				@endforeach
			@endif

		</select>
	</div>
</div>

<!-- Points Field -->
<!-- <div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Points
	</label>
	<div class="col-sm-9">
		<input value="{{isset($editPricing['points']) ? $editPricing['points'] : ''}}" name="points" type="text" placeholder="" class="form-control">
	</div>
</div> -->

<!-- Unit Cost Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Cost per vote
	</label>
	<div class="col-sm-9">
		<input value="{{isset($editPricing['unit_cost']) ? $editPricing['unit_cost'] : ''}}" name="unit_cost" type="number" placeholder="" class="form-control">
	</div>
</div>


<script type="text/javascript">
	$(document).ready(function() {
  		$(".js-example-basic-single").select2();
	});
</script>