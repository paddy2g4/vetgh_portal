<?php

namespace SMSAccounts\Controller;


use SMSAccounts\Model\SMSAccounts;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\admin\Module;
use App\admin\ModuleAlbum;
use App\Http\Controllers\Controller;
use Regulus\ActivityLog\Models\Activity;
use Validator;
use Illuminate\Http\Request;
use File;
use App\Http\Requests;
use Illuminate\Pagination\Paginator;
use Zizaco\Entrust\Traits\EntrustTrait;
use Nicolaslopezj\Searchable\SearchableTrait;
#####use
class SMSAccountsController extends Controller
{
        use SearchableTrait;
    //  use EntrustUserTrait ;
	
    /**
     * @param Request $request
     * @return string
     * Export Excel & CSV & PDF
     */
    public function export_excel(Request $request){
        #### $request['export_type'] is export mode  "EXCEL or CSV"
        ### Check export PDF permission
        if($request['export_type']=='pdf'&& !Auth::user()->can('export_pdf') )
            return 'You not have this permission';
        ### Check export CSV permission
        if($request['export_type']=='csv'&& !Auth::user()->can('export_csv') )
            return 'You not have this permission';
        ### Check export EXCEL permission
        if($request['export_type']=='xls'&& !Auth::user()->can('export_xls') )
            return 'You not have this permission';
        if ($request['serach_txt']) {
            $SMSAccounts = SMSAccounts::search($request['serach_txt'], null, false)->get();
        } else {  ###other
            $SMSAccounts = SMSAccounts::all();
        }  if($request['export_type']=='pdf'){ //export PDF
            $html='<h1 style="text-align: center">YEP test pdf </h1>';
            $html .= '<style>
						table, th, td {text-align: center;}
						th, td {padding: 5px;}
						th {color: #43A047;border-color: black;background-color: #C5E1A5}
						</style>
						<table border="2" style="width:100%;">
						<tr>
							<th>Title</th>
							<th>Created date</th>
						</tr>';
            foreach ($SMSAccounts as $cat ){

                $html .="<tr>
							<td>$cat->title</td>
							<td>$cat->created_at</td>
						  </tr>";
            }
			$html .= '</table>';
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadHTML($html);
            return $pdf->download('SMSAccounts.pdf');
        }else { // export excel & csv
            Excel::create('SMSAccounts', function ($excel) use ($SMSAccounts) {
                $excel->sheet('Sheet 1', function ($sheet) use ($SMSAccounts) {
                    $sheet->fromArray($SMSAccounts);
                });
            })->download($request['export_type']);
        }
    }
	
 
    /**
     * @param Request $request
     * @return mixed
     * Search and paging
     */
    public function search(Request $request)
    {
        #####relation
        $currentPage = $request['page']; // You can set this to any page you want to paginate to
        Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });
        if ($request['paging'] > 0)
            $limit = $request['paging'];
        else
            $limit = 10;
        ### search
        if ($request['serach_txt']) {
            $SMSAccounts = SMSAccounts::search($request['serach_txt'], null, false)->get();
            $page = $request->has('page') ? $request->page - 1 : 0;
            $total = $SMSAccounts->count();
            $SMSAccounts = $SMSAccounts->slice($page * $limit, $limit);
            $SMSAccounts = new \Illuminate\Pagination\LengthAwarePaginator($SMSAccounts, $total, $limit);
        } else {  ###other
            $SMSAccounts = SMSAccounts::paginate($limit);
        }
        return view("SMSAccountsView::SMSAccountsajax")->with(['request' => $request, 'tab' =>1, 'SMSAccounts' => $SMSAccounts]);
    }
	
    /**
     * @return mixed
     */
    public function index()
    {
        #####relation
        if (\Session::get('role_id') == 1 ) { // if(Auth::user()->can("view_SMSAccounts")) {
            $module_menus=app('App\Http\Controllers\admin\CrudBuilderController')->createMenumodule();
            $SMSAccounts = SMSAccounts::paginate(10);
            $awards = \DB::table('awards')
                ->leftJoin('entity_masters', 'entity_masters.id', '=', 'awards.entity_id')
                ->select('entity_masters.entity_name','awards.id','awards.plan_name','awards.plan_id')
                ->where('awards.act_status',true)->where('awards.del_status',false)
                ->orderby('awards.id', 'awards.asc')->get();
            return view("SMSAccountsView::SMSAccounts")->with(['module_menus'=>$module_menus,'tab' => 1, 'SMSAccounts' => $SMSAccounts, 'awards' => $awards]);
        }else{
            return redirect('404');
        }
    }
	
    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
         $module_id=\Request::segment(2);
         $module_id=Module::where('link_name',$module_id)->first();
         $Module_type_id=$module_id->id;
         #####relation

        $rules = array(
            'plan_id' => ['required',"regex:/^[\p{L}\p{P}\p{Zs}\p{N}]+$/"],
            'account_name'=>['required','min:1','max:200',"regex:/^[\p{L}\p{P}\p{Zs}\p{N}]+$/"],
            'account_password'=>['max:200',"regex:/^[\p{L}\p{P}\p{Zs}\p{N}]+$/"],
            'sender_id' => ['required','min:1','max:9',"regex:/^[\p{L}\p{P}\p{Zs}\p{N}]+$/"],
        );	

		$SMSAccounts=new SMSAccounts;
        if (\Session::get('role_id') == 1) { // if(Auth::user()->can("add_SMSAccounts")){  #####check permission
			if($SMSAccounts->rules){
				$validator = Validator::make($request->all(), $rules);
				if ($validator->fails()) {
					$backSMSAccounts = SMSAccounts::paginate(10);
					return view("SMSAccountsView::SMSAccountsajax")->withErrors($validator)->with(['SMSAccounts'=>$backSMSAccounts,'tab'=>2,'editSMSAccounts'=>$request->all()]);
				}
			}

            // $SMSAccounts=SMSAccounts::create($request->all());
            
            $user_id = \Auth::user()->id;

            $SMSAccounts->plan_id = $request->plan_id;
            $SMSAccounts->account_name = $request->account_name;
            $SMSAccounts->account_password = $request->account_password;
            $SMSAccounts->sender_id = $request->sender_id;
            $SMSAccounts->user_id = $user_id;
            $SMSAccounts->save();

	 	     Activity::log([
                'contentId'   => $SMSAccounts->id,
                'contentType' => 'SMSAccounts',
                'action'      => 'Create',
                'description' => 'Created a SMSAccounts',
                'details'     => 'Use ID: '.Auth::user()->id,
             ]);
			####Upload image
	 	    ####Multi upload image
            ######store
            $SMSAccounts = SMSAccounts::paginate(10);
            return view("SMSAccountsView::SMSAccountsajax")->with(['tab'=>1,'flag'=>3,'SMSAccounts'=>$SMSAccounts]);
        }else{
            $SMSAccounts = SMSAccounts::paginate(10);
            return view("SMSAccountsView::SMSAccountsajax")->with(['tab'=>1,'flag'=>6,'SMSAccounts'=>$SMSAccounts]);
        }

    }
	
    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function edit(Request $request, $id)
    {
         $module_id=\Request::segment(2);
         $module_id=Module::where('link_name',$module_id)->first();
         $Module_type_id=$module_id->id;
        #####relation
        #####edit many to many
        $SMSAccounts = SMSAccounts::paginate(10);
        $editSMSAccounts = SMSAccounts::find($id);
        ####Edit multiple upload image
        return view("SMSAccountsView::SMSAccountsajax")->with(['editSMSAccounts'=>$editSMSAccounts,'tab'=>2,'SMSAccounts'=>$SMSAccounts]);
    }
    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function update(Request $request, $id)
    {
        $module_id=\Request::segment(2);
        $module_id=Module::where('link_name',$module_id)->first();
        $Module_type_id=$module_id->id;
		#####relation
		$SMSAccounts=new SMSAccounts;
        if (\Session::get('role_id') == 1 ) { // if(Auth::user()->can('edit_SMSAccounts')) {  #####check permission
			if($SMSAccounts->rules){
				$validator = Validator::make($request->all(),$SMSAccounts->rules);
				if ($validator->fails()) {
					$backSMSAccounts = SMSAccounts::paginate(10);
					return view("SMSAccountsView::SMSAccountsajax")->withErrors($validator)->with(['tab' => 2,'SMSAccounts'=>$backSMSAccounts, 'editSMSAccounts' => $request->all()]);
				}
			}
           $SMSAccounts = SMSAccounts::find($request['id']);
            $SMSAccounts->update($request->all());
            Activity::log([
                'contentId'   => $SMSAccounts->id,
                'contentType' => 'SMSAccounts',
                'description' => 'Update a SMSAccounts',
                'details'     => 'Username: '.Auth::user()->name,
                'updated'     => (bool) $SMSAccounts->id,
            ]);
			####update Multi upload image
			####Upload image
			######update Many to many
            ######store
            $backSMSAccounts = SMSAccounts::paginate(10);
            $backSMSAccounts->setPath('/SMSAccounts');
            return view("SMSAccountsView::SMSAccountsajax")->with(['tab' => 1, 'SMSAccounts' => $backSMSAccounts, 'flag' => 4]);
        }else{
            $backSMSAccounts = SMSAccounts::paginate(10);
            return view("SMSAccountsView::SMSAccountsajax")->with(['tab' => 1, 'SMSAccounts' => $backSMSAccounts, 'flag' => 6]);
        }
    }	
    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $module_id=\Request::segment(2);
        $module_id=Module::where('link_name',$module_id)->first();
        $Module_type_id=$module_id->id;
         #####relation
         if (\Session::get('role_id') == 1) { // if(Auth::user()->can('delete_SMSAccounts')) {  #####check permission
            $temp = explode(",", $id);
            foreach ($temp as $val) {
                if ($temp) {
                   ### delete multi uploader
                    $user = SMSAccounts::find($val);
                    $user->delete();
                     Activity::log([
                            'contentId'   => $val,
                            'contentType' => 'SMSAccounts',
                            'action'      => 'Delete',
                            'description' => 'Delete  a SMSAccounts',
                            'details'     => 'Username: '.Auth::user()->name,
                     ]);
                }
            }
            $SMSAccounts = SMSAccounts::paginate(10);
            $SMSAccounts->setPath('/SMSAccounts');
            return view("SMSAccountsView::SMSAccountsajax")->with(['tab' => 1, 'SMSAccounts' => $SMSAccounts, 'flag' => 5]);
        }else{
            $SMSAccounts = SMSAccounts::paginate(10);
            return view("SMSAccountsView::SMSAccountsajax")->with(['tab' => 1, 'SMSAccounts' => $SMSAccounts, 'flag' => 6]);
        }
    }	
}
