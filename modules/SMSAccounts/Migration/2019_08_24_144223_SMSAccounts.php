<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class SMSAccounts extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    #####start_up_function#####
        Schema::create('sms_accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('plan_id');
            $table->string('account_name', 30);
            $table->string('account_password', 30);
            $table->string('sender_id', 30);
            $table->boolean('act_status');
            $table->boolean('del_status');
            $table->timestamps();
        });
        DB::table("modules")->insert(
            array("name" =>"SMSAccounts","description" =>"Manage all SMS accounts","link_name" => "smsaccounts","status"=>1,"created_at"=>"2019-08-24 14:42:23")
        );
		        /**
         * role permission
         */
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'view_SMSAccounts','display_name' => 'view_SMSAccounts')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'add_SMSAccounts','display_name' => 'add_SMSAccounts')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'edit_SMSAccounts','display_name' => 'edit_SMSAccounts')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'delete_SMSAccounts','display_name' => 'delete_SMSAccounts')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
     #####end_up_function#####
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
     #####start_down_function#####
        DB::table('permissions')->where('name',  'view_SMSAccounts')->delete();
        DB::table('permissions')->where('name',  'add_SMSAccounts')->delete();
        DB::table('permissions')->where('name',  'edit_SMSAccounts')->delete();
        DB::table('permissions')->where('name',  'delete_SMSAccounts')->delete();
        ######remove primary key
        Schema::drop('sms_accounts');
     #####end_down_function#####
    }
}
