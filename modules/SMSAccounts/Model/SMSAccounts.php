<?php

namespace SMSAccounts\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Database\Eloquent\Model;
#####use
 
class SMSAccounts extends Model
{
  use SearchableTrait;
    public $table = 'sms_accounts';
    
	
	 public $searchable = [
	    'columns' => [
            'sms_accounts.plan_id'=>100,
    'sms_accounts.account_name'=>101,
    'sms_accounts.account_password'=>102,
    'sms_accounts.sender_id'=>103,
    'sms_accounts.act_status'=>104,
    'sms_accounts.del_status'=>105,
        ########relation_id
        ],
        ########join
	  ];
 

    public $fillable = [
        'plan_id',
        'account_name',
        'account_password',
        'sender_id',
        'act_status',
        'del_status'
    ];

 

    /**
     * Validation rules
     *
     * @var array
     */
    public   $rules = [
        'account_name' => 'required|string',
        'account_password' => 'required|string',
        'sender_id' => 'string|required'
    ];
 #####relation#####
}
