<?php
Route::group(['prefix' => 'admin'], function () {
####start
   /*
    |--------------------------------------------------------------------------
    | SMSAccounts  Routes
    |--------------------------------------------------------------------------
    |*/
    Route::resource('smsaccounts','SMSAccounts\Controller\SMSAccountsController');
    Route::post('smsaccounts/search','SMSAccounts\Controller\SMSAccountsController@search');
	Route::post('smsaccounts/export/excel','SMSAccounts\Controller\SMSAccountsController@export_excel');
####end
#####relation#####
});
 