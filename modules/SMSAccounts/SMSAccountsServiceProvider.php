<?php

namespace SMSAccounts;

use Illuminate\Support\ServiceProvider;

class SMSAccountsServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $this->app->bind('SMSAccounts',function($app){
            return new SMSAccounts;
        });

        // Define Router file
        require __DIR__ . '/Route/routes.php';

        // Define path for view files
        $this->loadViewsFrom(__DIR__.'/View','SMSAccountsView');

        // Define path for translation
        $this->loadTranslationsFrom(__DIR__.'/Lang','SMSAccountsLang');

        // Define Published migrations files
        $this->publishes([
            __DIR__.'/Migration' => database_path('migrations'),
        ], 'migrations');

        // Define Published Asset files
        $this->publishes([
            __DIR__.'/Asset' => public_path('assets/'),
        ], 'public');

    }


    public function register()
    {
        //
    }
}