<?php

namespace ShortCodeMasters\Controller;


use ShortCodeMasters\Model\ShortCodeMasters;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\admin\Module;
use App\admin\ModuleAlbum;
use App\Http\Controllers\Controller;
use Regulus\ActivityLog\Models\Activity;
use Validator;
use Illuminate\Http\Request;
use File;
use App\Http\Requests;
use Illuminate\Pagination\Paginator;
use Zizaco\Entrust\Traits\EntrustTrait;
use Nicolaslopezj\Searchable\SearchableTrait;
#####use
class ShortCodeMastersController extends Controller
{
        use SearchableTrait;
    //  use EntrustUserTrait ;
	
    /**
     * @param Request $request
     * @return string
     * Export Excel & CSV & PDF
     */
    public function export_excel(Request $request){
        #### $request['export_type'] is export mode  "EXCEL or CSV"
        ### Check export PDF permission
        if($request['export_type']=='pdf'&& !Auth::user()->can('export_pdf') )
            return 'You not have this permission';
        ### Check export CSV permission
        if($request['export_type']=='csv'&& !Auth::user()->can('export_csv') )
            return 'You not have this permission';
        ### Check export EXCEL permission
        if($request['export_type']=='xls'&& !Auth::user()->can('export_xls') )
            return 'You not have this permission';
        if ($request['serach_txt']) {
            $ShortCodeMasters = ShortCodeMasters::search($request['serach_txt'], null, false)->get();
        } else {  ###other
            $ShortCodeMasters = ShortCodeMasters::all();
        }  if($request['export_type']=='pdf'){ //export PDF
            $html='<h1 style="text-align: center">YEP test pdf </h1>';
            $html .= '<style>
						table, th, td {text-align: center;}
						th, td {padding: 5px;}
						th {color: #43A047;border-color: black;background-color: #C5E1A5}
						</style>
						<table border="2" style="width:100%;">
						<tr>
							<th>Title</th>
							<th>Created date</th>
						</tr>';
            foreach ($ShortCodeMasters as $cat ){

                $html .="<tr>
							<td>$cat->title</td>
							<td>$cat->created_at</td>
						  </tr>";
            }
			$html .= '</table>';
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadHTML($html);
            return $pdf->download('ShortCodeMasters.pdf');
        }else { // export excel & csv
            Excel::create('ShortCodeMasters', function ($excel) use ($ShortCodeMasters) {
                $excel->sheet('Sheet 1', function ($sheet) use ($ShortCodeMasters) {
                    $sheet->fromArray($ShortCodeMasters);
                });
            })->download($request['export_type']);
        }
    }
	
 
    /**
     * @param Request $request
     * @return mixed
     * Search and paging
     */
    public function search(Request $request)
    {
        #####relation
        $currentPage = $request['page']; // You can set this to any page you want to paginate to
        Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });
        if ($request['paging'] > 0)
            $limit = $request['paging'];
        else
            $limit = 10;
        ### search
        if ($request['serach_txt']) {
            $ShortCodeMasters = ShortCodeMasters::search($request['serach_txt'], null, false)->get();
            $page = $request->has('page') ? $request->page - 1 : 0;
            $total = $ShortCodeMasters->count();
            $ShortCodeMasters = $ShortCodeMasters->slice($page * $limit, $limit);
            $ShortCodeMasters = new \Illuminate\Pagination\LengthAwarePaginator($ShortCodeMasters, $total, $limit);
        } else {  ###other
//            $ShortCodeMasters = ShortCodeMasters::paginate($limit);
            $ShortCodeMasters = \DB::table('short_code_masters')
                ->leftJoin('awards', 'awards.plan_id', '=', 'short_code_masters.plan_id')
                ->leftJoin('entity_masters', 'entity_masters.id', '=', 'awards.entity_id')
                ->select('short_code_masters.short_code_ext', 'short_code_masters.act_status', 'entity_masters.entity_name','awards.id','awards.plan_name','awards.plan_id')
                ->where('short_code_masters.del_status',false)
                ->orderby('short_code_masters.id', 'short_code_masters.asc')->paginate($limit);
        }
        return view("ShortCodeMastersView::ShortCodeMastersajax")->with(['request' => $request, 'tab' =>1, 'ShortCodeMasters' => $ShortCodeMasters]);
    }
	
    /**
     * @return mixed
     */
    public function index()
    {
        #####relation
        if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2 ) { // if(Auth::user()->can("view_ShortCodeMasters")) {
            $module_menus=app('App\Http\Controllers\admin\CrudBuilderController')->createMenumodule();
//            $ShortCodeMasters = ShortCodeMasters::paginate(10);

            $awards = \DB::table('entity_division')
                ->leftJoin('entity_master', 'entity_master.entity_id', '=', 'entity_division.entity_id')
                ->select('entity_master.entity_name','entity_division.id','entity_division.div_name as plan_name','entity_division.assigned_code as plan_id')
                ->where('entity_division.active_status',true)->where('entity_division.del_status',false)
                ->orderby('entity_division.id', 'entity_division.asc')->get();

            $ShortCodeMasters = \DB::table('assigned_service_code')
                ->leftJoin('entity_division', 'entity_division.assigned_code', '=', 'assigned_service_code.entity_div_code')
                ->leftJoin('entity_master', 'entity_master.entity_id', '=', 'entity_division.entity_id')
                ->select('assigned_service_code.id as assigned_service_code_id', 'assigned_service_code.service_code', 'assigned_service_code.active_status', 'entity_master.entity_name','entity_division.id','entity_division.div_name as plan_name','entity_division.assigned_code as plan_id')
                ->where('assigned_service_code.del_status',false)
                ->where('entity_division.active_status',true)->where('entity_division.del_status',false)
                ->orderby('assigned_service_code.id', 'assigned_service_code.asc')->paginate(10);

            return view("ShortCodeMastersView::ShortCodeMasters")->with(['module_menus'=>$module_menus,'tab' => 1, 'ShortCodeMasters' => $ShortCodeMasters, 'awards'=>$awards]);
        }else{
            return redirect('404');
        }
    }
	
    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
         $module_id=\Request::segment(2);
         $module_id=Module::where('link_name',$module_id)->first();
//         $Module_type_id=$module_id->id;
         #####relation

        $rules = array(
            'plan_id' => ['required',"regex:/^[\p{L}\p{P}\p{Zs}\p{N}]+$/"],
            'short_code_ext'=>'required|numeric',
        );	
		$ShortCodeMasters=new ShortCodeMasters;
        if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2 ) { // if(Auth::user()->can("add_ShortCodeMasters")){  #####check permission
			if($ShortCodeMasters->rules){
				$validator = Validator::make($request->all(), $rules);
				if ($validator->fails()) {
					$backShortCodeMasters = ShortCodeMasters::paginate(10);
					return view("ShortCodeMastersView::ShortCodeMastersajax")->withErrors($validator)->with(['ShortCodeMasters'=>$backShortCodeMasters,'tab'=>2,'editShortCodeMasters'=>$request->all()]);
				}
			}

             // $ShortCodeMasters=ShortCodeMasters::create($request->all());
            $user_id = \Auth::user()->id;

            $ShortCodeMasters->entity_div_code = $request->plan_id;
            $ShortCodeMasters->service_code = $request->short_code_ext;
//            $ShortCodeMasters->approved_status = true;
            $ShortCodeMasters->user_id = $user_id;
            $ShortCodeMasters->save();


	 	     Activity::log([
                'contentId'   => $ShortCodeMasters->id,
                'contentType' => 'ShortCodeMasters',
                'action'      => 'Create',
                'description' => 'Created a ShortCodeMasters',
                'details'     => 'Username: '.Auth::user()->id,
             ]);
			####Upload image
	 	    ####Multi upload image
            ######store

            $awards = \DB::table('entity_division')
                ->leftJoin('entity_master', 'entity_master.entity_id', '=', 'entity_division.entity_id')
                ->select('entity_master.entity_name','entity_division.id','entity_division.div_name as plan_name','entity_division.assigned_code as plan_id')
                ->where('entity_division.active_status',true)->where('entity_division.del_status',false)
                ->orderby('entity_division.id', 'entity_division.asc')->get();

            $ShortCodeMasters = \DB::table('assigned_service_code')
                ->leftJoin('entity_division', 'entity_division.assigned_code', '=', 'assigned_service_code.entity_div_code')
                ->leftJoin('entity_master', 'entity_master.entity_id', '=', 'entity_division.entity_id')
                ->select('assigned_service_code.id as assigned_service_code_id', 'assigned_service_code.service_code', 'assigned_service_code.active_status', 'entity_master.entity_name','entity_division.id','entity_division.div_name as plan_name','entity_division.assigned_code as plan_id')
                ->where('assigned_service_code.del_status',false)
                ->where('entity_division.active_status',true)->where('entity_division.del_status',false)
                ->orderby('assigned_service_code.id', 'assigned_service_code.asc')->paginate(10);

            return view("ShortCodeMastersView::ShortCodeMastersajax")->with(['tab'=>1,'flag'=>3, 'ShortCodeMasters' => $ShortCodeMasters, 'awards'=>$awards]);
        }else{

            $awards = \DB::table('entity_division')
                ->leftJoin('entity_master', 'entity_master.entity_id', '=', 'entity_division.entity_id')
                ->select('entity_master.entity_name','entity_division.id','entity_division.div_name as plan_name','entity_division.assigned_code as plan_id')
                ->where('entity_division.active_status',true)->where('entity_division.del_status',false)
                ->orderby('entity_division.id', 'entity_division.asc')->get();

            $ShortCodeMasters = \DB::table('assigned_service_code')
                ->leftJoin('entity_division', 'entity_division.assigned_code', '=', 'assigned_service_code.entity_div_code')
                ->leftJoin('entity_master', 'entity_master.entity_id', '=', 'entity_division.entity_id')
                ->select('assigned_service_code.id as assigned_service_code_id', 'assigned_service_code.service_code', 'assigned_service_code.active_status', 'entity_master.entity_name','entity_division.id','entity_division.div_name as plan_name','entity_division.assigned_code as plan_id')
                ->where('assigned_service_code.del_status',false)
                ->where('entity_division.active_status',true)->where('entity_division.del_status',false)
                ->orderby('assigned_service_code.id', 'assigned_service_code.asc')->paginate(10);

            return view("ShortCodeMastersView::ShortCodeMastersajax")->with(['tab'=>1,'flag'=>6, 'ShortCodeMasters' => $ShortCodeMasters, 'awards'=>$awards]);
        }

    }
	
    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function edit(Request $request, $id)
    {
         $module_id=\Request::segment(2);
         $module_id=Module::where('link_name',$module_id)->first();
//         $Module_type_id=$module_id->id;
        #####relation
        #####edit many to many
//        $ShortCodeMasters = ShortCodeMasters::paginate(10);
        $ShortCodeMasters = \DB::table('assigned_service_code')
            ->leftJoin('entity_division', 'entity_division.assigned_code', '=', 'assigned_service_code.entity_div_code')
            ->leftJoin('entity_master', 'entity_master.entity_id', '=', 'entity_division.entity_id')
            ->select('assigned_service_code.id as assigned_service_code_id', 'assigned_service_code.service_code', 'assigned_service_code.active_status', 'entity_master.entity_name','entity_division.id','entity_division.div_name as plan_name','entity_division.assigned_code as plan_id')
            ->where('assigned_service_code.del_status',false)
            ->where('entity_division.active_status',true)->where('entity_division.del_status',false)
            ->orderby('assigned_service_code.id', 'assigned_service_code.asc')->paginate(10);

        $editShortCodeMasters = ShortCodeMasters::find($id);

        $awards = \DB::table('entity_division')
            ->leftJoin('entity_master', 'entity_master.entity_id', '=', 'entity_division.entity_id')
            ->select('entity_master.entity_name','entity_division.id','entity_division.div_name as plan_name','entity_division.assigned_code as plan_id')
            ->where('entity_division.active_status',true)->where('entity_division.del_status',false)
            ->orderby('entity_division.id', 'entity_division.asc')->get();

//        $request_full_url = \Request::getPathInfo();
//        $var = preg_split("#/#", $request_full_url);

//        dd($request,$editShortCodeMasters, $request["pathInfo"], $request->fullUrl(), \Request::getPathInfo(), $var[3]);
//        dd($request,$editShortCodeMasters, $request["pathInfo"], $request->fullUrl(), $request->url(), Request::getPathInfo());

        ####Edit multiple upload image
        return view("ShortCodeMastersView::ShortCodeMastersajax")->with(['editShortCodeMasters'=>$editShortCodeMasters,'tab'=>2,'ShortCodeMasters'=>$ShortCodeMasters, 'awards'=>$awards]);
    }
    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function update(Request $request, $id)
    {
        $module_id=\Request::segment(2);
        $module_id=Module::where('link_name',$module_id)->first();
        //$Module_type_id=$module_id->id;
		#####relation
		$ShortCodeMasters=new ShortCodeMasters;
        if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2 ) { // if(Auth::user()->can('edit_ShortCodeMasters')) {  #####check permission
			if($ShortCodeMasters->rules){
				$validator = Validator::make($request->all(),$ShortCodeMasters->rules);
				if ($validator->fails()) {
					$backShortCodeMasters = ShortCodeMasters::paginate(10);
					return view("ShortCodeMastersView::ShortCodeMastersajax")->withErrors($validator)->with(['tab' => 2,'ShortCodeMasters'=>$backShortCodeMasters, 'editShortCodeMasters' => $request->all()]);
				}
			}
           $ShortCodeMasters = ShortCodeMasters::find($request['id']);
            $ShortCodeMasters->update($request->all());
            Activity::log([
                'contentId'   => $ShortCodeMasters->id,
                'contentType' => 'ShortCodeMasters',
                'description' => 'Update a ShortCodeMasters',
                'details'     => 'Username: '.Auth::user()->name,
                'updated'     => (bool) $ShortCodeMasters->id,
            ]);
			####update Multi upload image
			####Upload image
			######update Many to many
            ######store
//            $backShortCodeMasters = ShortCodeMasters::paginate(10);

            $awards = \DB::table('entity_division')
                ->leftJoin('entity_master', 'entity_master.entity_id', '=', 'entity_division.entity_id')
                ->select('entity_master.entity_name','entity_division.id','entity_division.div_name as plan_name','entity_division.assigned_code as plan_id')
                ->where('entity_division.active_status',true)->where('entity_division.del_status',false)
                ->orderby('entity_division.id', 'entity_division.asc')->get();

            $backShortCodeMasters = \DB::table('assigned_service_code')
                ->leftJoin('entity_division', 'entity_division.assigned_code', '=', 'assigned_service_code.entity_div_code')
                ->leftJoin('entity_master', 'entity_master.entity_id', '=', 'entity_division.entity_id')
                ->select('assigned_service_code.id as assigned_service_code_id', 'assigned_service_code.service_code', 'assigned_service_code.active_status', 'entity_master.entity_name','entity_division.id','entity_division.div_name as plan_name','entity_division.assigned_code as plan_id')
                ->where('assigned_service_code.del_status',false)
                ->where('entity_division.active_status',true)->where('entity_division.del_status',false)
                ->orderby('assigned_service_code.id', 'assigned_service_code.asc')->paginate(10);

            $backShortCodeMasters->setPath('/ShortCodeMasters');
            return view("ShortCodeMastersView::ShortCodeMastersajax")->with(['tab' => 1, 'ShortCodeMasters' => $backShortCodeMasters, 'flag' => 4, 'awards'=>$awards]);
        }else{
//            $backShortCodeMasters = ShortCodeMasters::paginate(10);

            $awards = \DB::table('entity_division')
                ->leftJoin('entity_master', 'entity_master.entity_id', '=', 'entity_division.entity_id')
                ->select('entity_master.entity_name','entity_division.id','entity_division.div_name as plan_name','entity_division.assigned_code as plan_id')
                ->where('entity_division.active_status',true)->where('entity_division.del_status',false)
                ->orderby('entity_division.id', 'entity_division.asc')->get();

            $backShortCodeMasters = \DB::table('assigned_service_code')
                ->leftJoin('entity_division', 'entity_division.assigned_code', '=', 'assigned_service_code.entity_div_code')
                ->leftJoin('entity_master', 'entity_master.entity_id', '=', 'entity_division.entity_id')
                ->select('assigned_service_code.id as assigned_service_code_id', 'assigned_service_code.service_code', 'assigned_service_code.active_status', 'entity_master.entity_name','entity_division.id','entity_division.div_name as plan_name','entity_division.assigned_code as plan_id')
                ->where('assigned_service_code.del_status',false)
                ->where('entity_division.active_status',true)->where('entity_division.del_status',false)
                ->orderby('assigned_service_code.id', 'assigned_service_code.asc')->paginate(10);

            return view("ShortCodeMastersView::ShortCodeMastersajax")->with(['tab' => 1, 'ShortCodeMasters' => $backShortCodeMasters, 'flag' => 6, 'awards'=>$awards]);
        }
    }	
    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $module_id=\Request::segment(2);
        $module_id=Module::where('link_name',$module_id)->first();
        //$Module_type_id=$module_id->id;
         #####relation
         if (\Session::get('role_id') == 1 || \Session::get('role_id') == 2 ) { // if(Auth::user()->can('delete_ShortCodeMasters')) {  #####check permission
            $temp = explode(",", $id);
            foreach ($temp as $val) {
                if ($temp) {
                   ### delete multi uploader
                    $user = ShortCodeMasters::find($val);
                    $user->delete();
                     Activity::log([
                            'contentId'   => $val,
                            'contentType' => 'ShortCodeMasters',
                            'action'      => 'Delete',
                            'description' => 'Delete  a ShortCodeMasters',
                            'details'     => 'Username: '.Auth::user()->name,
                     ]);
                }
            }
            $ShortCodeMasters = ShortCodeMasters::paginate(10);
            $ShortCodeMasters->setPath('/ShortCodeMasters');
            return view("ShortCodeMastersView::ShortCodeMastersajax")->with(['tab' => 1, 'ShortCodeMasters' => $ShortCodeMasters, 'flag' => 5]);
        }else{
            $ShortCodeMasters = ShortCodeMasters::paginate(10);
            return view("ShortCodeMastersView::ShortCodeMastersajax")->with(['tab' => 1, 'ShortCodeMasters' => $ShortCodeMasters, 'flag' => 6]);
        }
    }	
}
