<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class ShortCodeMasters extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    #####start_up_function#####
        Schema::create('short_code_masters', function (Blueprint $table) {
            $table->increments('id');
            $table->string('plan_id', 200);
            $table->string('short_code_ext', 30);
            $table->boolean('approved_status');
            $table->string('user_id', 200);
            $table->boolean('act_status');
            $table->string('del_status', 30);
            $table->timestamps();
        });
        DB::table("modules")->insert(
            array("name" =>"ShortCodeMasters","description" =>"Manage all Short code extensions","link_name" => "shortcodemasters","status"=>1,"created_at"=>"2019-08-24 11:59:43")
        );
		        /**
         * role permission
         */
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'view_ShortCodeMasters','display_name' => 'view_ShortCodeMasters')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'add_ShortCodeMasters','display_name' => 'add_ShortCodeMasters')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'edit_ShortCodeMasters','display_name' => 'edit_ShortCodeMasters')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'delete_ShortCodeMasters','display_name' => 'delete_ShortCodeMasters')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
     #####end_up_function#####
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
     #####start_down_function#####
        DB::table('permissions')->where('name',  'view_ShortCodeMasters')->delete();
        DB::table('permissions')->where('name',  'add_ShortCodeMasters')->delete();
        DB::table('permissions')->where('name',  'edit_ShortCodeMasters')->delete();
        DB::table('permissions')->where('name',  'delete_ShortCodeMasters')->delete();
        ######remove primary key
        Schema::drop('short_code_masters');
     #####end_down_function#####
    }
}
