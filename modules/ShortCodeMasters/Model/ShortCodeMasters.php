<?php

namespace ShortCodeMasters\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Database\Eloquent\Model;
#####use
 
class ShortCodeMasters extends Model
{
  use SearchableTrait;
    public $table = 'assigned_service_code';
    
	
	 public $searchable = [
	    'columns' => [
            'assigned_service_code.entity_div_code'=>100,
    'assigned_service_code.service_code'=>101,
    'assigned_service_code.comment'=>102,
    'assigned_service_code.user_id'=>103,
    'assigned_service_code.active_status'=>104,
    'assigned_service_code.del_status'=>105,
        ########relation_id
        ],
        ########join
	  ];
 

    public $fillable = [
        'entity_div_code',
        'service_code',
        'comment',
        'user_id',
        'active_status',
        'del_status'
    ];

 

    /**
     * Validation rules
     *
     * @var array
     */
    public   $rules = [
        'entity_div_code' => 'string',
        'service_code' => 'required|string',
        'active_status' => 'required'
    ];
 #####relation#####
}
