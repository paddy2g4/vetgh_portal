<?php
Route::group(['prefix' => 'admin'], function () {
####start
   /*
    |--------------------------------------------------------------------------
    | ShortCodeMasters  Routes
    |--------------------------------------------------------------------------
    |*/
    Route::resource('shortcodemasters','ShortCodeMasters\Controller\ShortCodeMastersController');
    Route::post('shortcodemasters/search','ShortCodeMasters\Controller\ShortCodeMastersController@search');
	Route::post('shortcodemasters/export/excel','ShortCodeMasters\Controller\ShortCodeMastersController@export_excel');
####end
#####relation#####
});
 