<?php

namespace ShortCodeMasters;

use Illuminate\Support\ServiceProvider;

class ShortCodeMastersServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $this->app->bind('ShortCodeMasters',function($app){
            return new ShortCodeMasters;
        });

        // Define Router file
        require __DIR__ . '/Route/routes.php';

        // Define path for view files
        $this->loadViewsFrom(__DIR__.'/View','ShortCodeMastersView');

        // Define path for translation
        $this->loadTranslationsFrom(__DIR__.'/Lang','ShortCodeMastersLang');

        // Define Published migrations files
        $this->publishes([
            __DIR__.'/Migration' => database_path('migrations'),
        ], 'migrations');

        // Define Published Asset files
        $this->publishes([
            __DIR__.'/Asset' => public_path('assets/'),
        ], 'public');

    }


    public function register()
    {
        //
    }
}