<!-- Plan Id Field -->
<div class="form-group">
	<label class="col-sm-4 control-label" for="form-field-1">
		Award
	</label>
	<div class="col-sm-8">
		<select name="plan_id" class="js-example-basic-single col-sm-12">
			@if(isset($awards))
				@foreach($awards as $awards)
					<option  {{isset($editShortCodeMasters)&&$editShortCodeMasters->entity_div_code==$awards->plan_id ?  'selected' :''}}  value="{{$awards->plan_id}}"> {{$awards->plan_name}} - {{$awards->entity_name}}</option>
				@endforeach
			@endif
		</select>
	</div>
</div>

<!-- Short Code Ext Field -->
<div class="form-group">
	<label class="col-sm-4 control-label" for="form-field-1">
		Short Code
	</label>
	<div class="col-sm-8">
		<input value="{{isset($editShortCodeMasters['service_code']) ? $editShortCodeMasters['service_code'] : ''}}" name="short_code_ext" type="text" placeholder="" class="form-control">
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
  		$(".js-example-basic-single").select2();
	});
</script>