<?php

namespace Subjects\Controller;


use Subjects\Model\Subjects;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\admin\Module;
use App\admin\ModuleAlbum;
use App\Http\Controllers\Controller;
use Regulus\ActivityLog\Models\Activity;
use Validator;
use Illuminate\Http\Request;
use File;
use App\Http\Requests;
use Illuminate\Pagination\Paginator;
use Zizaco\Entrust\Traits\EntrustTrait;
use Nicolaslopezj\Searchable\SearchableTrait;
#####use
class SubjectsController extends Controller
{
        use SearchableTrait;
    //  use EntrustUserTrait ;
	
    /**
     * @param Request $request
     * @return string
     * Export Excel & CSV & PDF
     */
    public function export_excel(Request $request){
        #### $request['export_type'] is export mode  "EXCEL or CSV"
        ### Check export PDF permission
        if($request['export_type']=='pdf'&& !Auth::user()->can('export_pdf') )
            return 'You not have this permission';
        ### Check export CSV permission
        if($request['export_type']=='csv'&& !Auth::user()->can('export_csv') )
            return 'You not have this permission';
        ### Check export EXCEL permission
        if($request['export_type']=='xls'&& !Auth::user()->can('export_xls') )
            return 'You not have this permission';
        if ($request['serach_txt']) {
            $Subjects = Subjects::search($request['serach_txt'], null, false)->get();
        } else {  ###other
            $Subjects = Subjects::all();
        }  if($request['export_type']=='pdf'){ //export PDF
            $html='<h1 style="text-align: center">YEP test pdf </h1>';
            $html .= '<style>
						table, th, td {text-align: center;}
						th, td {padding: 5px;}
						th {color: #43A047;border-color: black;background-color: #C5E1A5}
						</style>
						<table border="2" style="width:100%;">
						<tr>
							<th>Title</th>
							<th>Created date</th>
						</tr>';
            foreach ($Subjects as $cat ){

                $html .="<tr>
							<td>$cat->title</td>
							<td>$cat->created_at</td>
						  </tr>";
            }
			$html .= '</table>';
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadHTML($html);
            return $pdf->download('Subjects.pdf');
        }else { // export excel & csv
            Excel::create('Subjects', function ($excel) use ($Subjects) {
                $excel->sheet('Sheet 1', function ($sheet) use ($Subjects) {
                    $sheet->fromArray($Subjects);
                });
            })->download($request['export_type']);
        }
    }
	
 
    /**
     * @param Request $request
     * @return mixed
     * Search and paging
     */
    public function search(Request $request)
    {
        #####relation
        $currentPage = $request['page']; // You can set this to any page you want to paginate to
        Paginator::currentPageResolver(function () use ($currentPage) {
            return $currentPage;
        });
        if ($request['paging'] > 0)
            $limit = $request['paging'];
        else
            $limit = 10;
        ### search
        if ($request['serach_txt']) {
            $Subjects = Subjects::search($request['serach_txt'], null, false)->get();
            $page = $request->has('page') ? $request->page - 1 : 0;
            $total = $Subjects->count();
            $Subjects = $Subjects->slice($page * $limit, $limit);
            $Subjects = new \Illuminate\Pagination\LengthAwarePaginator($Subjects, $total, $limit);
        } else {  ###other
            $Subjects = Subjects::paginate($limit);
        }
        return view("SubjectsView::Subjectsajax")->with(['request' => $request, 'tab' =>1, 'Subjects' => $Subjects]);
    }
	
    /**
     * @return mixed
     */
    public function index()
    {
        #####relation
        if(Auth::user()->can("view_Subjects")) {
            $module_menus=app('App\Http\Controllers\admin\CrudBuilderController')->createMenumodule();
            $Subjects = Subjects::paginate(10);
            return view("SubjectsView::Subjects")->with(['module_menus'=>$module_menus,'tab' => 1, 'Subjects' => $Subjects]);
        }else{
            return redirect('404');
        }
    }
	
    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {
         $module_id=\Request::segment(2);
         $module_id=Module::where('link_name',$module_id)->first();
         $Module_type_id=$module_id->id;
         #####relation
		$Subjects=new Subjects;
        if(Auth::user()->can("add_Subjects")){  #####check permission
			if($Subjects->rules){
				$validator = Validator::make($request->all(), $Subjects->rules);
				if ($validator->fails()) {
					$backSubjects = Subjects::paginate(10);
					return view("SubjectsView::Subjectsajax")->withErrors($validator)->with(['Subjects'=>$backSubjects,'tab'=>2,'editSubjects'=>$request->all()]);
				}
			}

	 	    $Subjects=Subjects::create($request->all());
	 	     Activity::log([
                'contentId'   => $Subjects->id,
                'contentType' => 'Subjects',
                'action'      => 'Create',
                'description' => 'Created a Subjects',
                'details'     => 'Username: '.Auth::user()->name,
             ]);
			####Upload image
	 	    ####Multi upload image
            ######store
            $Subjects = Subjects::paginate(10);
            return view("SubjectsView::Subjectsajax")->with(['tab'=>1,'flag'=>3,'Subjects'=>$Subjects]);
        }else{
            $Subjects = Subjects::paginate(10);
            return view("SubjectsView::Subjectsajax")->with(['tab'=>1,'flag'=>6,'Subjects'=>$Subjects]);
        }

    }
	
    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function edit(Request $request, $id)
    {
         $module_id=\Request::segment(2);
         $module_id=Module::where('link_name',$module_id)->first();
         $Module_type_id=$module_id->id;
        #####relation
        #####edit many to many
        $Subjects = Subjects::paginate(10);
        $editSubjects = Subjects::find($id);
        ####Edit multiple upload image
        return view("SubjectsView::Subjectsajax")->with(['editSubjects'=>$editSubjects,'tab'=>2,'Subjects'=>$Subjects]);
    }
    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function update(Request $request, $id)
    {
        $module_id=\Request::segment(2);
        $module_id=Module::where('link_name',$module_id)->first();
        $Module_type_id=$module_id->id;
		#####relation
		$Subjects=new Subjects;
        if(Auth::user()->can('edit_Subjects')) {  #####check permission
			if($Subjects->rules){
				$validator = Validator::make($request->all(),$Subjects->rules);
				if ($validator->fails()) {
					$backSubjects = Subjects::paginate(10);
					return view("SubjectsView::Subjectsajax")->withErrors($validator)->with(['tab' => 2,'Subjects'=>$backSubjects, 'editSubjects' => $request->all()]);
				}
			}
           $Subjects = Subjects::find($request['id']);
            $Subjects->update($request->all());
            Activity::log([
                'contentId'   => $Subjects->id,
                'contentType' => 'Subjects',
                'description' => 'Update a Subjects',
                'details'     => 'Username: '.Auth::user()->name,
                'updated'     => (bool) $Subjects->id,
            ]);
			####update Multi upload image
			####Upload image
			######update Many to many
            ######store
            $backSubjects = Subjects::paginate(10);
            $backSubjects->setPath('/Subjects');
            return view("SubjectsView::Subjectsajax")->with(['tab' => 1, 'Subjects' => $backSubjects, 'flag' => 4]);
        }else{
            $backSubjects = Subjects::paginate(10);
            return view("SubjectsView::Subjectsajax")->with(['tab' => 1, 'Subjects' => $backSubjects, 'flag' => 6]);
        }
    }	
    /**
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $module_id=\Request::segment(2);
        $module_id=Module::where('link_name',$module_id)->first();
        $Module_type_id=$module_id->id;
         #####relation
        if(Auth::user()->can('delete_Subjects')) {  #####check permission
            $temp = explode(",", $id);
            foreach ($temp as $val) {
                if ($temp) {
                   ### delete multi uploader
                    $user = Subjects::find($val);
                    $user->delete();
                     Activity::log([
                            'contentId'   => $val,
                            'contentType' => 'Subjects',
                            'action'      => 'Delete',
                            'description' => 'Delete  a Subjects',
                            'details'     => 'Username: '.Auth::user()->name,
                     ]);
                }
            }
            $Subjects = Subjects::paginate(10);
            $Subjects->setPath('/Subjects');
            return view("SubjectsView::Subjectsajax")->with(['tab' => 1, 'Subjects' => $Subjects, 'flag' => 5]);
        }else{
            $Subjects = Subjects::paginate(10);
            return view("SubjectsView::Subjectsajax")->with(['tab' => 1, 'Subjects' => $Subjects, 'flag' => 6]);
        }
    }	
}
