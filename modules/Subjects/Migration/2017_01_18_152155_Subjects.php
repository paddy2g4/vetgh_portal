<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Subjects extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    #####start_up_function#####
        Schema::create('subjects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 30);
            $table->string('status', 4);
            $table->string('mountable', 4);
            $table->timestamps();
        });
        DB::table("modules")->insert(
            array("name" =>"Subjects","description" =>"Subjects of various courses","link_name" => "subjects","status"=>1,"created_at"=>"2017-01-18 15:21:55")
        );
		        /**
         * role permission
         */
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'view_Subjects','display_name' => 'view_Subjects')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'add_Subjects','display_name' => 'add_Subjects')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'edit_Subjects','display_name' => 'edit_Subjects')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
        $perm_id=DB::table('permissions')->insertGetId(
            array('name' => 'delete_Subjects','display_name' => 'delete_Subjects')
        );
        DB::table('permission_role')->insert(
            array('permission_id' =>$perm_id,'role_id' => 1)
        );
     #####end_up_function#####
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
     #####start_down_function#####
        DB::table('permissions')->where('name',  'view_Subjects')->delete();
        DB::table('permissions')->where('name',  'add_Subjects')->delete();
        DB::table('permissions')->where('name',  'edit_Subjects')->delete();
        DB::table('permissions')->where('name',  'delete_Subjects')->delete();
        ######remove primary key
        Schema::drop('subjects');
     #####end_down_function#####
    }
}
