<?php

namespace Subjects\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Database\Eloquent\Model;
#####use
 
class Subjects extends Model
{
  use SearchableTrait;
    public $table = 'subjects';
    
	
	 public $searchable = [
	    'columns' => [
            'subjects.name'=>100,
    'subjects.status'=>101,
    'subjects.mountable'=>102,
        ########relation_id
        ],
        ########join
	  ];
 

    public $fillable = [
        'name',
        'status',
        'mountable'
    ];

 

    /**
     * Validation rules
     *
     * @var array
     */
    public   $rules = [
        'name' => 'required',
        'status' => 'required',
        'mountable' => 'required'
    ];
 #####relation#####
}
