<?php
Route::group(['prefix' => 'admin'], function () {
####start
   /*
    |--------------------------------------------------------------------------
    | Subjects  Routes
    |--------------------------------------------------------------------------
    |*/
    Route::resource('subjects','Subjects\Controller\SubjectsController');
    Route::post('subjects/search','Subjects\Controller\SubjectsController@search');
	Route::post('subjects/export/excel','Subjects\Controller\SubjectsController@export_excel');
####end
#####relation#####
});
 