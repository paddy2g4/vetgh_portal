<?php

namespace Subjects;

use Illuminate\Support\ServiceProvider;

class SubjectsServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $this->app->bind('Subjects',function($app){
            return new Subjects;
        });

        // Define Router file
        require __DIR__ . '/Route/routes.php';

        // Define path for view files
        $this->loadViewsFrom(__DIR__.'/View','SubjectsView');

        // Define path for translation
        $this->loadTranslationsFrom(__DIR__.'/Lang','SubjectsLang');

        // Define Published migrations files
        $this->publishes([
            __DIR__.'/Migration' => database_path('migrations'),
        ], 'migrations');

        // Define Published Asset files
        $this->publishes([
            __DIR__.'/Asset' => public_path('assets/'),
        ], 'public');

    }


    public function register()
    {
        //
    }
}