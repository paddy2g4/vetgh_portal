<!-- Name Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Name
	</label>
	<div class="col-sm-9">
		<input value="{{isset($editSubjects['name']) ? $editSubjects['name'] : ''}}" name="name" type="text" placeholder="" class="form-control">
	</div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label" for="form-field-1">
       Status
    </label>
    <div class="col-sm-9">
        <select name="status" class="form-control">
            <option  {{isset($editSubjects["status"])&&$editSubjects["status"]=="YES" ? "selected" : ""}}   value="YES"">YES</option><option  {{isset($editSubjects["status"])&&$editSubjects["status"]=="NO" ? "selected" : ""}}   value="NO"">NO</option>
        </select>
    </div>
</div>




<div class="form-group">
    <label class="col-sm-3 control-label" for="form-field-1">
       Mountable
    </label>
    <div class="col-sm-9">
        <select name="mountable" class="form-control">
            <option  {{isset($editSubjects["mountable"])&&$editSubjects["mountable"]=="YES" ? "selected" : ""}}   value="YES"">YES</option><option  {{isset($editSubjects["mountable"])&&$editSubjects["mountable"]=="NO" ? "selected" : ""}}   value="NO"">NO</option>
        </select>
    </div>
</div>



