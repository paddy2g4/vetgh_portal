@extends('master')
@section('content')
    <!-- widget progress .col-md-12 -->
   <div class="col-md-12">
      <br><br>

      <div  class="panel panel-default">
         <div class="panel-body">
            <i class="glyphicon glyphicon-stats"></i>
            <b>{{ trans('main.admin_overview') }}</b>
            <hr>

            <div class="row">
               <h2 style="text-align: center;">Welcome to VETGH Admin Portal</h2>
            </div>
            <br><br>

         </div>
      </div>

   </div>

@endsection
