<div id="MainMenu" class="">

    <ul id="menu-list" class="nav nav-list">

        @if(\Session::get('role_id') == 1 || \Session::get('role_id') == 2 )
        <li class="separate-menu"><span>{{ trans('main.configurations') }}</span></li>

        <li class="@if(Request::segment(2)=='users' || Request::segment(2)=='permissions' || Request::segment(2)=='roles') active open @endif">
            <a href="{{url('/admin/users')}}" class="dropdown-toggle">
                <i class="menu-icon fa fa-user"></i>
                <span class="menu-text"> {{ trans('main.users_management') }}</span>
                <b class="arrow fa fa-angle-down"></b>
            </a>
            <b class="arrow"></b>
            <ul class="submenu">

                @if(\Session::get('role_id') == 1)

                    <li class="@if(Request::segment(2)=='permissions') active @endif">
                        <a href="{{url('/admin/permissions')}}" >
                            <i class="menu-icon fa fa-list"></i>
                            <span class="menu-text"> {{ trans('main.permissions') }} </span>
                        </a>
                    </li>
                @endif

                @if(\Session::get('role_id') == 1)
                    <li class="@if(Request::segment(2)=='roles') active @endif">
                        <a href="{{url('/admin/roles')}}">
                            <i class="menu-icon fa fa-bar-chart-o"></i>
                            <span class="menu-text"> {{ trans('main.roles') }} </span>
                        </a>
                    </li>
                @endif

                @if(\Session::get('role_id') == 1 || \Session::get('role_id') == 2 )
                    <li class="@if(Request::segment(2)=='users') active @endif">
                        <a href="{{url('/admin/users')}}">
                            <i class="menu-icon fa fa-desktop"></i>
                            <span class="menu-text"> {{ trans('main.users') }}</span>
                        </a>
                        <b class="arrow"></b>
                    </li>
                @endif

            </ul>
        </li>
        @endif

        {{-- @if(isset($module_menus))
            @foreach($module_menus as $module_menu)
                <li class="@if(($module_menu['child']>""&&in_array(Request::segment(2),$module_menu['child'])||Request::segment(2)==$module_menu['name'])) active open @endif">
                    <a href="#" class="dropdown-toggle">
                        <i class="menu-icon fa fa-user"></i>
                        <span class="menu-text"> {{$module_menu['name'].' module'}}</span>
                        <b class="arrow fa fa-angle-down"></b>
                    </a>
                    <b class="arrow"></b>
                    <ul class="submenu">
                        <li class="@if(Request::segment(2)==$module_menu['name']) active @endif">
                            <a href="{{url('/admin/'.$module_menu['name'])}}">
                                <i class="menu-icon fa fa-tachometer"></i>
                                <span class="menu-text"> {{ $module_menu['name'] }} </span>
                            </a>
                            <b class="arrow"></b>
                        </li>
                        @if($module_menu['child']>"")
                            @foreach($module_menu['child'] as $val)
                                <li class="@if(Request::segment(2)==$val) active @endif">
                                    <a href="{{url('/admin/'.$val)}}">
                                        <i class="menu-icon fa fa-tachometer"></i>
                                        <span class="menu-text"> {{ $val }} </span>
                                    </a>
                                    <b class="arrow"></b>
                                </li>
                            @endforeach

                        @endif
                    </ul>
                </li>
            @endforeach
        @endif --}}



        @if(\Session::get('role_id') == 1 || \Session::get('role_id') == 2)

            <li class="separate-menu"><span>Award Setups</span></li>

            <li class="@if(Request::segment(2)=='entitymasters' || Request::segment(2)=='awards' || Request::segment(2)=='pricings' || Request::segment(2)=='shortcodemasters') active open @endif">
                <a href="{{url('/admin/entitymasters')}}" class="dropdown-toggle">
                    <i class="menu-icon fa fa-cogs"></i>
                    <span class="menu-text"> Award Setups</span>
                    <b class="arrow fa fa-angle-down"></b>
                </a>
                <b class="arrow"></b>
                <ul class="submenu">
                    @if(\Session::get('role_id') == 1 || \Session::get('role_id') == 2 )
                    <li class="@if(Request::segment(2)=='entitymasters') active @endif">
                        <a href="{{url('/admin/entitymasters')}}" >
                            <i class="menu-icon fa fa-list"></i>
                            <span class="menu-text">Institutions</span>
                        </a>
                    </li>
                    @endif

                    @if(\Session::get('role_id') == 1 || \Session::get('role_id') == 2)
                    <li class="@if(Request::segment(2)=='events') active @endif">
                        <a href="{{url('/admin/events')}}" >
                            <i class="menu-icon fa fa-list"></i>
                            <span class="menu-text">Events</span>
                        </a>
                    </li>
                    @endif

                </ul>
            </li>
        @endif


        @if(\Session::get('role_id') == 1 || \Session::get('role_id') == 2)

            <li class="separate-menu"><span>Ticketing Setups</span></li>

            <li class="@if(Request::segment(2)=='ticket_types' || Request::segment(2)=='ticket_classifications' || Request::segment(2)=='ticket_details') active open @endif">
                <a href="#" class="dropdown-toggle">
                    <i class="menu-icon fa fa-cogs"></i>
                    <span class="menu-text"> Ticketing Setups </span>
                    <b class="arrow fa fa-angle-down"></b>
                </a>
                <b class="arrow"></b>
                <ul class="submenu">
                    <li class="@if(Request::segment(2)=='ticket_types') active @endif">
                        <a href="{{url('/admin/ticket_types')}}" >
                            <i class="menu-icon fa fa-list"></i>
                            <span class="menu-text">Ticket Types</span>
                        </a>
                    </li>

                    <li class="@if(Request::segment(2)=='ticket_classifications') active @endif">
                        <a href="{{url('/admin/ticket_classifications')}}" >
                            <i class="menu-icon fa fa-list"></i>
                            <span class="menu-text">Ticket Dates & Amount</span>
                        </a>
                    </li>
                </ul>
            </li>
        @endif


        @if(\Session::get('role_id') == 1 )
            <li class="@if(Request::segment(2)=='keys' || Request::segment(2)=='smsaccounts') active open @endif">
                <a href="{{url('/admin/keys')}}" class="dropdown-toggle">
                    <i class="menu-icon fa fa-key"></i>
                    <span class="menu-text"> Keys & Accounts</span>
                    <b class="arrow fa fa-angle-down"></b>
                </a>
                <b class="arrow"></b>
                <ul class="submenu">
                    @if(\Session::get('role_id') == 1 || \Session::get('role_id') == 2 )
                        <li class="@if(Request::segment(2)=='keys') active @endif">
                            <a href="{{url('/admin/keys')}}" >
                                <i class="menu-icon fa fa-list"></i>
                                <span class="menu-text">Keys</span>
                            </a>
                        </li>
                    @endif
                    @if(\Session::get('role_id') == 1)
                        <li class="@if(Request::segment(2)=='permissions') active @endif">
                            <a href="{{url('/admin/smsaccounts')}}" >
                                <i class="menu-icon fa fa-list"></i>
                                <span class="menu-text">SMS Accounts</span>
                            </a>
                        </li>
                    @endif
                </ul>
            </li>
        @endif

{{--        @if(\Session::get('role_id') == 1)--}}
        @if(\Session::get('role_id') == 1 || \Session::get('role_id') == 2)
            <li class="@if(Request::segment(2)=='categories' || Request::segment(2)=='nominees' || Request::segment(2)=='nominees_reports') active open @endif">
                <a href="{{url('/admin/nominees')}}" class="dropdown-toggle">
                    <i class="menu-icon fa fa-users"></i>
                    <span class="menu-text"> Nominees & Categories</span>
                    <b class="arrow fa fa-angle-down"></b>
                </a>
                <b class="arrow"></b>
                <ul class="submenu">
                    {{-- @permission('view_Categories') --}}
                    <li class="@if(Request::segment(2)=='categories') active @endif">
                        <a href="{{url('/admin/categories')}}" >
                            <i class="menu-icon fa fa-list"></i>
                            <span class="menu-text">Categories</span>
                        </a>
                    </li>
                    {{-- @endpermission --}}
                    {{-- @permission('view_Nominees') --}}
                    <li class="@if(Request::segment(2)=='nominees') active @endif">
                        <a href="{{url('/admin/nominees')}}" >
                            <i class="menu-icon fa fa-list"></i>
                            <span class="menu-text">Nominees</span>
                        </a>
                    </li>
                    {{-- @endpermission --}}

                    <li class="@if(Request::segment(2)=='nominees_reports') active @endif">
                        <a href="{{url('/admin/nominees_reports')}}" >
                            <i class="menu-icon fa fa-list"></i>
                            <span class="menu-text">Nominees Report</span>
                        </a>
                    </li>

                </ul>
            </li>
        @endif

        @if(\Session::get('role_id') == 1 || \Session::get('role_id') == 2 || \Session::get('role_id') == 3)
            <li class="@if(Request::segment(2)=='reports' || Request::segment(2)=='votingresults' || Request::segment(2)=='transactions' || Request::segment(2)=='tickets_transactions' || Request::segment(2)=='tickets_sold_report') active open @endif">
                <a href="{{url('/admin/reports')}}" class="dropdown-toggle">
                    <i class="menu-icon fa fa-bar-chart-o"></i>
                    <span class="menu-text"> Reports & Statistics</span>
                    <b class="arrow fa fa-angle-down"></b>
                </a>
                <b class="arrow"></b>
                <ul class="submenu">
                    {{-- @permission('view_permission') --}}
                    <li class="@if(Request::segment(2)=='votingresults') active @endif">
                        <a href="{{url('/admin/votingresults')}}" >
                            <i class="menu-icon fa fa-list"></i>
                            <span class="menu-text">Results</span>
                        </a>
                    </li>
                    {{-- @endpermission --}}
                    {{-- @permission('view_permission') --}}
                    <li class="@if(Request::segment(2)=='transactions') active @endif">
                        <a href="{{url('/admin/transactions')}}" >
                            <i class="menu-icon fa fa-list"></i>
                            <span class="menu-text">Transactions</span>
                        </a>
                    </li>

                    <li class="@if(Request::segment(2)=='statistics') active @endif">
                        <a href="{{url('/admin/statistics')}}" >
                            <i class="menu-icon fa fa-list"></i>
                            <span class="menu-text">Statistics</span>
                        </a>
                    </li>

                    <li class="@if(Request::segment(2)=='tickets_transactions') active @endif">
                        <a href="{{url('/admin/tickets_transactions')}}" >
                            <i class="menu-icon fa fa-list"></i>
                            <span class="menu-text">Ticket Transactions</span>
                        </a>
                    </li>

                    <li class="@if(Request::segment(2)=='tickets_sold_report') active @endif">
                        <a href="{{url('/admin/tickets_sold_report')}}" >
                            <i class="menu-icon fa fa-list"></i>
                            <span class="menu-text">Tickets Sold</span>
                        </a>
                    </li>
                    {{-- @endpermission --}}
                </ul>
            </li>
        @endif


        @if(\Session::get('role_id') == 1 || \Session::get('role_id') == 2 || \Session::get('role_id') == 3)
            <li class="separate-menu"><span>ADMINISTRATOR</span></li>
            <li class="@if(Request::segment(2)=='dashboard') active @endif">
                <a href="{{url('/admin/dashboard')}}">
                    <i class="menu-icon fa fa-tachometer"></i>
                    <span class="menu-text"> {{ trans('main.dashboard') }} </span>
                </a>
                <b class="arrow"></b>
            </li>
        @endif

        @if(\Session::get('role_id') == 1  )
            <li class="@if(Request::segment(2)=='blastSMS') active @endif">
                <a href="{{url('/admin/blastSMS')}}" >
                    <i class="menu-icon fa fa-comments-o "></i>
                    <span class="menu-text">SMS Blast</span>
                </a>
            </li>
        @endif
        
        @if(\Session::get('role_id') == 1)
            <li class="@if(Request::segment(2)=='news') active @endif">
                <a href="{{url('/admin/news')}}">
                    <i class="menu-icon fa fa-pencil-square-o"></i>
                    <span class="menu-text"> {{ trans('main.news') }} </span>
                </a>
            </li>
        @endif
        @if(\Session::get('role_id') == 1)
            <li class="@if(Request::segment(2)=='news_category') active @endif">
                <a href="{{url('/admin/news_category')}}">
                    <i class="menu-icon fa fa-pencil-square-o"></i>
                    <span class="menu-text"> {{ trans('main.news_category') }} </span>
                </a>
            </li>
        @endif
        @if(\Session::get('role_id') == 1)
            <li class="@if(Request::segment(2)=='modules') active @endif">
                <a href="{{url('/admin/modulesbuilder')}}">
                    <i class="menu-icon fa fa-pencil-square-o"></i>
                    <span class="menu-text">  Module builder </span>
                </a>
            </li>
        @endif
        @if(\Session::get('role_id') == 1)
            <li class="@if(Request::segment(2)=='relation') active @endif">
                <a href="{{url('/admin/relation')}}">
                    <i class="menu-icon fa fa-exchange"></i>
                    <span class="menu-text"> Relation </span>
                </a>
            </li>
        @endif
        @if(\Session::get('role_id') == 1)
            <li class="@if(Request::segment(2)=='reportbuilders') active @endif">
                <a href="{{url('/admin/reportbuilders')}}">
                    <i class="menu-icon fa fa-line-chart"></i>
                    <span class="menu-text">  Report builders </span>
                </a>
            </li>
        @endif
        @if(\Session::get('role_id') == 1)
            <li class="@if(Request::segment(2)=='activitylogs') active @endif">
                <a href="{{url('/admin/activitylogs')}}">
                    <i class="menu-icon fa fa-history"></i>
                    <span class="menu-text"> Activity Logs </span>
                </a>
            </li>
        @endif

        @if(\Session::get('role_id') == 1)
            <li class="separate-menu"><span>HTML template</span></li>
            <li class="">
                <a href="{{url("/html_version/form-elements.html")}}"  >
                    <i class="menu-icon fa fa-desktop"></i>
                    <span class="menu-text">Admin template</span>


                </a>

                <b class="arrow"></b>

            </li>
        @endif


    </ul>


    <a class="sidebar-collapse" id="sidebar-collapse" data-toggle="collapse" data-target="#test">
        <i id="icon-sw-s-b" class="fa fa-angle-double-left"></i>
    </a>
</div>