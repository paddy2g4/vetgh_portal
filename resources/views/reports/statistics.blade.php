@extends('reportmaster')
@section('content')

    <!-- widget progress .col-md-12 -->

    <div class="col-md-6 col-sm-offset-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title">
                    <i class="fa fa-table"></i>

                    <div class="bars pull-right">
                        <a href="#"><i class="maximum fa fa-expand" data-toggle="tooltip" data-placement="bottom" title="Maximize"></i></a>
                        <a href="#"><i class="minimize fa fa-chevron-down" data-toggle="tooltip" data-placement="bottom" title="Collapse"></i></a>
                        <a href="#"><i data-target="#panel2" data-dismiss="alert" data-toggle="tooltip" data-placement="bottom" title="Close" class="fa fa-times"></i></a>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                @if(count($errors) > 0)
                    <div class="alert alert-danger">
                        Validation Error<br><br>
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if($message = Session::get('success'))
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>{{ $message }}</strong>
                    </div>
                @endif

                <form method="post" role="form" clASs="form-horizontal" action="{{url("/admin/statistics/filter_events")}}" enctype="multipart/form-data" autocomplete="off">
                    <INput type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div clASs="form-group{{ $errors->hAS('old') ? ' hAS-error' : '' }}">

                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="form-field-1" required>
                                Event
                            </label>
                            <select name="plan_id" class="js-example-basic-single col-sm-6">

                                @if(isset($awards))
                                    @foreach($awards as $awards)
                                        <option  {{isset($editPricing)&&$editPricing->plan_id==$awards->plan_id ?  'selected' :''}}  value="{{$awards->plan_id}}"> {{$awards->plan_name}} - {{$awards->entity_name}}</option>
                                    @endforeach
                                @endif

                            </select>
                        </div>

                    </div>
                    <div clASs="box-footer">
                        <button type="submit" class="btn btn-success btn-md col-sm-offset-5" name="btnview_results">VIEW BALANCES</button>
                    </div>
                </form>

                <?php
                if (isset($_POST['month'])){
                    $search = filter_var($_POST['month'], FILTER_SANITIZE_STRING);
                    $_SESSION['month'] = $search;
                    //$tokengenerated= bin2hex(openssl_random_pseudo_bytes(3));
                }
                ?>
            </div>
        </div><!-- end panel -->
    </div><!-- end .col-md-12 -->


    @if($total_transactions != null)

        <div class="col-md-12">
            <br><br>
            <div class="row">
                {{--         $total_transactions, 'total_passed_transactions'=> $total_passed_transactions, 'total_pending_transactions'=>$total_pending_transactions, 'total_failed_transactions'=>$total_failed_transactions, --}}
                {{--         'passed_transactions_today'=>$passed_transactions_today, 'pending_transactions_today'=>$pending_transactions_today, 'failed_transactions_today'=>$failed_transactions_today--}}
                <div class="col-sm-3 col-sm-offset-2">
                    <div class="well well-sm text-center">
                       <span class="h1 shadow-block no-margin orange">
                          GHS {{$sum_amount_today}}
                       </span>
                        <span class="text-center">
                            <i class="fa fa-user text-muted"></i>
                        </span>
                        <span class="text-muted">Today's Amount</span>
                    </div>
                </div>
{{--                <div class="col-sm-3">--}}
{{--                    <div class="well well-sm text-center">--}}
{{--                        <span class="h1 shadow-block no-margin green">{{$passed_transactions_today}}</span>--}}
{{--                        <span class="text-center">--}}
{{--                     <i class="fa fa-user text-muted"></i>--}}
{{--                 </span>--}}
{{--                        <span class="text-muted">Today's Successful Transactions</span>--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--                <div class="col-sm-3">--}}
{{--                    <div class="well well-sm text-center">--}}
{{--                        <span class="h1 shadow-block no-margin red">{{$failed_transactions_today}}</span>--}}
{{--                        <span class="text-center">--}}
{{--                     <i class="fa fa-user text-muted"></i>--}}
{{--                 </span>--}}
{{--                        <span class="text-muted">Today's Failed Transactions</span>--}}
{{--                    </div>--}}
{{--                </div>--}}
                <div class="col-sm-3">
                    <div class="well well-sm text-center">
                        <span class="h1 shadow-block no-margin purple">{{$transactions_today}}</span>
                        <span class="text-center">
                             <i class="fa fa-user text-muted"></i>
                         </span>
                        <span class="text-muted">Today's Transactions</span>
                    </div>
                </div>

                @if(\Session::get('role_id') != 3)
                    <div class="col-sm-3">
                        <div class="well well-sm text-center">
                            <span class="h1 shadow-block no-margin purple">{{$sms_balance}}</span>
                            <span class="text-center">
                                 <i class="fa fa-user text-muted"></i>
                             </span>
                            <span class="text-muted">SMS Balance</span>
                        </div>
                    </div>
                @endif
            </div>
            <br><br>

            <div class="row">
                @if(\Session::get('role_id') != 3)
                    <div class="col-sm-3 col-sm-offset-2">
                        <div class="well well-sm text-center">
                       <span class="h1 shadow-block no-margin orange">
                          {{$total_passed_transactions}}
                       </span>
                            <span class="text-center">
                            <i class="fa fa-user text-muted"></i>
                        </span>
                            <span class="text-muted">Total Successful Transactions</span>
                        </div>
                    </div>
                @endif
                <div class="col-sm-3">
                    <div class="well well-sm text-center">
                        <span class="h1 shadow-block no-margin green">{{$total_failed_transactions}}</span>
                        <span class="text-center">
                     <i class="fa fa-user text-muted"></i>
                 </span>
                        <span class="text-muted">Total Unsuccessful Transactions</span>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="well well-sm text-center">
                        <span class="h1 shadow-block no-margin purple">{{$total_transactions}}</span>
                        <span class="text-center">
                     <i class="fa fa-user text-muted"></i>
                 </span>
                        <span class="text-muted">Total Transactions</span>
                    </div>
                </div>
            </div>

            <br><br>
            <div class="row">
{{--                @if(\Session::get('role_id') != 3)--}}
                <div class="col-sm-3 col-sm-offset-2">
                    <div class="well well-sm text-center">
                       <span class="h1 shadow-block no-margin orange">
                          GHS {{$event_gross_bal}}
                       </span>
                        <span class="text-center">
                            <i class="fa fa-user text-muted"></i>
                        </span>
                        <span class="text-muted">Total Gross Amount</span>
                    </div>
                </div>
{{--                @endif--}}
                <div class="col-sm-3">
                    <div class="well well-sm text-center">
                        <span class="h1 shadow-block no-margin green">GHS {{$event_net_bal}}</span>
                        <span class="text-center">
                     <i class="fa fa-user text-muted"></i>
                 </span>
                        <span class="text-muted">Total Net Amount</span>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="well well-sm text-center">
                        <span class="h1 shadow-block no-margin purple">{{$total_transactions}}</span>
                        <span class="text-center">
                     <i class="fa fa-user text-muted"></i>
                 </span>
                        <span class="text-muted">Total Transactions</span>
                    </div>
                </div>
            </div>


        </div>
        <!-- /end widget progress .col-md-12 -->

{{--        This will contain total amounts for all events. Will only be accessible by an Admin--}}
{{--        <div class="col-md-6 col-sm-offset-3"> --}}
{{--            <div class="panel panel-default">--}}
{{--                <div class="panel-heading">--}}
{{--                    <div class="panel-title">--}}
{{--                        <i class="fa fa-table"></i>--}}

{{--                        <div class="bars pull-right">--}}
{{--                            <a href="#"><i class="maximum fa fa-expand" data-toggle="tooltip" data-placement="bottom" title="Maximize"></i></a>--}}
{{--                            <a href="#"><i class="minimize fa fa-chevron-down" data-toggle="tooltip" data-placement="bottom" title="Collapse"></i></a>--}}
{{--                            <a href="#"><i data-target="#panel2" data-dismiss="alert" data-toggle="tooltip" data-placement="bottom" title="Close" class="fa fa-times"></i></a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="panel-body">--}}
{{--                    @if(count($errors) > 0)--}}
{{--                        <div class="alert alert-danger">--}}
{{--                            Validation Error<br><br>--}}
{{--                            <ul>--}}
{{--                                @foreach($errors->all() as $error)--}}
{{--                                    <li>{{ $error }}</li>--}}
{{--                                @endforeach--}}
{{--                            </ul>--}}
{{--                        </div>--}}
{{--                    @endif--}}

{{--                    @if($message = Session::get('success'))--}}
{{--                        <div class="alert alert-success alert-block">--}}
{{--                            <button type="button" class="close" data-dismiss="alert">×</button>--}}
{{--                            <strong>{{ $message }}</strong>--}}
{{--                        </div>--}}
{{--                    @endif--}}

{{--                </div>--}}
{{--            </div><!-- end panel -->--}}
{{--        </div>--}}


        <!-- Admin over view .col-md-12 -->

        <div class="col-md-12 ">
            <div  class="panel panel-default">
                <div class="panel-body">

                    <i class="glyphicon glyphicon-stats"></i>
                    <b>Today's Transactions</b>
                    <hr>
                    <div class="row">

                        <!-- chart section -->
                        <div class="col-sm-9">
                            <div class="well well-sm well-light padding-10">
                                <h4 >
                                    <strong>Transactions by source (USSD & WEB)    </strong>
                                    <a href="javascript:void(0);" class="pull-right ">
                                        <i class="fa fa-refresh"></i>
                                    </a>
                                </h4>
                                <div class="panel panel-default">
                                    <div class="panel-body no-padding">
                                        {!! $statisticsChart->container() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- ./chart section -->

                        <!-- pie chart -->
                        <div class="col-sm-3 ">
                            <div class="col-md-6 no-padd-left-right">
                                <div class="well well-sm text-center">
                                    <span class="h1 shadow-block no-margin purple">{{$sum_amount_today}}</span>
                                    <span class="text-center">
                                  <i class="fa fa-upload text-muted"></i>
                              </span>
                                    <span class="text-muted">Amount</span>
                                </div>
                            </div>
                            <div class="col-md-6 no-padd-left-right">
                                <div class="well well-sm text-center">
                                    <span class="h1 shadow-block no-margin green">{{$passed_transactions_today}}</span>
                                    <span class="text-center">
                                  <i class="fa fa-file text-muted"></i>
                              </span>
                                    <span class="text-muted">Successful Transactions</span>
                                </div>
                            </div>
                            <div class="col-md-6 no-padd-left-right">
                                <div class="well well-sm text-center">
                                    <span class="h1 shadow-block no-margin orange">{{$failed_transactions_today}}</span>
                                    <span class="text-center">
                                  <i class="fa fa-user text-muted"></i>
                              </span>
                                    <span class="text-muted">Failed Transactions</span>
                                </div>
                            </div>
                            <div class="col-sm-6 no-padd-left-right hidden-sm">
                                <div class="well well-sm text-center">
                                    <span class="h1 shadow-block no-margin darkblue">{{$transactions_today}}</span>
                                    <span class="text-center">
                                     <i class="fa fa-rss text-muted"></i>
                                 </span>
                                    <span class="text-muted">Total Transactions</span>
                                </div>
                            </div>
                            <p class="hidden-sm limit-3-line">{{ trans('main.content2') }}</p>
                        </div>
                        <!-- ./pie chart -->
                    </div>
                </div>

            </div><!-- end panel -->
        </div>








        {{--    <div class="col-md-12 ">--}}
        {{--          <div  class="panel panel-default">--}}
        {{--             <div class="panel-body">--}}

        {{--                <i class="glyphicon glyphicon-stats"></i>--}}
        {{--                <b>{{ trans('main.admin_overview') }}</b>--}}
        {{--                <hr>--}}
        {{--                <div class="row">--}}
        {{--                   <!-- progress section -->--}}
        {{--                   <div class="col-sm-3">--}}
        {{--                      <div class="headprogress">--}}
        {{--                         <strong>{{ trans('main.tasks') }}</strong>--}}
        {{--                         <strong class="progress-value">60/100 <i class="fa fa-tasks"></i></strong>--}}
        {{--                      </div>--}}
        {{--                      <div class="progress progress-xs">--}}
        {{--                         <div class="progress-bar progress-bar-info" role="progressbar"  style="width: 60%;">--}}
        {{--                         </div>--}}
        {{--                      </div>--}}

        {{--                      <div class="headprogress">--}}
        {{--                         <strong>{{ trans('main.capacity') }}</strong>--}}
        {{--                         <strong class="progress-value">40/100 GB <i class="fa fa-pie-chart"></i></strong>--}}
        {{--                      </div>--}}
        {{--                      <div class="progress  progress-xs">--}}
        {{--                         <div class="progress-bar progress-bar-info" role="progressbar" style="width: 40%;">--}}
        {{--                         </div>--}}
        {{--                      </div>--}}

        {{--                      <div class="headprogress">--}}
        {{--                         <strong>{{ trans('main.email') }}</strong>--}}
        {{--                         <strong class="progress-value">70% <i class="fa fa-envelope"></i></strong>--}}
        {{--                      </div>--}}
        {{--                      <div class="progress progress-xs">--}}
        {{--                         <div class="progress-bar progress-bar-info" role="progressbar" style="width: 70%;">--}}
        {{--                         </div>--}}
        {{--                      </div>--}}

        {{--                      <div class="headprogress">--}}
        {{--                         <strong>{{ trans('main.close_request') }}</strong>--}}
        {{--                         <strong class="progress-value">90% <i class="fa fa-ticket"></i></strong>--}}
        {{--                      </div>--}}
        {{--                      <div class="progress progress-xs">--}}
        {{--                         <div class="progress-bar progress-bar-info" role="progressbar"  style="width: 90%;">--}}
        {{--                         </div>--}}
        {{--                      </div>--}}

        {{--                      <a class="btn btn-success btn-block"><i class="fa fa-download"></i> {{ trans('main.download') }} <strong>{{ trans('main.report') }}</strong></a>--}}
        {{--                      <a class="btn btn-default btn-block"><i class="fa fa-file-o"></i> {{ trans('main.report_a') }} <strong>{{ trans('main.bug') }}</strong></a>--}}
        {{--                   </div>--}}

        {{--                   <div class="col-sm-6">--}}
        {{--                      <div class="well well-sm well-light padding-10">--}}
        {{--                         <h4 >--}}
        {{--                            {{ trans('main.active') }} <strong>{{ trans('main.visit') }}</strong>--}}
        {{--                            <a href="javascript:void(0);" class="pull-right ">--}}
        {{--                               <i class="fa fa-refresh"></i>--}}
        {{--                            </a>--}}
        {{--                         </h4>--}}
        {{--                         <div id="areaChart" class="chart-dashboard"></div>--}}
        {{--                      </div>--}}
        {{--                   </div>--}}

        {{--                   <div class="col-sm-3 ">--}}
        {{--                      <div class="col-md-6 no-padd-left-right">--}}
        {{--                         <div class="well well-sm text-center">--}}
        {{--                            <span class="h1 shadow-block no-margin purple">752</span>--}}
        {{--                              <span class="text-center">--}}
        {{--                                  <i class="fa fa-upload text-muted"></i>--}}
        {{--                              </span>--}}
        {{--                            <span class="text-muted">{{ trans('main.uploads') }}</span>--}}
        {{--                         </div>--}}
        {{--                      </div>--}}
        {{--                      <div class="col-md-6 no-padd-left-right">--}}
        {{--                         <div class="well well-sm text-center">--}}
        {{--                            <span class="h1 shadow-block no-margin green">563</span>--}}
        {{--                              <span class="text-center">--}}
        {{--                                  <i class="fa fa-file text-muted"></i>--}}
        {{--                              </span>--}}
        {{--                            <span class="text-muted">{{ trans('main.all_news') }}</span>--}}
        {{--                         </div>--}}
        {{--                      </div>--}}
        {{--                      <div class="col-md-6 no-padd-left-right">--}}
        {{--                         <div class="well well-sm text-center">--}}
        {{--                            <span class="h1 shadow-block no-margin orange">236</span>--}}
        {{--                              <span class="text-center">--}}
        {{--                                  <i class="fa fa-user text-muted"></i>--}}
        {{--                              </span>--}}
        {{--                            <span class="text-muted">{{ trans('main.users') }}</span>--}}
        {{--                         </div>--}}
        {{--                      </div>--}}
        {{--                      <div class="col-sm-6 no-padd-left-right hidden-sm">--}}
        {{--                         <div class="well well-sm text-center">--}}
        {{--                            <span class="h1 shadow-block no-margin darkblue">471</span>--}}
        {{--                                 <span class="text-center">--}}
        {{--                                     <i class="fa fa-rss text-muted"></i>--}}
        {{--                                 </span>--}}
        {{--                            <span class="text-muted">{{ trans('main.feeds') }}</span>--}}
        {{--                         </div>--}}
        {{--                      </div>--}}
        {{--                      <p class="hidden-sm limit-3-line">{{ trans('main.content2') }}</p>--}}
        {{--                   </div>--}}
        {{--                </div>--}}
        {{--             </div>--}}

        {{--          </div>--}}

        {{--       </div>--}}

        {{--    <h1>Stats Graphs</h1>--}}




        {{--    @if($statisticsChart)--}}
        {!! $statisticsChart->script() !!}
        {{--    @endif--}}

        <div style="width: 50%">

        </div>

    @endif


    <script type="text/javascript">
        $(document).ready(function() {
            $(".js-example-basic-single").select2();
        });
    </script>


    <!-- /end Admin over view .col-md-12 -->
@endsection
