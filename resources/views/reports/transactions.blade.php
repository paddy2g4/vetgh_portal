@extends('reportmaster')
@section('content')
    <!-- /end widget progress .col-md-12 -->
    <!-- Admin over view .col-md-12 -->

    <!-- Admin over view .col-md-12 -->
    <div class="col-md-12 ">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title">
                    <i class="fa fa-table"></i>

                    <div class="bars pull-right">
                        <a href="#"><i class="maximum fa fa-expand" data-toggle="tooltip" data-placement="bottom" title="Maximize"></i></a>
                        <a href="#"><i class="minimize fa fa-chevron-down" data-toggle="tooltip" data-placement="bottom" title="Collapse"></i></a>
                        <a href="#"><i data-target="#panel2" data-dismiss="alert" data-toggle="tooltip" data-placement="bottom" title="Close" class="fa fa-times"></i></a>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <form method="post" action="" role="form" clASs="form-horizontal">
                    <INput type="hidden" name="_token" value="{{ csrf_token() }}">

                    <table id="example2" class="table table-striped table-bordered width-100 cellspace-0">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th width="10%">Trans Ref</th>
                             <th width="10%">Award</th>
                            <th width="10%">Nominee</th>
                            <th width="10%">Customer Number</th>
                            <th width="10%">Amount</th>
                            <th width="10%">Nw</th>
                            <th width="10%">Payment Method</th>
                            <!-- <th>Vote Count</th> -->
                            <!-- <th width="10%">Source</th> -->
                            <th width="10%">Status</th>
                            <th width="10%">Date</th>
                        </tr>
                        </thead>
                        <tbody>

                            @if(isset($transactions))
                                @foreach($transactions as  $key=>$transactions)

                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{$transactions->processing_id}}</td>
                                        <td><strong>{{$transactions->plan_name}}</strong></td>
                                        <td>{{$transactions->nom_name}}</td>
                                        <td>{{$transactions->customer_number}}</td>
                                        <td>{{$transactions->amount}}</td>
                                        <td>{{$transactions->nw}}</td>
                                        <td>{{$transactions->payment_mode}}</td>
                                        <td>
                                            @if($transactions->processed == '0')
                                                Failed
                                            @elseif ($transactions->processed == '1')
                                                Passed
                                            @else
                                                Pending
                                            @endif
                                        </td>
                                        <td>{{date('d-F-Y g:i A', strtotime($transactions->created_at) )}}</td>
                                    </tr>

                                @endforeach

                            @endif

                        </tbody>
                    </table>
                    <div clASs="box-footer">
                    </div>
                </form>
            </div>
        </div><!-- end panel -->
    </div><!-- end .col-md-12 -->
    <!-- /end Admin over view .col-md-12 -->

    <!-- General JS script library-->
    {{-- <script type="text/javascript" src="../assets/vendors/jquery/jquery.min.js"></script> --}}
    {{-- <script type="text/javascript" src="../assets/vendors/jquery-ui/js/jquery-ui.min.js"></script>	 --}}
    <script type="text/javascript" src="../assets/vendors/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../assets/vendors/jquery-searchable/js/jquery.searchable.min.js"></script>
    <script type="text/javascript" src="../assets/vendors/jquery-fullscreen/js/jquery.fullscreen.min.js"></script>

    <!-- Related JavaScript Library to This Pagee -->
    <script type="text/javascript" src="../assets/vendors/jquery-datatables/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../assets/vendors/jquery-datatables/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="../assets/vendors/jquery-datatables/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="../assets/vendors/jquery-datatables/js/dataTables.tableTools.min.js"></script>
    <script type="text/javascript" src="../assets/vendors/jquery-datatables/js/dataTables.colVis.min.js"></script>


    <!-- Plugins Script -->
    <script type="text/javascript">
        $(document).ready(function() {
            $(".js-example-basic-single").select2();
        });

        // Default Data Table Script
        $('#example1').dataTable({
            responsive: true
        });

        // Table Tools Data Tables




        $('#example2').dataTable({
            // destroy: true,
            retrieve: true,
            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>"+
                "t"+
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
            "oTableTools": {
                "aButtons": [
                    "copy",
                    "csv",
                    "xls",
                    {
                        "sExtends": "pdf",
                        "sTitle": "vetGHTransactionsReport_PDF",
                        "sPdfMessage": "VETGH Transactions Report PDF Export",
                        "sPdfSize": "letter"
                    },
                    {
                        "sExtends": "print",
                        "sMessage": "Generated by VETGH <i>(press Esc to close)</i>"
                    }
                ],

            },
            "autoWidth" : true,
            responsive: true

        });

        // Column show & hide
        // $('#example3').DataTable( {
        //     "dom": '<"pull-left"f><"pull-right"l>tip',
        //     "dom": 'C<"clear">lfrtip',
        //
        //     "bLengthChange": false,
        //     responsive: true
        // });


        $('#transactionTable').dataTable({
            processing: true,
            serverSide: true,
            ajax: "{{route('transactions.getdata')}}",
            columns: [
                { data: 'sn' },
                { data: 'trans_ref' },
                { data: 'plan_name' },
                { data: 'nom_name' },
                { data: 'customer_number' },
                { data: 'amount' },
                { data: 'nw' },
                { data: 'payment_med' },
                { data: 'vote_count' },
                { data: 'status' },
                { data: 'created_at' },
            ]
        });

    </script>

    <div class="col-md-6">
        <!-- end panel -->
        <script type="text/javascript">
            $(document).ready(function() {
                $(".js-example-basic-single").select2();
            });
        </script>
    </div>




    <!-- /end Admin over view .col-md-12 -->
@endsection
