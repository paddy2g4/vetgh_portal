<!-- Entity Id Field -->
<div class="form-group">
<label class="col-sm-4 control-label" for="form-field-1">
		Company
	</label>
	<div class="col-sm-8">
		
		<select name="entity_id" id="entity_id_1" class="js-example-basic-single col-sm-12">

			<?php if(isset($entity_masters)): ?>
				<?php foreach($entity_masters as $entity_masters): ?>
					<option  <?php echo e(isset($editAwards)&&$editAwards->entity_id==$entity_masters->id ?  'selected' :''); ?>  value="<?php echo e($entity_masters->id); ?>"><?php echo e($entity_masters->entity_name); ?></option>
				<?php endforeach; ?>
			<?php endif; ?>


		</select>
	</div>
</div>

<!-- Plan Name Field -->
<div class="form-group">
	<label class="col-sm-4 control-label" for="form-field-1">
		Award name
	</label>
	<div class="col-sm-8">
		<input value="<?php echo e(isset($editAwards['plan_name']) ? $editAwards['plan_name'] : ''); ?>" name="plan_name" type="text" placeholder="" class="form-control">
	</div>
</div>

<!-- Plan Desc Field -->
<div class="form-group">
	<label class="col-sm-4 control-label" for="form-field-1">
	Award Description
	</label>
	<div class="col-sm-8">
		<input value="<?php echo e(isset($editAwards['plan_desc']) ? $editAwards['plan_desc'] : ''); ?>" name="plan_desc" type="text" placeholder="" class="form-control">
	</div>
</div>

<!-- Start Date Field -->
<div class="form-group">
	<label class="col-sm-4 control-label" for="form-field-1">
		Award Start Date
	</label>
<?php /*	<div class="col-sm-8">*/ ?>
<?php /*		<input value="<?php echo e(isset($editAwards['start_date']) ? $editAwards['start_date'] : ''); ?>" name="start_date" type="text" data-date-format="yyyy-mm-dd" placeholder="" class="datepicker">*/ ?>
<?php /*	</div>*/ ?>

	<div class='col-sm-8 input-group date' id='datetimepicker1'>
		<input name="start_date" type="text" placeholder="" class="form-control" />
		<span class="input-group-addon">
			<span class="glyphicon glyphicon-calendar"></span>
		</span>
	</div>

</div>

<!-- End Date Field -->
<div class="form-group">
	<label class="col-sm-4 control-label" for="form-field-1">
		Award End Date
	</label>
<?php /*	<div class="col-sm-8">*/ ?>
<?php /*		<input value="<?php echo e(isset($editAwards['end_date']) ? $editAwards['end_date'] : ''); ?>" name="end_date" type="text" data-date-format="yyyy-mm-dd 00:00" placeholder="" class="datepicker">*/ ?>
<?php /*	</div>*/ ?>

	<div class='col-sm-8 input-group date' id='datetimepicker2'>
		<input name="end_date" type="text" placeholder="" class="form-control" />
		<span class="input-group-addon">
			<span class="glyphicon glyphicon-calendar"></span>
		</span>
	</div>

</div>

<!-- Logo Field -->
<div class="form-group">
	<label class="col-sm-4 control-label" for="form-field-1">
		Award Logo
	</label>
	<!-- <div class="col-sm-9">
		<div class="dropzone dz-clickable dz-single dz-uploadimage1" data-name="logo" ></div>
		<input value="<?php echo e(isset($editAwards['logo']) ? $editAwards['logo'] : ''); ?>" name="logo" type="text" placeholder="" class="form-control">
	</div> -->

	<div class="col-sm-8">
<?php /*		<div class="dropzone dz-clickable dz-single dz-uploadimage1" data-name="logo" ></div>*/ ?>
<?php /*		<input name="logo" type="hidden" value="<?php echo e(isset($editAwards['logo']) ? $editAwards['logo'] : ''); ?>">*/ ?>

		<input type="file" name="image_name" class="form-control" id="name" value="" onchange="loadFile(event)"><br>
		<img id="output" src="#" alt="your image" width="120px" height="120px"/>
	</div>
</div>



<script src="https://cdn.jsdelivr.net/momentjs/2.14.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">

<script type="text/javascript">

	$(function () {

		var start_date = "<?php echo isset($editAwards['start_date']) ? strtotime($editAwards['start_date']) : ''; ?>";
		var end_date = "<?php echo isset($editAwards['end_date']) ? strtotime($editAwards['end_date']) : ''; ?>";
		var award_start_date = new Date(start_date * 1000);
		var award_end_date = new Date(end_date * 1000);
		// console.log(`start_date = ${start_date}, award_start_date = ${award_start_date}`);

		if (award_start_date){

			$('#datetimepicker1').datetimepicker({
				date: award_start_date
			});

		}else {
			$('#datetimepicker1').datetimepicker({
				// setDate : new Date(),
			});

		}

		if (award_end_date){

			$('#datetimepicker2').datetimepicker({
				date: award_end_date
			});

		}else {
			$('#datetimepicker2').datetimepicker({
				// setDate : new Date(),
			});

		}

	});

	$(document).ready(function() {
  		$(".js-example-basic-single").select2();
	});
</script>

<script>
	var loadFile = function(event) {
		var output = document.getElementById('output');
		output.src = URL.createObjectURL(event.target.files[0]);
	};
</script>