<div class="col-md-12" id="ajax_div">
    <!--panel details_company -->
    <div class="panel with-nav-tabs panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">
                <div class="bars pull-right">
                    <a href="#"><i class="maximum fa fa-expand" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Maximize"></i></a>
                    <a href="#"><i class="minimize fa fa-chevron-down" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Collapse"></i></a>
                </div>
            </h3>
            <ul class="nav nav-tabs">
                <li class="<?php echo e(isset($tab)&&$tab==1?'active':''); ?>">
                    <a href="#tab1Awards" data-toggle="tab">
                        <i class="fa fa-Awards fa-lg danger"></i>
                        Awards
                    </a>
                </li>
                <li class="<?php echo e(isset($tab)&&$tab==2?'active':''); ?>" <?php if($tab!=2): ?>style="display:none;"<?php endif; ?>>
                    <a href="#newAwards" id="newAwards-1" data-toggle="tab">
                        <i class="fa fa-Awards-plus success"></i>
                        New Awards
                    </a>
                </li>
            </ul>
        </div>
        <div class="panel-body no-padding">
            <?php echo $__env->make('message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <div class="tab-content">
                <div class="tab-pane fade  <?php echo e(isset($tab)&&$tab==1?'active in':''); ?> tab1" id="tab1Awards">
                    <div class="col-md-12"></br>
                        <div class="form-inline">
                            <div class="form-group">
                                <select class="form-control paging" id="paging" name="paging">
                                    <option  <?php echo e(isset($request['paging'])&&$request['paging']==10 ?  'selected' :''); ?>  value="10">10</option>
                                    <option  <?php echo e(isset($request['paging'])&&$request['paging']==25 ?  'selected' :''); ?>  value="25">25</option>
                                    <option  <?php echo e(isset($request['paging'])&&$request['paging']==50?  'selected' :''); ?>  value="50">50</option>
                                    <option  <?php echo e(isset($request['paging'])&&$request['paging']==100?  'selected' :''); ?>  value="100">100</option>
                                </select>
                            </div>

                            <div class="btn-group">
                                <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" >
                                    Actions <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <?php if(\Session::get('role_id') == 1 || \Session::get('role_id') == 2): ?>
                                            <a href="javascript:void(0);" data-toggle="modal" data-target="#mydelete"><i class="fa fa-trash danger"></i> Delete</a>
                                        <?php endif; ?>
                                        <?php if (\Entrust::can('export_csv')) : ?>
                                        <a href="#" id='csv' class="export" ><i class="fa fa-download blue"></i> Export to CSV</a>
                                        <?php endif; // Entrust::can ?>
                                        <?php if (\Entrust::can('export_xls')) : ?>
                                        <a href="#"  id='xls' class="export" ><i class="fa fa-download green"></i> Export to EXCEL</a>
                                        <?php endif; // Entrust::can ?>
                                        <?php if (\Entrust::can('export_pdf')) : ?>
                                        <a href="#"  id='pdf' class="export" ><i class="fa fa-download red"></i> Export to PDF</a>
                                        <?php endif; // Entrust::can ?>
                                    </li>
                                </ul>
                            </div>

                            <?php if(\Session::get('role_id') == 1 || \Session::get('role_id') == 2 || \Session::get('role_id') == 4 ): ?>
                                <div class="pull-right">
                                    <a href="#" class="btn btn-success pull-right" data-tab-destination="newAwards-1">
                                        <i class="fa fa-plus"></i> Add
                                    </a>
                                </div>
                            <?php endif; ?>
                        </div>
                        <hr>
                        <div class="res-table">
                            <table class="table table-striped table-hover table-fixed ellipsis-table" >
                                <thead>
                                <tr>
                                    <th width="5%" class="center">
                                        <div class="checkbox checkbox-primary">
                                            <input id="checkbox2" type="checkbox" class="conchkSelectAll">
                                            <label for="checkbox2">
                                            </label>
                                        </div>
                                    </th>
                                    <?php echo $__env->make("AwardsView::Awardsshow_table_header", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                    <th width="5%">
                                        <i class="fa fa-bolt"></i>
                                        <a href="#"><i class="fa fa-search search-toggle-btn"></i></a>
                                    </th>
                                    <th width="13%">
                                        <i class="fa fa-bolt">Results</i>
                                    </th>
                                    <th width="10%">
                                        <i class="fa fa-bolt">Status</i>
                                    </th>

                                    <th width="10%">
                                        <i class="fa fa-bolt">Delete</i>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td colspan="3">
                                        <input  value="<?php echo e(isset($request['serach_txt'])?  $request['serach_txt']:''); ?>" id='search_text' name="search_text" type="text" class="form-control" placeholder="search">
                                    </td>
                                    <td>
                                        <a class="btn btn-default search-btn"><i class="fa fa-search"></i></a>
                                    </td>
                                </tr>
                                <?php if(isset($Awards)): ?>
                                    <?php foreach($Awards as $content): ?>
                                        <tr>
                                            <td class="center">
                                                <div class="checkbox checkbox-primary">
                                                    <input id="<?php echo e($content->id); ?>" type="checkbox" class="conchkNumber">
                                                    <label for="<?php echo e($content->id); ?>">
                                                    </label>
                                                </div>
                                            </td>
                                            <?php echo $__env->make("AwardsView::Awardsshow_table_fields", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                            <td>
                                                <?php if(\Session::get('role_id') == 1 || \Session::get('role_id') == 2 || \Session::get('role_id') == 4 ): ?>
                                                    <a id="<?php echo e($content->id); ?>" href="#" class="fa fa-pencil fa-lg edit-href" ></a>
                                                <?php endif; ?>
                                            </td>

                                            <td data-title="active">
                                                <div class="m-switch">
                                                    <form id="form"   class="form-horizontal form" method="post" action="<?php echo e(url("/admin/awards/client_result_status")); ?>" autocomplete="off">
                                                        <?php echo e(csrf_token()); ?>

                                                        <input type="hidden" name="div_id" id="div_id" value="<?php echo e(isset($content->id) ? $content->id : 0); ?>">
                                                        <?php if($content->show_client_result == 1): ?>
                                                            <button type="submit" class="btn btn-warning" name="btnstatus" value="hide">Hide Results</span> </button>
                                                        <?php else: ?>
                                                            <button type="submit" class="btn btn-primary" name="btnstatus" value="show">Show Results</span> </button>
                                                        <?php endif; ?>

                                                    </form>

                                                </div>
                                            </td>

                                            <td data-title="active">
                                                <div class="m-switch">
                                                    <form id="form"   class="form-horizontal form" method="post" action="<?php echo e(url("/admin/awards/action_status")); ?>" autocomplete="off">
                                                        <?php echo e(csrf_token()); ?>

                                                        <input type="hidden" name="div_id1" id="div_id1" value="<?php echo e(isset($content->id) ? $content->id : 0); ?>">
                                                        <?php if($content->active_status == 1): ?>
                                                            <button type="submit" class="btn btn-danger" name="btnstatus" value="disable"> Disable</span> </button>
                                                        <?php else: ?>
                                                            <button type="submit" class="btn btn-success" name="btnstatus" value="enable"> Enable</span> </button>
                                                        <?php endif; ?>

                                                    </form>

                                                </div>
                                            </td>

                                            <td data-title="active">
                                                <div class="m-switch">
                                                    <input type="hidden" name="div_id2" id="div_id2" value="<?php echo e(isset($content->id) ? $content->id : 0); ?>">
                                                    <a href="#" id="deleteawardmodalhref" name="deleteawardmodalhref" data-toggle="modal" data-target="#deleteAwardModal" type="button" class="btn btn-sm btn-danger" onclick="getrecordID(<?php echo e(isset($content->id) ? $content->id : 0); ?>)">Delete</a>
                                                </div>
                                            </td>

                                        </tr>
                                    <?php endforeach; ?>
                                <?php endif; ?>

                                </tbody>
                            </table>
                            <?php if(isset($Awards)): ?>
                                <?php echo $Awards->render(); ?>

                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade  <?php echo e(isset($tab)&&$tab==2?'active in':''); ?> tab2" id="newAwards">
                    <?php echo $__env->make('errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <form id="form" class="form-horizontal form" method="post" action="<?php echo e(url("/admin/awards")); ?>" enctype="multipart/form-data" autocomplete="off">
                        <?php echo e(csrf_token()); ?>

                        <input type="hidden" name="url" id="url" value="<?php echo e(url("/admin/awards")); ?>">
                        <input type="hidden" name="id" id="id" value="<?php echo e(isset($editAwards['id']) ? $editAwards['id'] : 0); ?>">
                        <input type="hidden" name="page" id="page" value="<?php echo e(isset($request['page']) ?$request['page'] : 1); ?>">
                        <div class="col-md-6"><br>
                            <?php echo $__env->make("AwardsView::Awardsfields", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                            <?php /*relation fields*/ ?>

                        </div>
                        <div class="col-md-12 layout no-padding">
                            <div class="panel-footer">
                                <div class="progress-btn">
                                    <?php if(\Session::get('role_id') == 1 || \Session::get('role_id') == 2 || \Session::get('role_id') == 4 ): ?>
                                        <?php if(!isset($editAwards['id'])||$editAwards['id']==""): ?>
                                            <button type="submit" class="btn btn-success" name="btnsave" value="save"><i class="fa fa-save"></i><span class="ladda-label"> Save</span> </button>
                                            <?php /* <a   class=" submit-btn btn btn-success ladda-button" data-style="expand-right"><i class="fa fa-save"></i><span class="ladda-label"> Save</span></a>*/ ?>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                    <?php if(\Session::get('role_id') == 1 || \Session::get('role_id') == 2 || \Session::get('role_id') == 4 ): ?>
                                        <?php if(isset($editAwards['id'])&&$editAwards['id']>""): ?>
                                            <?php /*                                        <a    class="btn btn-info edit-btn"><i class="fa fa-save"></i>Update</a>*/ ?>
                                            <button type="submit" class="btn btn-info edit-btn" name="btnupdate" value="update"><i class="fa fa-save"></i> Update</span> </button>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                    <button class="btn btn-link cancel-btn"> Cancel</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- end panel -->

    <div class="modal fade" id="deleteAwardModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel3">DELETE CONFIRMATION</h4>
                </div>
                <div class="modal-body">
                    <p class="statusMsg"></p>
                    <form id="form"   class="form-horizontal form" method="post" action="<?php echo e(url("/admin/awards/delete")); ?>" autocomplete="off">
                        <?php echo e(csrf_token()); ?>

                        <input type="hidden" name="award_id" id="award_id" value="">
                        <h4 style="text-align: center;">Are you sure you want to delete this record?</h4>
                        <button type="submit" class="btn btn-danger" name="btnstatus" value="disable" data-toggle="modal" data-target="#smsBalanceModel"> Yes, Delete</span> </button>
                    </form>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        // $(document).ready(function() {
        function getrecordID(record_id){
            // var record_id = document.getElementById("id").getAttribute("value");
            document.getElementById("award_id").value = record_id;
            // console.log(`In  getrecordID. record_id = ${record_id}`);
        }
        // });
    </script>

</div>