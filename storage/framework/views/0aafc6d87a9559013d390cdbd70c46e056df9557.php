<!-- Institutionname Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Institutionname
	</label>
	<div class="col-sm-9">
		<input value="<?php echo e(isset($editGeneralSettings['institutionname']) ? $editGeneralSettings['institutionname'] : ''); ?>" name="institutionname" type="text" placeholder="" class="form-control">
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label" for="form-field-1">
		Institutionlogo
	</label>
	<div class="col-sm-10">
		<div class="dropzone dz-clickable dz-single dz-uploadimage1" data-name="institutionlogo" ></div>
		<input name="institutionlogo" type="hidden" value="<?php echo e(isset($editGeneralSettings['institutionlogo']) ? $editGeneralSettings['institutionlogo'] : ''); ?>">
	</div>
</div>

<!-- Electiondate Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Electiondate
	</label>
	<div class="col-sm-9">
		<input value="<?php echo e(isset($editGeneralSettings['electiondate']) ? $editGeneralSettings['electiondate'] : ''); ?>" name="electiondate" type="text" data-date-format="yyyy-mm-dd" placeholder="" class="datepicker">
	</div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label" for="form-field-1">
       Votestart
    </label>
    <div class="col-sm-9">
        <select name="votestart" class="form-control">
            <option  <?php echo e(isset($editGeneralSettings["votestart"])&&$editGeneralSettings["votestart"]=="YES" ? "selected" : ""); ?>   value="YES"">YES</option><option  <?php echo e(isset($editGeneralSettings["votestart"])&&$editGeneralSettings["votestart"]=="NO" ? "selected" : ""); ?>   value="NO"">NO</option>
        </select>
    </div>
</div>




<!-- Votestarttime Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Votestarttime
	</label>
	<div class="col-sm-9">
		<input value="<?php echo e(isset($editGeneralSettings['votestarttime']) ? $editGeneralSettings['votestarttime'] : ''); ?>" name="votestarttime" type="text" data-date-format="yyyy-mm-dd" placeholder="" class="datepicker">
	</div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label" for="form-field-1">
       Voteend
    </label>
    <div class="col-sm-9">
        <select name="voteend" class="form-control">
            <option  <?php echo e(isset($editGeneralSettings["voteend"])&&$editGeneralSettings["voteend"]=="YES" ? "selected" : ""); ?>   value="YES"">YES</option><option  <?php echo e(isset($editGeneralSettings["voteend"])&&$editGeneralSettings["voteend"]=="NO" ? "selected" : ""); ?>   value="NO"">NO</option>
        </select>
    </div>
</div>




<!-- Voteendtime Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Voteendtime
	</label>
	<div class="col-sm-9">
		<input value="<?php echo e(isset($editGeneralSettings['voteendtime']) ? $editGeneralSettings['voteendtime'] : ''); ?>" name="voteendtime" type="text" data-date-format="yyyy-mm-dd" placeholder="" class="datepicker">
	</div>
</div>

<!-- Academicyear Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Academicyear
	</label>
	<div class="col-sm-9">
		<input value="<?php echo e(isset($editGeneralSettings['academicyear']) ? $editGeneralSettings['academicyear'] : ''); ?>" name="academicyear" type="text" placeholder="" class="form-control">
	</div>
</div>

<!-- Registeredby Field -->
<?php /* <div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Registeredby
	</label>
	<div class="col-sm-9">
		<input value="<?php echo e(isset($editGeneralSettings['registeredby']) ? $editGeneralSettings['registeredby'] : ''); ?>" name="registeredby" type="text" placeholder="" class="form-control">
	</div>
</div> */ ?>

<div class="form-group">
    <label class="col-sm-3 control-label" for="form-field-1">
       Activestatus
    </label>
    <div class="col-sm-9">
        <select name="activestatus" class="form-control">
            <option  <?php echo e(isset($editGeneralSettings["activestatus"])&&$editGeneralSettings["activestatus"]=="YES" ? "selected" : ""); ?>   value="YES"">YES</option><option  <?php echo e(isset($editGeneralSettings["activestatus"])&&$editGeneralSettings["activestatus"]=="NO" ? "selected" : ""); ?>   value="NO"">NO</option>
        </select>
    </div>
</div>



