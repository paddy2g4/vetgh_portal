<?php $__env->startSection('content'); ?>
    <!-- /end widget progress .col-md-12 -->
    <!-- Admin over view .col-md-12 -->

    <!-- Admin over view .col-md-12 -->
    <div class="col-md-12 ">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title">
                    <?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <i class="fa fa-table"></i>

                    <button type="button" class="btn btn-success btn-md col-sm-offset-5" name="btn_checkbalance" data-toggle="modal" data-target="#newticketTypeModel">NEW RECORD</button>

                    <div class="bars pull-right">
                        <a href="#"><i class="maximum fa fa-expand" data-toggle="tooltip" data-placement="bottom" title="Maximize"></i></a>
                        <a href="#"><i class="minimize fa fa-chevron-down" data-toggle="tooltip" data-placement="bottom" title="Collapse"></i></a>
                        <!-- <a href="#"><i data-target="#panel2" data-dismiss="alert" data-toggle="tooltip" data-placement="bottom" title="Close" class="fa fa-times"></i></a> -->
                    </div>
                </div>
            </div>
            <div class="panel-body">

                    <table id="example3" class="table table-striped table-bordered width-100 cellspace-0">
                        <thead>
                        <tr>
                            <th width="5%">#</th>
                            <th width="20%">Event</th>
                            <th width="20%">Ticket Type</th>
                            <th width="10%">Status</th>
                            <th width="15%">Date Created</th>
                            <th width="10%">Actions</th>
                            <th width="10%">Actions</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php if(isset($ticket_types)): ?>
                            <?php foreach($ticket_types as  $key=>$ticket_types): ?>

                                <tr>
                                    <td><?php echo e($key+1); ?></td>
                                    <td><?php echo e($ticket_types->div_name); ?></td>
                                    <td><strong><?php echo e($ticket_types->class_desc); ?></strong></td>
                                    <td>
                                        <?php if($ticket_types->active_status == '0'): ?>
                                            In Active
                                        <?php elseif($ticket_types->active_status == '1'): ?>
                                            Active
                                        <?php endif; ?>
                                    </td>
                                    <td><?php echo e(date('d-F-Y g:i A', strtotime($ticket_types->created_at) )); ?></td>
                                    <td>
                                        <button type="button" class="btn btn-primary btn-md" name="btn_edit" id="btn_edit" onclick="ShowEditModal(<?php echo e(isset($ticket_types->id) ? $ticket_types->id : 0); ?>)">Edit</button>
                                        <input type="hidden" name="update_id" id="update_id"  value="<?php echo e(isset($ticket_types->id) ? $ticket_types->id : 0); ?>">
                                    </td>
                                    <td>
                                        <form action="<?php echo e(url('admin/ticket_types/edit', [$ticket_types->id])); ?>" method="POST">
                                            <?php echo e(csrf_field()); ?>

                                            <?php if($ticket_types->active_status == '1'): ?>
                                                <button type="submit" class="btn btn-danger" name="btn_status" value="deactivate">Deactivate</span> </button>
                                            <?php else: ?>
                                                <button type="submit" class="btn btn-success" name="btn_status" value="activate">Activate</span> </button>
                                            <?php endif; ?>
                                        </form>
                                    </td>
                                </tr>

                            <?php endforeach; ?>

                        <?php endif; ?>

                        </tbody>
                    </table>
                    <div clASs="box-footer">
                        <?php /* <button type="submit" clASs="btn btn-primary btn-lg col-sm-offset-5" name="btnconfirmresults">PRINT</button> */ ?>
                    </div>
            </div>
        </div><!-- end panel -->
    </div><!-- end .col-md-12 -->

    <div class="modal fade" id="newticketTypeModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel3">TICKET TYPES DETAILS</h4>
                </div>
                <div class="modal-body">
                    <p class="statusMsg"></p>
                    <form method="post" role="form" action="<?php echo e(url("/admin/ticket_types/create")); ?>" enctype="multipart/form-data" autocomplete="off">
                        <?php echo e(csrf_field()); ?>


                        <div class="form-group">
                            <label for="inputName" class="col-sm-12">Event</label>
                            <select name="entity_div_code" class="js-example-basic-single col-sm-12" required>

                                <?php if(isset($Awards)): ?>
                                    <?php foreach($Awards as $Awards): ?>
                                        <option  <?php echo e(isset($editSMSAccounts)&&$editSMSAccounts->plan_id==$Awards->entity_div_code ?  'selected' :''); ?>  value="<?php echo e($Awards->entity_div_code); ?>"> <?php echo e($Awards->event_name); ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>

                            </select><br>

                        </div><br>

                        <div class="form-group col-sm-12">
                            <br><label for="inputName">Ticket Type</label>
                            <input type="text" class="form-control" name="class_desc" id="class_desc" placeholder="Enter Ticket Type" required/>
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="inputName">Comment</label>
                            <input type="textarea" class="form-control" name="comment" id="comment" placeholder="Comment"/>
                        </div>


                        <div clASs="box-footer">
                            <button type="submit" class="btn btn-success btn-md col-sm-offset-5" name="btnview_results">SAVE</button> <br><br>
                        </div>

                    </form>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="editticketTypeModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel3">TICKET TYPES DETAILS</h4>
                </div>
                <div class="modal-body">
                    <p class="statusMsg"></p>
                    <form method="post" role="form" action="<?php echo e(url("/admin/ticket_types/ticket_types_update")); ?>" enctype="multipart/form-data" autocomplete="off">
                        <?php echo e(csrf_field()); ?>


                        <input type="hidden" name="ticket_type_id" id="ticket_type_id" value="">

                        <div class="form-group">
                            <label for="inputName" class="col-sm-12">Event</label>
                            <select name="edit_plan_id" id="edit_plan_id" class="js-example-basic-single col-sm-12" required>
                            </select><br>

                        </div><br>

                        <div class="form-group col-sm-12">
                            <br><label for="inputName">Ticket Type</label>
                            <input type="text" class="form-control" name="edit_class_desc" id="edit_class_desc" placeholder="Enter Ticket Type" required/>
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="inputName">Comment</label>
                            <input type="textarea" class="form-control" name="edit_comment" id="edit_comment" placeholder="Comment"/>
                        </div>


                        <div clASs="box-footer">
                            <button type="submit" class="btn btn-success btn-md col-sm-offset-5" name="btn_update_ticket">UPDATE</button> <br><br>
                        </div>

                    </form>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


    <!-- /end Admin over view .col-md-12 -->

    <!-- General JS script library-->
    <?php /* <script type="text/javascript" src="../assets/vendors/jquery/jquery.min.js"></script> */ ?>
    <?php /* <script type="text/javascript" src="../assets/vendors/jquery-ui/js/jquery-ui.min.js"></script>	 */ ?>
<?php /*    <script type="text/javascript" src="../assets/vendors/bootstrap/js/bootstrap.min.js"></script>*/ ?>
    <script type="text/javascript" src="../assets/vendors/jquery-searchable/js/jquery.searchable.min.js"></script>
    <script type="text/javascript" src="../assets/vendors/jquery-fullscreen/js/jquery.fullscreen.min.js"></script>

    <!-- Related JavaScript Library to This Pagee -->
    <script type="text/javascript" src="../assets/vendors/jquery-datatables/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../assets/vendors/jquery-datatables/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="../assets/vendors/jquery-datatables/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="../assets/vendors/jquery-datatables/js/dataTables.tableTools.min.js"></script>
    <script type="text/javascript" src="../assets/vendors/jquery-datatables/js/dataTables.colVis.min.js"></script>


    <!-- Plugins Script -->
    <script type="text/javascript">
        $(document).ready(function() {
            $(".js-example-basic-single").select2();


        });

        // Default Data Table Script
        $('#example1').dataTable({
            responsive: true
        });

        // Table Tools Data Tables
        $('#example2').dataTable({
            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>"+
                "t"+
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
            "oTableTools": {
                "aButtons": [
                    "copy",
                    "csv",
                    "xls",
                    {
                        "sExtends": "pdf",
                        "sTitle": "castVoteGH_PDF",
                        "sPdfMessage": "castVoteGH PDF Export",
                        "sPdfSize": "letter"
                    },
                    {
                        "sExtends": "print",
                        "sMessage": "Generated by CASTVOTE GH <i>(press Esc to close)</i>"
                    }
                ],

            },
            "autoWidth" : true,
            responsive: true

        });

        // Column show & hide
        // $('#example3').DataTable( {
        //     "dom": '<"pull-left"f><"pull-right"l>tip',
        //     "dom": 'C<"clear">lfrtip',
        //
        //     "bLengthChange": false,
        //     responsive: true
        // });


        function ShowEditModal(ticket_type_id) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                }
            });
            // e.preventDefault();
            var formData = {
                ticket_type_id: ticket_type_id
            };
            var type = "POST";
            var ajaxurl = 'ticket_types/get_ticket_types_details';
            $.ajax({
                type: type,
                url: ajaxurl,
                data: formData,
                dataType: 'json',
                success: function (data) {
                    var awards = data.awards;
                    var ticket_types_details = data.ticket_types_details[0];
                    var selected_plan_id = ticket_types_details['assigned_code'];

                    // console.log(ticket_types_details['id'], ticket_types_details['plan_name'], ticket_types_details['class_desc'], ticket_types_details['comment']);
                    $('#edit_class_desc').val(ticket_types_details['class_desc']);
                    $('#edit_comment').val(ticket_types_details['comment']);

                    var edit_plan_id = $('#edit_plan_id');
                    edit_plan_id.empty();
                    $.each(awards, function(index, element) {

                        if(element.assigned_code == selected_plan_id){
                            var newOption = new Option(element.div_name,element.assigned_code, true, false);
                            edit_plan_id.append(newOption);

                        }else{
                            var newOption = new Option(element.div_name,element.assigned_code, false, false);
                            edit_plan_id.append(newOption);
                        }

                    });

                    edit_plan_id.val(selected_plan_id); // Select the option with a value of '1'
                    edit_plan_id.trigger('change');

                    $('#ticket_type_id').val(ticket_type_id);

                    jQuery('#editticketTypeModel').modal('show');

                },
                error: function (data) {
                    console.log(data);
                }
            });
        }


    </script>

    <div class="col-md-6">
        <!-- end panel -->
        <script type="text/javascript">
            $(document).ready(function() {
                $(".js-example-basic-single").select2();
            });
        </script>
    </div>




    <!-- /end Admin over view .col-md-12 -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('reportmaster', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>