<!-- Name Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Name
	</label>
	<div class="col-sm-9">
		<input value="<?php echo e(isset($editTrial1['name']) ? $editTrial1['name'] : ''); ?>" name="name" type="text" placeholder="" class="form-control">
	</div>
</div>
