<?php $__env->startSection('content'); ?>
    <!-- widget progress .col-md-12 -->
   <div class="col-md-12">
      <br><br>

      <div  class="panel panel-default">
         <div class="panel-body">
            <i class="glyphicon glyphicon-stats"></i>
            <b><?php echo e(trans('main.admin_overview')); ?></b>
            <hr>

            <div class="row">
               <h2 style="text-align: center;">Welcome to CASTVOTEGH Admin Portal</h2>
            </div>
            <br><br>

         </div>
      </div>

   </div>
   <!-- /end widget progress .col-md-12 -->
   <!-- Admin over view .col-md-12 -->
<?php /*   <div class="col-md-12 ">*/ ?>
<?php /*      <div  class="panel panel-default">*/ ?>
<?php /*         <div class="panel-body">*/ ?>

<?php /*            <i class="glyphicon glyphicon-stats"></i>*/ ?>
<?php /*            <b><?php echo e(trans('main.admin_overview')); ?></b>*/ ?>
<?php /*            <hr>*/ ?>
<?php /*            <div class="row">*/ ?>
<?php /*               <!-- progress section -->*/ ?>
<?php /*               <div class="col-sm-3">*/ ?>
<?php /*                  <div class="headprogress">*/ ?>
<?php /*                     <strong><?php echo e(trans('main.tasks')); ?></strong>*/ ?>
<?php /*                     <strong class="progress-value">60/100 <i class="fa fa-tasks"></i></strong>*/ ?>
<?php /*                  </div>*/ ?>
<?php /*                  <div class="progress progress-xs">*/ ?>
<?php /*                     <div class="progress-bar progress-bar-info" role="progressbar"  style="width: 60%;">*/ ?>
<?php /*                     </div>*/ ?>
<?php /*                  </div>*/ ?>

<?php /*                  <div class="headprogress">*/ ?>
<?php /*                     <strong><?php echo e(trans('main.capacity')); ?></strong>*/ ?>
<?php /*                     <strong class="progress-value">40/100 GB <i class="fa fa-pie-chart"></i></strong>*/ ?>
<?php /*                  </div>*/ ?>
<?php /*                  <div class="progress  progress-xs">*/ ?>
<?php /*                     <div class="progress-bar progress-bar-info" role="progressbar" style="width: 40%;">*/ ?>
<?php /*                     </div>*/ ?>
<?php /*                  </div>*/ ?>

<?php /*                  <div class="headprogress">*/ ?>
<?php /*                     <strong><?php echo e(trans('main.email')); ?></strong>*/ ?>
<?php /*                     <strong class="progress-value">70% <i class="fa fa-envelope"></i></strong>*/ ?>
<?php /*                  </div>*/ ?>
<?php /*                  <div class="progress progress-xs">*/ ?>
<?php /*                     <div class="progress-bar progress-bar-info" role="progressbar" style="width: 70%;">*/ ?>
<?php /*                     </div>*/ ?>
<?php /*                  </div>*/ ?>

<?php /*                  <div class="headprogress">*/ ?>
<?php /*                     <strong><?php echo e(trans('main.close_request')); ?></strong>*/ ?>
<?php /*                     <strong class="progress-value">90% <i class="fa fa-ticket"></i></strong>*/ ?>
<?php /*                  </div>*/ ?>
<?php /*                  <div class="progress progress-xs">*/ ?>
<?php /*                     <div class="progress-bar progress-bar-info" role="progressbar"  style="width: 90%;">*/ ?>
<?php /*                     </div>*/ ?>
<?php /*                  </div>*/ ?>

<?php /*                  <a class="btn btn-success btn-block"><i class="fa fa-download"></i> <?php echo e(trans('main.download')); ?> <strong><?php echo e(trans('main.report')); ?></strong></a>*/ ?>
<?php /*                  <a class="btn btn-default btn-block"><i class="fa fa-file-o"></i> <?php echo e(trans('main.report_a')); ?> <strong><?php echo e(trans('main.bug')); ?></strong></a>*/ ?>
<?php /*               </div>*/ ?>
<?php /*               <!-- ./preogress section -->*/ ?>
<?php /*               <!-- chart section -->*/ ?>
<?php /*               <div class="col-sm-6">*/ ?>
<?php /*                  <div class="well well-sm well-light padding-10">*/ ?>
<?php /*                     <h4 >*/ ?>
<?php /*                        <?php echo e(trans('main.active')); ?> <strong><?php echo e(trans('main.visit')); ?></strong>*/ ?>
<?php /*                        <a href="javascript:void(0);" class="pull-right ">*/ ?>
<?php /*                           <i class="fa fa-refresh"></i>*/ ?>
<?php /*                        </a>*/ ?>
<?php /*                     </h4>*/ ?>
<?php /*                     <div id="areaChart" class="chart-dashboard"></div>*/ ?>
<?php /*                  </div>*/ ?>
<?php /*               </div>*/ ?>
<?php /*               <!-- ./chart section -->*/ ?>

<?php /*               <!-- pie chart -->*/ ?>
<?php /*               <div class="col-sm-3 ">*/ ?>
<?php /*                  <div class="col-md-6 no-padd-left-right">*/ ?>
<?php /*                     <div class="well well-sm text-center">*/ ?>
<?php /*                        <span class="h1 shadow-block no-margin purple">752</span>*/ ?>
<?php /*                          <span class="text-center">*/ ?>
<?php /*                              <i class="fa fa-upload text-muted"></i>*/ ?>
<?php /*                          </span>*/ ?>
<?php /*                        <span class="text-muted"><?php echo e(trans('main.uploads')); ?></span>*/ ?>
<?php /*                     </div>*/ ?>
<?php /*                  </div>*/ ?>
<?php /*                  <div class="col-md-6 no-padd-left-right">*/ ?>
<?php /*                     <div class="well well-sm text-center">*/ ?>
<?php /*                        <span class="h1 shadow-block no-margin green">563</span>*/ ?>
<?php /*                          <span class="text-center">*/ ?>
<?php /*                              <i class="fa fa-file text-muted"></i>*/ ?>
<?php /*                          </span>*/ ?>
<?php /*                        <span class="text-muted"><?php echo e(trans('main.all_news')); ?></span>*/ ?>
<?php /*                     </div>*/ ?>
<?php /*                  </div>*/ ?>
<?php /*                  <div class="col-md-6 no-padd-left-right">*/ ?>
<?php /*                     <div class="well well-sm text-center">*/ ?>
<?php /*                        <span class="h1 shadow-block no-margin orange">236</span>*/ ?>
<?php /*                          <span class="text-center">*/ ?>
<?php /*                              <i class="fa fa-user text-muted"></i>*/ ?>
<?php /*                          </span>*/ ?>
<?php /*                        <span class="text-muted"><?php echo e(trans('main.users')); ?></span>*/ ?>
<?php /*                     </div>*/ ?>
<?php /*                  </div>*/ ?>
<?php /*                  <div class="col-sm-6 no-padd-left-right hidden-sm">*/ ?>
<?php /*                     <div class="well well-sm text-center">*/ ?>
<?php /*                        <span class="h1 shadow-block no-margin darkblue">471</span>*/ ?>
<?php /*                             <span class="text-center">*/ ?>
<?php /*                                 <i class="fa fa-rss text-muted"></i>*/ ?>
<?php /*                             </span>*/ ?>
<?php /*                        <span class="text-muted"><?php echo e(trans('main.feeds')); ?></span>*/ ?>
<?php /*                     </div>*/ ?>
<?php /*                  </div>*/ ?>
<?php /*                  <p class="hidden-sm limit-3-line"><?php echo e(trans('main.content2')); ?></p>*/ ?>
<?php /*               </div>*/ ?>
<?php /*               <!-- ./pie chart -->*/ ?>
<?php /*            </div>*/ ?>
<?php /*         </div>*/ ?>

<?php /*      </div><!-- end panel -->*/ ?>
<?php /*   </div>*/ ?>
   <!-- /end Admin over view .col-md-12 -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>