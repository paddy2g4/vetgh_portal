<!-- Plan Id Field -->
<div class="form-group">
	<label class="col-sm-4 control-label" for="form-field-1">
		Award
	</label>
	<div class="col-sm-8">
		<select name="plan_id" class="js-example-basic-single col-sm-12">
			<?php if(isset($awards)): ?>
				<?php foreach($awards as $awards): ?>
					<option  <?php echo e(isset($editShortCodeMasters)&&$editShortCodeMasters->plan_id==$awards->plan_id ?  'selected' :''); ?>  value="<?php echo e($awards->plan_id); ?>"> <?php echo e($awards->plan_name); ?> - <?php echo e($awards->entity_name); ?></option>
				<?php endforeach; ?>
			<?php endif; ?>
		</select>
	</div>
</div>

<!-- Short Code Ext Field -->
<div class="form-group">
	<label class="col-sm-4 control-label" for="form-field-1">
		Short Code
	</label>
	<div class="col-sm-8">
		<input value="<?php echo e(isset($editShortCodeMasters['short_code_ext']) ? $editShortCodeMasters['short_code_ext'] : ''); ?>" name="short_code_ext" type="text" placeholder="" class="form-control">
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
  		$(".js-example-basic-single").select2();
	});
</script>