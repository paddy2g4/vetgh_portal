<?php $__env->startSection('content'); ?>
    <!-- /end widget progress .col-md-12 -->
    <!-- Admin over view .col-md-12 -->

    <!-- Admin over view .col-md-12 -->

    <div class="col-md-6 col-sm-offset-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title">
                    <i class="fa fa-table"></i>
                    AWARD SELECTOR
                    <div class="bars pull-right">
                        <a href="#"><i class="maximum fa fa-expand" data-toggle="tooltip" data-placement="bottom" title="Maximize"></i></a>
                        <a href="#"><i class="minimize fa fa-chevron-down" data-toggle="tooltip" data-placement="bottom" title="Collapse"></i></a>
                        <a href="#"><i data-target="#panel2" data-dismiss="alert" data-toggle="tooltip" data-placement="bottom" title="Close" class="fa fa-times"></i></a>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <form method="post" role="form" clASs="form-horizontal" action="<?php echo e(url("/admin/votingresults")); ?>">
                    <INput type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                    <div clASs="form-group<?php echo e($errors->hAS('old') ? ' hAS-error' : ''); ?>">
<?php /*                        <div class="form-group">*/ ?>
<?php /*                            <label class="col-sm-4 control-label" for="form-field-1" required>*/ ?>
<?php /*                                Institution*/ ?>
<?php /*                            </label>*/ ?>
<?php /*                            <select name="entity_id" id="entity_id_1" class="js-example-basic-single col-sm-6">*/ ?>
<?php /*                                <?php if(isset($entity_masters)): ?>*/ ?>
<?php /*                                    <?php foreach($entity_masters as $entity_masters): ?>*/ ?>
<?php /*                                        <option  <?php echo e(isset($editAwards)&&$editAwards->entity_id==$entity_masters->id ?  'selected' :''); ?>  value="<?php echo e($entity_masters->id); ?>"><?php echo e($entity_masters->entity_name); ?></option>*/ ?>
<?php /*                                    <?php endforeach; ?>*/ ?>
<?php /*                                <?php endif; ?>*/ ?>
<?php /*                            </select>*/ ?>

<?php /*                        </div>*/ ?>
                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="form-field-1" required>
                                Award
                            </label>
                            <select name="plan_id" class="js-example-basic-single col-sm-6">

                                <?php if(isset($awards)): ?>
                                    <?php foreach($awards as $awards): ?>
                                        <option  <?php echo e(isset($editPricing)&&$editPricing->plan_id==$awards->plan_id ?  'selected' :''); ?>  value="<?php echo e($awards->plan_id); ?>"> <?php echo e($awards->plan_name); ?> - <?php echo e($awards->entity_name); ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>

                            </select>
                        </div>

                    </div>
                    <div clASs="box-footer">
                        <button type="submit" clASs="btn btn-success btn-md col-sm-offset-5" name="btnview_results">VIEW RESULTS</button>
                    </div>
                </form>

                <?php
                if (isset($_POST['month'])){
                    $search = filter_var($_POST['month'], FILTER_SANITIZE_STRING);
                    $_SESSION['month'] = $search;
                    //$tokengenerated= bin2hex(openssl_random_pseudo_bytes(3));
                }
                ?>
            </div>
        </div><!-- end panel -->
    </div><!-- end .col-md-12 -->


    <div class="col-md-8 col-sm-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title">
                    <i class="fa fa-table"></i>
                        <?php if(isset($award_name)): ?>
                            <?php echo e($award_name); ?>

                        <?php endif; ?>
                    <div class="bars pull-right">
                        <a href="#"><i class="maximum fa fa-expand" data-toggle="tooltip" data-placement="bottom" title="Maximize"></i></a>
                        <a href="#"><i class="minimize fa fa-chevron-down" data-toggle="tooltip" data-placement="bottom" title="Collapse"></i></a>
                        <a href="#"><i data-target="#panel2" data-dismiss="alert" data-toggle="tooltip" data-placement="bottom" title="Close" class="fa fa-times"></i></a>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <form method="post" action="" role="form" clASs="form-horizontal">
                    <INput type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">

                    <table id="example3" class="table table-striped table-bordered table-hover width-100 cellspace-0">
                        <tbody>
                        <?php

                        $position1 = DB::table('categories')->select(DB::raw('DISTINCT(cat_name),cat_id'))->where('plan_id','=',\Session::get('result_plan_id'))->where('act_status', '=', true)->orderBy('cat_id', 'asc')->get();

                        foreach ($position1 as $position1){
                            prINt '<tr><td colspan="5" style="text-align:center">POSITION : <strong>'.strtoupper($position1->cat_name).'</strong></td></tr>';
                            prINt'
                                  <tr>
                                      <td>CANDIDATE NAME</td>
                                      <td>VOTES</td>
                                  </tr>';

                            $result2 = DB::table('nominees')->select('nom_name','avatar','nom_id')->where('cat_id', $position1->cat_id)->where('act_status', true)->get();

                            $totalvotecountsforelection = DB::table('voting_summaries')->sum('vote_count');
                            $totalvotecountsforelection = intval($totalvotecountsforelection);

                            $totalvotescastedperposition = DB::select( DB::raw("SELECT SUM(vm.vote_count) AS total_vote_per_category,nom.cat_id,cat.cat_name FROM voting_summaries vm LEFT JOIN nominees nom ON nom.nom_id = vm.nom_id INNER JOIN categories cat ON cat.cat_id = nom.cat_id WHERE cat.cat_id = :category_id  AND vm.act_status = true and vm.del_status = false GROUP BY nom.cat_id,cat.cat_name;"), array(
                                'category_id' => $position1->cat_id,
                            ));
                            foreach($totalvotescastedperposition as $totalvotescastedperposition){
                                if ($totalvotescastedperposition->total_vote_per_category){
                                    $total_vote_per_category = $totalvotescastedperposition->total_vote_per_category;
                                }else{
                                    $total_vote_per_category = 0;
                                }

                            }

                            $nominee_vote = 0;
                            $totalpercentage = "0";
                            $total_vote_per_category = 0;

                            foreach ($result2 as $result2){
                                $resultcandidatename = htmlentities($result2->nom_name);
                                $candidavatar = "../uploads/nominees/".htmlentities($result2->avatar);

                                $total_nominee_vote = DB::select( DB::raw("SELECT SUM(vote_count) AS total_vote FROM voting_summaries WHERE nom_id = :nominee_id AND act_status = true and del_status = false;"), array(
                                    'nominee_id' => $result2->nom_id,
                                ));

                                foreach ($total_nominee_vote as $total_nominee_vote) {
                                    $nominee_vote = intval($total_nominee_vote->total_vote);
                                    $total_vote_per_category = $total_vote_per_category + $nominee_vote;
                                }

                                print '<tr><td><strong>'.strtoupper($resultcandidatename).'</strong></td><td><strong>'.htmlentities($nominee_vote).'</strong></td></tr>';
                            }
                            print '<tr><td colspan="1"></td><td>TOTAL =  '.intval($total_vote_per_category).'</td></tr>';
                            print '<tr><td colspan="3"></td></tr>';
                        }
                        prINt '</table>';
                        ?>

                        </tbody>
                    </table>





                    <div clASs="box-footer">
                        <?php /* <button type="submit" clASs="btn btn-primary btn-lg col-sm-offset-5" name="btnconfirmresults">PRINT</button> */ ?>
                    </div>
                </form>
            </div>
        </div><!-- end panel -->
    </div><!-- end .col-md-12 -->
    <!-- /end Admin over view .col-md-12 -->

    <!-- General JS script library-->
    <?php /* <script type="text/javascript" src="../assets/vendors/jquery/jquery.min.js"></script> */ ?>
    <?php /* <script type="text/javascript" src="../assets/vendors/jquery-ui/js/jquery-ui.min.js"></script>	 */ ?>
    <script type="text/javascript" src="../assets/vendors/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../assets/vendors/jquery-searchable/js/jquery.searchable.min.js"></script>
    <script type="text/javascript" src="../assets/vendors/jquery-fullscreen/js/jquery.fullscreen.min.js"></script>

    <!-- Related JavaScript Library to This Pagee -->
    <script type="text/javascript" src="../assets/vendors/jquery-datatables/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../assets/vendors/jquery-datatables/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="../assets/vendors/jquery-datatables/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="../assets/vendors/jquery-datatables/js/dataTables.tableTools.min.js"></script>
    <script type="text/javascript" src="../assets/vendors/jquery-datatables/js/dataTables.colVis.min.js"></script>


    <!-- Plugins Script -->
    <script type="text/javascript">
        $(document).ready(function() {
            $(".js-example-basic-single").select2();
        });

        // Default Data Table Script
        $('#example1').dataTable({
            responsive: true
        });

        // Table Tools Data Tables
        $('#example2').dataTable({
            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>"+
                "t"+
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
            "oTableTools": {
                "aButtons": [
                    "copy",
                    "csv",
                    "xls",
                    {
                        "sExtends": "pdf",
                        "sTitle": "castVoteGH_PDF",
                        "sPdfMessage": "castVoteGH PDF Export",
                        "sPdfSize": "letter"
                    },
                    {
                        "sExtends": "print",
                        "sMessage": "Generated by CASTVOTE GH <i>(press Esc to close)</i>"
                    }
                ],

            },
            "autoWidth" : true,
            responsive: true

        });

        // Column show & hide
        // $('#example3').DataTable( {
        //     "dom": '<"pull-left"f><"pull-right"l>tip',
        //     "dom": 'C<"clear">lfrtip',
        //
        //     "bLengthChange": false,
        //     responsive: true
        // });


    </script>

    <div class="col-md-6">
        <!-- end panel -->
        <script type="text/javascript">
            $(document).ready(function() {
                $(".js-example-basic-single").select2();
            });
        </script>
    </div>




    <!-- /end Admin over view .col-md-12 -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('reportmaster', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>