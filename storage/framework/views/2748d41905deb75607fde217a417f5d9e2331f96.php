<!-- Cat Id Field -->
<div class="form-group">
	<label class="col-sm-4 control-label" for="form-field-1">
		Category
	</label>
	<div class="col-sm-8">
		<select name="cat_id" class="js-example-basic-single col-sm-12">

			<?php if(isset($categories)): ?>
				<?php foreach($categories as $categories): ?>
					<option  <?php echo e(isset($editNominees["cat_id"])&&$editNominees["cat_id"]==$categories->cat_id ? "selected" : ""); ?> value=<?php echo e($categories->cat_id); ?>><?php echo e($categories->cat_name); ?></option>
				<?php endforeach; ?>
			<?php endif; ?>

		</select>
	</div>
</div>

<!-- Nom Name Field -->
<div class="form-group">
	<label class="col-sm-4 control-label" for="form-field-1">
		Nominee
	</label>
	<div class="col-sm-8">
		<input value="<?php echo e(isset($editNominees['nom_name']) ? $editNominees['nom_name'] : ''); ?>" name="nom_name" type="text" placeholder="" class="form-control">
	</div>
</div>

<!-- Avatar Field -->
<div class="form-group">
	<label class="col-sm-4 control-label" for="form-field-1">
		Nominee's Avatar
	</label>
	<div class="col-sm-8">
		<div class="dropzone dz-clickable dz-single dz-uploadimage1" data-name="avatar" ></div>
		<input name="avatar" type="hidden" value="<?php echo e(isset($editNominees['avatar']) ? $editNominees['avatar'] : ''); ?>">
	</div>
</div>



<script type="text/javascript">
	$(document).ready(function() {
  		$(".js-example-basic-single").select2();
	});
</script>