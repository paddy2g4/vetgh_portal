
<?php $__env->startSection('content'); ?>
    <!-- /end widget progress .col-md-12 -->
    <!-- Admin over view .col-md-12 -->

    <!-- Admin over view .col-md-12 -->

    <div class="col-md-6 col-sm-offset-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title">
                    <i class="fa fa-table"></i>
                    BLAST CONTACTS UPLOADER
                    <div class="bars pull-right">
                        <a href="#"><i class="maximum fa fa-expand" data-toggle="tooltip" data-placement="bottom" title="Maximize"></i></a>
                        <a href="#"><i class="minimize fa fa-chevron-down" data-toggle="tooltip" data-placement="bottom" title="Collapse"></i></a>
                        <a href="#"><i data-target="#panel2" data-dismiss="alert" data-toggle="tooltip" data-placement="bottom" title="Close" class="fa fa-times"></i></a>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <?php if(count($errors) > 0): ?>
                    <div class="alert alert-danger">
                        Upload Validation Error<br><br>
                        <ul>
                            <?php foreach($errors->all() as $error): ?>
                                <li><?php echo e($error); ?></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                <?php endif; ?>

                <?php if($message = Session::get('success')): ?>
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong><?php echo e($message); ?></strong>
                    </div>
                <?php endif; ?>

                <form method="post" role="form" clASs="form-horizontal" action="<?php echo e(url("/admin/blastSMS/sms_blast")); ?>" enctype="multipart/form-data" autocomplete="off">
                    <INput type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                    <div clASs="form-group<?php echo e($errors->hAS('old') ? ' hAS-error' : ''); ?>">

                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="form-field-1" required>
                                Contacts
                            </label>
                            <div class="col-sm-6">
                                <input type="file" name="select_file" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="form-field-1" required>
                                Message
                            </label>
                            <div class="col-sm-6">
                                <textarea placeholder="SMS Message" id="form-field-22" name="msg_body" class="form-control" required></textarea>
                            </div>
                        </div>

                    </div>
                    <div clASs="box-footer">
                        <button type="submit" class="btn btn-success btn-md col-sm-offset-5" name="btnview_results">SEND MESSAGES</button> <br><br>
                        <button type="button" class="btn btn-primary btn-md col-sm-offset-5" name="btn_checkbalance" data-toggle="modal" data-target="#smsBalanceModel">CHECK BALANCE</button>

<?php /*                        <br>*/ ?>
<?php /*                        <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#smsBalanceModel">*/ ?>
<?php /*                            CHECK BALANCE*/ ?>
<?php /*                        </button>*/ ?>

                    </div>
                </form>

                <?php
                if (isset($_POST['month'])){
                    $search = filter_var($_POST['month'], FILTER_SANITIZE_STRING);
                    $_SESSION['month'] = $search;
                    //$tokengenerated= bin2hex(openssl_random_pseudo_bytes(3));
                }
                ?>
            </div>
        </div><!-- end panel -->
    </div><!-- end .col-md-12 -->


    <div class="col-md-8 col-sm-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title">
                    <i class="fa fa-table"></i>
                    <?php if(isset($award_name)): ?>
                        <?php echo e($award_name); ?>

                    <?php endif; ?>
                    <div class="bars pull-right">
                        <a href="#"><i class="maximum fa fa-expand" data-toggle="tooltip" data-placement="bottom" title="Maximize"></i></a>
                        <a href="#"><i class="minimize fa fa-chevron-down" data-toggle="tooltip" data-placement="bottom" title="Collapse"></i></a>
                        <a href="#"><i data-target="#panel2" data-dismiss="alert" data-toggle="tooltip" data-placement="bottom" title="Close" class="fa fa-times"></i></a>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <form method="post" action="" role="form" clASs="form-horizontal">
                    <INput type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">

                    <table id="example3" class="table table-striped table-bordered width-100 cellspace-0">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Mobile Number</th>
                            <th>Message</th>
                            <th>Status</th>
                            <th>Date</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php if(isset($message_logs)): ?>
                            <?php foreach($message_logs as $key=>$message_logs): ?>

                                <tr>
                                    <td><?php echo e($key+1); ?></td>
                                    <td><strong><?php echo e($message_logs->phone_number); ?></strong></td>
                                    <td><?php echo e($message_logs->message); ?></td>

                                    <td>
                                        <?php if($message_logs->status == '0'): ?>
                                            Failed
                                        <?php elseif($message_logs->status == '1'): ?>
                                            Passed
                                        <?php else: ?>
                                            Pending
                                        <?php endif; ?>
                                    </td>
                                    <td><?php echo e($message_logs->created_at); ?></td>
                                </tr>

                            <?php endforeach; ?>

                        <?php endif; ?>

                        </tbody>
                    </table>

                    <div clASs="box-footer">
                        <?php /* <button type="submit" clASs="btn btn-primary btn-lg col-sm-offset-5" name="btnconfirmresults">PRINT</button> */ ?>
                    </div>
                </form>
            </div>
        </div><!-- end panel -->
    </div>


    <div class="modal fade" id="smsBalanceModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel3">SMS BALANCE</h4>
                </div>
                <div class="modal-body">
                    <p class="statusMsg"></p>
                    <form id="form" method="post">
                        <?php echo e(csrf_field()); ?>

                        <div class="form-group">
                            <label for="inputName">Balance (GHS)</label>
                            <input type="text" class="form-control" name="balance" id="balance" disabled="disabled"  value="<?php echo e($sms_balance); ?>" placeholder="Balance"/>
                        </div>
                    </form>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
<?php /*                    <button type="button" class="btn btn-primary">Check Balance</button>*/ ?>
                </div>
            </div>
        </div>
    </div>


    <!-- end .col-md-12 -->
    <!-- /end Admin over view .col-md-12 -->

    <!-- General JS script library-->
    <?php /* <script type="text/javascript" src="../assets/vendors/jquery/jquery.min.js"></script> */ ?>
    <?php /* <script type="text/javascript" src="../assets/vendors/jquery-ui/js/jquery-ui.min.js"></script>	 */ ?>
<?php /*    <script type="text/javascript" src="../assets/vendors/bootstrap/js/bootstrap.min.js"></script>*/ ?>
    <script type="text/javascript" src="../assets/vendors/jquery-searchable/js/jquery.searchable.min.js"></script>
    <script type="text/javascript" src="../assets/vendors/jquery-fullscreen/js/jquery.fullscreen.min.js"></script>

    <!-- Related JavaScript Library to This Pagee -->
    <script type="text/javascript" src="../assets/vendors/jquery-datatables/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../assets/vendors/jquery-datatables/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="../assets/vendors/jquery-datatables/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="../assets/vendors/jquery-datatables/js/dataTables.tableTools.min.js"></script>
    <script type="text/javascript" src="../assets/vendors/jquery-datatables/js/dataTables.colVis.min.js"></script>


    <!-- Plugins Script -->
    <script type="text/javascript">
        $(document).ready(function() {
            $(".js-example-basic-single").select2();
        });

        // Default Data Table Script
        $('#example1').dataTable({
            responsive: true
        });

        // Table Tools Data Tables
        $('#example2').dataTable({
            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>"+
                "t"+
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
            "oTableTools": {
                "aButtons": [
                    "copy",
                    "csv",
                    "xls",
                    {
                        "sExtends": "pdf",
                        "sTitle": "castVoteGH_PDF",
                        "sPdfMessage": "castVoteGH PDF Export",
                        "sPdfSize": "letter"
                    },
                    {
                        "sExtends": "print",
                        "sMessage": "Generated by CASTVOTE GH <i>(press Esc to close)</i>"
                    }
                ],

            },
            "autoWidth" : true,
            responsive: true

        });

        // Column show & hide
        // $('#example3').DataTable( {
        //     "dom": '<"pull-left"f><"pull-right"l>tip',
        //     "dom": 'C<"clear">lfrtip',
        //
        //     "bLengthChange": false,
        //     responsive: true
        // });


    </script>

    <div class="col-md-6">
        <!-- end panel -->
        <script type="text/javascript">
            $(document).ready(function() {
                $(".js-example-basic-single").select2();
            });
        </script>
    </div>




    <!-- /end Admin over view .col-md-12 -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('reportmaster', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>