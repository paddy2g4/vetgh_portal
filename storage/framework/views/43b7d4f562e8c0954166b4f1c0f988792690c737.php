<!-- Plan Id Field -->
<div class="form-group">
	<label class="col-sm-4 control-label" for="form-field-1">
		Award
	</label>
	<div class="col-sm-8">
		<select name="plan_id" class="js-example-basic-single col-sm-12">
			<?php if(isset($awards)): ?>
				<?php foreach($awards as $awards): ?>
					<option  <?php echo e(isset($editSMSAccounts)&&$editSMSAccounts->plan_id==$awards->plan_id ?  'selected' :''); ?>  value="<?php echo e($awards->plan_id); ?>"> <?php echo e($awards->plan_name); ?> - <?php echo e($awards->entity_name); ?></option>
				<?php endforeach; ?>
			<?php endif; ?>

		</select>
	</div>
</div>

<!-- Account Name Field -->
<div class="form-group">
	<label class="col-sm-4 control-label" for="form-field-1">
		Account Name
	</label>
	<div class="col-sm-8">
		<input value="<?php echo e(isset($editSMSAccounts['account_name']) ? $editSMSAccounts['account_name'] : ''); ?>" name="account_name" type="text" placeholder="" class="form-control">
	</div>
</div>

<!-- Account Password Field -->
<div class="form-group">
	<label class="col-sm-4 control-label" for="form-field-1">
		Account Password
	</label>
	<div class="col-sm-8">
		<input value="<?php echo e(isset($editSMSAccounts['account_password']) ? $editSMSAccounts['account_password'] : ''); ?>" name="account_password" type="text" placeholder="" class="form-control">
	</div>
</div>

<!-- Sender Id Field -->
<div class="form-group">
	<label class="col-sm-4 control-label" for="form-field-1">
		SMS Sender Id (Should not be more than 9 characters)
	</label>
	<div class="col-sm-8">
		<input value="<?php echo e(isset($editSMSAccounts['sender_id']) ? $editSMSAccounts['sender_id'] : ''); ?>" name="sender_id" type="text" placeholder="" class="form-control">
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
  		$(".js-example-basic-single").select2();
	});
</script>