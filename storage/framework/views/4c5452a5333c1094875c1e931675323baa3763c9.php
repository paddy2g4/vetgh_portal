<!-- Name Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Name
	</label>
	<div class="col-sm-9">
		<input value="<?php echo e(isset($editTrialElect['name']) ? $editTrialElect['name'] : ''); ?>" name="name" type="text" placeholder="" class="form-control">
	</div>
</div>

<!-- Studentnumber Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Studentnumber
	</label>
	<div class="col-sm-9">
		<input value="<?php echo e(isset($editTrialElect['studentnumber']) ? $editTrialElect['studentnumber'] : ''); ?>" name="studentnumber" type="text" placeholder="" class="form-control">
	</div>
</div>
