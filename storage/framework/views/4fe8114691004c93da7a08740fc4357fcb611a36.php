
<?php $__env->startSection('content'); ?>

    <!-- widget progress .col-md-12 -->

    <div class="col-md-6 col-sm-offset-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title">
                    <i class="fa fa-table"></i>

                    <div class="bars pull-right">
                        <a href="#"><i class="maximum fa fa-expand" data-toggle="tooltip" data-placement="bottom" title="Maximize"></i></a>
                        <a href="#"><i class="minimize fa fa-chevron-down" data-toggle="tooltip" data-placement="bottom" title="Collapse"></i></a>
                        <a href="#"><i data-target="#panel2" data-dismiss="alert" data-toggle="tooltip" data-placement="bottom" title="Close" class="fa fa-times"></i></a>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <?php if(count($errors) > 0): ?>
                    <div class="alert alert-danger">
                        Validation Error<br><br>
                        <ul>
                            <?php foreach($errors->all() as $error): ?>
                                <li><?php echo e($error); ?></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                <?php endif; ?>

                <?php if($message = Session::get('success')): ?>
                    <div class="alert alert-success alert-block">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong><?php echo e($message); ?></strong>
                    </div>
                <?php endif; ?>

                <form method="post" role="form" clASs="form-horizontal" action="<?php echo e(url("/admin/statistics/filter_events")); ?>" enctype="multipart/form-data" autocomplete="off">
                    <INput type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                    <div clASs="form-group<?php echo e($errors->hAS('old') ? ' hAS-error' : ''); ?>">

                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="form-field-1" required>
                                Event
                            </label>
                            <select name="plan_id" class="js-example-basic-single col-sm-6">

                                <?php if(isset($awards)): ?>
                                    <?php foreach($awards as $awards): ?>
                                        <option  <?php echo e(isset($editPricing)&&$editPricing->plan_id==$awards->plan_id ?  'selected' :''); ?>  value="<?php echo e($awards->plan_id); ?>"> <?php echo e($awards->plan_name); ?> - <?php echo e($awards->entity_name); ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>

                            </select>
                        </div>

                    </div>
                    <div clASs="box-footer">
                        <button type="submit" class="btn btn-success btn-md col-sm-offset-5" name="btnview_results">VIEW BALANCES</button>
                    </div>
                </form>

                <?php
                if (isset($_POST['month'])){
                    $search = filter_var($_POST['month'], FILTER_SANITIZE_STRING);
                    $_SESSION['month'] = $search;
                    //$tokengenerated= bin2hex(openssl_random_pseudo_bytes(3));
                }
                ?>
            </div>
        </div><!-- end panel -->
    </div><!-- end .col-md-12 -->


    <?php if($total_transactions != null): ?>

        <div class="col-md-12">
            <br><br>
            <div class="row">
                <?php /*         $total_transactions, 'total_passed_transactions'=> $total_passed_transactions, 'total_pending_transactions'=>$total_pending_transactions, 'total_failed_transactions'=>$total_failed_transactions, */ ?>
                <?php /*         'passed_transactions_today'=>$passed_transactions_today, 'pending_transactions_today'=>$pending_transactions_today, 'failed_transactions_today'=>$failed_transactions_today*/ ?>
                <div class="col-sm-3">
                    <div class="well well-sm text-center">
               <span class="h1 shadow-block no-margin orange">
                  GHS <?php echo e($sum_amount_today); ?>

               </span>
                        <span class="text-center">
                     <i class="fa fa-user text-muted"></i>
                 </span>
                        <span class="text-muted">Today's Amount</span>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="well well-sm text-center">
                        <span class="h1 shadow-block no-margin green"><?php echo e($passed_transactions_today); ?></span>
                        <span class="text-center">
                     <i class="fa fa-user text-muted"></i>
                 </span>
                        <span class="text-muted">Today's Successful Transactions</span>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="well well-sm text-center">
                        <span class="h1 shadow-block no-margin red"><?php echo e($failed_transactions_today); ?></span>
                        <span class="text-center">
                     <i class="fa fa-user text-muted"></i>
                 </span>
                        <span class="text-muted">Today's Failed Transactions</span>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="well well-sm text-center">
                        <span class="h1 shadow-block no-margin purple"><?php echo e($transactions_today); ?></span>
                        <span class="text-center">
                     <i class="fa fa-user text-muted"></i>
                 </span>
                        <span class="text-muted">Today's Transactions</span>
                    </div>
                </div>
            </div>
            <br><br>
            <div class="row">
                <div class="col-sm-3">
                    <div class="well well-sm text-center">
               <span class="h1 shadow-block no-margin orange">
                  GHS <?php echo e($total_sum_amount); ?>

               </span>
                        <span class="text-center">
                     <i class="fa fa-user text-muted"></i>
                 </span>
                        <span class="text-muted">Total Amount</span>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="well well-sm text-center">
                        <span class="h1 shadow-block no-margin green"><?php echo e($total_passed_transactions); ?></span>
                        <span class="text-center">
                     <i class="fa fa-user text-muted"></i>
                 </span>
                        <span class="text-muted">Total Successful Transactions</span>
                    </div>
                </div>

                <div class="col-sm-3">
                    <div class="well well-sm text-center">
                        <span class="h1 shadow-block no-margin red"><?php echo e($total_failed_transactions); ?></span>
                        <span class="text-center">
                     <i class="fa fa-user text-muted"></i>
                 </span>
                        <span class="text-muted">Total Failed Transactions</span>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="well well-sm text-center">
                        <span class="h1 shadow-block no-margin purple"><?php echo e($total_transactions); ?></span>
                        <span class="text-center">
                     <i class="fa fa-user text-muted"></i>
                 </span>
                        <span class="text-muted">Total Transactions</span>
                    </div>
                </div>
            </div>


        </div>
        <!-- /end widget progress .col-md-12 -->
        <!-- Admin over view .col-md-12 -->

        <div class="col-md-12 ">
            <div  class="panel panel-default">
                <div class="panel-body">

                    <i class="glyphicon glyphicon-stats"></i>
                    <b>Today's Transactions</b>
                    <hr>
                    <div class="row">

                        <!-- chart section -->
                        <div class="col-sm-9">
                            <div class="well well-sm well-light padding-10">
                                <h4 >
                                    <strong>Transactions by source (USSD & WEB)    </strong>
                                    <a href="javascript:void(0);" class="pull-right ">
                                        <i class="fa fa-refresh"></i>
                                    </a>
                                </h4>
                                <div class="panel panel-default">
                                    <div class="panel-body no-padding">
                                        <?php echo $statisticsChart->container(); ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- ./chart section -->

                        <!-- pie chart -->
                        <div class="col-sm-3 ">
                            <div class="col-md-6 no-padd-left-right">
                                <div class="well well-sm text-center">
                                    <span class="h1 shadow-block no-margin purple"><?php echo e($sum_amount_today); ?></span>
                                    <span class="text-center">
                                  <i class="fa fa-upload text-muted"></i>
                              </span>
                                    <span class="text-muted">Amount</span>
                                </div>
                            </div>
                            <div class="col-md-6 no-padd-left-right">
                                <div class="well well-sm text-center">
                                    <span class="h1 shadow-block no-margin green"><?php echo e($passed_transactions_today); ?></span>
                                    <span class="text-center">
                                  <i class="fa fa-file text-muted"></i>
                              </span>
                                    <span class="text-muted">Successful Transactions</span>
                                </div>
                            </div>
                            <div class="col-md-6 no-padd-left-right">
                                <div class="well well-sm text-center">
                                    <span class="h1 shadow-block no-margin orange"><?php echo e($failed_transactions_today); ?></span>
                                    <span class="text-center">
                                  <i class="fa fa-user text-muted"></i>
                              </span>
                                    <span class="text-muted">Failed Transactions</span>
                                </div>
                            </div>
                            <div class="col-sm-6 no-padd-left-right hidden-sm">
                                <div class="well well-sm text-center">
                                    <span class="h1 shadow-block no-margin darkblue"><?php echo e($transactions_today); ?></span>
                                    <span class="text-center">
                                     <i class="fa fa-rss text-muted"></i>
                                 </span>
                                    <span class="text-muted">Total Transactions</span>
                                </div>
                            </div>
                            <p class="hidden-sm limit-3-line"><?php echo e(trans('main.content2')); ?></p>
                        </div>
                        <!-- ./pie chart -->
                    </div>
                </div>

            </div><!-- end panel -->
        </div>




        <?php /*    <div class="col-md-12 ">*/ ?>
        <?php /*          <div  class="panel panel-default">*/ ?>
        <?php /*             <div class="panel-body">*/ ?>

        <?php /*                <i class="glyphicon glyphicon-stats"></i>*/ ?>
        <?php /*                <b><?php echo e(trans('main.admin_overview')); ?></b>*/ ?>
        <?php /*                <hr>*/ ?>
        <?php /*                <div class="row">*/ ?>
        <?php /*                   <!-- progress section -->*/ ?>
        <?php /*                   <div class="col-sm-3">*/ ?>
        <?php /*                      <div class="headprogress">*/ ?>
        <?php /*                         <strong><?php echo e(trans('main.tasks')); ?></strong>*/ ?>
        <?php /*                         <strong class="progress-value">60/100 <i class="fa fa-tasks"></i></strong>*/ ?>
        <?php /*                      </div>*/ ?>
        <?php /*                      <div class="progress progress-xs">*/ ?>
        <?php /*                         <div class="progress-bar progress-bar-info" role="progressbar"  style="width: 60%;">*/ ?>
        <?php /*                         </div>*/ ?>
        <?php /*                      </div>*/ ?>

        <?php /*                      <div class="headprogress">*/ ?>
        <?php /*                         <strong><?php echo e(trans('main.capacity')); ?></strong>*/ ?>
        <?php /*                         <strong class="progress-value">40/100 GB <i class="fa fa-pie-chart"></i></strong>*/ ?>
        <?php /*                      </div>*/ ?>
        <?php /*                      <div class="progress  progress-xs">*/ ?>
        <?php /*                         <div class="progress-bar progress-bar-info" role="progressbar" style="width: 40%;">*/ ?>
        <?php /*                         </div>*/ ?>
        <?php /*                      </div>*/ ?>

        <?php /*                      <div class="headprogress">*/ ?>
        <?php /*                         <strong><?php echo e(trans('main.email')); ?></strong>*/ ?>
        <?php /*                         <strong class="progress-value">70% <i class="fa fa-envelope"></i></strong>*/ ?>
        <?php /*                      </div>*/ ?>
        <?php /*                      <div class="progress progress-xs">*/ ?>
        <?php /*                         <div class="progress-bar progress-bar-info" role="progressbar" style="width: 70%;">*/ ?>
        <?php /*                         </div>*/ ?>
        <?php /*                      </div>*/ ?>

        <?php /*                      <div class="headprogress">*/ ?>
        <?php /*                         <strong><?php echo e(trans('main.close_request')); ?></strong>*/ ?>
        <?php /*                         <strong class="progress-value">90% <i class="fa fa-ticket"></i></strong>*/ ?>
        <?php /*                      </div>*/ ?>
        <?php /*                      <div class="progress progress-xs">*/ ?>
        <?php /*                         <div class="progress-bar progress-bar-info" role="progressbar"  style="width: 90%;">*/ ?>
        <?php /*                         </div>*/ ?>
        <?php /*                      </div>*/ ?>

        <?php /*                      <a class="btn btn-success btn-block"><i class="fa fa-download"></i> <?php echo e(trans('main.download')); ?> <strong><?php echo e(trans('main.report')); ?></strong></a>*/ ?>
        <?php /*                      <a class="btn btn-default btn-block"><i class="fa fa-file-o"></i> <?php echo e(trans('main.report_a')); ?> <strong><?php echo e(trans('main.bug')); ?></strong></a>*/ ?>
        <?php /*                   </div>*/ ?>

        <?php /*                   <div class="col-sm-6">*/ ?>
        <?php /*                      <div class="well well-sm well-light padding-10">*/ ?>
        <?php /*                         <h4 >*/ ?>
        <?php /*                            <?php echo e(trans('main.active')); ?> <strong><?php echo e(trans('main.visit')); ?></strong>*/ ?>
        <?php /*                            <a href="javascript:void(0);" class="pull-right ">*/ ?>
        <?php /*                               <i class="fa fa-refresh"></i>*/ ?>
        <?php /*                            </a>*/ ?>
        <?php /*                         </h4>*/ ?>
        <?php /*                         <div id="areaChart" class="chart-dashboard"></div>*/ ?>
        <?php /*                      </div>*/ ?>
        <?php /*                   </div>*/ ?>

        <?php /*                   <div class="col-sm-3 ">*/ ?>
        <?php /*                      <div class="col-md-6 no-padd-left-right">*/ ?>
        <?php /*                         <div class="well well-sm text-center">*/ ?>
        <?php /*                            <span class="h1 shadow-block no-margin purple">752</span>*/ ?>
        <?php /*                              <span class="text-center">*/ ?>
        <?php /*                                  <i class="fa fa-upload text-muted"></i>*/ ?>
        <?php /*                              </span>*/ ?>
        <?php /*                            <span class="text-muted"><?php echo e(trans('main.uploads')); ?></span>*/ ?>
        <?php /*                         </div>*/ ?>
        <?php /*                      </div>*/ ?>
        <?php /*                      <div class="col-md-6 no-padd-left-right">*/ ?>
        <?php /*                         <div class="well well-sm text-center">*/ ?>
        <?php /*                            <span class="h1 shadow-block no-margin green">563</span>*/ ?>
        <?php /*                              <span class="text-center">*/ ?>
        <?php /*                                  <i class="fa fa-file text-muted"></i>*/ ?>
        <?php /*                              </span>*/ ?>
        <?php /*                            <span class="text-muted"><?php echo e(trans('main.all_news')); ?></span>*/ ?>
        <?php /*                         </div>*/ ?>
        <?php /*                      </div>*/ ?>
        <?php /*                      <div class="col-md-6 no-padd-left-right">*/ ?>
        <?php /*                         <div class="well well-sm text-center">*/ ?>
        <?php /*                            <span class="h1 shadow-block no-margin orange">236</span>*/ ?>
        <?php /*                              <span class="text-center">*/ ?>
        <?php /*                                  <i class="fa fa-user text-muted"></i>*/ ?>
        <?php /*                              </span>*/ ?>
        <?php /*                            <span class="text-muted"><?php echo e(trans('main.users')); ?></span>*/ ?>
        <?php /*                         </div>*/ ?>
        <?php /*                      </div>*/ ?>
        <?php /*                      <div class="col-sm-6 no-padd-left-right hidden-sm">*/ ?>
        <?php /*                         <div class="well well-sm text-center">*/ ?>
        <?php /*                            <span class="h1 shadow-block no-margin darkblue">471</span>*/ ?>
        <?php /*                                 <span class="text-center">*/ ?>
        <?php /*                                     <i class="fa fa-rss text-muted"></i>*/ ?>
        <?php /*                                 </span>*/ ?>
        <?php /*                            <span class="text-muted"><?php echo e(trans('main.feeds')); ?></span>*/ ?>
        <?php /*                         </div>*/ ?>
        <?php /*                      </div>*/ ?>
        <?php /*                      <p class="hidden-sm limit-3-line"><?php echo e(trans('main.content2')); ?></p>*/ ?>
        <?php /*                   </div>*/ ?>
        <?php /*                </div>*/ ?>
        <?php /*             </div>*/ ?>

        <?php /*          </div>*/ ?>

        <?php /*       </div>*/ ?>

        <?php /*    <h1>Stats Graphs</h1>*/ ?>




        <?php /*    <?php if($statisticsChart): ?>*/ ?>
        <?php echo $statisticsChart->script(); ?>

        <?php /*    <?php endif; ?>*/ ?>

        <div style="width: 50%">

        </div>

    <?php endif; ?>


    <script type="text/javascript">
        $(document).ready(function() {
            $(".js-example-basic-single").select2();
        });
    </script>


    <!-- /end Admin over view .col-md-12 -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('reportmaster', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>