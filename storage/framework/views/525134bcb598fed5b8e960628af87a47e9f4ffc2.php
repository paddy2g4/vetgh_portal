<?php $__env->startSection('content'); ?>
    <!-- /end widget progress .col-md-12 -->
    <!-- Admin over view .col-md-12 -->

    <!-- Admin over view .col-md-12 -->
    <div class="col-md-12 ">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title">

                    <?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <i class="fa fa-table"></i>
                    <button type="button" class="btn btn-success btn-md col-sm-offset-5" name="btn_checkbalance" data-toggle="modal" data-target="#newticketClassModel">NEW RECORD</button>

                    <div class="bars pull-right">
                        <a href="#"><i class="maximum fa fa-expand" data-toggle="tooltip" data-placement="bottom" title="Maximize"></i></a>
                        <a href="#"><i class="minimize fa fa-chevron-down" data-toggle="tooltip" data-placement="bottom" title="Collapse"></i></a>
                        <a href="#"><i data-target="#panel2" data-dismiss="alert" data-toggle="tooltip" data-placement="bottom" title="Close" class="fa fa-times"></i></a>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                    <table id="example3" class="table table-striped table-bordered width-100 cellspace-0">
                        <thead>
                        <tr>
                            <th width="3%">#</th>
                            <th width="20%">Event</th>
                            <th width="10%">Date & Time</th>
                            <!-- <th width="10%">Time</th> -->
                            <th width="12%">Ticket Type</th>
                            <th width="10%">Amount (GHS)</th>
                            <th width="8%">Status</th>
                            <th width="12%">Date Created</th>
                            <th width="10%">Actions</th>
                            <th width="10%"></th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php if(isset($ticket_classifications)): ?>
                            <?php foreach($ticket_classifications as  $key=>$item): ?>
                                    <tr>
                                        <td><?php echo e($key+1); ?></td>
                                        <td><?php echo e($item->div_name); ?></td>
                                        <td><strong><?php echo e(date('d-F-Y', strtotime($item->activity_date) )); ?> <?php echo e(date('g:i A', strtotime($item->activity_time) )); ?></strong></td>
                                        <!-- <td><strong><?php echo e(date('g:i A', strtotime($item->activity_time) )); ?></strong></td> -->
                                        <td><strong><?php echo e($item->class_desc); ?></strong></td>
                                        <td><strong><?php echo e($item->amount); ?></strong></td>
                                        <td>
                                            <?php if($item->active_status == '0'): ?>
                                                In Active
                                            <?php elseif($item->active_status == '1'): ?>
                                                Active
                                            <?php endif; ?>
                                        </td>
                                        <td><?php echo e(date('d-F-Y g:i A', strtotime($item->created_at) )); ?></td>
                                        <!-- <td>
                                            <button type="button" class="btn btn-primary btn-md" name="btn_edit" data-toggle="modal" data-target="#editticketTypeModel">Edit</button>
                                        </td> -->
                                        <td>
                                            <button type="button" class="btn btn-primary" name="btn_edit" id="btn_edit" data-toggle="modal" onclick="ShowEditModal(<?php echo e(isset($item->activity_sub_div_id) ? $item->activity_sub_div_id : 0); ?>)">Edit</button>
                                        </td>
                                        <td>
                                            <form action="<?php echo e(url('admin/ticket_classifications/edit')); ?>" method="POST">
                                                <?php echo e(csrf_field()); ?>

                                                <input type="hidden" name="update_id"  value="<?php echo e(isset($item->activity_div_id) ? $item->activity_div_id : 0); ?>">
                                                <input type="hidden" name="activity_div_id" value="<?php echo e(isset($item->activity_div_id) ? $item->activity_div_id : 0); ?>">
                                                <input type="hidden" name="activity_sub_div_id" value="<?php echo e(isset($item->activity_sub_div_id) ? $item->activity_sub_div_id : 0); ?>">

                                                <?php if($item->active_status == '1'): ?>
                                                    <button type="submit" class="btn btn-danger" name="btn_status" value="deactivate">Deactivate</span> </button>
                                                <?php else: ?>
                                                    <button type="submit" class="btn btn-success" name="btn_status" value="activate">Activate</span> </button>
                                                <?php endif; ?>
                                            </form>
                                        </td>

                                    </tr>
                            <?php endforeach; ?>

                        <?php endif; ?>

                        </tbody>
                    </table>
                    <div clASs="box-footer">
                        <?php /* <button type="submit" clASs="btn btn-primary btn-lg col-sm-offset-5" name="btnconfirmresults">PRINT</button> */ ?>
                    </div>
            </div>
        </div><!-- end panel -->
    </div><!-- end .col-md-12 -->
    <!-- /end Admin over view .col-md-12 -->

    <div class="modal fade" id="newticketClassModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel3">TICKET DETAILS</h4>
                </div>
                <div class="modal-body">
                    <p class="statusMsg"></p>
                    <form method="post" role="form" action="<?php echo e(url("/admin/ticket_classifications/create")); ?>" enctype="multipart/form-data" autocomplete="off">
                        <?php echo e(csrf_field()); ?>


                        <div class="form-group">
                            <label for="inputName" class="col-sm-12">Event</label>
                            <select name="plan_id" id="plan_id" class="js-example-basic-single col-sm-12" required>
                                <option selected>Please an event</option>
                                <?php if(isset($Awards)): ?>
                                    <?php foreach($Awards as $Awards): ?>
                                        <option  <?php echo e(isset($editSMSAccounts)&&$editSMSAccounts->plan_id==$Awards->assigned_code ?  'selected' :''); ?>  value="<?php echo e($Awards->assigned_code); ?>"> <?php echo e($Awards->div_name); ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>

                            </select><br>

                        </div><br>

                        <div class="form-group">
                            <br><label for="inputName" class="col-sm-12">Ticket Type</label>
                            <select name="classification" id="classification" class="js-example-basic-single col-sm-12" required>
                                <option>Please your ticket type</option>
                            </select><br>
                        </div>

                        <br> <br>
                        <div class="form-group col-sm-12">
                            <label for="form-field-1">
                                Date & Time of event
                            </label>
                            <div class='input-group date' id='datetimepicker1'>
                                <input name="start_date" type="text" placeholder="" class="form-control" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>

                        </div>

                        <div class="form-group col-sm-12">
                            <label for="inputName">Amount (GHS)</label>
                            <input type="text" class="form-control" name="amount" id="amount" placeholder="Enter Ticket Amount for selected ticket type" required/>
                        </div>

                        <div clASs="box-footer">
                            <button type="submit" class="btn btn-success btn-md col-sm-offset-5" name="btnview_results">SAVE</button> <br><br>
                        </div>

                    </form>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="EditticketClassModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel3">TICKET DETAILS</h4>
                </div>
                <div class="modal-body">
                    <p class="statusMsg"></p>
                    <form method="post" role="form" action="<?php echo e(url("/admin/ticket_classifications/update")); ?>" enctype="multipart/form-data" autocomplete="off">
                        <?php echo e(csrf_field()); ?>



                        <input type="hidden" name="selected_activity_sub_div_id" id="selected_activity_sub_div_id" value="">
                        <input type="hidden" name="selected_activity_div_id" id="selected_activity_div_id" value="">

                        <div class="form-group">
                            <label for="inputName" class="col-sm-12">Event</label>
                            <select name="edit_plan_id" id="edit_plan_id" class="js-example-basic-single col-sm-12" required>
                            </select><br>

                        </div><br>

                        <div class="form-group">
                            <br><label for="inputName" class="col-sm-12">Ticket Type</label>
                            <select name="edit_classification" id="edit_classification" class="js-example-basic-single col-sm-12" required>
                                <option>Please your ticket type</option>

                            </select><br>
                        </div>

                        <br> <br>
                        <div class="form-group col-sm-12">
                            <label for="form-field-1">
                                Date & Time of event
                            </label>
                            <div class='input-group date' id='datetimepicker2'>
                                <input name="edit_start_date" id="edit_start_date" type="text" placeholder="" class="form-control" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>

                        </div>

                        <div class="form-group col-sm-12">
                            <label for="inputName">Amount (GHS)</label>
                            <input type="text" class="form-control" name="edit_amount" id="edit_amount" placeholder="Enter Ticket Amount for selected ticket type" required/>
                        </div>

                        <div clASs="box-footer">
                            <button type="submit" class="btn btn-success btn-md col-sm-offset-5" name="btnview_results">UPDATE</button> <br><br>
                        </div>

                    </form>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


    <!-- General JS script library-->
<?php /*     <script type="text/javascript" src="../assets/vendors/jquery/jquery.min.js"></script>*/ ?>
<?php /*     <script type="text/javascript" src="../assets/vendors/jquery-ui/js/jquery-ui.min.js"></script>*/ ?>
<?php /*    <script type="text/javascript" src="../assets/vendors/bootstrap/js/bootstrap.min.js"></script>*/ ?>
<?php /*    <script type="text/javascript" src="../assets/vendors/jquery-searchable/js/jquery.searchable.min.js"></script>*/ ?>
<?php /*    <script type="text/javascript" src="../assets/vendors/jquery-fullscreen/js/jquery.fullscreen.min.js"></script>*/ ?>

<?php /*    <!-- Related JavaScript Library to This Pagee -->*/ ?>
    <script type="text/javascript" src="../assets/vendors/jquery-datatables/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../assets/vendors/jquery-datatables/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="../assets/vendors/jquery-datatables/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="../assets/vendors/jquery-datatables/js/dataTables.tableTools.min.js"></script>
    <script type="text/javascript" src="../assets/vendors/jquery-datatables/js/dataTables.colVis.min.js"></script>

    <script src="https://cdn.jsdelivr.net/momentjs/2.14.1/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">


    <!-- Plugins Script -->
    <script type="text/javascript">


        jQuery(document).ready(function($){
            $('#plan_id').change(function(){
                // console.log($(this).val());
                var uRl = "<?php echo e(url('admin/ticket_classifications/get_ticket_types')); ?>";
                $.get(uRl,
                    { plan_id: $(this).val() },
                    function(data) {
                        // console.log(data);
                        var classification = $('#classification');
                        classification.empty();

                        $.each(data, function(index, element) {
                            classification.append("<option value='"+ element.id +"'>" + element.class_desc + "</option>");
                        });
                    });
            });


            <?php /*$('#edit_plan_id').change(function(){*/ ?>
            <?php /*    console.log($(this).val());*/ ?>
            <?php /*    var uRl = "<?php echo e(url('admin/ticket_classifications/get_ticket_types')); ?>";*/ ?>
            <?php /*    $.get(uRl,*/ ?>
            <?php /*        { plan_id: $(this).val() },*/ ?>
            <?php /*        function(data) {*/ ?>
            <?php /*            console.log(data);*/ ?>
            <?php /*            var edit_classification = $('#edit_classification');*/ ?>
            <?php /*            edit_classification.empty();*/ ?>

            <?php /*            $.each(data, function(index, element) {*/ ?>
            <?php /*                edit_classification.append("<option value='"+ element.id +"'>" + element.class_desc + "</option>");*/ ?>
            <?php /*            });*/ ?>
            <?php /*        });*/ ?>
            <?php /*});*/ ?>

        });

        $(function () {
            $('#datetimepicker1').datetimepicker();
            $('#datetimepicker2').datetimepicker();

        });


        $(document).ready(function() {
            $(".js-example-basic-single").select2();

            // $(function () {
            //     $('.datetimepicker').datetimepicker();
            // });
        });


       

        function editChanged() {
            // console.log()
        }


        function ShowEditModal(ticket_sub_div_id) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                }
            });
            // e.preventDefault();
            var formData = {
                activity_sub_div_id: ticket_sub_div_id
            };
            var type = "POST";
            var ajaxurl = 'ticket_classifications/get_details';
            $.ajax({
                type: type,
                url: ajaxurl,
                data: formData,
                dataType: 'json',
                success: function (data) {
                    var awards = data.awards;
                    var ticket_types = data.ticket_types;
                    var ticket_classifications = data.ticket_classifications[0];
                    var selected_plan_ticket_types = data.selected_plan_ticket_types;
                    var selected_plan_id = ticket_classifications['entity_div_code'];
                    var activity_div_id = ticket_classifications['activity_div_id'];
                    var selected_class_id = ticket_classifications['classification_id'];
                    // var ticket_date_time = ticket_classifications['activity_date'] + ticket_classifications['activity_time'];
                    var ticket_date_time = moment(ticket_classifications['activity_date'] + " " + ticket_classifications['activity_time']);

                    // console.log(ticket_types_details['id'], ticket_types_details['plan_name'], ticket_types_details['class_desc'], ticket_types_details['comment']);
                    $('#edit_amount').val(ticket_classifications['amount']);
                    // $('#edit_comment').val(ticket_types_details['comment']);

                    var edit_plan_id = $('#edit_plan_id');
                    edit_plan_id.empty();
                    $.each(awards, function(index, element) {
                        if(element.entity_div_code == selected_plan_id){
                            var newOption = new Option(element.event_name,element.entity_div_code, true, false);
                            edit_plan_id.append(newOption);
                        }
                        // else{
                        //     var newOption = new Option(element.plan_name,element.plan_id, false, false);
                        //     edit_plan_id.append(newOption);
                        // }
                    });

                    edit_plan_id.val(selected_plan_id); // Select the option with a value of '1'
                    edit_plan_id.trigger('change');




                    var edit_classification = $('#edit_classification');
                    edit_classification.empty();
                    $.each(selected_plan_ticket_types, function(index, element) {
                        // console.log(element);
                        console.log(element.class_desc);
                        if(element.id == selected_class_id){
                            var newOption = new Option(element.class_desc,element.id, true, false);
                            edit_classification.append(newOption);
                        }else{
                            var newOption = new Option(element.class_desc,element.id, false, false);
                            edit_classification.append(newOption);
                        }
                    });

                    edit_classification.val(selected_class_id); // Select the option with a value of '1'
                    edit_classification.trigger('change');


                    var new_ticket_date_time = ticket_date_time.format("MM/D/YYYY h:mm a");
                    console.log(new_ticket_date_time);

                    $('#edit_start_date').val(new_ticket_date_time);

                    $('#selected_activity_sub_div_id').val(ticket_sub_div_id);
                    $('#selected_activity_div_id').val(activity_div_id);

                    jQuery('#EditticketClassModel').modal('show');

                },
                error: function (data) {
                    console.log(data);
                }
            });
        }


        // function fetch_ticket_classes() {
        //     plan_id = document.getElementById("plan_id").value;
        //     alert(plan_id);
        // }



    </script>


    <!-- /end Admin over view .col-md-12 -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>