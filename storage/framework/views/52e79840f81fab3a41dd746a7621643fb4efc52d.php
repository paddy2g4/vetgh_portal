<div id="MainMenu" class="">

    <ul id="menu-list" class="nav nav-list">

        <?php if(\Session::get('role_id') == 1 || \Session::get('role_id') == 2 ): ?>
        <li class="separate-menu"><span><?php echo e(trans('main.configurations')); ?></span></li>

        <li class="<?php if(Request::segment(2)=='users' || Request::segment(2)=='permissions' || Request::segment(2)=='roles'): ?> active open <?php endif; ?>">
            <a href="<?php echo e(url('/admin/users')); ?>" class="dropdown-toggle">
                <i class="menu-icon fa fa-user"></i>
                <span class="menu-text"> <?php echo e(trans('main.users_management')); ?></span>
                <b class="arrow fa fa-angle-down"></b>
            </a>
            <b class="arrow"></b>
            <ul class="submenu">

                <?php if(\Session::get('role_id') == 1): ?>

                    <li class="<?php if(Request::segment(2)=='permissions'): ?> active <?php endif; ?>">
                        <a href="<?php echo e(url('/admin/permissions')); ?>" >
                            <i class="menu-icon fa fa-list"></i>
                            <span class="menu-text"> <?php echo e(trans('main.permissions')); ?> </span>
                        </a>
                    </li>
                <?php endif; ?>

                <?php if(\Session::get('role_id') == 1): ?>
                    <li class="<?php if(Request::segment(2)=='roles'): ?> active <?php endif; ?>">
                        <a href="<?php echo e(url('/admin/roles')); ?>">
                            <i class="menu-icon fa fa-bar-chart-o"></i>
                            <span class="menu-text"> <?php echo e(trans('main.roles')); ?> </span>
                        </a>
                    </li>
                <?php endif; ?>

                <?php if(\Session::get('role_id') == 1 || \Session::get('role_id') == 2 ): ?>
                    <li class="<?php if(Request::segment(2)=='users'): ?> active <?php endif; ?>">
                        <a href="<?php echo e(url('/admin/users')); ?>">
                            <i class="menu-icon fa fa-desktop"></i>
                            <span class="menu-text"> <?php echo e(trans('main.users')); ?></span>
                        </a>
                        <b class="arrow"></b>
                    </li>
                <?php endif; ?>

            </ul>
        </li>
        <?php endif; ?>

        <?php /* <?php if(isset($module_menus)): ?>
            <?php foreach($module_menus as $module_menu): ?>
                <li class="<?php if(($module_menu['child']>""&&in_array(Request::segment(2),$module_menu['child'])||Request::segment(2)==$module_menu['name'])): ?> active open <?php endif; ?>">
                    <a href="#" class="dropdown-toggle">
                        <i class="menu-icon fa fa-user"></i>
                        <span class="menu-text"> <?php echo e($module_menu['name'].' module'); ?></span>
                        <b class="arrow fa fa-angle-down"></b>
                    </a>
                    <b class="arrow"></b>
                    <ul class="submenu">
                        <li class="<?php if(Request::segment(2)==$module_menu['name']): ?> active <?php endif; ?>">
                            <a href="<?php echo e(url('/admin/'.$module_menu['name'])); ?>">
                                <i class="menu-icon fa fa-tachometer"></i>
                                <span class="menu-text"> <?php echo e($module_menu['name']); ?> </span>
                            </a>
                            <b class="arrow"></b>
                        </li>
                        <?php if($module_menu['child']>""): ?>
                            <?php foreach($module_menu['child'] as $val): ?>
                                <li class="<?php if(Request::segment(2)==$val): ?> active <?php endif; ?>">
                                    <a href="<?php echo e(url('/admin/'.$val)); ?>">
                                        <i class="menu-icon fa fa-tachometer"></i>
                                        <span class="menu-text"> <?php echo e($val); ?> </span>
                                    </a>
                                    <b class="arrow"></b>
                                </li>
                            <?php endforeach; ?>

                        <?php endif; ?>
                    </ul>
                </li>
            <?php endforeach; ?>
        <?php endif; ?> */ ?>



        <?php if(\Session::get('role_id') == 1 || \Session::get('role_id') == 2 ): ?>

            <li class="separate-menu"><span>Award Setups</span></li>

            <li class="<?php if(Request::segment(2)=='entitymasters' || Request::segment(2)=='awards' || Request::segment(2)=='pricings' || Request::segment(2)=='shortcodemasters'): ?> active open <?php endif; ?>">
                <a href="<?php echo e(url('/admin/entitymasters')); ?>" class="dropdown-toggle">
                    <i class="menu-icon fa fa-cogs"></i>
                    <span class="menu-text"> Award Setups</span>
                    <b class="arrow fa fa-angle-down"></b>
                </a>
                <b class="arrow"></b>
                <ul class="submenu">
                    <?php if(\Session::get('role_id') == 1 || \Session::get('role_id') == 2 ): ?>
                    <li class="<?php if(Request::segment(2)=='entitymasters'): ?> active <?php endif; ?>">
                        <a href="<?php echo e(url('/admin/entitymasters')); ?>" >
                            <i class="menu-icon fa fa-list"></i>
                            <span class="menu-text">Institutions</span>
                        </a>
                    </li>
                    <?php endif; ?>

                    <?php if(\Session::get('role_id') == 1 || \Session::get('role_id') == 2): ?>
                    <li class="<?php if(Request::segment(2)=='awards'): ?> active <?php endif; ?>">
                        <a href="<?php echo e(url('/admin/awards')); ?>" >
                            <i class="menu-icon fa fa-list"></i>
                            <span class="menu-text">Awards</span>
                        </a>
                    </li>
                    <?php endif; ?>

                    <?php if(\Session::get('role_id') == 1 || \Session::get('role_id') == 2 ): ?>
                    <li class="<?php if(Request::segment(2)=='pricings'): ?> active <?php endif; ?>">
                        <a href="<?php echo e(url('/admin/pricings')); ?>" >
                            <i class="menu-icon fa fa-list"></i>
                            <span class="menu-text">Pricings</span>
                        </a>
                    </li>
                    <?php endif; ?>
                    <?php /* <?php if (\Entrust::can('view_ShortCodeMasters')) : ?> */ ?>

                    <?php if(\Session::get('role_id') == 1 || \Session::get('role_id') == 2 ): ?>
                        <li class="<?php if(Request::segment(2)=='shortcodemasters'): ?> active <?php endif; ?>">
                            <a href="<?php echo e(url('/admin/shortcodemasters')); ?>" >
                                <i class="menu-icon fa fa-list"></i>
                                <span class="menu-text">Short Codes (USSD)</span>
                            </a>
                        </li>
                    <?php endif; ?>
                    <?php /* <?php endif; // Entrust::can ?> */ ?>
                </ul>
            </li>
        <?php endif; ?>

        <?php if(\Session::get('role_id') == 1 ): ?>
            <li class="<?php if(Request::segment(2)=='keys' || Request::segment(2)=='smsaccounts'): ?> active open <?php endif; ?>">
                <a href="<?php echo e(url('/admin/keys')); ?>" class="dropdown-toggle">
                    <i class="menu-icon fa fa-key"></i>
                    <span class="menu-text"> Keys & Accounts</span>
                    <b class="arrow fa fa-angle-down"></b>
                </a>
                <b class="arrow"></b>
                <ul class="submenu">
                    <?php if(\Session::get('role_id') == 1 || \Session::get('role_id') == 2 ): ?>
                        <li class="<?php if(Request::segment(2)=='keys'): ?> active <?php endif; ?>">
                            <a href="<?php echo e(url('/admin/keys')); ?>" >
                                <i class="menu-icon fa fa-list"></i>
                                <span class="menu-text">Keys</span>
                            </a>
                        </li>
                    <?php endif; ?>
                    <?php if(\Session::get('role_id') == 1): ?>
                        <li class="<?php if(Request::segment(2)=='permissions'): ?> active <?php endif; ?>">
                            <a href="<?php echo e(url('/admin/smsaccounts')); ?>" >
                                <i class="menu-icon fa fa-list"></i>
                                <span class="menu-text">SMS Accounts</span>
                            </a>
                        </li>
                    <?php endif; ?>
                </ul>
            </li>
        <?php endif; ?>

        <?php if(\Session::get('role_id') == 1 || \Session::get('role_id') == 2): ?>
            <li class="<?php if(Request::segment(2)=='categories' || Request::segment(2)=='nominees'): ?> active open <?php endif; ?>">
                <a href="<?php echo e(url('/admin/nominees')); ?>" class="dropdown-toggle">
                    <i class="menu-icon fa fa-users"></i>
                    <span class="menu-text"> Nominees & Categories</span>
                    <b class="arrow fa fa-angle-down"></b>
                </a>
                <b class="arrow"></b>
                <ul class="submenu">
                    <?php /* <?php if (\Entrust::can('view_Categories')) : ?> */ ?>
                    <li class="<?php if(Request::segment(2)=='categories'): ?> active <?php endif; ?>">
                        <a href="<?php echo e(url('/admin/categories')); ?>" >
                            <i class="menu-icon fa fa-list"></i>
                            <span class="menu-text">Categories</span>
                        </a>
                    </li>
                    <?php /* <?php endif; // Entrust::can ?> */ ?>
                    <?php /* <?php if (\Entrust::can('view_Nominees')) : ?> */ ?>
                    <li class="<?php if(Request::segment(2)=='nominees'): ?> active <?php endif; ?>">
                        <a href="<?php echo e(url('/admin/nominees')); ?>" >
                            <i class="menu-icon fa fa-list"></i>
                            <span class="menu-text">Nominees</span>
                        </a>
                    </li>
                    <?php /* <?php endif; // Entrust::can ?> */ ?>
                </ul>
            </li>
        <?php endif; ?>

        <?php if(\Session::get('role_id') == 1 || \Session::get('role_id') == 2 || \Session::get('role_id') == 3): ?>
            <li class="<?php if(Request::segment(2)=='reports' || Request::segment(2)=='votingresults' || Request::segment(2)=='transactions'): ?> active open <?php endif; ?>">
                <a href="<?php echo e(url('/admin/reports')); ?>" class="dropdown-toggle">
                    <i class="menu-icon fa fa-bar-chart-o"></i>
                    <span class="menu-text"> Reports & Statistics</span>
                    <b class="arrow fa fa-angle-down"></b>
                </a>
                <b class="arrow"></b>
                <ul class="submenu">
                    <?php /* <?php if (\Entrust::can('view_permission')) : ?> */ ?>
                    <li class="<?php if(Request::segment(2)=='votingresults'): ?> active <?php endif; ?>">
                        <a href="<?php echo e(url('/admin/votingresults')); ?>" >
                            <i class="menu-icon fa fa-list"></i>
                            <span class="menu-text">Results</span>
                        </a>
                    </li>
                    <?php /* <?php endif; // Entrust::can ?> */ ?>
                    <?php /* <?php if (\Entrust::can('view_permission')) : ?> */ ?>
                    <li class="<?php if(Request::segment(2)=='transactions'): ?> active <?php endif; ?>">
                        <a href="<?php echo e(url('/admin/transactions')); ?>" >
                            <i class="menu-icon fa fa-list"></i>
                            <span class="menu-text">Transactions</span>
                        </a>
                    </li>

                    <li class="<?php if(Request::segment(2)=='statistics'): ?> active <?php endif; ?>">
                        <a href="<?php echo e(url('/admin/statistics')); ?>" >
                            <i class="menu-icon fa fa-list"></i>
                            <span class="menu-text">Statistics</span>
                        </a>
                    </li>
                    <?php /* <?php endif; // Entrust::can ?> */ ?>
                </ul>
            </li>
        <?php endif; ?>


        <?php if(\Session::get('role_id') == 1 || \Session::get('role_id') == 2 || \Session::get('role_id') == 3): ?>
            <li class="separate-menu"><span>ADMINISTRATOR</span></li>
            <li class="<?php if(Request::segment(2)=='dashboard'): ?> active <?php endif; ?>">
                <a href="<?php echo e(url('/admin/dashboard')); ?>">
                    <i class="menu-icon fa fa-tachometer"></i>
                    <span class="menu-text"> <?php echo e(trans('main.dashboard')); ?> </span>
                </a>
                <b class="arrow"></b>
            </li>
        <?php endif; ?>

        <?php if(\Session::get('role_id') == 1 || \Session::get('role_id') == 2 ): ?>
            <li class="<?php if(Request::segment(2)=='blastSMS'): ?> active <?php endif; ?>">
                <a href="<?php echo e(url('/admin/blastSMS')); ?>" >
                    <i class="menu-icon fa fa-comments-o "></i>
                    <span class="menu-text">SMS Blast</span>
                </a>
            </li>
        <?php endif; ?>
        
        <?php if(\Session::get('role_id') == 1): ?>
            <li class="<?php if(Request::segment(2)=='news'): ?> active <?php endif; ?>">
                <a href="<?php echo e(url('/admin/news')); ?>">
                    <i class="menu-icon fa fa-pencil-square-o"></i>
                    <span class="menu-text"> <?php echo e(trans('main.news')); ?> </span>
                </a>
            </li>
        <?php endif; ?>
        <?php if(\Session::get('role_id') == 1): ?>
            <li class="<?php if(Request::segment(2)=='news_category'): ?> active <?php endif; ?>">
                <a href="<?php echo e(url('/admin/news_category')); ?>">
                    <i class="menu-icon fa fa-pencil-square-o"></i>
                    <span class="menu-text"> <?php echo e(trans('main.news_category')); ?> </span>
                </a>
            </li>
        <?php endif; ?>
        <?php if(\Session::get('role_id') == 1): ?>
            <li class="<?php if(Request::segment(2)=='modules'): ?> active <?php endif; ?>">
                <a href="<?php echo e(url('/admin/modulesbuilder')); ?>">
                    <i class="menu-icon fa fa-pencil-square-o"></i>
                    <span class="menu-text">  Module builder </span>
                </a>
            </li>
        <?php endif; ?>
        <?php if(\Session::get('role_id') == 1): ?>
            <li class="<?php if(Request::segment(2)=='relation'): ?> active <?php endif; ?>">
                <a href="<?php echo e(url('/admin/relation')); ?>">
                    <i class="menu-icon fa fa-exchange"></i>
                    <span class="menu-text"> Relation </span>
                </a>
            </li>
        <?php endif; ?>
        <?php if(\Session::get('role_id') == 1): ?>
            <li class="<?php if(Request::segment(2)=='reportbuilders'): ?> active <?php endif; ?>">
                <a href="<?php echo e(url('/admin/reportbuilders')); ?>">
                    <i class="menu-icon fa fa-line-chart"></i>
                    <span class="menu-text">  Report builders </span>
                </a>
            </li>
        <?php endif; ?>
        <?php if(\Session::get('role_id') == 1): ?>
            <li class="<?php if(Request::segment(2)=='activitylogs'): ?> active <?php endif; ?>">
                <a href="<?php echo e(url('/admin/activitylogs')); ?>">
                    <i class="menu-icon fa fa-history"></i>
                    <span class="menu-text"> Activity Logs </span>
                </a>
            </li>
        <?php endif; ?>

        <?php if(\Session::get('role_id') == 1): ?>
            <li class="separate-menu"><span>HTML template</span></li>
            <li class="">
                <a href="<?php echo e(url("/html_version/form-elements.html")); ?>"  >
                    <i class="menu-icon fa fa-desktop"></i>
                    <span class="menu-text">Admin template</span>


                </a>

                <b class="arrow"></b>

            </li>
        <?php endif; ?>


    </ul>


    <a class="sidebar-collapse" id="sidebar-collapse" data-toggle="collapse" data-target="#test">
        <i id="icon-sw-s-b" class="fa fa-angle-double-left"></i>
    </a>
</div>