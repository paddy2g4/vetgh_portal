<?php $__env->startSection('content'); ?>
    <header>
        <div class="header-content">
            <div class="header-content-inner">
                <h1>CASTVOTEGH</h1>
                <hr>
                <p>Your favorite Online Voting System</p>
                <a href="#about" class="btn btn-primary btn-xl page-scroll">Find Out More</a>
            </div>
        </div>
    </header>

    <section class="bg-primary" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading">We've got what you need!</h2>
                    <hr class="light">
                    <p class="text-faded">CASTVOTEGH has everything you need to get your voting events up and running in no time! </p>
<?php /*                    <p>Acknowlegment: This is based on PLMB templates and themes!</p>*/ ?>
                    <a href="#" class="btn btn-default btn-xl">Get Started!</a>
                </div>
            </div>
        </div>
    </section>

    <section id="services">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">At Your Service</h2>
                    <hr class="primary">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-diamond wow bounceIn text-primary"></i>
                        <h3>Full Analytics </h3>
                        <p class="text-muted">Our platform gives you the tools to collect and analysis all your voting data in real-time.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-paper-plane wow bounceIn text-primary" data-wow-delay=".1s"></i>
                        <h3>Diagnose quickly</h3>
                        <p class="text-muted">We have an agile response team that attend to technical issues at quick as possible!</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-newspaper-o wow bounceIn text-primary" data-wow-delay=".2s"></i>
                        <h3>Safe & Secure</h3>
                        <p class="text-muted">Your data and information is safe and secured with us.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-heart wow bounceIn text-primary" data-wow-delay=".3s"></i>
                        <h3>Best Pricing</h3>
                        <p class="text-muted">We have the most affordable market percentage ratio.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>


<?php /*    <aside>*/ ?>
<?php /*        <div class="container text-center bg-dark" style="padding: 15px;">*/ ?>

<?php /*        </div>*/ ?>
<?php /*    </aside>*/ ?>

    <!-- Team Section -->
<?php /*    <section id="team" class="bg-light-gray">*/ ?>
<?php /*        <div class="container">*/ ?>
<?php /*            <div class="row">*/ ?>
<?php /*                <div class="col-lg-12 text-center">*/ ?>
<?php /*                    <h2 class="section-heading">Our Amazing Team</h2>*/ ?>
<?php /*                    <h3 class="section-subheading text-muted">We work tirelessly to give you the best solutions.</h3>*/ ?>
<?php /*                </div>*/ ?>
<?php /*            </div>*/ ?>
<?php /*        </div>*/ ?>
<?php /*        <div class="container">*/ ?>
<?php /*            <div class="row">*/ ?>

<?php /*                <div class="col-sm-4 col-sm-offset-4" style="text-align:-webkit-center;">*/ ?>
<?php /*                    <div class="team-member">*/ ?>
<?php /*                        <img src='<?php echo e(url("front_end/img/team/padmore.jpg")); ?>' class="img-responsive img-circle" alt="">*/ ?>
<?php /*                        <h4>George Padmore Yeboah</h4>*/ ?>
<?php /*                        <p class="text-muted">Developer/CEO & Founder of Amazing IT Systems-GH. </p>*/ ?>
<?php /*                        <ul class="list-inline social-buttons">*/ ?>
<?php /*                            <a href="#" class="fa-stack">*/ ?>
<?php /*                                <i class="fa fa-circle fa-stack-2x text-primary"></i>*/ ?>
<?php /*                                <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>*/ ?>
<?php /*                            </a>*/ ?>
<?php /*                            <a href="#" class="fa-stack">*/ ?>
<?php /*                                <i class="fa fa-circle fa-stack-2x text-primary"></i>*/ ?>
<?php /*                                <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>*/ ?>
<?php /*                            </a>*/ ?>
<?php /*                            <a href="#" class="fa-stack">*/ ?>
<?php /*                                <i class="fa fa-circle fa-stack-2x text-primary"></i>*/ ?>
<?php /*                                <i class="fa fa-linkedin fa-stack-1x fa-inverse"></i>*/ ?>
<?php /*                            </a>*/ ?>
<?php /*                        </ul>*/ ?>
<?php /*                    </div>*/ ?>
<?php /*                </div>*/ ?>
<?php /*            </div>*/ ?>

<?php /*        </div>*/ ?>
<?php /*    </section>*/ ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('front_end.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>