<!-- Plan Name Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Plan Name
	</label>
	<div class="col-sm-9">
		<input value="<?php echo e(isset($editAward['plan_name']) ? $editAward['plan_name'] : ''); ?>" name="plan_name" type="text" placeholder="" class="form-control">
	</div>
</div>

<!-- Plan Desc Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Plan Desc
	</label>
	<div class="col-sm-9">
		<input value="<?php echo e(isset($editAward['plan_desc']) ? $editAward['plan_desc'] : ''); ?>" name="plan_desc" type="text" placeholder="" class="form-control">
	</div>
</div>

<!-- Start Date Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Start Date
	</label>
	<div class="col-sm-9">
		<input value="<?php echo e(isset($editAward['start_date']) ? $editAward['start_date'] : ''); ?>" name="start_date" type="text" data-date-format="yyyy-mm-dd" placeholder="" class="datepicker">
	</div>
</div>

<!-- End Date Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		End Date
	</label>
	<div class="col-sm-9">
		<input value="<?php echo e(isset($editAward['end_date']) ? $editAward['end_date'] : ''); ?>" name="end_date" type="text" data-date-format="yyyy-mm-dd" placeholder="" class="datepicker">
	</div>
</div>

<!-- End Status Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		End Status
	</label>
	<div class="col-sm-9">
		<input value="<?php echo e(isset($editAward['end_status']) ? $editAward['end_status'] : ''); ?>" name="end_status" type="text" placeholder="" class="form-control">
	</div>
</div>

<!-- Pic Path Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Pic Path
	</label>
	<div class="col-sm-9">
		<input value="<?php echo e(isset($editAward['pic_path']) ? $editAward['pic_path'] : ''); ?>" name="pic_path" type="text" placeholder="" class="form-control">
	</div>
</div>

<!-- Pic Name Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Pic Name
	</label>
	<div class="col-sm-9">
		<input value="<?php echo e(isset($editAward['pic_name']) ? $editAward['pic_name'] : ''); ?>" name="pic_name" type="text" placeholder="" class="form-control">
	</div>
</div>

<!-- User Id Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		User Id
	</label>
	<div class="col-sm-9">
		<input value="<?php echo e(isset($editAward['user_id']) ? $editAward['user_id'] : ''); ?>" name="user_id" type="text" placeholder="" class="form-control">
	</div>
</div>
