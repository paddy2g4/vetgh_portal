<?php $__env->startSection('content'); ?>
    <!-- /end widget progress .col-md-12 -->
    <!-- Admin over view .col-md-12 -->

    <!-- Admin over view .col-md-12 -->
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title">
                    <?php echo $__env->make('flash::message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php echo $__env->make('message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <i class="fa fa-table"></i>

                    <div class="col-md-12 flash-message"></div>

                    <?php if((\Session::get('role_id') == 1 || \Session::get('role_id') == 2)): ?>
                        <button type="button" class="btn btn-info btn-md col-sm-offset-5" name="btn_checkbalance" data-toggle="modal" data-target="#eventAddModal">NEW EVENT</button>
                    <?php endif; ?>

                    <div class="bars pull-right">
                        <a href="#"><i class="maximum fa fa-expand" data-toggle="tooltip" data-placement="bottom" title="Maximize"></i></a>
                        <a href="#"><i class="minimize fa fa-chevron-down" data-toggle="tooltip" data-placement="bottom" title="Collapse"></i></a>
                        <a href="#"><i data-target="#panel2" data-dismiss="alert" data-toggle="tooltip" data-placement="bottom" title="Close" class="fa fa-times"></i></a>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                
                <table id="awardsTable" class="table table-striped table-bordered width-100 cellspace-0">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Logo</th>
                        <th>Event</th>
                        <th>Type</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Date Status</th>
                        <th>Results</th>
                        <th>Status</th>
                        <th>Details</th>
                        <th>Edit</th>
                        <th>Action</th>
                    </tr>
                    </thead>

                </table>
                <div clASs="box-footer">
                    <!-- <button type="submit" clASs="btn btn-primary btn-lg col-sm-offset-5" name="btnconfirmresults">PRINT</button> -->
                </div>
            </div>


            <div class="modal fade" id="eventAddModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel3">ADD NEW EVENT</h4>
                        </div>
                        <div class="modal-body">
                            <form method="post" role="form" action="<?php echo e(url("/admin/events/create")); ?>" enctype="multipart/form-data" autocomplete="off">
                                <?php echo e(csrf_field()); ?>


                                <div class="form-group col-sm-12">
                                    <br><label for="inputName" class="col-sm-12">Event Organizer</label>
                                    
                                    <select name="entity_id" class="js-example-basic-single col-sm-12" required>
                                        <option value="" selected>Select an institution/event organizer</option> 
                                        <?php if(isset($Entities)): ?>
                                            <?php foreach($Entities as $Entities): ?>
                                                <option <?php echo e(isset($editEntities)&&$editEntities->entity_id==$Entities->entity_id ?  'selected' :''); ?>  value="<?php echo e($Entities->entity_id); ?>"> <?php echo e($Entities->entity_name); ?></option>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </select><br>

                                </div>

                                <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                    <label for="inputName">Event Name</label>
                                    <input type="text" class="form-control" name="add_event_name" id="add_event_name" maxlength="150" required/>
                                </div>

                                <div class="form-group col-sm-12">
                                    <label for="inputName">Event Display Name</label>
                                    <input type="text" class="form-control" name="add_event_alias" id="add_event_alias" maxlength="150" required/>
                                </div>

                                <div class="form-group col-sm-6 col-md-6 col-lg-6">
                                    <label for="inputName">Event Initials</label>
                                    <input type="text" minlength="2" maxlength="2" class="form-control" name="add_event_initials" id="add_event_initials" placeholder="e.g. AB" required/>
                                </div>

                                <div class="form-group col-sm-6 col-md-6 col-lg-6">
                                    <label for="inputName" class="col-sm-12 col-md-12 col-lg-12">Event Type</label>
                                    <select name="new_event_type" id="add_event_type" class="js-example-basic-single col-sm-12 col-md-12 col-lg-12" required>
                                        <option value="VOT" selected>Awards</option>
                                        <option value="SHW">Ticketing</option>
                                        <option value="VOT-SHW">Awards & Ticketing</option>
                                    </select>
                                </div>


                                <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                    <label for="inputName">Cost per vote (GHS)</label>
                                    <input type="number" min="0.1" step="0.1" class="form-control" name="add_event_price" id="add_event_price" placeholder="e.g. 1 or 0.5" required/>
                                </div>

                                <div class="form-group col-sm-6 col-md-6 col-lg-6">
                                    <label for="inputName">Event's Shortcode</label>
                                    <input type="number" class="form-control" name="add_event_service_code" id="add_event_service_code" value="661" required/>
                                </div>

                                <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                    <label for="inputName" class="col-sm-4 col-md-4 col-lg-4">Event Start Date</label>
                                    <!-- <input type="text" class="form-control" id="add_event_start_date"/> -->
                                    <div class='col-sm-8 col-md-8 col-lg-8 input-group date' id='datetimepicker1'>
                                        <input name="add_event_start_date" type="text" placeholder="" class="form-control" required/>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>

                                <div class="form-group col-sm-12 col-md-12 col-lg-12">
                                    <label for="inputName" class="col-sm-4 col-md-4 col-lg-4">Event End Date</label>
                                    <div class='col-sm-8 col-md-8 col-lg-8 input-group date' id='datetimepicker2'>
                                        <input name="add_event_end_date" type="text" placeholder="" class="form-control" required/>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>

                                <div class="form-group col-sm-6 col-md-6 col-lg-6">
                                    <label for="inputName">Client Results</label>
                                    <select name="add_event_results" id="add_event_results" class="js-example-basic-single col-sm-12" required>
                                        <option value="false" selected>Hide Result</option>
                                        <option value="true">Show Result</option>
                                    </select>
                                </div>

                                <div class="form-group col-sm-6">
                                    <label for="inputName">SMS Sender ID (9 characters long)</label>
                                    <input type="text" class="form-control" name="add_sms_sender_id" id="add_sms_sender_id" placeholder="Enter your SMS Sender ID here." maxlength="9" required/>
                                </div>

                                <br><br>
                                <!-- <p class="col-sm-10 col-md-10 col-lg-10" style="align-text: center;">
                                    NB: SMS Sender ID Must NOT include any special charcters (eg. !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~...)
                                </p> -->

                                <div clASs="box-footer">
                                    <button type="submit" class="btn btn-success btn-md col-sm-offset-5" name="btn_save_event">SAVE</button> <br><br>
                                </div>

                            </form>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>


            <div class="modal fade" id="eventDetailsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel3">EVENT DETAILS</h4>
                        </div>
                        <div class="modal-body">
                            <form method="post" role="form">
                                <?php echo e(csrf_field()); ?>


                                <div class="form-group col-sm-12">
                                    <br><label for="inputName">Event Organizer</label>
                                    <input type="text" class="form-control" id="entity_name" disabled/>
                                </div>

                                <div class="form-group col-sm-12">
                                    <label for="inputName">Event Name</label>
                                    <input type="text" class="form-control" id="event_name" disabled/>
                                </div>

                                <div class="form-group col-sm-12">
                                    <label for="inputName">Event Alias</label>
                                    <input type="text" class="form-control" id="event_alias" disabled/>
                                </div>

                                <div class="form-group col-sm-6">
                                    <label for="inputName">Event Initials</label>
                                    <input type="text" class="form-control" id="event_initials" disabled/>
                                </div>

                                <div class="form-group col-sm-6">
                                    <label for="inputName">Event Type</label>
                                    <input type="text" class="form-control" id="event_type" disabled/>
                                </div>

                                <div class="form-group col-sm-6">
                                    <label for="inputName">Event Start Date</label>
                                    <input type="text" class="form-control" id="event_start_date" disabled/>
                                </div>

                                <div class="form-group col-sm-6">
                                    <label for="inputName">Event End Date</label>
                                    <input type="text" class="form-control" id="event_end_date" disabled/>
                                </div>

                                <div class="form-group col-sm-6">
                                    <label for="inputName">Client Results</label>
                                    <input type="text" class="form-control" id="event_results" disabled/>
                                </div>

                                <div class="form-group col-sm-6">
                                    <label for="inputName">SMS Sender ID</label>
                                    <input type="text" class="form-control" id="sms_sender_id" disabled/>
                                </div>

                                <div class="form-group col-sm-6">
                                    <label for="inputName">Event's Status</label>
                                    <input type="text" class="form-control" id="event_status" disabled/>
                                </div>

                                <div class="form-group col-sm-6">
                                    <label for="inputName">Date Created</label>
                                    <input type="text" class="form-control" id="event_created_at" disabled/>
                                </div>

                            </form>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>


            <div class="modal fade" id="eventEditEndDateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel3">EVENT DETAILS</h4>
                        </div>
                        <div class="modal-body">
                            <form method="post" role="form" action="<?php echo e(url("/admin/events/enddate_edit")); ?>" enctype="multipart/form-data" autocomplete="off">
                                <?php echo e(csrf_field()); ?>

                                <input type="hidden" class="form-control" name="id" id="event_id_eventEditEndDate"/>

                                <div class="form-group col-sm-12">
                                    <br><label for="inputName">Event Organizer</label>
                                    <input type="text" class="form-control" id="entity_name_eventEditEndDate" disabled/>
                                </div>

                                <div class="form-group col-sm-12">
                                    <label for="inputName">Event Name</label>
                                    <input type="text" class="form-control" id="event_name_eventEditEndDate" disabled/>
                                </div>

                                <div class="form-group col-sm-6">
                                    <label for="inputName">Event Start Date</label>
                                    <input type="text" class="form-control" id="event_start_date_eventEditEndDate" disabled/>
                                </div>

                                <div class="form-group col-sm-6">
                                    <label for="inputName">Event End Date</label>
                                    <!-- <input type="text" class="form-control" id="event_end_date_eventEditEndDate" disabled/> -->
                                    <div class='input-group date' id='datetimepicker3'>
                                        <input name="end_event_end_date" type="text" placeholder="" class="form-control" required/>
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>

                                <br><br>
                                <div clASs="box-footer">
                                    <button type="submit" class="btn btn-success btn-md col-sm-offset-5" name="btn_update_event_enddate" >SAVE</button> <br><br>
                                </div>

                            </form>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

        </div><!-- end panel -->   
    </div><!-- end .col-md-12 -->
    <!-- /end Admin over view .col-md-12 -->

    <!-- General JS script library-->
    <!-- <script type="text/javascript" src="../assets/vendors/jquery/jquery.min.js"></script> -->
    <!-- <script type="text/javascript" src="../assets/vendors/jquery-ui/js/jquery-ui.min.js"></script> -->
    <!-- <script type="text/javascript" src="../assets/vendors/bootstrap/js/bootstrap.min.js"></script> -->
    <script type="text/javascript" src="../assets/vendors/jquery-searchable/js/jquery.searchable.min.js"></script>
    <script type="text/javascript" src="../assets/vendors/jquery-fullscreen/js/jquery.fullscreen.min.js"></script>

    <!-- Related JavaScript Library to This Pagee -->
    <script type="text/javascript" src="../assets/vendors/jquery-datatables/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../assets/vendors/jquery-datatables/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="../assets/vendors/jquery-datatables/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="../assets/vendors/jquery-datatables/js/dataTables.tableTools.min.js"></script>
    <script type="text/javascript" src="../assets/vendors/jquery-datatables/js/dataTables.colVis.min.js"></script>
    <script src="https://cdn.jsdelivr.net/momentjs/2.14.1/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">



    <!-- Plugins Script -->
    <script type="text/javascript">

        $(function () {
            $('#datetimepicker1').datetimepicker({
				// setDate : new Date(),
			});

            $('#datetimepicker2').datetimepicker({
				// setDate : new Date(),
			});   
            $('#datetimepicker3').datetimepicker({
				// setDate : new Date(),
			});    
        });

        $(document).ready(function() {
            $(".js-example-basic-single").select2();
            loadEventsData();
        });


        function loadEventsData() {
            $('#awardsTable').dataTable({
                processing: true,
                serverSide: true,
                ajax: "<?php echo e(route('events.events_getData')); ?>",
                columns: [
                    { data: 'sn' },
                    {
                        data: null,
                        bSortable: false,
                        render: function (data, type, full) {
                            if (!data["logo"]) {
                                return '<img src="<?php echo e(\Session::get('avatar_url')); ?>" alt="Avatar" style="border-radius: 50%; height: 45px;">';
                            }else{
                                return '<img src="'+data["logo"]+'" alt="Avatar" style="border-radius: 50%; height: 45px;">';
                            }      
                        }
                    },
                    // { data: 'entity_name' },
                    { data: 'div_name' },
                    { data: 'activity_type' },
                    { data: 'start_date' },
                    { data: 'end_date' },
                    {
                        data: null,
                        bSortable: false,
                        render: function (data, type, full) {
                            if (data["date_status"] == "ongoing") {
                                return '<p style="color:green;">Ongoing</p>';
                            }else{
                                return '<p style="color:red;">Ended</p>';
                            }      
                        }
                    },
                    {
                        data: null,
                        bSortable: false,
                        render: function (data, type, full) {
                            if (data["show_client_result"] == true) {
                                return 'Showing';
                            }else{
                                return 'Hidden';
                            }
                        }
                    },
                    { data: 'status' },
                    {
                        data: null,
                        bSortable: false,
                        render: function (data, type, full) {
                            // console.log(data["entity_div_code"]);
                                return '<button type="button" class="btn btn-info btn-sm" name="btn_checkbalance" onclick="openEventDetailsModal('+ data["event_id"] + ')">Details</button>';
                            }
                    },
                    {
                        data: null,
                        bSortable: false,
                        render: function (data, type, full) {
                                return '<a class="btn btn-info btn-sm" href=#/' + full[0] + '>' + 'Edit' + '</a>';
                            }
                    },
                    {
                        data: null,
                        bSortable: false,
                        render: function (data, type, full) {

                            // var button_group='<div class="btn-group"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Actions <span class="caret"></span></button><ul class="dropdown-menu" role="menu"><li><a href="#">Show Results</a></li><li><a href="#">End Event Now</a></li><li><a href="#">Deactivate</a></li></ul></div>';
                            // return button_group;
                                if (data["status"] == "Active") {

                                    if (data["date_status"] == "ongoing") {

                                        if (data["show_client_result"] == true) {
                                            var button_group='<div class="btn-group"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Actions <span class="caret"></span></button><ul class="dropdown-menu" role="menu"><li><a href="#" onclick="HideResults('+data["event_id"]+')">Hide Results</a></li><li><a href="#" onclick="EndEvent('+data["event_id"]+')">End Event Now</a></li><li><a href="#" class="btn btn-danger btn-sm" onclick="DeactivateEvent('+data["event_id"]+')">Deactivate</a></li></ul></div>';
                                        }else{
                                            var button_group='<div class="btn-group"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Actions <span class="caret"></span></button><ul class="dropdown-menu" role="menu"><li><a href="#" onclick="ShowResults('+data["event_id"]+')">Show Results</a></li><li><a href="#" onclick="EndEvent('+data["event_id"]+')">End Event Now</a></li><li><a href="#" class="btn btn-danger btn-sm" onclick="DeactivateEvent('+data["event_id"]+')">Deactivate</a></li></ul></div>';
                                        }

                                    } else {

                                        if (data["show_client_result"] == true) {
                                            var button_group='<div class="btn-group"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Actions <span class="caret"></span></button><ul class="dropdown-menu" role="menu"><li><a href="#" onclick="HideResults('+data["event_id"]+')">Hide Results</a></li><li><a href="#" onclick="ExtendEvent('+data["event_id"]+')">Extend Event Date</a></li><li><a href="#" class="btn btn-danger btn-sm" onclick="DeactivateEvent('+data["event_id"]+')">Deactivate</a></li></ul></div>'; 
                                        }else{
                                            var button_group='<div class="btn-group"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Actions <span class="caret"></span></button><ul class="dropdown-menu" role="menu"><li><a href="#" onclick="ShowResults('+data["event_id"]+')">Show Results</a></li><li><a href="#" onclick="ExtendEvent('+data["event_id"]+')">Extend Event Date</a></li><li><a href="#" class="btn btn-danger btn-sm" onclick="DeactivateEvent('+data["event_id"]+')">Deactivate</a></li></ul></div>'; 
                                        } 
                                    }
                                    
                                    return button_group;
                                    // return '<button type="button" class="btn btn-danger btn-sm" name="btn_deactivate" onclick="DeactivateEvent('+data["event_id"]+')">Deactivate</button>';
                                }else{
                                    
                                    if (data["date_status"] == "ongoing") {

                                        if (data["show_client_result"] == true) {
                                            var button_group='<div class="btn-group"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Actions <span class="caret"></span></button><ul class="dropdown-menu" role="menu"><li><a href="#" onclick="HideResults('+data["event_id"]+')">Hide Results</a></li><li><a href="#" onclick="EndEvent('+data["event_id"]+')">End Event Now</a></li><li><a href="#" class="btn btn-success btn-sm" onclick="ActivateEvent('+data["event_id"]+')">Activate</a></li></ul></div>';
                                        }else{
                                            var button_group='<div class="btn-group"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Actions <span class="caret"></span></button><ul class="dropdown-menu" role="menu"><li><a href="#" onclick="ShowResults('+data["event_id"]+')">Show Results</a></li><li><a href="#" onclick="EndEvent('+data["event_id"]+')">End Event Now</a></li><li><a href="#" class="btn btn-success btn-sm" onclick="ActivateEvent('+data["event_id"]+')">Activate</a></li></ul></div>';
                                        } 

                                    } else {

                                        if (data["show_client_result"] == true) {
                                            var button_group='<div class="btn-group"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Actions <span class="caret"></span></button><ul class="dropdown-menu" role="menu"><li><a href="#" onclick="HideResults('+data["event_id"]+')">Hide Results</a></li><li><a href="#" onclick="ExtendEvent('+data["event_id"]+')">Extend Event Date</a></li><li><a href="#" class="btn btn-success btn-sm" onclick="ActivateEvent('+data["event_id"]+')">Activate</a></li></ul></div>';
                                        }else{
                                            var button_group='<div class="btn-group"><button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">Actions <span class="caret"></span></button><ul class="dropdown-menu" role="menu"><li><a href="#" onclick="ShowResults('+data["event_id"]+')">Show Results</a></li><li><a href="#" onclick="ExtendEvent('+data["event_id"]+')">Extend Event Date</a></li><li><a href="#" class="btn btn-success btn-sm" onclick="ActivateEvent('+data["event_id"]+')">Activate</a></li></ul></div>';
                                        } 
                                    }
                                    
                                    
                                    return button_group;
                                    // return '<button type="button" class="btn btn-success btn-sm" name="btn_deactivate" onclick="ActivateEvent('+data["event_id"]+')">Activate</button>';
                                }
                                // if (data["status"] == "Active") {
                                //     return '<button type="button" class="btn btn-danger btn-sm" name="btn_deactivate" onclick="DeactivateEvent('+data["event_id"]+')">Deactivate</button>';
                                // }else{
                                //     return '<button type="button" class="btn btn-success btn-sm" name="btn_deactivate" onclick="ActivateEvent('+data["event_id"]+')">Activate</button>';
                                // }
                            }
                    }
                ]
            });
        }


        function openEventDetailsModal(event_id) {
            $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            }
            });
            // e.preventDefault();
            var formData = {
                event_id: event_id
            };
            var type = "POST";
            var ajaxurl = '/admin/events/getEventDetails';
            $.ajax({
                type: type,
                url: ajaxurl,
                data: formData,
                dataType: 'json',
                success: function (data) {
                    var activity_type = null;
                    var result_status = null;
                    var status = null;

                    var event_details = data.event_details[0];
                    var activity_type_code = event_details['activity_type_code'];
                    var start_date = event_details['start_date'];
                    var end_date = event_details['end_date'];
                    var show_client_result = event_details['show_client_result'];
                    var event_active_status = event_details['event_active_status'];
                    var event_del_status = event_details['event_del_status'];
                    var event_created_at = event_details['event_created_at'];

                    // console.log(event_details);
                    // console.log(event_details['div_name']);

                    switch (activity_type_code) {
                        case "VOT":
                            activity_type = "Voting";
                            break;

                        case "SHW":
                            activity_type = "Show";
                            break;

                        case "PAG":
                            activity_type = "Pageant";
                            break;

                        case "VOT-SHW":
                            activity_type = "Voting & Show";
                            break;

                        case "PAG-SHW":
                            activity_type = "Pageant & Show";
                            break;
                        
                        default:
                            activity_type = "Voting";
                            break;
                    }

                    switch (show_client_result) {
                        case true:
                            result_status = "Results Showing";
                            break;
                        default:
                            result_status = "Results Hidden";
                            break;
                    }

                    if(event_active_status == true && event_del_status == false ){
                        status="Active";
                    } else{
                        status="In Active";
                    }


                    $('#entity_name').val(event_details['entity_name']);
                    $('#event_name').val(event_details['div_name']);
                    $('#event_alias').val(event_details['div_alias']);
                    $('#event_initials').val(event_details['div_initials']);
                    $('#event_type').val(activity_type);
                    $('#event_start_date').val(event_details['start_date']);
                    $('#event_end_date').val(event_details['end_date']);
                    $('#event_results').val(result_status);
                    $('#sms_sender_id').val(event_details['sms_sender_id']);
                    $('#event_status').val(status);
                    $('#event_created_at').val(event_details['event_created_at']);

                    $('#eventDetailsModal').modal('show');

                },
                error: function (data) {
                    // console.log(data);
                }
            });  
        }


        function submit_status_request(event_id, status) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                }
            });
            // e.preventDefault();
            var formData = {
                id: event_id,
                btnstatus: status
            };
            var type = "POST";
            var ajaxurl = '/admin/events/req_edit_status';
            $.ajax({
                type: type,
                url: ajaxurl,
                data: formData,
                dataType: 'json',
                success: function (data) {
                    $('#awardsTable').DataTable().ajax.reload();
                    if (data.status) {
                        var msg='<br><ul style="list-style: none;" class="alert alert-success"><li>'+data.message+'</li></ul>';
                    } else {
                        var msg='<br><ul style="list-style: none;" class="alert alert-warning"><li>'+data.message+'</li></ul>';
                    }
                    
                    $('div.flash-message').html(msg);                    
                },
                error: function (data) {
                    $('#awardsTable').DataTable().ajax.reload();
                    var msg='<br><ul style="list-style: none;" class="alert alert-warning"><li>Error</li></ul>';
                    $('div.flash-message').html(msg);
                }
            });
        }


        function DeactivateEvent(event_id) {
            if (confirm("You are about to deactivate this event. Do you wish to proceed?")) {
                submit_status_request(event_id, "disable");
            }
        }


        function ActivateEvent(event_id) {
            if (confirm("You are about to activate this event. Do you wish to proceed?")) {
                submit_status_request(event_id, "enable");
            }
        }



        function submit_result_request(event_id, status) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                }
            });
            // e.preventDefault();
            var formData = {
                id: event_id,
                btnstatus: status
            };
            var type = "POST";
            var ajaxurl = '/admin/events/req_results_status';
            $.ajax({
                type: type,
                url: ajaxurl,
                data: formData,
                dataType: 'json',
                success: function (data) {
                    $('#awardsTable').DataTable().ajax.reload();
                    if (data.status) {
                        var msg='<br><ul style="list-style: none;" class="alert alert-success"><li>'+data.message+'</li></ul>';
                    } else {
                        var msg='<br><ul style="list-style: none;" class="alert alert-warning"><li>'+data.message+'</li></ul>';
                    }
                    
                    $('div.flash-message').html(msg);                    
                },
                error: function (data) {
                    $('#awardsTable').DataTable().ajax.reload();
                    var msg='<br><ul style="list-style: none;" class="alert alert-warning"><li>Error</li></ul>';
                    $('div.flash-message').html(msg);
                }
            });
        }


        function ShowResults(event_id) {
            if (confirm("You are about to show the results to the public. Do you wish to proceed?")) {
                submit_result_request(event_id, "show");
            }
        }


        function HideResults(event_id) {
            if (confirm("You are about to hide the results to the public. Do you wish to proceed?")) {
                submit_result_request(event_id, "hide");
            }
        }


        function event_date_request(event_id, status) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
                }
            });
            // e.preventDefault();
            var formData = {
                id: event_id,
                btnstatus: status
            };
            var type = "POST";
            var ajaxurl = '/admin/events/req_set_event_date';
            $.ajax({
                type: type,
                url: ajaxurl,
                data: formData,
                dataType: 'json',
                success: function (data) {
                    $('#awardsTable').DataTable().ajax.reload();
                    if (data.status) {
                        var msg='<br><ul style="list-style: none;" class="alert alert-success"><li>'+data.message+'</li></ul>';
                    } else {
                        var msg='<br><ul style="list-style: none;" class="alert alert-warning"><li>'+data.message+'</li></ul>';
                    }
                    
                    $('div.flash-message').html(msg);                    
                },
                error: function (data) {
                    $('#awardsTable').DataTable().ajax.reload();
                    var msg='<br><ul style="list-style: none;" class="alert alert-warning"><li>Error</li></ul>';
                    $('div.flash-message').html(msg);
                }
            });
        }

        function EndEvent(event_id) {
            if (confirm("You are about to end this event. Do you wish to proceed?")) {
                event_date_request(event_id, "end");
            }
        }


        function ExtendEvent(event_id) {
           console.log("extend event date");

           $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
            }
            });
            // e.preventDefault();
            var formData = {
                event_id: event_id
            };
            var type = "POST";
            var ajaxurl = '/admin/events/getEventDetails';
            $.ajax({
                type: type,
                url: ajaxurl,
                data: formData,
                dataType: 'json',
                success: function (data) {
                    var activity_type = null;
                    var result_status = null;
                    var status = null;

                    var event_details = data.event_details[0];
                    var activity_type_code = event_details['activity_type_code'];
                    var start_date = event_details['start_date'];
                    var end_date = event_details['end_date'];

                    $('#event_id_eventEditEndDate').val(event_details['event_id']);
                    $('#entity_name_eventEditEndDate').val(event_details['entity_name']);
                    $('#event_name_eventEditEndDate').val(event_details['div_name']);
                    $('#event_start_date_eventEditEndDate').val(event_details['start_date']);
                    $('#event_end_date_eventEditEndDate').val(event_details['end_date']);

                    $('#eventEditEndDateModal').modal('show');

                },
                error: function (data) {
                    // console.log(data);
                }
            });  


        }

        // Table Tools Data Tables
        // $('#example2').dataTable({
        //     "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>"+
        //     "t"+
        //     "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
        //     "oTableTools": {
        //     "aButtons": [
        //         "copy",
        //         "csv",
        //         "xls",
        //         {
        //             "sExtends": "pdf",
        //             "sTitle": "castVoteGH_PDF",
        //             "sPdfMessage": "castVoteGH PDF Export",
        //             "sPdfSize": "letter"
        //         },
        //         {
        //             "sExtends": "print",
        //             "sMessage": "Generated by CASTVOTE GH <i>(press Esc to close)</i>"
        //         }
        //     ],
        //
        //     },
        //     "autoWidth" : true,
        //     responsive: true
        //
        // });

        // Column show & hide
        // $('#example3').DataTable( {
        //     "dom": '<"pull-left"f><"pull-right"l>tip',
        //     "dom": 'C<"clear">lfrtip',
        //
        //     "bLengthChange": false,
        //     responsive: true
        // });


    </script>

    <div class="col-md-6">
        <!-- end panel -->
        <script type="text/javascript">
            $(document).ready(function() {
                $(".js-example-basic-single").select2();
            });
        </script>
    </div>

    <!-- /end Admin over view .col-md-12 -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('reportmaster', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>