<?php 
    $awards=DB::table('awards')->where('plan_id',$content->plan_id)->first();
    if($awards){
        print '<td data-title="plan_id">'.htmlentities($awards->plan_name).'</td>';
    }else{
        print '<td data-title="plan_id"></td>';
    }
?>
<td data-title="account_name"><?php echo e($content->account_name); ?></td>

<td data-title="sender_id"><?php echo e($content->sender_id); ?></td>

<td data-title="act_status">
    <?php if( $content->act_status == 1): ?>
        Enabled
    <?php else: ?>
        Disabled
    <?php endif; ?>
</td>

