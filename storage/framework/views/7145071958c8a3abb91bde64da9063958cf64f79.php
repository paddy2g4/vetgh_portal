<?php $__env->startSection('content'); ?>
    <!-- /end widget progress .col-md-12 -->
    <!-- Admin over view .col-md-12 -->

    <!-- Admin over view .col-md-12 -->
    <div class="col-md-12 ">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title">
                    <i class="fa fa-table"></i>

                    <div class="bars pull-right">
                        <a href="#"><i class="maximum fa fa-expand" data-toggle="tooltip" data-placement="bottom" title="Maximize"></i></a>
                        <a href="#"><i class="minimize fa fa-chevron-down" data-toggle="tooltip" data-placement="bottom" title="Collapse"></i></a>
                        <a href="#"><i data-target="#panel2" data-dismiss="alert" data-toggle="tooltip" data-placement="bottom" title="Close" class="fa fa-times"></i></a>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <?php /*                <form method="post" action="" role="form" clASs="form-horizontal">*/ ?>
                <?php /*                    <INput type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">*/ ?>

                <table id="example3" class="table table-striped table-bordered width-100 cellspace-0">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Trans Ref</th>
                        <th>Event</th>
                        <th>Date & Time</th>
                        <th>Qty</th>
                        <th>Customer Number</th>
                        <th>Amount</th>
                        <th>Network</th>
                        <th>Payment Method</th>
                        <th>Source</th>
                        <th>Status</th>
                        <th>Date</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php if(isset($transactions)): ?>
                        <?php foreach($transactions as  $key=>$transactions): ?>
                            <form action="<?php echo e(url('admin/transactions/resend_card_callback', [$transactions->trans_ref])); ?>" method="POST">
                                <?php echo e(csrf_field()); ?>

                                <tr>
                                    <td><?php echo e($key+1); ?></td>
                                    <td><?php echo e($transactions->trans_ref); ?></td>
                                    <td><strong><?php echo e($transactions->plan_name); ?></strong></td>
                                    <?php /*                                    <td><?php echo e($transactions->activity_date); ?> @ <?php echo e($transactions->activity_time); ?></td>*/ ?>
                                    <td><?php echo e(date('d-F-Y', strtotime($transactions->activity_date) )); ?> @ <?php echo e(date('g:i A', strtotime($transactions->activity_time) )); ?></td>

                                    <td><?php echo e($transactions->qty); ?></td>
                                    <td><?php echo e($transactions->customer_number); ?></td>
                                    <td><?php echo e($transactions->amount); ?></td>
                                    <td><?php echo e($transactions->nw); ?></td>
                                    <td><?php echo e($transactions->payment_med); ?></td>
                                    <td><?php echo e($transactions->src); ?></td>
                                    <td>
                                        <?php if($transactions->status == '0'): ?>
                                            Failed
                                        <?php elseif($transactions->status == '1'): ?>
                                            Passed
                                        <?php else: ?>
                                            Pending
                                        <?php endif; ?>
                                    </td>
                                    <td><?php echo e(date('d-F-Y g:i A', strtotime($transactions->created_at) )); ?></td>
                                </tr>


                            </form>

                        <?php endforeach; ?>

                    <?php endif; ?>

                    </tbody>
                </table>
                <div clASs="box-footer">
                    <?php /* <button type="submit" clASs="btn btn-primary btn-lg col-sm-offset-5" name="btnconfirmresults">PRINT</button> */ ?>
                </div>
                <?php /*                </form>*/ ?>
            </div>
        </div><!-- end panel -->
    </div><!-- end .col-md-12 -->
    <!-- /end Admin over view .col-md-12 -->

    <!-- General JS script library-->
    <?php /* <script type="text/javascript" src="../assets/vendors/jquery/jquery.min.js"></script> */ ?>
    <?php /* <script type="text/javascript" src="../assets/vendors/jquery-ui/js/jquery-ui.min.js"></script>	 */ ?>
    <script type="text/javascript" src="../assets/vendors/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../assets/vendors/jquery-searchable/js/jquery.searchable.min.js"></script>
    <script type="text/javascript" src="../assets/vendors/jquery-fullscreen/js/jquery.fullscreen.min.js"></script>

    <!-- Related JavaScript Library to This Pagee -->
    <script type="text/javascript" src="../assets/vendors/jquery-datatables/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../assets/vendors/jquery-datatables/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="../assets/vendors/jquery-datatables/js/dataTables.responsive.min.js"></script>
    <script type="text/javascript" src="../assets/vendors/jquery-datatables/js/dataTables.tableTools.min.js"></script>
    <script type="text/javascript" src="../assets/vendors/jquery-datatables/js/dataTables.colVis.min.js"></script>


    <!-- Plugins Script -->
    <script type="text/javascript">
        $(document).ready(function() {
            $(".js-example-basic-single").select2();
        });

        // Default Data Table Script
        $('#example1').dataTable({
            responsive: true
        });

        // Table Tools Data Tables
        $('#example2').dataTable({
            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>"+
                "t"+
                "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
            "oTableTools": {
                "aButtons": [
                    "copy",
                    "csv",
                    "xls",
                    {
                        "sExtends": "pdf",
                        "sTitle": "castVoteGH_PDF",
                        "sPdfMessage": "castVoteGH PDF Export",
                        "sPdfSize": "letter"
                    },
                    {
                        "sExtends": "print",
                        "sMessage": "Generated by VET GH <i>(press Esc to close)</i>"
                    }
                ],

            },
            "autoWidth" : true,
            responsive: true

        });

        // Column show & hide
        // $('#example3').DataTable( {
        //     "dom": '<"pull-left"f><"pull-right"l>tip',
        //     "dom": 'C<"clear">lfrtip',
        //
        //     "bLengthChange": false,
        //     responsive: true
        // });


    </script>

    <div class="col-md-6">
        <!-- end panel -->
        <script type="text/javascript">
            $(document).ready(function() {
                $(".js-example-basic-single").select2();
            });
        </script>
    </div>




    <!-- /end Admin over view .col-md-12 -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('reportmaster', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>