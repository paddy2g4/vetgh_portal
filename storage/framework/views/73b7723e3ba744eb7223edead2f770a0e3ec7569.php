<!-- Plan Id Field -->
<div class="form-group">
	<label class="col-sm-4 control-label" for="form-field-1">
		Award
	</label>
	<div class="col-sm-8">
		<select name="plan_id" class="js-example-basic-single col-sm-12">
			<?php if(isset($awards)): ?>
				<?php foreach($awards as $awards): ?>
					<option  <?php echo e(isset($editCategories["plan_id"])&&$editCategories["plan_id"]==$awards->id ? "selected" : ""); ?>   value=<?php echo e($awards->id); ?>><?php echo e($awards->plan_name); ?></option>
				<?php endforeach; ?>
			<?php endif; ?>

		</select>
	</div>
</div>

<!-- Cat Name Field -->
<div class="form-group">
	<label class="col-sm-4 control-label" for="form-field-1">
		Category
	</label>
	<div class="col-sm-8">
		<input value="<?php echo e(isset($editCategories['cat_name']) ? $editCategories['cat_name'] : ''); ?>" name="cat_name" type="text" placeholder="" class="form-control">
	</div>
</div>

<!-- Cat Desc Field -->
<div class="form-group">
	<label class="col-sm-4 control-label" for="form-field-1">
		Category Desc
	</label>
	<div class="col-sm-8">
		<input value="<?php echo e(isset($editCategories['cat_desc']) ? $editCategories['cat_desc'] : ''); ?>" name="cat_desc" type="text" placeholder="" class="form-control">
	</div>
</div>


<!-- Avatar Field -->
<div class="form-group">
	<label class="col-sm-4 control-label" for="form-field-1">
		Category Picture/Logo
	</label>
	<div class="col-sm-8">
		<div class="dropzone dz-clickable dz-single dz-uploadimage1" data-name="avatar" ></div>
		<input name="logo" type="hidden" value="<?php echo e(isset($editCategories['avatar']) ? $editCategories['avatar'] : ''); ?>">
	</div>
</div>


<script type="text/javascript">
	$(document).ready(function() {
		$(".js-example-basic-single").select2();
	});
</script>