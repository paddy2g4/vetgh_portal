<!-- Candidatename Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Candidatename
	</label>
	<div class="col-sm-9">
		<input value="<?php echo e(isset($editCandidates['candidatename']) ? $editCandidates['candidatename'] : ''); ?>" name="candidatename" type="text" placeholder="" class="form-control">
	</div>
</div>

<!-- Studentnumber Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Studentnumber
	</label>
	<div class="col-sm-9">
		<input value="<?php echo e(isset($editCandidates['studentnumber']) ? $editCandidates['studentnumber'] : ''); ?>" name="studentnumber" type="text" placeholder="" class="form-control">
	</div>
</div>

<?php /* <div class="form-group">
    <label class="col-sm-3 control-label" for="form-field-1">
       Studentclass
    </label>
    <div class="col-sm-9">
        <select name="studentclass" class="form-control">
            <option  <?php echo e(isset($editCandidates["studentclass"])&&$editCandidates["studentclass"]=="1 Agric" ? "selected" : ""); ?>   value="1 Agric"">1 Agric</option>
        </select>
    </div>
</div> */ ?>




<div class="form-group">
	<label class="col-sm-2 control-label" for="form-field-1">
		Avatar
	</label>
	<div class="col-sm-10">
		<div class="dropzone dz-clickable dz-single dz-uploadimage1" data-name="avatar" ></div>
		<input name="avatar" type="hidden" value="<?php echo e(isset($editCandidates['avatar']) ? $editCandidates['avatar'] : ''); ?>">
	</div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label" for="form-field-1">
       Candidatestatus
    </label>
    <div class="col-sm-9">
        <select name="candidatestatus" class="form-control">
            <option  <?php echo e(isset($editCandidates["candidatestatus"])&&$editCandidates["candidatestatus"]=="YES" ? "selected" : ""); ?>   value="YES"">YES</option><option  <?php echo e(isset($editCandidates["candidatestatus"])&&$editCandidates["candidatestatus"]=="NO" ? "selected" : ""); ?>   value="NO"">NO</option>
        </select>
    </div>
</div>



<?php /* 
<div class="form-group">
    <label class="col-sm-3 control-label" for="form-field-1">
       Candidateposition
    </label>
    <div class="col-sm-9">
        <select name="candidateposition" class="form-control">
            <option  <?php echo e(isset($editCandidates["candidateposition"])&&$editCandidates["candidateposition"]=="yes" ? "selected" : ""); ?>   value="yes"">yes</option><option  <?php echo e(isset($editCandidates["candidateposition"])&&$editCandidates["candidateposition"]=="no" ? "selected" : ""); ?>   value="no"">no</option>
        </select>
    </div>
</div> */ ?>




<div class="form-group">
    <label class="col-sm-3 control-label" for="form-field-1">
       Voteend
    </label>
    <div class="col-sm-9">
        <select name="voteend" class="form-control">
            <option  <?php echo e(isset($editCandidates["voteend"])&&$editCandidates["voteend"]=="YES" ? "selected" : ""); ?>   value="YES"">YES</option><option  <?php echo e(isset($editCandidates["voteend"])&&$editCandidates["voteend"]=="NO" ? "selected" : ""); ?>   value="NO"">NO</option>
        </select>
    </div>
</div>



