<?php 
    $entity_masters=DB::table('entity_masters')
    ->leftJoin('awards', 'entity_masters.id', '=', 'awards.entity_id')
    ->select('entity_masters.entity_name', 'entity_masters.id')
    ->where([['awards.plan_id',$content->plan_id],])->first();
    if($entity_masters){
        print '<td data-title="entity_id">'.htmlentities($entity_masters->entity_name).'</td>';  
    }else{
        print '<td data-title="entity_id"></td>';  
    }
    
?>

<?php 
    $awards=DB::table('awards')->where('plan_id',$content->plan_id)->first();
    print '<td data-title="plan_id">'.htmlentities($awards->plan_name).'</td>';
?>

<td data-title="unit_cost"><?php echo e($content->unit_cost); ?></td>

<td data-title="act_status">
    <?php if( $content->act_status == 1): ?>
        Enabled
    <?php else: ?>
        Disabled
    <?php endif; ?>
</td>