<?php 
    $entity_masters=DB::table('entity_masters')
    ->leftJoin('awards', 'entity_masters.id', '=', 'awards.entity_id')
    ->select('entity_masters.entity_name', 'entity_masters.id')
    ->where([['awards.plan_id',$content->plan_id],])->first();
    if($entity_masters){
        print '<td data-title="entity_id">'.htmlentities($entity_masters->entity_name).'</td>';
    }else{
        print '<td data-title="entity_id"></td>';
    }
?>
<?php 
    $awards=DB::table('awards')->where('plan_id',$content->plan_id)->first();
    if($awards){
        print '<td data-title="plan_id">'.htmlentities($awards->plan_name).'</td>';
    }else{
        print '<td data-title="plan_id"></td>';
    }
    
?>

<td data-title="short_code_ext"><?php echo e($content->short_code_ext); ?></td>

<td data-title="act_status">
    <?php if( $content->act_status == 1): ?>
        Enabled
    <?php else: ?>
        Disabled
    <?php endif; ?>
</td>
