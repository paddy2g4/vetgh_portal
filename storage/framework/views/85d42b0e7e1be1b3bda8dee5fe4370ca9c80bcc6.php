<!-- Name Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Name
	</label>
	<div class="col-sm-9">
		<input value="<?php echo e(isset($editStudentClasses['name']) ? $editStudentClasses['name'] : ''); ?>" name="name" type="text" placeholder="" class="form-control">
	</div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label" for="form-field-1">
       Status
    </label>
    <div class="col-sm-9">
        <select name="status" class="form-control">
            <option  <?php echo e(isset($editStudentClasses["status"])&&$editStudentClasses["status"]=="YES" ? "selected" : ""); ?>   value="YES"">YES</option>
        </select>
    </div>
</div>




<div class="form-group">
    <label class="col-sm-3 control-label" for="form-field-1">
       Mountable
    </label>
    <div class="col-sm-9">
        <select name="mountable" class="form-control">
            <option  <?php echo e(isset($editStudentClasses["mountable"])&&$editStudentClasses["mountable"]=="YES" ? "selected" : ""); ?>   value="YES"">YES</option>
        </select>
    </div>
</div>



