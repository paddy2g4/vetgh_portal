<div class="col-md-12 " id="ajax_div">
    <!--panel details_company -->
    <div class="panel with-nav-tabs panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">
                <div class="bars pull-right">
                    <a href="#"><i class="maximum fa fa-expand" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Maximize"></i></a>
                    <a href="#"><i class="minimize fa fa-chevron-down" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Collapse"></i></a>
                </div>
            </h3>
            <ul class="nav nav-tabs">
                <li class="<?php echo e(isset($tab)&&$tab==1?'active':''); ?>">
                    <a href="#tab1user" data-toggle="tab">
                        <i class="fa fa-user fa-lg danger"></i>
                        <?php echo e(trans('main.users')); ?>

                    </a>
                </li>
                <li class="<?php echo e(isset($tab)&&$tab==2?'active':''); ?>" <?php if($tab!=2): ?>style="display:none;"<?php endif; ?>>
                    <a href="#newuser" id="newuser-1" data-toggle="tab">
                        <i class="fa fa-user-plus success"></i>
                        <?php echo e(trans('main.new_user')); ?>

                    </a>
                </li>
            </ul>
        </div>
        <div class="panel-body no-padding">
            <?php echo $__env->make('message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <div class="tab-content">
                <div class="tab-pane fade  <?php echo e(isset($tab)&&$tab==1?'active in':''); ?> tab1" id="tab1user">
                    <div class="col-md-12"></br>
                        <div class="form-inline">

                            <div class="form-group">
                                <select class="form-control paging" id="paging" name="paging">
                                        <option  <?php echo e(isset($request['paging'])&&$request['paging']==10 ?  'selected' :''); ?>  value="10">10</option>
                                        <option  <?php echo e(isset($request['paging'])&&$request['paging']==25 ?  'selected' :''); ?>  value="25">25</option>
                                        <option  <?php echo e(isset($request['paging'])&&$request['paging']==50?  'selected' :''); ?>  value="50">50</option>
                                        <option  <?php echo e(isset($request['paging'])&&$request['paging']==100?  'selected' :''); ?>  value="100">100</option>
                                </select>
                            </div>
                            <div class="btn-group">
                                <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" >
                                    <?php echo e(trans('main.actions')); ?> <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <?php if(\Session::get('role_id') == 1 || \Session::get('role_id') == 2): ?>
                                        <!-- <?php if (\Entrust::can('delete_user')) : ?> -->
                                        <a href="javascript:void(0);" data-toggle="modal" data-target="#mydelete"><i class="fa fa-trash danger"></i> <?php echo e(trans('main.delete')); ?></a>
                                        <!-- <?php endif; // Entrust::can ?> -->
                                        <!-- <?php if (\Entrust::can('export_csv')) : ?> -->
                                        <a href="#" id='csv' class="export" ><i class="fa fa-download blue"></i> <?php echo e(trans('main.export_to_csv')); ?></a>
                                        <!-- <?php endif; // Entrust::can ?> -->
                                        <!-- <?php if (\Entrust::can('export_xls')) : ?> -->
                                        <a href="#"  id='xls' class="export" ><i class="fa fa-download green"></i> <?php echo e(trans('main.export_to_excel')); ?></a>
                                        <!-- <?php endif; // Entrust::can ?> -->
                                        <?php endif; ?>
                                    </li>
                                </ul>
                            </div>
                            <?php if(\Session::get('role_id') == 1 || \Session::get('role_id') == 2): ?>
                            <div class="pull-right">
                                <a href="#" class="btn btn-success pull-right" data-tab-destination="newuser-1">
                                    <i class="fa fa-plus"></i> <?php echo e(trans('main.add')); ?>

                                </a>
                            </div>
                            <?php endif; ?>
                        </div>

                        <hr>
                        <div class="res-table">
                            <table class="table table-striped table-hover table-fixed ellipsis-table" >
                                <thead>
                                <tr>
                                    <th width="5%" class="center">
                                        <div class="checkbox checkbox-primary">
                                            <input id="checkbox2" type="checkbox" class="conchkSelectAll">
                                            <label for="checkbox2">
                                            </label>
                                        </div>
                                    </th>
                                    <th width="15%">Avatar</th>
                                    <th width="45%"><?php echo e(trans('main.name')); ?></th>
                                    <th width="15%">Username</th>
                                    <th width="45%"><?php echo e(trans('main.email')); ?></th>
                                    <th width="30%">Created On</th>

                                    <th width="5%">
                                        <i class="fa fa-bolt"></i>
                                        <a href="#"><i class="fa fa-search search-toggle-btn"></i></a>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td colspan="3">
                                        <input  value="<?php echo e(isset($request['serach_txt'])?  $request['serach_txt']:''); ?>" id='search_text' name="search_text" type="text" class="form-control" placeholder="<?php echo e(trans('main.search')); ?>">
                                    </td>
                                    <td>
                                        <a class="btn btn-default search-btn"><i class="fa fa-search"></i></a>
                                    </td>
                                </tr>
                                <?php if(isset($users)): ?>
                                <?php foreach($users as $user): ?>
                                <tr>
                                    <td class="center">
                                        <div class="checkbox checkbox-primary">
                                            <input id="<?php echo e($user->id); ?>" type="checkbox" class="conchkNumber">
                                            <label for="<?php echo e($user->id); ?>">
                                            </label>
                                        </div>
                                    </td>
                                    <?php /* <td data-title="avatar_url"><?php echo e($user->avatar_url); ?></td> */ ?>
                                    <td><img class="img-circle" src="<?php echo e(asset('uploads/'.$user->avatar_url)); ?>" style="width:50px; height:50px"</td>
                                    <td data-title="name"><?php echo e($user->name); ?></td>
                                    <td data-title="username"><?php echo e($user->username); ?></td>
                                    <td data-title="email"><?php echo e($user->email); ?></td>
                                    <td data-title="created_at"><?php echo e($user->created_at); ?></td>


                                    <td>
                                        <?php if(\Session::get('role_id') == 1 || \Session::get('role_id') == 2): ?>
                                            <?php if($user->id == 1 && \Session::get('role_id') != 1): ?>
                                            <?php else: ?>
                                                <a id="<?php echo e($user->id); ?>" href="#" class="fa fa-pencil fa-lg edit-href" ></a>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                                <?php endif; ?>

                                </tbody>
                            </table>
                            <?php if(isset($users)): ?>
                            <?php echo $users->render(); ?>

                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade  <?php echo e(isset($tab)&&$tab==2?'active in':''); ?> tab2" id="newuser">
                    <?php echo $__env->make('errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <form id="form"   class="form-horizontal form" method="post" action="<?php echo e(url("/admin/users")); ?>" autocomplete="off">
                    <input type="hidden" name="_token" id="_token" value="<?php echo e(csrf_token()); ?>">
                    <input type="hidden" name="url" id="url" value="<?php echo e(url("/admin/users")); ?>">
                    <input type="hidden" name="id" id="id" value="<?php echo e(isset($edituser['id']) ? $edituser['id'] : 0); ?>">
                    <input type="hidden" name="page" id="page" value="<?php echo e(isset($request['page']) ?$request['page'] : 1); ?>">
                    <div class="col-md-6"><br>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-field-1">
                                <?php echo e(trans('main.name')); ?>

                            </label>
                            <div class="col-sm-9">
                                <input value="<?php echo e(isset($edituser['name']) ? $edituser['name'] : ''); ?>" name="name" type="text" placeholder="" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-field-1">
                                Username
                            </label>
                            <div class="col-sm-9">
                                <input value="<?php echo e(isset($edituser['username']) ? $edituser['username'] : ''); ?>" name="username" type="text" placeholder="" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-field-1">
                                <?php echo e(trans('main.email')); ?>

                            </label>
                            <div class="col-sm-9">
                                <input value="<?php echo e(isset($edituser['email']) ? $edituser['email'] : ''); ?>" name="email" type="email" placeholder="" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-field-1">
                                <?php echo e(trans('main.password')); ?>

                            </label>
                            <div class="col-sm-9">
                                <input value="<?php echo e(isset($edituser['password']) ? $edituser['password'] : ''); ?>" name="password" type="password" placeholder="" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-field-1">
                                <?php echo e(trans('main.role')); ?>

                            </label>
                            <div class="col-sm-9">
                                <select name="role_id" class="form-control" id="role_input">
                                    <?php foreach($roles as $group): ?>
                                    <option  <?php if(isset($role_user)&&$role_user==$group->id) echo 'selected'?> value="<?php echo e($group->id); ?>"><?php echo e($group->name); ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group" id="entity_field_Div">
                            <label class="col-sm-3 control-label" for="form-field-1">
                               Institution
                            </label>
                            <div class="col-sm-9">
                                <select name="entity_id" class="form-control" id="entity_field">
                                    <?php if(isset($entity_master)): ?>
                                        <?php foreach($entity_master as $entity_master): ?>
                                            <option  <?php echo e(isset($edituser)&&$edituser->entity_id==$entity_master->id ?  'selected' :''); ?>  value="<?php echo e($entity_master->id); ?>"> <?php echo e($entity_master->entity_name); ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="form-field-1">
                                <?php echo e(trans('main.avatar')); ?>

                            </label>
                            <div class="col-sm-9">
                                <div class="dropzone dz-clickable dz-single dz-uploadimage1" data-name="avatar_url" ></div>
                                <input name="avatar_url" type="hidden" value="<?php echo e(isset($edituser['avatar_url']) ? $edituser['avatar_url'] : ''); ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 layout no-padding">
                        <div class="panel-footer">
                            <div class="progress-btn">
                                <?php if(\Session::get('role_id') == 1 || \Session::get('role_id') == 2): ?>
                                <?php if(!isset($edituser['id'])||$edituser['id']==0): ?>
                                <a   class=" submit-btn btn btn-success ladda-button" data-style="expand-right"><i class="fa fa-save"></i><span class="ladda-label"> <?php echo e(trans('main.save')); ?></span></a>
                                <?php endif; ?>
                                <?php endif; ?>
                                <?php if(\Session::get('role_id') == 1 || \Session::get('role_id') == 2): ?>
                                <?php if(isset($edituser['id'])&&$edituser['id']>0): ?>
                                 <a    class="btn btn-info edit-btn"><i class="fa fa-save"></i><?php echo e(trans('main.update')); ?></a>
                                <?php endif; ?>
                                <?php endif; ?>
                                <button class="btn btn-link cancel-btn"> <?php echo e(trans('main.cancel')); ?></button>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- end panel -->

    <script type="text/javascript">

        $(document).ready(function() {
            console.log($("#role_input").val());
            $('#entity_field_Div').hide();

            $("#role_input").change(function() {
                if ($(this).val() == 3 || $(this).val() == "3") {
                    $('#entity_field_Div').show();
                    $('#entity_field').attr('required', '');
                    $('#entity_field').attr('data-error', 'This field is required.');
                } else {
                    $('#entity_field_Div').hide();
                    $('#entity_field').removeAttr('required');
                    $('#entity_field').removeAttr('data-error');
                }
            });

            if ($("#role_input").val() == 3 || $("#role_input").val() == "3"){
                $('#entity_field_Div').show();
                $('#entity_field').attr('required', '');
                $('#entity_field').attr('data-error', 'This field is required.');
            }
        });

    </script>

</div>