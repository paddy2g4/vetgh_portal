<!-- Name Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Name
	</label>
	<div class="col-sm-9">
		<input value="<?php echo e(isset($editElectorates['name']) ? $editElectorates['name'] : ''); ?>" name="name" type="text" placeholder="" class="form-control">
	</div>
</div>

<!-- Studentnumber Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Studentnumber
	</label>
	<div class="col-sm-9">
		<input value="<?php echo e(isset($editElectorates['studentnumber']) ? $editElectorates['studentnumber'] : ''); ?>" name="studentnumber" type="text" placeholder="" class="form-control">
	</div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label" for="form-field-1">
       Accesslevel
    </label>
    <div class="col-sm-9">
        <select name="accesslevel" class="form-control">
            <option  <?php echo e(isset($editElectorates["accesslevel"])&&$editElectorates["accesslevel"]=="electorate" ? "selected" : ""); ?>   value="electorate"">electorate</option>
        </select>
    </div>
</div>




<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Avatar
	</label>
	<div class="col-sm-9">
		<div class="dropzone dz-clickable dz-single dz-uploadimage1" data-name="avatar" ></div>
		<input name="avatar" type="hidden" value="<?php echo e(isset($editElectorates['avatar']) ? $editElectorates['avatar'] : ''); ?>">
	</div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label" for="form-field-1">
       Userstatus
    </label>
    <div class="col-sm-9">
        <select name="userstatus" class="form-control">
            <option  <?php echo e(isset($editElectorates["userstatus"])&&$editElectorates["userstatus"]=="ACTIVE" ? "selected" : ""); ?>   value="ACTIVE"">ACTIVE</option><option  <?php echo e(isset($editElectorates["userstatus"])&&$editElectorates["userstatus"]=="INACTIVE" ? "selected" : ""); ?>   value="INACTIVE"">INACTIVE</option>
        </select>
    </div>
</div>



