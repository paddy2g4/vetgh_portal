<?php $__env->startSection('content'); ?>
    <!-- widget progress .col-md-12 -->
   <div class="col-md-12">
      <br><br>

      <div  class="panel panel-default">
         <div class="panel-body">
            <i class="glyphicon glyphicon-stats"></i>
            <b><?php echo e(trans('main.admin_overview')); ?></b>
            <hr>

            <div class="row">
               <h2 style="text-align: center;">Welcome to VETGH Admin Portal</h2>
            </div>
            <br><br>

         </div>
      </div>

   </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>