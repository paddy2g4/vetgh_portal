<!-- Plan Id Field -->
<div class="form-group">
	<label class="col-sm-4 control-label" for="form-field-1">
		Award
	</label>
	<div class="col-sm-8">
		<select name="plan_id" class="js-example-basic-single col-sm-12">
			<?php
				$awards = DB::table('awards')->select('id','plan_name')->where([['act_status',true],['del_status',false]])->orderby('id', 'asc')->get();
				foreach ($awards as $awards){
					$award_name = htmlspecialchars($awards->plan_name);
					$award_id = htmlspecialchars($awards->id);
					?>
					<option <?php if(isset($editShortCodeMasters)&&($editShortCodeMasters['plan_id']==$award_id)) echo "selected"?> value="<?php echo e($award_id); ?>"><?php echo e($award_name); ?></option>
				<?php }
			?>
		</select>
	</div>
</div>

<!-- Short Code Ext Field -->
<div class="form-group">
	<label class="col-sm-4 control-label" for="form-field-1">
		Short Code
	</label>
	<div class="col-sm-8">
		<input value="<?php echo e(isset($editShortCodeMasters['short_code_ext']) ? $editShortCodeMasters['short_code_ext'] : ''); ?>" name="short_code_ext" type="text" placeholder="" class="form-control">
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
  		$(".js-example-basic-single").select2();
	});
</script>