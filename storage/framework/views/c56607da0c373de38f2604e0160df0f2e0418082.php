
<?php if(empty($content->avatar)): ?>
    <td data-title="logo"><img class="img-circle" src="<?php echo e(asset('/uploads/no_avatar.jpg')); ?>" style="width:50px; height:50px"></td>
<?php else: ?>
    <td data-title="logo"><img class="img-circle" src="<?php echo e(asset($content->avatar)); ?>" style="width:60px; height:60px"</td>
<?php endif; ?>

<td data-title="nom_name"><?php echo e($content->nom_name); ?></td>

<td data-title="nom_name"><?php echo e($content->nom_code); ?></td>

<td data-title="nom_name"><?php echo e($content->cat_name); ?></td>

<td data-title="nom_name"><?php echo e($content->plan_name); ?></td>

<td data-title="act_status">
    <?php if($content->active_status == 1): ?>
        Active
    <?php else: ?>
        In Active
    <?php endif; ?>
</td>
