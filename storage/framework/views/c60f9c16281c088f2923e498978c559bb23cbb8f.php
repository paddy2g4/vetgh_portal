<!DOCTYPE html>
<html>
<head>
    <title>VetGH - Admin Portal</title>
    <meta charset="utf-8">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>" />
    <meta name="base_url" content="<?php echo e(url('')); ?>" id="base_url"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?php echo e(url('assets/vendors/bootstrap/css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(url('assets/vendors/font-awesome/css/font-awesome.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(url('assets/vendors/bootstrap-tagsinput/css/bootstrap-tagsinput.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(url('assets/vendors/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(url('assets/vendors/select2/css/select2.min.css')); ?>">
    <?php if(Session::get('lang')==='ar'): ?>
        <link rel="stylesheet" href="<?php echo e(url('assets/css/yep-rtl.min.css')); ?>">
    <?php endif; ?>
    <?php /*multi ajax uploader*/ ?>
    <link rel="stylesheet" href="<?php echo e(url('assets/vendors/dropzone/css/basic.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(url('assets/vendors/dropzone/css/dropzone.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(url('assets/vendors/ui-select/css/select.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(url('assets/vendors/angular-wizard/css/angular-wizard.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(url('assets/vendors/angular-ui-notification/css/angular-ui-notification.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(url('assets/vendors/sweetalert/css/sweetalert.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(url('assets/vendors/bootstrap-duallistbox/css/bootstrap-duallistbox.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(url('assets/vendors/bootstrap-daterangepicker/css/daterangepicker.min.css')); ?>">
    <?php /* Related css to this page */ ?><?php /* don't remove this comment */ ?>
	<link rel="stylesheet" href="<?php echo e(url('assets/vendors/jquery-datatables/css/dataTables.bootstrap.min.css')); ?>">
	<link rel="stylesheet" href="<?php echo e(url('assets/vendors/jquery-datatables/css/dataTables.responsive.min.css')); ?>">
	<link rel="stylesheet" href="<?php echo e(url('assets/vendors/jquery-datatables/css/dataTables.tableTools.min.css')); ?>">
	<link rel="stylesheet" href="<?php echo e(url('assets/vendors/jquery-datatables/css/dataTables.colVis.min.css')); ?>">
    <?php /*<link rel="stylesheet" href="<?php echo e(url('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.min.css')); ?>">*/ ?>

    <?php /* Yeptemplate css */ ?><?php /* Please use *.min.css in production */ ?>
    <link rel="stylesheet" href="<?php echo e(url('assets/css/yep-style.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(url('assets/css/yep-custom.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(url('assets/css/yep-vendors.css')); ?>">

    <?php /* favicon */ ?>
    <link rel="shortcut icon" href="<?php echo e(url('assets/img/favicon/favicon.ico')); ?>" type="image/x-icon">
    <link rel="icon" href="<?php echo e(url('assets/img/favicon/favicon.ico')); ?>" type="image/x-icon">
    <script type="text/javascript" src="<?php echo e(url('assets/vendors/jquery/jquery.min.js')); ?>"></script>
    <style>
        .main {
            min-height: 725px;
        }
    </style>
</head>

<body id="mainbody"  <?php if(Session::get('lang')==='ar'): ?>class="rtl" <?php endif; ?> lang="<?php echo e(Session::get('lang')); ?>">
<div id="container" class="container-fluid skin-1">

    <header id="header">
        <nav class="navbar navbar-default nopadding" >
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                <button type="button" id="menu-open" class="navbar-toggle menu-toggler pull-left">
                    <span class="sr-only">Toggle sidebar</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <?php /* <a class="navbar-brand" href="#" id="logo-panel">
                    <img src="<?php echo e(url('assets/img/logo.png')); ?>" alt="PLMB">
                </a> */ ?>
<?php /*                <a class="navbar-brand" href="<?php echo url('/'); ?>" id="logo-panel">*/ ?>
<?php /*                    <img src="<?php echo e(url('assets/img/castvotegh.jpg')); ?>" alt="VetGH" width="150px" height="40px">*/ ?>
<?php /*                </a><br><br>*/ ?>
            </div>
            <form action="#" class="form-search-mobile pull-right">
                <input id="search-fld" class="search-mobile" type="text" name="param" placeholder="Search ...">
                <button id="submit-search-mobile" type="submit">
                    <i class="fa fa-search"></i>
                </button>
                <a href="#" id="cancel-search-mobile" title="Cancel Search"><i class="fa fa-times"></i></a>
            </form>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li id="search-show-li" class="dropdown">
                        <a href="#" id="search-mobile-show" class="dropdown-toggle" >
                            <i class="fa fa-search"></i>
                        </a>
                    </li>
<?php /*                    <li class="dropdown">*/ ?>
<?php /*                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-language fa-lg primary"></i></span></a>*/ ?>
<?php /*                        <ul class="dropdown-menu">*/ ?>
<?php /*                            <li class="<?php if(Session::get('lang')=='en'): ?> active <?php endif; ?>"><a href="<?php echo e(url("/admin/language?lang=en")); ?>">English </a></li>*/ ?>
<?php /*                            <li class="<?php if(Session::get('lang')=='ar'): ?> active <?php endif; ?>"><a href="<?php echo e(url("/admin/language?lang=ar")); ?>">Arabic </a></li>*/ ?>
<?php /*                            <li class="<?php if(Session::get('lang')=='pt'): ?> active <?php endif; ?>"><a href="<?php echo e(url("/admin/language?lang=pt")); ?>">Brazil </a></li>*/ ?>
<?php /*                        </ul>*/ ?>
<?php /*                    </li>*/ ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img alt="<?php echo e(isset(Auth::user()->name)?Auth::user()->name:''); ?>" src="<?php echo e(\Session::get('avatar_url')); ?>" height="50" width="50" class="img-circle" />
                            <?php echo e(isset(Auth::user()->name)?Auth::user()->name:''); ?>

                            <strong class="caret"></strong>
							 
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="<?php echo e(url("admin/users")); ?>"><?php echo e(trans('main.profile')); ?><span class="fa fa-user pull-right"></span></a>
                            </li>
                            <li class="divider">
                            </li>
                            <li>
                                <a href="<?php echo e(url("/auth/logout")); ?>"><?php echo e(trans('main.sign_out')); ?><span class="fa fa-power-off pull-right"></span></a>
                            </li>
                        </ul>
                    </li>
                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <li id="fullscreen-li">
                        <a href="#" id="fullscreen" class="dropdown-toggle" >
                            <i class="fa fa-arrows-alt"></i>
                        </a>
                    </li>

                    <li id="side-hide-li" class="dropdown">
                        <a href="#" id="side-hide" class="dropdown-toggle" >
                            <i class="fa fa-reorder"></i>
                        </a>
                    </li>
                </ul>
                <?php /* search form in header */ ?>
<?php /*                <form class="navbar-form navbar-right" >*/ ?>
<?php /*                    <div class="form-group">*/ ?>
<?php /*                        <input type="text" class="form-control search-header" placeholder="<?php echo e(trans('main.enter_keyword')); ?>" />*/ ?>
<?php /*                        <button type="submit" class="btn btn-link search-header-btn" >*/ ?>
<?php /*                            <i class="fa fa-search"></i>*/ ?>
<?php /*                        </button>*/ ?>
<?php /*                    </div>*/ ?>
<?php /*                </form>*/ ?>
            </div>

        </nav>
    </header>

    <?php /* sidebar menu */ ?>
    <div id="sidebar" class="sidebar" >
        <div class="tabbable-panel">
            <div class="tabbable-line">
                <ul class="nav nav-tabs nav-justified">
                    <li id="tab_menu_a" class="active">
                        <a href="#tab_menu_1" data-toggle="tab">
                            <i class="fa fa-reorder"></i>
                        </a>
                    </li>
                    <li id="contact-tab">
                        <a href="#tab_contact_2" data-toggle="tab">
                            <i class="fa fa-user"></i>
                        </a>
                    </li>
                    <li id="report-tab">
                        <a href="#tab_report_3" data-toggle="tab">
                            <i class="fa fa-pie-chart"></i>
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_menu_1">
                        <form class="search-menu-form" >
                            <div class="">
                                <input id="menu-list-search" placeholder="<?php echo e(trans('main.search_menu')); ?>" type="text" class="form-control search-menu">
                            </div>
                        </form>

                        <?php /* sidebar Menu */ ?>
                        <?php echo $__env->make('mastermenu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    </div>

                </div><?php /* end tab-content*/ ?>
            </div><?php /* end tabbable-line */ ?>
        </div><?php /* end tabbable-panel */ ?>
    </div>
    <?php /* /end #sidebar */ ?>

    <?php /* main content  */ ?>
    <div id="main" class="main">
        <div class="row">
            <?php /* breadcrumb section */ ?>
            <div class="ribbon">
                <ul class="breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="<?php echo e(url("admin/dashboard")); ?>">Home</a>
                    </li>
                    <li>
                        <a href="#"><?php echo e(Request::segment(2)); ?></a>
                    </li>
                </ul>

            </div>

            <?php /* main content */ ?>
            <div id="content">
                <?php /* if you want active dragable panel, you should add #sortable-panel. handler drag-drop configured on .panel */ ?>
                <div id="" class="">
                    <div id="titr-content" class="col-md-12">
                        <h2><?php echo e(ucfirst(Request::segment(2))); ?></h2>
<?php /*                        <h5>Edit and add <?php echo e(ucfirst(Request::segment(2))); ?></h5>*/ ?>
                    </div>

                    <?php /* page content load*/ ?>
                    <?php echo $__env->yieldContent('content'); ?>
                            <?php /*/page content load*/ ?>

                </div><?php /* end col-md-12 */ ?>
            </div>
            <?php /* end #content */ ?>

        </div><?php /* end .row */ ?>
    </div>
    <?php /* ./end #main  */ ?>

    <?php /* footer */ ?>
    <div class="page-footer">
        <div class="col-xs-12 col-sm-12 text-center">
            <p><a href="http://amasingtechblog.wordpress.com"> amazingsystems.com</a>. Developed by <a href="http://amasingtechblog.wordpress.com"><strong class="">Amazing Systems</a> &copy; <?php echo date("Y")?> </strong> - All rights reserved.</p>
            <a href="#">
                <i class="fa fa-twitter-square bigger-120"></i>
            </a>
            <a href="#">
                <i class="fa fa-facebook-square bigger-120"></i>
            </a>
            <a href="#">
                <i class="fa fa-rss-square orange bigger-120"></i>
            </a>
        </div>
    </div>
    <?php /* /footer */ ?>
</div>
<?php /* end #container */ ?>

<?php /* Modal mydelete */ ?>
<div class="modal fade" id="mydelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel3">
    <form>
        <div class="modal-dialog" role="document">
            <input type="hidden" name="delete_value" id="delete_value">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel3">Delete</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-warning">Are you sure to delete this item?</div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger delete_content"><span class="fa fa-trash"></span> Delete </button>
                    <button type="button" class="btn btn-link" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </form>
</div>
<?php /* General JS script library*/ ?>

<script type="text/javascript" src="<?php echo e(url('assets/vendors/angularjs/js/angular.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(url('assets/vendors/angularjs/js/angular-sanitize.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(url('assets/vendors/jquery-ui/js/jquery-ui.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(url('assets/vendors/bootstrap/js/bootstrap.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(url('assets/vendors/jquery-searchable/js/jquery.searchable.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(url('assets/vendors/jquery-fullscreen/js/jquery.fullscreen.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(url('assets/vendors/underscore/js/underscore.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(url('assets/vendors/restangular/restangular.min.js')); ?>"></script> <?php /* it's dependent to underscore.js */ ?>

<?php /* Yeptemplate JS Script */ ?><?php /* Please use *.min.js in production */ ?>
<script type="text/javascript" src="<?php echo e(url('assets/js/yep-script.min.js')); ?>"></script>
<?php /* <script type="text/javascript" src="<?php echo e(url('assets/js/yep-demo.min.js')); ?>"></script> */ ?>

<script type="text/javascript" src="<?php echo e(url('assets/vendors/jquery-require/jquery.require.min.js')); ?>"></script>

<?php /* Related JavaScript Library to This Pagee */ ?>
<?php /*<script type="text/javascript" src="<?php echo e(url('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.min.js')); ?>"></script><?php echo e(-- Use for input mask */ ?>--); ?>

<?php /* Related JavaScript Library to This Pagee */ ?>
		<script type="text/javascript" src="../assets/vendors/jquery-datatables/js/jquery.dataTables.min.js"></script>
		<?php /* <script type="text/javascript" src="../assets/vendors/jquery-datatables/js/dataTables.bootstrap.min.js"></script> */ ?>
		<?php /* <script type="text/javascript" src="../assets/vendors/jquery-datatables/js/dataTables.responsive.min.js"></script>				 */ ?>
		<?php /* <script type="text/javascript" src="../assets/vendors/jquery-datatables/js/dataTables.tableTools.min.js"></script>							 */ ?>
		<?php /* <script type="text/javascript" src="../assets/vendors/jquery-datatables/js/dataTables.colVis.min.js"></script>	 */ ?>

<script type="text/javascript" src="<?php echo e(url('assets/vendors/nprogress/js/nprogress.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(url('assets/vendors/dropzone/js/dropzone.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(url('assets/vendors/ckeditor/js/ckeditor.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(url('assets/vendors/morrisjs/js/raphael.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(url('assets/vendors/morrisjs/js/morris.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(url('assets/vendors/easy-pie-chart/js/jquery.easypiechart.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(url('assets/vendors/bootstrap-datepicker/js/bootstrap-datepicker.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(url('assets/vendors/select2/js/select2.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(url('assets/js/plugin.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(url('assets/vendors/ui-select/js/select.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(url('assets/vendors/angular-wizard/js/angular-wizard.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(url('assets/vendors/angular-ui-notification/js/angular-ui-notification.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(url('assets/vendors/sweetalert/js/sweetalert.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(url('assets/vendors/ng-sweet-alert/SweetAlert.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(url('assets/vendors/ui-bootstrap/js/ui-bootstrap-custom-tpls.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(url('assets/vendors/bootstrap-duallistbox/js/jquery.bootstrap-duallistbox.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(url('assets/vendors/angular-bootstrap-duallistbox/angular-bootstrap-duallistbox.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(url('assets/vendors/momentjs/js/moment.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(url('assets/vendors/bootstrap-daterangepicker/js/daterangepicker.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(url('assets/vendors/angular-daterangepicker/angular-daterangepicker.min.js')); ?>"></script>

						
					
		<?php /* Plugins Script */ ?>
		<script type="text/javascript">
					
		    // Default Data Table Script
		    $('#example1').dataTable({
		    	responsive: true
		    });

	    	// Table Tools Data Tables
	    	$('#example2').dataTable({
		    	"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'T>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
		        "oTableTools": {
		        	 "aButtons": [
		             "copy",
		             "csv",
		             "xls",
		                {
		                    "sExtends": "pdf",
		                    "sTitle": "Yeptemplate_PDF",
		                    "sPdfMessage": "Yeptemplate PDF Export",
		                    "sPdfSize": "letter"
		                },
		             	{
	                    	"sExtends": "print",
	                    	"sMessage": "Generated by YepTemplate <i>(press Esc to close)</i>"
	                	}
		             ],
		            
		        },
				"autoWidth" : true,
				responsive: true
				
		    });

    		// Column show & hide
		    $('#example3').DataTable( {
		    	"dom": '<"pull-left"f><"pull-right"l>tip',
		        "dom": 'C<"clear">lfrtip',

		        "bLengthChange": false,
		        responsive: true
		    });
			    	
			
		</script>


<?php /* Yeptemplate Vendors JS Script */ ?><?php /* Please use *.min.js in production */ ?>
<script type="text/javascript" src="<?php echo e(url('assets/js/yep-vendors.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(url('assets/js/yep-custom.js')); ?>"></script>

<script type="text/javascript" src="<?php echo e(url('assets/js/app.js')); ?>"></script>
<?php /*<script type="text/javascript" src="../assets/vendors/bootstrap/js/bootstrap.min.js"></script>*/ ?> <?php /* This hinders the logout dropdown from toggling. But it works for something. I don't know exactly what.*/ ?>

</body>
</html>