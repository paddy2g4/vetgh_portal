<!-- Entity Id Field -->
<div class="form-group">
<label class="col-sm-4 control-label" for="form-field-1">
		Company
	</label>
	<div class="col-sm-8">
		<select name="entity_id" class="js-example-basic-single col-sm-12">
			<?php
				$entity_masters = DB::table('entity_masters')->select('id','entity_name')->where([['act_status',true],['del_status',false]])->orderby('id', 'asc')->get();
				foreach ($entity_masters as $entity){
					$entity_name = htmlspecialchars($entity->entity_name);
					$entity_id = htmlspecialchars($entity->id);
					?>
					<option <?php if(isset($editAwards)&&($editAwards['entity_id']==$entity_id)) echo "selected"?> value="<?php echo e($entity_id); ?>"><?php echo e($entity_name); ?></option>
				<?php }
			?>
		</select>
	</div>
</div>

<!-- Plan Name Field -->
<div class="form-group">
	<label class="col-sm-4 control-label" for="form-field-1">
		Award name
	</label>
	<div class="col-sm-8">
		<input value="<?php echo e(isset($editAwards['plan_name']) ? $editAwards['plan_name'] : ''); ?>" name="plan_name" type="text" placeholder="" class="form-control">
	</div>
</div>

<!-- Plan Desc Field -->
<div class="form-group">
	<label class="col-sm-4 control-label" for="form-field-1">
	Award Description
	</label>
	<div class="col-sm-8">
		<input value="<?php echo e(isset($editAwards['plan_desc']) ? $editAwards['plan_desc'] : ''); ?>" name="plan_desc" type="text" placeholder="" class="form-control">
	</div>
</div>

<!-- Start Date Field -->
<div class="form-group">
	<label class="col-sm-4 control-label" for="form-field-1">
		Award Start Date
	</label>
	<div class="col-sm-8">
		<input value="<?php echo e(isset($editAwards['start_date']) ? $editAwards['start_date'] : ''); ?>" name="start_date" type="text" data-date-format="yyyy-mm-dd" placeholder="" class="datepicker">
	</div>
</div>

<!-- End Date Field -->
<div class="form-group">
	<label class="col-sm-4 control-label" for="form-field-1">
		Award End Date
	</label>
	<div class="col-sm-8">
		<input value="<?php echo e(isset($editAwards['end_date']) ? $editAwards['end_date'] : ''); ?>" name="end_date" type="text" data-date-format="yyyy-mm-dd" placeholder="" class="datepicker">
	</div>
</div>

<!-- Logo Field -->
<div class="form-group">
	<label class="col-sm-4 control-label" for="form-field-1">
		Award Logo
	</label>
	<!-- <div class="col-sm-9">
		<div class="dropzone dz-clickable dz-single dz-uploadimage1" data-name="logo" ></div>
		<input value="<?php echo e(isset($editAwards['logo']) ? $editAwards['logo'] : ''); ?>" name="logo" type="text" placeholder="" class="form-control">
	</div> -->

	<div class="col-sm-8">
		<div class="dropzone dz-clickable dz-single dz-uploadimage1" data-name="logo" ></div>
		<input name="logo" type="hidden" value="<?php echo e(isset($editAwards['logo']) ? $editAwards['logo'] : ''); ?>">
	</div>
</div>


<script type="text/javascript">
	$(document).ready(function() {
  		$(".js-example-basic-single").select2();
	});
</script>