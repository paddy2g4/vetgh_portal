<!-- Entity Name Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Company Name
	</label>
	<div class="col-sm-9">
		<input value="<?php echo e(isset($editEntityMaster['entity_name']) ? $editEntityMaster['entity_name'] : ''); ?>" name="entity_name" type="text" placeholder="" class="form-control">
	</div>
</div>

<!-- Phone Number Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Phone Number
	</label>
	<div class="col-sm-9">
		<input value="<?php echo e(isset($editEntityMaster['phone_number']) ? $editEntityMaster['phone_number'] : ''); ?>" name="phone_number" type="text" placeholder="" class="form-control">
	</div>
</div>

<!-- Email Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Email
	</label>
	<div class="col-sm-9">
		<input value="<?php echo e(isset($editEntityMaster['email']) ? $editEntityMaster['email'] : ''); ?>" name="email" type="text" placeholder="" class="form-control">
	</div>
</div>

<!-- Other Details Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Other Details
	</label>
	<div class="col-sm-9">
		<input value="<?php echo e(isset($editEntityMaster['other_details']) ? $editEntityMaster['other_details'] : ''); ?>" name="other_details" type="text" placeholder="" class="form-control">
	</div>
</div>

<!-- <div class="form-group">
    <label class="col-sm-3 control-label" for="form-field-1">
       Act Status
    </label>
    <div class="col-sm-9">
        <select name="act_status" class="form-control">
            <option  <?php echo e(isset($editEntityMaster["act_status"])&&$editEntityMaster["act_status"]=="Active" ? "selected" : ""); ?>   value="Active"">Active</option><option  <?php echo e(isset($editEntityMaster["act_status"])&&$editEntityMaster["act_status"]=="Inactive" ? "selected" : ""); ?>   value="Inactive"">Inactive</option>
        </select>
    </div>
</div> -->



