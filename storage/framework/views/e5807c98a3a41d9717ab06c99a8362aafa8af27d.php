<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width">
    <title>Cloudinary Image Upload</title>
    <meta name="description" content="Prego is a project management app built for learning purposes">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo e(asset('css/style.css')); ?>">
</head>
<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Cloudinary Image Upload</a>
        </div>
    </div>
</nav>

<div class="container" style="margin-top: 100px;">
    <div class="row">
        <h4 class="text-center">
            Upload Images
        </h4>

        <div class="row">
            <div id="formWrapper" class="col-md-4 col-md-offset-4">
                <form class="form-vertical" role="form" enctype="multipart/form-data" method="post" action="<?php echo e(route('uploadImage')); ?>">
                    <?php echo e(csrf_field()); ?>

                    <?php if(session()->has('status')): ?>
                        <div class="alert alert-info" role="alert">
                            <?php echo e(session()->get('status')); ?>

                        </div>
                    <?php endif; ?>
                    <div class="form-group<?php echo e($errors->has('name') ? ' has-error' : ''); ?>">
                        <input type="file" name="image_name" class="form-control" id="name" value="" onchange="loadFile(event)"><br>
                        <img id="output" src="#" alt="your image" width="120px" height="120px"/>
                        <?php if($errors->has('image_name')): ?>
                            <span class="help-block"><?php echo e($errors->first('image_name')); ?></span>
                        <?php endif; ?>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-success">Upload Image </button>
                    </div>
                </form>

            </div>
        </div>
    </div>



</div>
</body>
<script>
    var loadFile = function(event) {
        var output = document.getElementById('output');
        output.src = URL.createObjectURL(event.target.files[0]);
    };

    </script>
</html>

<?php /*<script type="text/javascript">*/ ?>
<?php /*    // $(document).ready(function() {*/ ?>
<?php /*    console.log("In Cloudinary upload page: script")*/ ?>
<?php /*    function readURL(input) {*/ ?>
<?php /*        if (input.files && input.files[0]) {*/ ?>
<?php /*            var reader = new FileReader();*/ ?>

<?php /*            reader.onload = function(e) {*/ ?>
<?php /*                $('#blah').attr('src', e.target.result);*/ ?>
<?php /*            }*/ ?>

<?php /*            reader.readAsDataURL(input.files[0]);*/ ?>
<?php /*        }*/ ?>
<?php /*    }*/ ?>

<?php /*    $("#name").change(function() {*/ ?>
<?php /*        readURL(this);*/ ?>
<?php /*    });*/ ?>
<?php /*    // });*/ ?>
<?php /*</script>*/ ?>