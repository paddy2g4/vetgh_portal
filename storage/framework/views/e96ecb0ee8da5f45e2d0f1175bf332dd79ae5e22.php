<div class="col-md-12" id="ajax_div">
    <!--panel details_company -->
    <div class="panel with-nav-tabs panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">
                <div class="bars pull-right">
                    <a href="#"><i class="maximum fa fa-expand" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Maximize"></i></a>
                    <a href="#"><i class="minimize fa fa-chevron-down" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Collapse"></i></a>
                </div>
            </h3>
            <ul class="nav nav-tabs">
                <li class="<?php echo e(isset($tab)&&$tab==1?'active':''); ?>">
                    <a href="#tab1Pricing" data-toggle="tab">
                        <i class="fa fa-Pricing fa-lg danger"></i>
                        Pricing
                    </a>
                </li>
                <li class="<?php echo e(isset($tab)&&$tab==2?'active':''); ?>" <?php if($tab!=2): ?>style="display:none;"<?php endif; ?>>
                    <a href="#newPricing" id="newPricing-1" data-toggle="tab">
                        <i class="fa fa-Pricing-plus success"></i>
                        New Pricing
                    </a>
                </li>
            </ul>
        </div>
        <div class="panel-body no-padding">
            <?php echo $__env->make('message', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            <div class="tab-content">
                <div class="tab-pane fade  <?php echo e(isset($tab)&&$tab==1?'active in':''); ?> tab1" id="tab1Pricing">
                    <div class="col-md-12"></br>
                        <div class="form-inline">
                            <div class="form-group">
                                <select class="form-control paging" id="paging" name="paging">
                                    <option  <?php echo e(isset($request['paging'])&&$request['paging']==10 ?  'selected' :''); ?>  value="10">10</option>
                                    <option  <?php echo e(isset($request['paging'])&&$request['paging']==25 ?  'selected' :''); ?>  value="25">25</option>
                                    <option  <?php echo e(isset($request['paging'])&&$request['paging']==50?  'selected' :''); ?>  value="50">50</option>
                                    <option  <?php echo e(isset($request['paging'])&&$request['paging']==100?  'selected' :''); ?>  value="100">100</option>
                                </select>
                            </div>

                            <div class="btn-group">
                                <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" >
                                    Actions <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <?php if (\Entrust::can('delete_Pricing')) : ?>
                                        <a href="javascript:void(0);" data-toggle="modal" data-target="#mydelete"><i class="fa fa-trash danger"></i> Delete</a>
                                        <?php endif; // Entrust::can ?>
                                        <?php if (\Entrust::can('export_csv')) : ?>
                                        <a href="#" id='csv' class="export" ><i class="fa fa-download blue"></i> Export to CSV</a>
                                        <?php endif; // Entrust::can ?>
                                        <?php if (\Entrust::can('export_xls')) : ?>
                                        <a href="#"  id='xls' class="export" ><i class="fa fa-download green"></i> Export to EXCEL</a>
                                        <?php endif; // Entrust::can ?>
                                        <?php if (\Entrust::can('export_pdf')) : ?>
                                        <a href="#"  id='pdf' class="export" ><i class="fa fa-download red"></i> Export to PDF</a>
                                        <?php endif; // Entrust::can ?>
                                    </li>
                                </ul>
                            </div>

                            <?php if(\Session::get('role_id') == 1 || \Session::get('role_id') == 2): ?> <?php /* <?php if (\Entrust::can('add_Pricing')) : ?>*/ ?>
                            <div class="pull-right">
                                <a href="#" class="btn btn-success pull-right" data-tab-destination="newPricing-1">
                                    <i class="fa fa-plus"></i> Add
                                </a>
                            </div>
                            <?php endif; ?>  <?php /*<?php endif; // Entrust::can ?>*/ ?>
                        </div>
                        <hr>
                        <div class="res-table">
                            <table class="table table-striped table-hover table-fixed ellipsis-table" >
                                <thead>
                                <tr>
                                    <th width="5%" class="center">
                                        <div class="checkbox checkbox-primary">
                                            <input id="checkbox2" type="checkbox" class="conchkSelectAll">
                                            <label for="checkbox2">
                                            </label>
                                        </div>
                                    </th>
                                    <?php echo $__env->make("PricingView::Pricingshow_table_header", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                    <th width="5%">
                                        <i class="fa fa-bolt"></i>
                                        <a href="#"><i class="fa fa-search search-toggle-btn"></i></a>
                                    </th>
                                    <th width="10%">
                                        <i class="fa fa-bolt">Enable/Disable</i>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td colspan="3">
                                        <input  value="<?php echo e(isset($request['serach_txt'])?  $request['serach_txt']:''); ?>" id='search_text' name="search_text" type="text" class="form-control" placeholder="search">
                                    </td>
                                    <td>
                                        <a class="btn btn-default search-btn"><i class="fa fa-search"></i></a>
                                    </td>
                                </tr>
                                <?php if(isset($Pricing)): ?>
                                    <?php foreach($Pricing as $content): ?>
                                        <tr>
                                            <td class="center">
                                                <div class="checkbox checkbox-primary">
                                                    <input id="<?php echo e($content->id); ?>" type="checkbox" class="conchkNumber">
                                                    <label for="<?php echo e($content->id); ?>">
                                                    </label>
                                                </div>
                                            </td>
                                            <?php echo $__env->make("PricingView::Pricingshow_table_fields", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                            <td>
                                                <?php if(\Session::get('role_id') == 1 || \Session::get('role_id') == 2): ?> <?php /*<?php if (\Entrust::can('edit_Pricing')) : ?>*/ ?>
                                                <a id="<?php echo e($content->id); ?>" href="#" class="fa fa-pencil fa-lg edit-href" ></a>
                                                <?php endif; ?>  <?php /* <?php endif; // Entrust::can ?>*/ ?>
                                            </td>

                                            <td data-title="active">
                                                <div class="m-switch">
                                                    <form id="form"   class="form-horizontal form" method="post" action="<?php echo e(url("/admin/pricings/action_status")); ?>" autocomplete="off">
                                                        <input type="hidden" name="_token" id="_token" value="<?php echo e(csrf_token()); ?>">
                                                        <input type="hidden" name="id" id="id" value="<?php echo e(isset($content->id) ? $content->id : 0); ?>">
                                                        <?php if($content->act_status == 1): ?>
                                                            <button type="submit" class="btn btn-danger" name="btnstatus" value="disable"> Disable</span> </button>
                                                        <?php else: ?>
                                                            <button type="submit" class="btn btn-success" name="btnstatus" value="enable"> Enable</span> </button>
                                                        <?php endif; ?>

                                                    </form>

                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php endif; ?>

                                </tbody>
                            </table>
                            <?php if(isset($Pricing)): ?>
                                <?php echo $Pricing->render(); ?>

                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade  <?php echo e(isset($tab)&&$tab==2?'active in':''); ?> tab2" id="newPricing">
                    <?php echo $__env->make('errors', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <form id="form"   class="form-horizontal form" method="post" action="<?php echo e(url("/admin/pricings")); ?>" autocomplete="off">
                        <input type="hidden" name="_token" id="_token" value="<?php echo e(csrf_token()); ?>">
                        <input type="hidden" name="url" id="url" value="<?php echo e(url("/admin/pricings")); ?>">
                        <input type="hidden" name="id" id="id" value="<?php echo e(isset($editPricing['id']) ? $editPricing['id'] : 0); ?>">
                        <input type="hidden" name="page" id="page" value="<?php echo e(isset($request['page']) ?$request['page'] : 1); ?>">
                        <div class="col-md-6"><br>
							<?php echo $__env->make("PricingView::Pricingfields", array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
							 <?php /*relation fields*/ ?>

                        </div>
                        <div class="col-md-12 layout no-padding">
                            <div class="panel-footer">
                                <div class="progress-btn">
                                    <?php if(\Session::get('role_id') == 1 || \Session::get('role_id') == 2): ?> <?php /*<?php if (\Entrust::can('add_Pricing')) : ?>*/ ?>
                                    <?php if(!isset($editPricing['id'])||$editPricing['id']==""): ?>
                                        <a   class=" submit-btn btn btn-success ladda-button" data-style="expand-right"><i class="fa fa-save"></i><span class="ladda-label"> Save</span></a>
                                    <?php endif; ?>
                                    <?php endif; ?>

                                    <?php if(\Session::get('role_id') == 1 || \Session::get('role_id') == 2): ?><?php /*<?php if (\Entrust::can('edit_Pricing')) : ?>*/ ?>
                                    <?php if(isset($editPricing['id'])&&$editPricing['id']>""): ?>
                                        <a    class="btn btn-info edit-btn"><i class="fa fa-save"></i>Update</a>
                                    <?php endif; ?>
                                    <?php endif; ?>
                                     <button class="btn btn-link cancel-btn"> Cancel</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- end panel -->

</div>