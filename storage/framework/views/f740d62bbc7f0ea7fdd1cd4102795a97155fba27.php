<!-- Plan Id Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Award
	</label>
	<div class="col-sm-9">
		<select name="plan_id" class="js-example-basic-single col-sm-12">

			<?php if(isset($awards)): ?>
				<?php foreach($awards as $awards): ?>
					<option  <?php echo e(isset($editPricing)&&$editPricing->plan_id==$awards->plan_id ?  'selected' :''); ?>  value="<?php echo e($awards->plan_id); ?>"> <?php echo e($awards->plan_name); ?> - <?php echo e($awards->entity_name); ?></option>
				<?php endforeach; ?>
			<?php endif; ?>

		</select>
	</div>
</div>

<!-- Points Field -->
<!-- <div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Points
	</label>
	<div class="col-sm-9">
		<input value="<?php echo e(isset($editPricing['points']) ? $editPricing['points'] : ''); ?>" name="points" type="text" placeholder="" class="form-control">
	</div>
</div> -->

<!-- Unit Cost Field -->
<div class="form-group">
	<label class="col-sm-3 control-label" for="form-field-1">
		Cost per vote
	</label>
	<div class="col-sm-9">
		<input value="<?php echo e(isset($editPricing['unit_cost']) ? $editPricing['unit_cost'] : ''); ?>" name="unit_cost" type="number" placeholder="" class="form-control">
	</div>
</div>


<script type="text/javascript">
	$(document).ready(function() {
  		$(".js-example-basic-single").select2();
	});
</script>