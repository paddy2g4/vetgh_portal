<!-- <div class="form-group">
    <label class="col-sm-4 control-label" for="form-field-1">
       Entity Id
    </label>
    <div class="col-sm-8">
        <select name="entity_id" class="form-control">
            <option  <?php echo e(isset($editKeys["entity_id"])&&$editKeys["entity_id"]=="" ? "selected" : ""); ?>   value="""></option>
        </select>
    </div>
</div> -->



<!-- Client Id Field -->
<div class="form-group">
	<label class="col-sm-4 control-label" for="form-field-1">
		Client Id
	</label>
	<div class="col-sm-8">
		<input value="<?php echo e(isset($editKeys['client_id']) ? $editKeys['client_id'] : ''); ?>" name="client_id" type="text" placeholder="" class="form-control">
	</div>
</div>

<!-- Client Token Field -->
<div class="form-group">
	<label class="col-sm-4 control-label" for="form-field-1">
		Client Token
	</label>
	<div class="col-sm-8">
		<input value="<?php echo e(isset($editKeys['client_token']) ? $editKeys['client_token'] : ''); ?>" name="client_token" type="text" placeholder="" class="form-control">
	</div>
</div>

<!-- Secret Token Field -->
<div class="form-group">
	<label class="col-sm-4 control-label" for="form-field-1">
		Secret Token
	</label>
	<div class="col-sm-8">
		<input value="<?php echo e(isset($editKeys['secret_token']) ? $editKeys['secret_token'] : ''); ?>" name="secret_token" type="text" placeholder="" class="form-control">
	</div>
</div>

<!-- Trans Type Field -->
<!-- <div class="form-group">
	<label class="col-sm-4 control-label" for="form-field-1">
		Transaction Type
	</label>
	<div class="col-sm-8">
		<input value="<?php echo e(isset($editKeys['trans_type']) ? $editKeys['trans_type'] : ''); ?>" name="trans_type" type="text" placeholder="" class="form-control">
	</div>
</div> -->



<!-- Payment Ref Field -->
<div class="form-group">
	<label class="col-sm-4 control-label" for="form-field-1">
		Payment Reference
	</label>
	<div class="col-sm-8">
		<input value="<?php echo e(isset($editKeys['payment_ref']) ? $editKeys['payment_ref'] : ''); ?>" name="payment_ref" type="text" placeholder="" class="form-control">
	</div>
</div>
